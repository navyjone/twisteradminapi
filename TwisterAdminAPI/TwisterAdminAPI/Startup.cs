﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using APIModels.Functions;
using Database.Entities;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Swagger;
using APIModels.Functions;
using AutoMapper;
using APIModels.Requests;
using Microsoft.IdentityModel.Logging;

namespace TwisterAdminAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        private readonly string apiVersion = System.Reflection.Assembly.GetEntryAssembly().GetName().Version.ToString();

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .ConfigureApiBehaviorOptions(options =>
                {
                    options.SuppressMapClientErrors = true;
                });

            services.AddDbContext<TwisterContext>(options => options.UseMySQL(
                    Configuration.GetConnectionString(Configuration["SelectConnectionString"])
                )
            );

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options =>
                    {
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidateAudience = true,
                            ValidateLifetime = true,
                            ValidateIssuerSigningKey = true,
                            ValidIssuer = Configuration["Jwt:Issuer"],
                            ValidAudiences = new List<string>
                            {
                                Configuration["Jwt:TwisterUser:Audience"],
                            },
                            IssuerSigningKeys = new List<SymmetricSecurityKey>
                            {
                                new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:TwisterUser:Key"])),
                                new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Refresh:Key"])),
                                //new SymmetricSecurityKey(Encoding.UTF8.GetBytes("1234")),
                                //new SymmetricSecurityKey(Encoding.UTF8.GetBytes("5678")),
                            }
                        };
                    });

            services.AddMemoryCache();
            services.AddCors(options =>
            {

            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc($"v{apiVersion}", new Info
                {
                    Title = Configuration["ProjectName"],
                    Version = $"v{apiVersion}"
                });

                c.AddSecurityDefinition("Bearer", new ApiKeyScheme { In = "header", Description = "Please enter JWT with Bearer into field", Name = "Authorization", Type = "apiKey" });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> {
                    { "Bearer", Enumerable.Empty<string>() },
                });

                // Set the comments path for the Swagger JSON and UI.
                c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "APICore.xml"));
            });

            services.AddScoped<IProduct, APIModels.Functions.Product>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseExceptionHandler(appBuilder =>
            {
                appBuilder.Run(async context =>
                {
                    var exceptionHandlerFeature = context.Features.Get<IExceptionHandlerFeature>();
                    if (exceptionHandlerFeature != null)
                    {
                        IdentityModelEventSource.ShowPII = true;
                        var logger = loggerFactory.CreateLogger("Global exception logger");
                        logger.LogError(500,
                                        exceptionHandlerFeature.Error,
                                        $"{exceptionHandlerFeature.Error.Source} {exceptionHandlerFeature.Error.Message} {exceptionHandlerFeature.Error.StackTrace}");
                    }

                    context.Response.StatusCode = 500;
                    await context.Response.Body.WriteAsync(Encoding.ASCII.GetBytes($"{exceptionHandlerFeature.Error.Source} {exceptionHandlerFeature.Error.Message}"));
                });
            });

            // Enable middleware to serve generated Swagger as a JSON endpoint
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint($"{Configuration["ProjectRootUrl"]}/swagger/v{apiVersion}/swagger.json", $"{Configuration["ProjectName"]}");
            });

            //AutoMapper.Mapper.Initialize(cfg =>
            //{
            //    APIModels.Requests.SignUpRequest.ConfigResponse(cfg);
            //});

            app.UseAuthentication();
            app.UseHttpsRedirection();
            app.UseMiddleware<APICore.Middleware.AdminChangeLog.AdminChangeLog>();
            app.UseMvc();
        }
    }
}
