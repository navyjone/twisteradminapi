﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIModels.Requests;
using APIModels.Responses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NVJUtilities;

namespace APICore.Controllers
{
    [Route("api")]
    [ApiController]
    public class GroupBuyingProductVariantController : ControllerBase
    {
        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        [HttpGet("v1/GroupBuying/ProductVariant/{id}")]
        [ProducesDefaultResponseType(typeof(GroupBuyingProductVariantResponse))]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> Get(int id)
        {
            return Ok();
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        [HttpGet("v1/GroupBuying/ProductVariant")]
        [ProducesDefaultResponseType(typeof(PagingResponse<GroupBuyingProductVariantResponse>))]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> Get([FromQuery] GroupBuyingProductVariantGetRequest request)
        {
            return Ok();
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        /// <remarks>
        /// <b>Nullable fields</b>
        /// <ul>
        /// <li>Rewards</li>
        /// </ul>
        /// </remarks> 
        [HttpPost("v1/GroupBuying/ProductVariant")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> Create([FromBody] List<GroupBuyingProductVariantCreateRequest> requests)
        {
            return Created("", null);
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        [HttpPut("v1/GroupBuying/ProductVariant")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> PutUpdate([FromBody] List<GroupBuyingProductVariantPutUpdateRequest> requests)
        {
            return Ok();
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>  
        /// <remarks>
        /// <b>Nullable fields</b>
        /// <ul>
        /// <li>ProductVariantId</li>
        /// <li>GroupBuyingConditionId</li>
        /// <li>DurationInMinutes</li>
        /// <li>StartDate</li>
        /// <li>EndDate</li>
        /// </ul>
        /// </remarks>      
        [HttpPatch("v1/GroupBuying/ProductVariant")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> PatchUpdate([FromBody] List<GroupBuyingProductVariantPatchUpdateRequest> requests)
        {
            return Ok();
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>  
        //[HttpDelete("v1/GroupBuying/ProductVariant")]
        //[ProducesResponseType(StatusCodes.Status200OK)]
        //[ProducesResponseType(StatusCodes.Status400BadRequest)]
        //[ProducesResponseType(StatusCodes.Status404NotFound)]
        //[Authorize(Roles = Role.TwisterUser.Name)]
        //public async Task<IActionResult> Delete([FromBody] List<SellerDeleteRequest> requests)
        //{
        //    return Ok();
        //}
    }
}