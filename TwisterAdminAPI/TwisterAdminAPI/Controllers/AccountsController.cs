﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using APIModels.Functions;
using APIModels.Requests;
using AutoMapper;
using DB = Database.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NVJUtilities;
using APIModels.Responses;

namespace APICore.Controllers
{
    [Route("api")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly DB.TwisterContext context;
        private readonly IConfiguration appConfig;

        public AccountsController (DB.TwisterContext context, IConfiguration appConfig)
        {
            this.context = context;
            this.appConfig = appConfig;
        }

        [HttpPost("v1/[controller]/signUp")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<IActionResult> SignUp([FromBody] SignUpRequest request)
        {
            var account = new Account(context, appConfig)
            {
                MarketplaceUser = request.Map<DB.MarketplaceUser>()
            };
            var result = await account.SignUp();

            if (result == HttpStatusCode.Conflict) return Conflict("Account already exist.");
            if (result == HttpStatusCode.NotFound) return NotFound("Not exist customerUser");
            return Created("", "");
        }

        [HttpPost("v1/[controller]/login")]
        [ProducesDefaultResponseType(typeof(LoginResponse))]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Login([FromBody] LoginRequest request)
        {
            var account = new Account(context, appConfig)
            {
                MarketplaceUser = new DB.MarketplaceUser { Username = request.Username, Password = request.Password }
            };

            var result = await account.Login();
            if (result.StatusCode == HttpStatusCode.NotFound) return NotFound("User does not exist.");
            if (result.StatusCode == HttpStatusCode.Unauthorized) return Unauthorized("Password incorrect.");
            return Ok(result.Object);
        }

        [HttpPut("v1/[controller]/changePassword")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordRequest request)
        {
            var identifier = int.Parse(User.Claims.First(c => c.Type == "identifier").Value);
            var marketplaceUser = await MarketplaceUser.Get(context, identifier);
            if (marketplaceUser is null || marketplaceUser.StatusId == NVJUtilities.Status.InActive.Id)
                return NotFound("User does not exist.");

            marketplaceUser.Password = request.CurrentPassword;
            var account = new Account(context, appConfig) { MarketplaceUser = marketplaceUser };

            var loginResult = await account.Login();
            if (loginResult.StatusCode == HttpStatusCode.NotFound) return NotFound("User does not exist.");
            if (loginResult.StatusCode == HttpStatusCode.Unauthorized) return Unauthorized("Current password incorrect.");

            await account.ChangePassword(request.NewPassword);
            return Ok();
        }

        [HttpPut("v1/[controller]/refresh")]
        [ProducesDefaultResponseType(typeof(LoginResponse))]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize (Roles = Role.Refresh)]
        public async Task<IActionResult> Refresh()
        {
            var account = new Account(context, appConfig);
            var identifier = int.Parse(User.Claims.First(c => c.Type == "identifier").Value);
            var refreshToken = HttpContext.Request.Headers["Authorization"].First().Substring("Bearer ".Length).Trim();
            var result = await account.Refresh(identifier, refreshToken);
            if (result.StatusCode == HttpStatusCode.Forbidden) return Forbid();
            return Ok(result.Object);
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        [HttpPut("v1/[controller]/{marketplaceUserId}/resetPassword")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> ResetPassword(int marketplaceUserId)
        {
            return Ok();
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        [HttpDelete("v1/[controller]/{marketplaceUserId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> Delete(int marketplaceUserId)
        {
            return Ok();
        }
    }
}