using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIModels.Functions;
using APIModels.Requests;
using APIModels.Responses;
using Database.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NVJUtilities;

namespace APICore.Controllers
{
    [ApiController]
    //[Authorize]
    public class ProductVariantsController : ControllerBase
    {
        private readonly IConfiguration appConfig;
        private readonly TwisterContext context;
        public ProductVariantsController(IConfiguration appConfig, TwisterContext context)
        {
            this.context = context;
            this.appConfig = appConfig;
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        [HttpGet("api/v1/[controller]/{id}")]
        [ProducesDefaultResponseType(typeof(ProductVariantResponse))]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> Get(int id)
        {
            return Ok();
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        [HttpGet("api/v1/products/{productId}/[controller]")]
        [ProducesDefaultResponseType(typeof(PagingResponse<ProductVariantResponse>))]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> Get([FromQuery] ProductVariantGetRequest request)
        {
            return Ok();
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        /// <remarks>
        /// <b>Nullable fields</b>
        /// <ul>
        /// <li>DiscountPercentage</li>
        /// </ul>
        /// </remarks>
        [HttpPost("api/v1/[controller]")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> Create([FromBody] List<ProductVariantCreateRequest> requests)
        {
            return Created("", null);
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        /// <remarks>
        /// <b>Nullable fields</b>
        /// <ul>
        /// <li>DiscountPercentage</li>
        /// </ul>
        /// </remarks>
        [HttpPut("api/v1/[controller]")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> PutUpdate([FromBody] List<ProductVariantPutUpdateRequest> requests)
        {
            return Ok();
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        /// <remarks>
        /// <b>Nullable fields</b>
        /// <ul>
        /// <li>ProductId</li>
        /// <li>Quantity</li>
        /// <li>Price</li>
        /// <li>SalePrice</li>
        /// <li>DiscountPercentage</li>
        /// <li>Sku</li>
        /// </ul>
        /// </remarks>
        [HttpPatch("api/v1/[controller]")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> PatchUpdate([FromBody] List<ProductVariantPatchUpdateRequest> requests)
        {
            return Ok();
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        [HttpDelete("api/v1/[controller]")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> Delete([FromBody] List<DeleteByIdRequest> requests)
        {
            return Ok();
        }
    }
}