﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIModels.Requests;
using APIModels.Responses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NVJUtilities;

namespace APICore.Controllers
{
    [Route("api")]
    [ApiController]
    public class SellersController : ControllerBase
    {
        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        [HttpGet("v1/[controller]/{id}")]
        [ProducesDefaultResponseType(typeof(SellerResponse))]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> Get(int id)
        {
            return Ok();
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        [HttpGet("v1/[controller]")]
        [ProducesDefaultResponseType(typeof(PagingResponse<SellerResponse>))]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> Get([FromQuery] SellerGetRequest request)
        {
            return Ok();
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        /// <remarks>
        /// <b>Nullable fields</b>
        /// <ul>
        /// <li>Telephone</li>
        /// <li>Email</li>
        /// <li>Sequence</li>
        /// <li>Description</li>
        /// <li>TaxNumber</li>
        /// <li>IsFood</li>
        /// <li>Latitude</li>
        /// <li>Longitude</li>
        /// </ul>
        /// </remarks> 
        [HttpPost("v1/[controller]")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> Create([FromBody] List<SellerCreateRequest> requests)
        {
            return Created("", null);
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        /// <remarks>
        /// <b>Nullable fields</b>
        /// <ul>
        /// <li>Telephone</li>
        /// <li>Email</li>
        /// <li>Sequence</li>
        /// <li>Description</li>
        /// <li>TaxNumber</li>
        /// <li>IsFood</li>
        /// <li>Latitude</li>
        /// <li>Longitude</li>
        /// <li>Stats</li>
        /// </ul>
        /// </remarks>        
        [HttpPut("v1/[controller]")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> PutUpdate([FromBody] List<SellerPutUpdateRequest> requests)
        {
            return Ok();
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>  
        /// <remarks>
        /// <b>Nullable fields</b>
        /// <ul>
        /// <li>Name</li>
        /// <li>Telephone</li>
        /// <li>Email</li>
        /// <li>Image</li>
        /// <li>Sequence</li>
        /// <li>Description</li>
        /// <li>TaxNumber</li>
        /// <li>IsFood</li>
        /// <li>Latitude</li>
        /// <li>Longitude</li>
        /// <li>Stats</li>
        /// </ul>
        /// </remarks>      
        [HttpPatch("v1/[controller]")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> PatchUpdate([FromBody] List<SellerPatchUpdateRequest> requests)
        {
            return Ok();
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>  
        [HttpDelete("v1/[controller]")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> Delete([FromBody] List<DeleteByIdRequest> requests)
        {
            return Ok();
        }
    }
}