﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIModels.Functions;
using APIModels.Requests;
using APIModels.Responses;
using Database.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NVJUtilities;

namespace APICore.Controllers
{
    [Route("api")]
    [ApiController]
    public class CategoryMarketplacesController : Controller
    {
        private readonly IConfiguration appConfig;
        private readonly TwisterContext context;
        public CategoryMarketplacesController(IConfiguration appConfig, TwisterContext context)
        {
            this.context = context;
            this.appConfig = appConfig;
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        [HttpGet("v1/[controller]/{id}")]
        [ProducesDefaultResponseType(typeof(CategoryMarketplaceResponse))]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> Get(int id)
        {
            return Ok();
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        /// <remarks>
        /// <b>Nullable fields</b>
        /// <ul>
        /// <li>Sex M or F</li>
        /// </ul>
        /// </remarks>   
        [HttpGet("v1/[controller]")]
        [ProducesDefaultResponseType(typeof(PagingResponse<CategoryMarketplaceResponse>))]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> Get([FromQuery] CategoryMarketplaceGetRequest request)
        {
            return Ok();
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        /// <remarks>
        /// <b>Nullable fields</b>
        /// <ul>
        /// <li>Sequence</li>
        /// <li>Image</li>
        /// </ul>
        /// </remarks>     
        [HttpPost("v1/[controller]")]
        [Authorize(Roles = Role.TwisterUser.Name)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Create([FromBody] List<CategoryMarketplaceCreateRequest> requests)
        {
            return Created("", null);
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        /// <remarks>
        /// <b>Nullable fields</b>
        /// <ul>
        /// <li>Sequence</li>
        /// <li>Image</li>
        /// </ul>
        /// </remarks>     
        [HttpPut("v1/[controller]")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> PutUpdate([FromBody] List<CategoryMarketplacePutUpdateRequest> requests)
        {
            return Ok();
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        /// <remarks>
        /// <b>Nullable fields</b>
        /// <ul>
        /// <li>Name</li>
        /// <li>Status</li>
        /// <li>Sequence</li>
        /// <li>Image</li>
        /// </ul>
        /// </remarks>     
        [HttpPatch("v1/[controller]")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> PatchUpdate([FromBody] List<CategoryMarketplacePatchUpdateRequest> requests)
        {
            return Ok();
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        [HttpDelete("v1/[controller]")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> Delete([FromBody] List<DeleteByIdRequest> requests)
        {
            return Ok();
        }
    }
}