﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIModels.Requests;
using APIModels.Responses;
using Database.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NVJUtilities;

namespace APICore.Controllers
{
    [Route("api")]
    [ApiController]
    public class ProductTypesController : ControllerBase
    {
        private readonly IConfiguration appConfig;
        private readonly TwisterContext context;
        public ProductTypesController(IConfiguration appConfig, TwisterContext context)
        {
            this.context = context;
            this.appConfig = appConfig;
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        [HttpGet("v1/[controller]")]
        [ProducesDefaultResponseType(typeof(IEnumerable<SimpleResponse>))]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> Get()
        {
            return Ok();
        }

        /*
        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        /// <remarks>
        /// <b>Nullable fields</b>
        /// <ul>
        /// <li>Description</li>
        /// </ul>
        /// </remarks>
        [HttpPost("v1/[controller]")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> Create([FromBody] List<ProductTypeCreateRequest> requests)
        {
            return Created("", null);
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        /// <remarks>
        /// <b>Nullable fields</b>
        /// <ul>
        /// <li>Description</li>
        /// </ul>
        /// </remarks>
        [HttpPut("v1/[controller]")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> PutUpdate([FromBody] List<ProductTypePutUpdateRequest> requests)
        {
            return Ok();
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        /// <remarks>
        /// <b>Nullable fields</b>
        /// <ul>
        /// <li>Name</li>
        /// <li>Description</li>
        /// </ul>
        /// </remarks>
        [HttpPatch("v1/[controller]")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> PatchUpdate([FromBody] List<ProductTypePatchUpdateRequest> requests)
        {
            return Ok();
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        [HttpDelete("v1/[controller]")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> Delete([FromBody] List<DeleteByIdRequest> requests)
        {
            return Ok();
        }
        */
    }
}