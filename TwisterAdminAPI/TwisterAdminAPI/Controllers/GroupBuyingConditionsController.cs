﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIModels.Requests;
using APIModels.Responses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NVJUtilities;

namespace APICore.Controllers
{
    [Route("api")]
    [ApiController]
    public class GroupBuyingConditionsController : ControllerBase
    {
        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        [HttpGet("v1/GroupBuying/Conditions/{id}")]
        [ProducesDefaultResponseType(typeof(GroupBuyingConditionResponse))]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> Get(int id)
        {
            return Ok();
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        [HttpGet("v1/GroupBuying/Conditions")]
        [ProducesDefaultResponseType(typeof(PagingResponse<GroupBuyingConditionResponse>))]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> Get([FromQuery] GroupBuyingConditionGetRequest request)
        {
            return Ok();
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        [HttpPost("v1/GroupBuying/Conditions")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> Create([FromBody] GroupBuyingConditionCreateRequest request)
        {
            return Created("", null);
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>  
        [HttpDelete("v1/GroupBuying/Conditions")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> Delete([FromBody] List<DeleteByIdRequest> requests)
        {
            return Ok();
        }
    }
}