﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIModels.Functions;
using APIModels.Requests;
using APIModels.Responses;
using Database.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NVJUtilities;

namespace APICore.Controllers
{
    [Route("api")]
    [ApiController]
    //[Authorize]
    public class ProductsController : ControllerBase
    {
        private readonly IConfiguration appConfig;
        private readonly TwisterContext context;
        public ProductsController(IConfiguration appConfig, TwisterContext context)
        {
            this.context = context;
            this.appConfig = appConfig;
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        [HttpGet("v1/[controller]/{id}")]
        [ProducesDefaultResponseType(typeof(ProductResponse))]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> Get(int id)
        {
            return Ok();
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        [HttpGet("v1/[controller]")]
        [ProducesDefaultResponseType(typeof(PagingResponse<ProductResponse>))]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> Get([FromQuery] ProductGetRequest request)
        {
            return Ok();
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        /// <remarks>
        /// <b>Nullable fields</b>
        /// <u>Product</u>
        /// <ul>
        /// <li>Description</li>
        /// <li>CategorySellerId</li>
        /// <li>CategoryMarketPlaceId</li>
        /// <li>PurchaseLimit</li>
        /// <li>IsFood</li>
        /// <li>Url</li>
        /// <li>ProductTypeId</li>
        /// <li>ExpireDate</li>
        /// <li>ExpireDurationInDay</li>
        /// <li>ProductVariants</li>
        /// <li>ProductImages</li>
        /// </ul>
        /// <u>Product Image</u>
        /// <ul>
        /// <li>Description</li>
        /// </ul>
        /// </remarks>
        [HttpPost("v1/[controller]")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> Create([FromBody] List<ProductCreateRequest> requests)
        {
            return Created("", null);
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        /// <remarks>
        /// <b>Nullable fields</b>
        /// <ul>
        /// <li>Description</li>
        /// <li>CategorySellerId</li>
        /// <li>CategoryMarketPlaceId</li>
        /// <li>PurchaseLimit</li>
        /// <li>IsFood</li>
        /// <li>Url</li>
        /// <li>ProductTypeId</li>
        /// <li>ExpireDate</li>
        /// <li>ExpireDurationInDay</li>
        /// </ul>
        /// </remarks>
        [HttpPut("v1/[controller]")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> PutUpdate([FromBody] List<ProductPutUpdateRequest> requests)
        {
            return Ok();
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        /// <remarks>
        /// <b>Nullable fields</b>
        /// <u>Product</u>
        /// <ul>
        /// <li>Name</li>
        /// <li>Description</li>
        /// <li>CategorySellerId</li>
        /// <li>CategoryMarketPlaceId</li>
        /// <li>SellerId</li>
        /// <li>PurchaseLimit</li>
        /// <li>IsFood</li>
        /// <li>Url</li>
        /// <li>ProductTypeId</li>
        /// <li>ExpireDate</li>
        /// <li>ExpireDurationInDay</li>
        /// <li>StatusId</li>
        /// </ul>
        /// </remarks>
        [HttpPatch("v1/[controller]")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> PatchUpdate([FromBody] List<ProductPatchUpdateRequest> requests)
        {
            return Ok();
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        [HttpDelete("v1/[controller]")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> Delete([FromBody] List<DeleteByIdRequest> requests)
        {
            return Ok();
        }
    }
}