﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIModels.Requests;
using APIModels.Responses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NVJUtilities;

namespace APICore.Controllers
{
    [Route("api")]
    [ApiController]
    public class ConfigsController : ControllerBase
    {
        /// <summary> 
        /// -- Not implemented --
        /// </summary>
        [HttpGet("v1/[controller]")]
        [ProducesDefaultResponseType(typeof(IEnumerable<ConfigResponse>))]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> Get()
        {
            return Ok();
        }

        /// <summary> 
        /// -- Not implemented --
        /// </summary>  
        [HttpPut("v1/[controller]")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = Role.TwisterUser.Name)]
        public async Task<IActionResult> PutUpdate([FromBody] ConfigPutUpdateRequest request)
        {
            return Ok();
        }
    }
}