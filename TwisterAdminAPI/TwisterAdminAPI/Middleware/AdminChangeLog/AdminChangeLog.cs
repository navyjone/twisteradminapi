﻿using Database.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace APICore.Middleware.AdminChangeLog
{
    public class AdminChangeLog
    {
        private readonly RequestDelegate next;

        public AdminChangeLog(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task InvokeAsync(HttpContext context, TwisterContext dbContext)
        {
            context.Request.EnableRewind();
            var bodyRaw = context.Request.Body;
            await next(context); 
            
            if (IsCreteUpdateDelete(context.Request.Method) &&
                IsResponseOk(context.Response.StatusCode))
            {
                bodyRaw.Position = 0;
                using (var reader = new StreamReader(bodyRaw, Encoding.UTF8))
                {                   
                    var body = await reader.ReadToEndAsync();
                    var adminChangeLog = new Database.Entities.AdminChangeLog
                    {
                        APIName = context.Request.Path,
                        MarketplaceUserId = 1,
                        Method = context.Request.Method,
                        Data = body
                    };

                    await dbContext.AddAsync(adminChangeLog);
                    await dbContext.SaveChangesAsync();
                }           
            }           
        }

        private static bool IsCreteUpdateDelete(string method)
        {
            return (method == "POST" || method == "PATCH" || method == "DELETE");
        }

        private static bool IsResponseOk(int statusCode)
        {
            return (statusCode == (int)HttpStatusCode.OK ||
                    statusCode == (int)HttpStatusCode.Created ||
                    statusCode == (int)HttpStatusCode.Accepted);
        }
    }
}
