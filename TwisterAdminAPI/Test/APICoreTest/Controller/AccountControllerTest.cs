using APICore.Controllers;
using FN = APIModels.Functions;
using APIModels.Requests;
using APIModels.Responses;
using Bogus;
using Database.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NVJUtilities;
using System;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using TestUtilities.Generators;
using TestUtilities.Helpers;
using TestUtilities.Preset;
using TwisterAdminAPI;
using Xunit;
using Xunit.Abstractions;

namespace APICoreTest
{
    public class AccountControllerTest : IClassFixture<TestSetupFixture>, IDisposable
    {
        private readonly ITestOutputHelper output;
        private TwisterContext context;
        private AccountsController controller;
        private readonly IConfiguration appConfig;
        private readonly Faker faker;

        public AccountControllerTest(TestSetupFixture fixture, ITestOutputHelper output)
        {
            this.output = output;
            this.context = TestUtils.GetMemoryContext($"Account{Guid.NewGuid().ToString()}");         
            this.appConfig = TestUtils.InitConfiguration();
            this.controller = new AccountsController(context, appConfig);
            this.faker = new Faker();
        }

        public void Dispose()
        {
            CachePer.RemoveMany();
            context.Database.EnsureDeleted();
        }

        private void SetDbContext()
        {
            context.Database.EnsureDeleted();
            this.context = TestUtils.GetDbContext($"Account{Guid.NewGuid().ToString()}", this.appConfig);
            this.controller = new AccountsController(this.context, this.appConfig);
        }

        [Fact]
        public async Task SignUp()
        {
            //Assign
            var common = await PresetCommon.Generate(context);
            var register = new SignUpRequest
            {
                CustomerUserId = (await CustomerUserGenerator.Generate(this.context)).Id,
                Name = faker.Name.FirstName(),
                RoleClassId = common.RoleClasses[1].Id,
                Username = faker.Internet.UserName(),
                Password = faker.Internet.Password()
            };

            //Act
            var signupResponse = await controller.SignUp(register) as ObjectResult;

            //Assert
            Assert.Equal((int)HttpStatusCode.Created, signupResponse.StatusCode);
        }

        [Fact]
        public async Task SignUpWithExistingUsernameReturnConflict()
        {
            //Assign
            var common = await PresetCommon.Generate(context);
            var marketplaceUser = await MarketplaceUserGenerator.Generate(context, new MarketplaceUser
            {
                RoleClassId = common.RoleClasses[1].Id,
                Username = faker.Internet.UserName(),
                Password = faker.Internet.Password()
            });
            var register = new SignUpRequest
            {
                CustomerUserId = (await CustomerUserGenerator.Generate(this.context)).Id,
                Name = faker.Name.FirstName(),
                RoleClassId = common.RoleClasses[1].Id,
                Username = marketplaceUser.Username,
                Password = faker.Internet.Password()
            };

            //Act
            var signupResponse = await controller.SignUp(register) as ObjectResult;

            //Assert
            Assert.Equal((int)HttpStatusCode.Conflict, signupResponse.StatusCode);
        }

        [Fact]
        public async Task SignUpWithExistingCustomerUserIdReturnConflict()
        {
            //Assign            
            var common = await PresetCommon.Generate(context);
            var marketplaceUser = await MarketplaceUserGenerator.Generate(context, new MarketplaceUser
            {
                RoleClassId = common.RoleClasses[1].Id,
                Username = faker.Internet.UserName(),
                Password = faker.Internet.Password()
            });

            var register = new SignUpRequest
            {
                CustomerUserId = marketplaceUser.CustomerUserId,
                Name = faker.Name.FirstName(),
                RoleClassId = common.RoleClasses[1].Id,
                Username = faker.Internet.UserName(),
                Password = faker.Internet.Password()
            };

            //Act
            var signupResponse = await controller.SignUp(register) as ObjectResult;

            //Assert
            Assert.Equal((int)HttpStatusCode.Conflict, signupResponse.StatusCode);
        }

        [Fact]
        public async Task SignUpWithMarketplaceUserUsernameAlreadyInActive()
        {
            //Assign
            var common = await PresetCommon.Generate(context);
            var marketplaceUser = await MarketplaceUserGenerator.Generate(context, new MarketplaceUser
            {
                RoleClassId = common.RoleClasses[1].Id,
                Username = faker.Internet.UserName(),
                Password = faker.Internet.Password(),
                StatusId = NVJUtilities.Status.InActive.Id,
            });

            var register = new SignUpRequest
            {
                CustomerUserId = CustomerUserGenerator.Generate(this.context).Id,
                Name = faker.Name.FirstName(),
                RoleClassId = common.RoleClasses[1].Id,
                Username = marketplaceUser.Username,
                Password = faker.Internet.Password()
            };

            //Act
            var signupResponse = await controller.SignUp(register) as ObjectResult;

            //Assert
            Assert.Equal((int)HttpStatusCode.Created, signupResponse.StatusCode);
        }

        [Fact]
        public async Task SignUpWithMarketplaceUserCustomerUserIdAlreadyInActive()
        {
            //Assign
            var faker = new Faker();
            var common = await PresetCommon.Generate(context);
            var marketplaceUser = await MarketplaceUserGenerator.Generate(context, new MarketplaceUser
            {
                RoleClassId = common.RoleClasses[1].Id,
                Username = faker.Internet.UserName(),
                Password = faker.Internet.Password(),
                StatusId = NVJUtilities.Status.InActive.Id,                
            });

            var register = new SignUpRequest
            {
                CustomerUserId = marketplaceUser.CustomerUserId,
                Name = faker.Name.FirstName(),
                RoleClassId = common.RoleClasses[1].Id,
                Username = faker.Internet.UserName(),
                Password = faker.Internet.Password()
            };

            //Act
            var signupResponse = await controller.SignUp(register) as ObjectResult;

            //Assert
            Assert.Equal((int)HttpStatusCode.Created, signupResponse.StatusCode);
        }

        [Fact]
        public async Task SignUpWithUserCustomerUserNotExisting()
        {
            //Assign
            var faker = new Faker();
            var common = await PresetCommon.Generate(context);

            var register = new SignUpRequest
            {
                CustomerUserId = 99999,
                Name = faker.Name.FirstName(),
                RoleClassId = common.RoleClasses[1].Id,
                Username = faker.Internet.UserName(),
                Password = faker.Internet.Password()
            };

            //Act
            var signupResponse = await controller.SignUp(register) as ObjectResult;

            //Assert
            Assert.Equal((int)HttpStatusCode.NotFound, signupResponse.StatusCode);
        }

        [Fact]
        public async Task SignUpWithUserCustomerUserAlreadyInActive()
        {
            //Assign
            var faker = new Faker();
            var common = await PresetCommon.Generate(context);
            var customer = await CustomerUserGenerator.Generate(context,
                new CustomerUser { StatusId = NVJUtilities.Status.InActive.Id });

            var register = new SignUpRequest
            {
                CustomerUserId = customer.Id,
                Name = faker.Name.FirstName(),
                RoleClassId = common.RoleClasses[1].Id,
                Username = faker.Internet.UserName(),
                Password = faker.Internet.Password()
            };

            //Act
            var signupResponse = await controller.SignUp(register) as ObjectResult;

            //Assert
            Assert.Equal((int)HttpStatusCode.NotFound, signupResponse.StatusCode);
        }

        [Fact]
        public async Task Login()
        {
            //Assign
            var faker = new Faker();
            var common = await PresetCommon.Generate(context);
            var username = faker.Internet.UserName();
            var password = faker.Internet.Password();
            await MarketplaceUserGenerator.Generate(context, new MarketplaceUser
            {
                RoleClassId = common.RoleClasses[1].Id,
                Username = username,
                Password = Crypto.EncryptPassword(this.appConfig, username, password)
            });
            var loginRequest = new LoginRequest
            {
                Username = username,
                Password = password
            };

            //Act
            var loginResponse = await controller.Login(loginRequest) as ObjectResult;
            var response = loginResponse.Value as LoginResponse;

            //Assert
            Assert.Equal((int)HttpStatusCode.OK, loginResponse.StatusCode);
            Assert.NotNull(response.AccessToken);
            Assert.NotNull(response.RefreshToken);
            Assert.Single(response.Abilities);
            Assert.Equal(common.RoleAbilities[0].Id, response.Abilities[0].Id);
            Assert.Equal(common.RoleAbilities[0].Name, response.Abilities[0].Name);
            Assert.Equal(common.RoleAbilities[0].Description, response.Abilities[0].Description);
        }

        [Fact]
        public async Task LoginWithNotExistingUserReturnNotFound()
        {
            //Assign
            await PresetCommon.Generate(context);
            var username = faker.Internet.UserName();
            var password = faker.Internet.Password();
            var loginRequest = new LoginRequest
            {
                Username = username,
                Password = password
            };

            //Act
            var loginResponse = await controller.Login(loginRequest) as ObjectResult;

            //Assert
            Assert.Equal((int)HttpStatusCode.NotFound, loginResponse.StatusCode);
        }

        [Fact]
        public async Task LoginWithWrongPasswordReturnUnAuthorized()
        {
            //Assign
            var faker = new Faker();
            var common = await PresetCommon.Generate(context);
            var username = faker.Internet.UserName();
            var password = faker.Internet.Password();
            await MarketplaceUserGenerator.Generate(context, new MarketplaceUser
            {
                RoleClassId = common.RoleClasses[1].Id,
                Username = username,
                Password = Crypto.EncryptPassword(this.appConfig, username, password)
            });
            var loginRequest = new LoginRequest
            {
                Username = username,
                Password = faker.Internet.Password()
            };

            //Act
            var loginResponse = await controller.Login(loginRequest) as ObjectResult;

            //Assert
            Assert.Equal((int)HttpStatusCode.Unauthorized, loginResponse.StatusCode);
        }
        
        [Fact]
        public async Task LoginWithDifferentDeviceNameShouldHave2RefreshToken()
        {
            //Assign
            var faker = new Faker();
            var common = await PresetCommon.Generate(context);
            var username = faker.Internet.UserName();
            var password = faker.Internet.Password();
            var marketplaceUser = await MarketplaceUserGenerator.Generate(context, new MarketplaceUser
            {
                RoleClassId = common.RoleClasses[1].Id,
                Username = username,
                Password = Crypto.EncryptPassword(this.appConfig, username, password)
            }); 
            var loginRequest1 = new LoginRequest
            {
                Username = username,
                Password = password,
                //DeviceName = faker.Internet.Ip()
            };

            var loginRequest2 = new LoginRequest
            {
                Username = username,
                Password = password,
                //DeviceName = faker.Internet.Ip()
            };

            //Act
            var loginResponse1 = await controller.Login(loginRequest1) as ObjectResult;
            var response1 = loginResponse1.Value as LoginResponse;

            var loginResponse2 = await controller.Login(loginRequest2) as ObjectResult;
            var response2 = loginResponse2.Value as LoginResponse;

            var adminRefreshToken = await context.AdminRefreshToken.Where(a => a.MarketplaceUserId == marketplaceUser.Id).ToListAsync();

            //Assert
            Assert.Equal(2, adminRefreshToken.Count);
            Assert.Single(adminRefreshToken.Where(a => a.Token == response1.RefreshToken));
            Assert.Equal((int)HttpStatusCode.OK, loginResponse1.StatusCode);
            Assert.NotNull(response1.AccessToken);
            Assert.NotNull(response1.RefreshToken);
            Assert.Single(response1.Abilities);
            Assert.Equal(common.RoleAbilities[0].Id, response1.Abilities[0].Id);
            Assert.Equal(common.RoleAbilities[0].Name, response1.Abilities[0].Name);
            Assert.Equal(common.RoleAbilities[0].Description, response1.Abilities[0].Description);

            Assert.Single(adminRefreshToken.Where(a => a.Token == response2.RefreshToken));
            Assert.Equal((int)HttpStatusCode.OK, loginResponse2.StatusCode);
            Assert.NotNull(response2.AccessToken);
            Assert.NotNull(response2.RefreshToken);
            Assert.Single(response2.Abilities);
            Assert.Equal(common.RoleAbilities[0].Id, response2.Abilities[0].Id);
            Assert.Equal(common.RoleAbilities[0].Name, response2.Abilities[0].Name);
            Assert.Equal(common.RoleAbilities[0].Description, response2.Abilities[0].Description);
        }

        [Fact]
        public async Task GetNewAccessTokenWithValidRefreshToken()
        {
            //Assign
            var preset = await PresetLogin.LogIn(this.context, this.appConfig, this.controller);
            PresetLogin.AddToken(this.controller, preset.LoginResponse.RefreshToken, preset.RefreshTokenIds[0]);
            //Act
            var refreshResponse = await controller.Refresh() as ObjectResult;
            var response = refreshResponse.Value as LoginResponse;

            //Assert
            Assert.Equal((int)HttpStatusCode.OK, refreshResponse.StatusCode);
            Assert.NotNull(response.AccessToken);
            Assert.Null(response.RefreshToken);
            Assert.Single(response.Abilities);
            Assert.Equal(preset.Common.RoleAbilities[0].Id, response.Abilities[0].Id);
            Assert.Equal(preset.Common.RoleAbilities[0].Name, response.Abilities[0].Name);
            Assert.Equal(preset.Common.RoleAbilities[0].Description, response.Abilities[0].Description);
        }

        [Fact]
        public async Task GetNewAccessTokenWithValidRefreshTokenAndExpireDateInRegeneratePeriod()
        {
            //Assign
            appConfig.GetSection("Jwt:Refresh:ExpireInMinutes").Value = "2";
            appConfig.GetSection("Jwt:Refresh:RegenerateBeforeExpireInMinutes").Value = "30";
            var preset = await PresetLogin.LogIn(this.context, this.appConfig, this.controller);
            PresetLogin.AddToken(this.controller, preset.LoginResponse.RefreshToken, preset.RefreshTokenIds[0]);

            //Act
            var refreshResponse = await controller.Refresh() as ObjectResult;
            var response = refreshResponse.Value as LoginResponse;

            //Assert
            Assert.Equal((int)HttpStatusCode.OK, refreshResponse.StatusCode);
            Assert.NotNull(response.AccessToken);
            Assert.NotNull(response.RefreshToken);
            Assert.Single(response.Abilities);
            Assert.Equal(preset.Common.RoleAbilities[0].Id, response.Abilities[0].Id);
            Assert.Equal(preset.Common.RoleAbilities[0].Name, response.Abilities[0].Name);
            Assert.Equal(preset.Common.RoleAbilities[0].Description, response.Abilities[0].Description);
        }

        [Fact]
        public async Task GetNewAccessTokenWithRefreshTokenNotExistingReturnForbidden()
        {
            //Assign
            var preset = await PresetLogin.LogIn(this.context, this.appConfig, this.controller);
            PresetLogin.AddToken(this.controller, "", preset.RefreshTokenIds[0]);
            //Act
            var refreshResponse = await controller.Refresh();

            //Assert
            Assert.Equal(typeof(ForbidResult), refreshResponse.GetType());
        }

        [Fact]
        public async Task GetNewAccessTokenWithUserInRefreshTokenAlreadyInActivedReturnForbidden()
        {
            //Assign
            var preset = await PresetLogin.LogIn(this.context, this.appConfig, this.controller);
            PresetLogin.AddToken(this.controller, "", preset.RefreshTokenIds[0]);
            context.Attach(preset.MarketplaceUser);
            preset.MarketplaceUser.StatusId = NVJUtilities.Status.InActive.Id;
            await context.SaveChangesAsync();
            context.Entry(preset.MarketplaceUser).State = EntityState.Detached;

            //Act
            var refreshResponse = await controller.Refresh();

            //Assert
            Assert.Equal(typeof(ForbidResult), refreshResponse.GetType());
        }

        [Fact]
        public async Task ChangePasswordAndLogin()
        {
            //Assign
            var preset = await PresetLogin.LogIn(this.context, this.appConfig, this.controller);
            PresetLogin.AddAccessToken(this.controller, preset.LoginResponse.AccessToken, preset.MarketplaceUser.Id, preset.Common.RoleClasses[0].Name);
            var changePasswordRequest = new ChangePasswordRequest
            {
                CurrentPassword = preset.Password,
                NewPassword = faker.Internet.Password()
            };

            var loginOldPasswordRequest = new LoginRequest
            {
                Username = preset.MarketplaceUser.Username,
                Password = preset.Password
            };

            var loginNewPasswordRequest = new LoginRequest
            {
                Username = preset.MarketplaceUser.Username,
                Password = changePasswordRequest.NewPassword
            };

            //Act
            var changePasswordResponse = await controller.ChangePassword(changePasswordRequest);
            var loginOldPasswordResponse = await controller.Login(loginOldPasswordRequest) as ObjectResult;
            var loginNewPasswordResponse = await controller.Login(loginNewPasswordRequest) as ObjectResult;
            var response = loginNewPasswordResponse.Value as LoginResponse;

            //Assert
            Assert.Equal(typeof(OkResult), changePasswordResponse.GetType());
            Assert.Equal((int)HttpStatusCode.Unauthorized, loginOldPasswordResponse.StatusCode);
            Assert.Equal((int)HttpStatusCode.OK, loginNewPasswordResponse.StatusCode);
            Assert.NotNull(response.AccessToken);
            Assert.NotNull(response.RefreshToken);
            Assert.Single(response.Abilities);
            Assert.Equal(preset.Common.RoleAbilities[0].Id, response.Abilities[0].Id);
            Assert.Equal(preset.Common.RoleAbilities[0].Name, response.Abilities[0].Name);
            Assert.Equal(preset.Common.RoleAbilities[0].Description, response.Abilities[0].Description);
        }
    }
}
