﻿using APICore.Controllers;
using Database.Entities;
using Microsoft.Extensions.Configuration;
using Moq;
using NVJUtilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestUtilities.Helpers
{
    public class NewController
    {
        //public AppointmentController AppointmentController { get; set; }

        public Config Config { get; set; }

        public IConfiguration Configuration { get; set; }

        public NewController(TwisterContext context)
        {
            context.Database.EnsureCreated();
            CachePer.RemoveMany();
            //Config = new Config(context, TestUtils.GetLogger<Config>());

            var configurationBuilder = new ConfigurationBuilder();
            // Duplicate here any configuration sources you use.
            configurationBuilder.AddJsonFile("AppSettings.json");
            Configuration = configurationBuilder.Build();
            
            //AppointmentTempCloseDateController = new AppointmentTempCloseDateController(AppointmentTempCloseDate);
        }
    }
}
