﻿using APICore.Controllers;
using Database;
using Database.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ValueGeneration;
using Microsoft.EntityFrameworkCore.ValueGeneration.Internal;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Moq;
using NVJUtilities;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace TestUtilities.Helpers
{
    public class TestUtils
    {
        public static IConfiguration InitConfiguration()
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();

            return config;
        }

        public static TwisterContext GetMemoryContext(string dbName)
        {
            var serviceProvider = new ServiceCollection()
                                    .AddEntityFrameworkInMemoryDatabase()
                                    .BuildServiceProvider();

            var optionsBuilder = new DbContextOptionsBuilder<TwisterContext>();
            optionsBuilder.UseInMemoryDatabase(dbName)
                            .UseInternalServiceProvider(serviceProvider);
            var context = new TwisterContext(optionsBuilder.Options);

            return context;
        }

        public static TwisterContext GetDbContext(string dbName, IConfiguration appConfig)
        {
            var optionsBuilder = new DbContextOptionsBuilder<TwisterContext>();
            var connectionString = appConfig.GetConnectionString("DefaultConnection");
            optionsBuilder.UseSqlServer(connectionString.Replace("twister", dbName));
            var context = new TwisterContext(optionsBuilder.Options);

            return context;
        }

        public static ILogger<T> GetLogger<T>()
        {
            var mock = new Mock<ILogger<T>>();
            var logger = Mock.Of<ILogger<T>>();

            return logger;
        }

        public static ControllerContext GenerateClaimInContext(string identifier)
        {
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                 new Claim("identifier", identifier)
            }));

            return new ControllerContext()
            {
                HttpContext = new DefaultHttpContext() { User = user }
            };
        }
    }
}
