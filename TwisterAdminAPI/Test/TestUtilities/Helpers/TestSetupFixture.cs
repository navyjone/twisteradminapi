﻿using APICore;
using APICore.Controllers;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace TestUtilities.Helpers
{
    public class TestSetupFixture : IDisposable
    {
        public readonly TestServer server;
        public readonly HttpClient client;

        public TestSetupFixture()
        {
            // TODO: There could be a better way to destroy or check this
            try
            {
                //Mapper.Initialize(cfg =>
                //{

                //});
            }
            catch (InvalidOperationException ex)
            {
                Console.Write("No problem. Still cannot find the way to check if it has already been init. So just catch it for now");
                Console.Write(ex.ToString());
            }
        }

        public void Dispose()
        {
            
        }
    }
}
