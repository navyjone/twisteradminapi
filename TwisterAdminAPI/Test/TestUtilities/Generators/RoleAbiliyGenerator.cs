﻿using Bogus;
using Database.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TestUtilities.Generators
{
    public class RoleAbiliyGenerator
    {
        public static async Task<RoleAbility> Generate (TwisterContext context, RoleAbility role = null)
        {
            var faker = new Faker();
            if (role is null) role = new RoleAbility();
            role.Name = UtilGenerator.Generate(role.Name, faker.Name.FirstName());
            role.Description = UtilGenerator.Generate(role.Description, faker.Name.LastName());
            await context.AddAsync(role);
            await context.SaveChangesAsync();
            context.Entry(role).State = EntityState.Detached;
            return role;
        }

        public static async Task<List<RoleAbility>> Generate(TwisterContext context)
        {
            var roleAbilities = new List<RoleAbility>
            {
                new RoleAbility{Name = "chat-able"},
            };

            await context.AddRangeAsync(roleAbilities);
            await context.SaveChangesAsync();
            foreach(var role in roleAbilities) context.Entry(role).State = EntityState.Detached;
            return roleAbilities;
        }
    }
}
