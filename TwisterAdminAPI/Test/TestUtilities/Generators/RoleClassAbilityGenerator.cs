﻿using Database.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TestUtilities.Generators
{
    public class RoleClassAbilityGenerator
    {
        public static async Task<RoleClassAbility> Map (TwisterContext context, int RoleClassId, int RoleAbilityId)
        {
            var map = new RoleClassAbility
            {
                RoleClassId = RoleClassId,
                RoleAbilityId = RoleAbilityId
            };
            await context.AddAsync(map);
            await context.SaveChangesAsync();
            context.Entry(map).State = EntityState.Detached;
            return map;
        }
    }
}
