﻿using Bogus;
using Database.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TestUtilities.Generators
{
    public class MarketplaceUserGenerator
    {
        public static async Task<MarketplaceUser> Generate(TwisterContext context, MarketplaceUser user = null)
        {
            var faker = new Faker();
            if (user is null) user = new MarketplaceUser();
            user.CreateDate = UtilGenerator.Generate(user.CreateDate);
            user.UpdateDate = UtilGenerator.Generate(user.UpdateDate);
            user.StatusId = UtilGenerator.Generate(user.StatusId, NVJUtilities.Status.Active.Id);
            user.RoleClassId = user.RoleClassId == 0 ? (await RoleClassGenerator.Generate(context, null)).Id : user.RoleClassId;
            user.CustomerUserId = user.CustomerUserId == 0 ? (await CustomerUserGenerator.Generate(context)).Id : user.CustomerUserId;
            await context.AddAsync(user);
            await context.SaveChangesAsync();
            context.Entry(user).State = EntityState.Detached;
            return user;
        }
    }
}
