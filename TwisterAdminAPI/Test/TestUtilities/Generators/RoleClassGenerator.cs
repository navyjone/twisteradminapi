﻿using Bogus;
using Database.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TestUtilities.Generators
{
    public class RoleClassGenerator
    {
        public static async Task<RoleClass> Generate (TwisterContext context, RoleClass role = null)
        {
            var faker = new Faker();
            if (role is null) role = new RoleClass();
            role.Name = UtilGenerator.Generate(role.Name, faker.Name.FirstName());
            role.Description = UtilGenerator.Generate(role.Description, faker.Name.LastName());
            await context.AddAsync(role);
            await context.SaveChangesAsync();
            context.Entry(role).State = EntityState.Detached;
            return role;
        }

        public static async Task<List<RoleClass>> Generate(TwisterContext context)
        {
            var roleClasses = new List<RoleClass>
            {
                new RoleClass{Name = "user"},
                new RoleClass{Name = "twister user"},
                new RoleClass{Name = "seller user"},
            };

            await context.AddRangeAsync(roleClasses);
            await context.SaveChangesAsync();
            foreach (var role in roleClasses) context.Entry(role).State = EntityState.Detached;
            return roleClasses;
        }
    }
}
