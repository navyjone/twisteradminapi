﻿using Bogus;
using Database.Entities;
using Microsoft.EntityFrameworkCore;
using NVJUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestUtilities.Generators
{
    public class CustomerUserGenerator
    {

        public static async Task<CustomerUser> Generate(TwisterContext context, CustomerUser customerUser = null)
        {
            var faker = new Faker();
            if (customerUser is null) customerUser = new CustomerUser();
            customerUser.Telephone = UtilGenerator.Generate(customerUser.Telephone, faker.Phone.PhoneNumber());
            customerUser.CreateDate = UtilGenerator.Generate(customerUser.CreateDate);
            customerUser.UpdateDate = UtilGenerator.Generate(customerUser.UpdateDate);
            customerUser.Point = UtilGenerator.Generate(customerUser.Point, faker.Random.Int(0, 99999));
            customerUser.CoinAmount = UtilGenerator.Generate(customerUser.CoinAmount, 0);
            customerUser.UniqueId = UtilGenerator.Generate(customerUser.UniqueId, faker.Name.FirstName());
            customerUser.LanguageId = UtilGenerator.Generate(customerUser.LanguageId, Constant.DefaultLanguage);
            customerUser.CreateDate = UtilGenerator.Generate(customerUser.CreateDate);
            customerUser.UpdateDate = UtilGenerator.Generate(customerUser.UpdateDate);
            customerUser.StatusId = UtilGenerator.Generate(customerUser.StatusId, NVJUtilities.Status.Active.Id);
            await context.AddAsync(customerUser);
            await context.SaveChangesAsync();
            context.Entry(customerUser).State = EntityState.Detached;
            return customerUser;
        }

    }
}
