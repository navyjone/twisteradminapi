﻿using APICore.Controllers;
using APIModels.Requests;
using APIModels.Responses;
using Bogus;
using Database.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NVJUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using TestUtilities.Generators;

namespace TestUtilities.Preset
{
    public class PresetLogin
    {
        public PresetCommon Common { get; set; }
        public MarketplaceUser MarketplaceUser { get; set; }
        public LoginResponse LoginResponse { get; set; }
        public List<int> RefreshTokenIds { get; set; }
        public string Password { get; set; }

        public static async Task<PresetLogin> LogIn(TwisterContext context, IConfiguration appConfig, AccountsController controller)
        {
            var faker = new Faker();
            var preset = new PresetLogin();
            preset.Common = await PresetCommon.Generate(context);
            var username = faker.Internet.UserName();
            preset.Password = faker.Internet.Password();
            preset.MarketplaceUser = await MarketplaceUserGenerator.Generate(context, new MarketplaceUser
            {
                RoleClassId = preset.Common.RoleClasses[1].Id,
                Username = username,
                Password = Crypto.EncryptPassword(appConfig, username, preset.Password)
            });
            var loginRequest = new LoginRequest
            {
                Username = username,
                Password = preset.Password
            };

            //Act
            var loginResponse = await controller.Login(loginRequest) as ObjectResult;
            preset.LoginResponse = loginResponse.Value as LoginResponse;
            preset.RefreshTokenIds = await context.AdminRefreshToken.AsNoTracking().Select(a => a.Id).ToListAsync();
            return preset;
        }

        public static void AddToken(AccountsController controller, string refreshToken, int refreshTokenId)
        {
            controller.ControllerContext.HttpContext = new DefaultHttpContext()
            {
                User = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
               {
                     new Claim("identifier", refreshTokenId.ToString())
               }))
            };
            controller.ControllerContext.HttpContext.Request.Headers.Add("Authorization", $"Bearer {refreshToken}");
        }

        public static void AddAccessToken(AccountsController controller, string accessToken, int identifier, string roleClass)
        {
            controller.ControllerContext.HttpContext = new DefaultHttpContext()
            {
                
                User = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
               {
                     new Claim("identifier", identifier.ToString()),
                     new Claim(ClaimTypes.Role, roleClass)
               }))
            };
            controller.ControllerContext.HttpContext.Request.Headers.Add("Authorization", $"Bearer {accessToken}");
        }
    }
}
