﻿using Database.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestUtilities.Generators;

namespace TestUtilities.Preset
{
    public class PresetCommon
    {
        public List<RoleClass> RoleClasses { get; set; }
        public List<RoleAbility> RoleAbilities { get; set; }
        public List<RoleClassAbility> RoleClassAbilities { get; set; }

        public static async Task<PresetCommon> Generate (TwisterContext context)
        {
            var preset = new PresetCommon
            {
                RoleClasses = await RoleClassGenerator.Generate(context),
                RoleAbilities = await RoleAbiliyGenerator.Generate(context),
            };

            preset.RoleClassAbilities = new List<RoleClassAbility>
            {
                await RoleClassAbilityGenerator.Map(context, preset.RoleClasses[1].Id,
                                            preset.RoleAbilities[0].Id)
            };

            return preset;
        }
    }
}
