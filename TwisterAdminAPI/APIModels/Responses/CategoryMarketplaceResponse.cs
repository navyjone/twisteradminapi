﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APIModels.Responses
{
    public class CategoryMarketplaceResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string Status { get; set; }
        public string Image { get; set; }
        public int Sequence { get; set; }
        //public string Banner { get; set; }

        public _CategoryMarketplaceLanguageResponse[] Languages { get; set; }

        public class _CategoryMarketplaceLanguageResponse
        {
            public string LanguageName { get; set; }
            public string Name { get; set; }
        }
    }
}
