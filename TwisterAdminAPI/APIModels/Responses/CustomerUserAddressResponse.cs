﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APIModels.Responses
{
    public class CustomerUserAddressResponse
    {
        public int Id { get; set; }
        //public int CustomerUserId { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public SimpleResponse City { get; set; }
        public SimpleResponse Province { get; set; }
        public SimpleResponse Country { get; set; }
        public string Zipcode { get; set; }
        public string TaxNumber { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        //public int StatusId { get; set; }
        public SimpleResponse Town { get; set; }
    }
}
