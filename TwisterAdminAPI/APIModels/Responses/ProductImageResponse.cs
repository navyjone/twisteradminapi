﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APIModels.Responses
{
    public class ProductImageResponse
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public bool IsCover { get; set; }
        public int Sequence { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string Status { get; set; }
        public string MediaType { get; set; }
        public bool IsIcon { get; set; }
    }
}
