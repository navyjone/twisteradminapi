﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APIModels.Responses
{
    public class GroupBuyingResponse
    {
        public int Id { get; set; }
        public GroupBuyingProductVariantResponse GroupBuyingProductVariant { get; set; }
        public CustomerUserResponse Creater { get; set; } // initiator
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string Status { get; set; }
    }
}
