using System;
using System.Collections.Generic;
using System.Text;

namespace APIModels.Responses
{
    public class ProductVariantResponse
    {
        public int Id { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string Status { get; set; }
        public decimal DiscountPercentage { get; set; }
        public decimal SalePrice { get; set; }
        public string Sku { get; set; }
    }
}
