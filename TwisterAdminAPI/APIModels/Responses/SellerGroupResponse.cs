﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APIModels.Responses
{
    public class SellerGroupResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string Status { get; set; }
    }
}
