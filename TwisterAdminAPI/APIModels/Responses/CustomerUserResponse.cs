﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APIModels.Responses
{
    public class CustomerUserResponse
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        //public string FacebookToken { get; set; }
        //public string LineToken { get; set; }
        //public string GoogleToken { get; set; }
        //public string Username { get; set; }
        //public string Password { get; set; }
        public DateTime? Birthday { get; set; }
        public string Sex { get; set; }
        //public string CitizenId { get; set; }
        //public string PassportId { get; set; }
        //public int? CountryId { get; set; }
        //public int? PassportIssuePlace { get; set; }
        //public int? Age { get; set; }
        public DateTime CreateDate { get; set; }
        //public DateTime UpdateDate { get; set; }
        public string Status { get; set; }
        //public int Point { get; set; }
        public string RemoteNotificationId { get; set; }
        public string UniqueId { get; set; }
        public string Image { get; set; }
        //public decimal CoinAmount { get; set; }
        public SimpleResponse Language { get; set; }
        public DateTime? LastLogin { get; set; }
        public string MobileAppVersion { get; set; }
    }
}
