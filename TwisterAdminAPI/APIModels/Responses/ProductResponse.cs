﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APIModels.Responses
{
    public class ProductResponse
    {       
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string Status { get; set; }
        public int? Rating { get; set; }
        //public int? Sold { get; set; }
        //public int? Wishlist { get; set; }
        public string CategorySeller { get; set; }
        public string CategoryMarketPlace { get; set; }
        //public string Sku { get; set; }
        //public string Size { get; set; }
        //public int? SellerAddressId { get; set; }
        public string Seller { get; set; }
        //public string Image { get; set; }
        public int? PurchaseLimit { get; set; }
        //public int? BrandId { get; set; }
        //public int? EventId { get; set; }
        public bool? IsFood { get; set; }
        public string Url { get; set; }
        public string ProductType { get; set; }
        public List<_ProductVariantResponse> ProductVariants { get; set; }
        public class _ProductVariantResponse
        {
            public int Id { get; set; }
            public int Quantity { get; set; }
            public decimal Price { get; set; }
            public decimal DiscountPercentage { get; set; }
            public decimal SalePrice { get; set; }
            public string Sku { get; set; }
        }
    }
}
