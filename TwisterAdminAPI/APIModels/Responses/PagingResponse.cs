﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APIModels.Responses
{
    public class PagingResponse<T>
    {
        public int Total { get; set; }
        public int TotalPage { get; set; }
        public IEnumerable<T> Data { get; set; }
    }
}
