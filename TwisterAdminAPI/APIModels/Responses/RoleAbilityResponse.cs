﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace APIModels.Responses
{
    public class RoleAbilityResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public static List<RoleAbilityResponse> Map<T>(List<T> classes)
        {
            var configuration = new MapperConfiguration(cfg => cfg.CreateMap<T, RoleAbilityResponse>());
            var mapper = configuration.CreateMapper();
            var response = new List<RoleAbilityResponse>();
            foreach(var @class in classes) response.Add(mapper.Map<T, RoleAbilityResponse>(@class));
            return response;
        }
    }
}
