﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APIModels.Responses
{
    public class ConfigResponse : SimpleResponse
    {
        public string Value { get; set; }
    }
}
