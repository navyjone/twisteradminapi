﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APIModels.Responses
{
    public class SellerResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string Status { get; set; }
        public string Image { get; set; }
        public int? Sequence { get; set; }
        public string Description { get; set; }
        public string TaxNumber { get; set; }
        public bool? IsFood { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        //public int? SellerWorkdayGroupId { get; set; }
        public List<_StatsResponse> Stats { get; set; }
        //public _GroupResponse SellerGroup { get; set; }

        public class _StatsResponse
        {
            public int SellerStatsTypeId { get; set; }
            public string Name { get; set; }
            public string Value { get; set; }
        }

        //public class _GroupResponse
        //{
        //    public int Id { get; set; }
        //    public string Name { get; set; }
        //}
    }
}
