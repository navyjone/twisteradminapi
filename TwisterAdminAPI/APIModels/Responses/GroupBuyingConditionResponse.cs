﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APIModels.Responses
{
    public class GroupBuyingConditionResponse
    {
        public int Id { get; set; }
        //public int? TargetBuyer { get; set; }
        public int? TargetQuantity { get; set; }
        //public decimal? TargetPrice { get; set; }
    }
}
