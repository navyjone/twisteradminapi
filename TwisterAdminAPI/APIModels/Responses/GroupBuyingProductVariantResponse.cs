﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APIModels.Responses
{
    public class GroupBuyingProductVariantResponse
    {
        public int Id { get; set; }
        public ProductResponse Product { get; set; }
        public GroupBuyingConditionResponse Condition { get; set; }
        public IEnumerable<GroupBuyingRewardsResponse> Rewards { get; set; }
        public int DurationInMinutes { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string Status { get; set; }
    }
}
