﻿using Database.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace APIModels.Responses
{
    public class LoginResponse
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }        
        public List<RoleAbilityResponse> Abilities { get; set; }
    }
}
