﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APIModels.Responses
{
    public class GroupBuyingRewardsResponse
    {
        public int Id { get; set; }
        //public int GroupBuyingProductVariantId { get; set; }
        public SimpleResponse GroupBuyingAchievementTypeId { get; set; }
        public decimal? SalePrice { get; set; }
        //public int? Point { get; set; }
        //public int? MarketplaceCouponGroupId { get; set; }
        //public int? ProductVariantId { get; set; }
    }
}
