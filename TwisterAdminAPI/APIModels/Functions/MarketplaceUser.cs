﻿using Microsoft.EntityFrameworkCore;
using NVJUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DB = Database.Entities;

namespace APIModels.Functions
{
    public class MarketplaceUser
    {
        public static async Task<DB.MarketplaceUser> Get(DB.TwisterContext context, int id)
        {
            var marketplaceUser = CachePer.Get<DB.MarketplaceUser>(CacheKey.MarketplaceUser(id));
            if (marketplaceUser != null) return marketplaceUser;

            marketplaceUser = await context.MarketplaceUser.Where(m => m.Id == id)
                                                .Include(m => m.RoleClass)                                                
                                                    .ThenInclude(r => r.RoleClassAbility)
                                                .Include(m => m.CustomerUser)
                                                .AsNoTracking()
                                                .FirstOrDefaultAsync();
            CachePer.Set(CacheKey.MarketplaceUser(id), marketplaceUser);
            return marketplaceUser;
        }
    }
}
