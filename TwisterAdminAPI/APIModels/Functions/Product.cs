﻿using Database.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIModels.Functions
{
    public interface IProduct
    {
        Task<Database.Entities.Product> Get(int id);
    }

    public class Product : IProduct
    {
        private readonly TwisterContext dbContext;

        public Product(TwisterContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<Database.Entities.Product> Get(int id)
        {
            var product = await dbContext.Product.Where(p => p.Id == id).FirstOrDefaultAsync();
            return product;
        }
    }
}
