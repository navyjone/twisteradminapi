﻿using Microsoft.Extensions.Primitives;
using System.IdentityModel.Tokens.Jwt;
using System;
using System.Collections.Generic;
using System.Net;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Threading.Tasks;
using DB = Database.Entities;
using NVJUtilities;
using Microsoft.EntityFrameworkCore;

namespace APIModels.Functions
{
    public class Token
    {
        private readonly IConfiguration appConfig;
        private readonly DB.TwisterContext context;
        public Token (DB.TwisterContext context, IConfiguration appConfig)
        {
            this.appConfig = appConfig;
            this.context = context;
        }

        public string Identifier { get; set; }
        public string Role { get; set; }
        public string JWTKey { get; set; }
        public string JWTIssuer { get; set; }
        public string JWTAudience { get; set; }
        public int ExpireInMinutes { get; set; }
        public string DeviceName { get; set; }

        public string BuildAccess()
        {
            var claims = new[]
            {
                new Claim("identifier", this.Identifier),
                new Claim(ClaimTypes.Role, this.Role),
            };

            return this.Create(claims, DateTime.Now.AddMinutes(this.ExpireInMinutes), this.JWTKey);
        }

        public async Task<string> BuildRefresh()
        {
            var adminRefreshToken = await context.AdminRefreshToken.Where(a => 
                                                                        a.MarketplaceUserId == int.Parse(this.Identifier) &&
                                                                        a.DeviceName == this.DeviceName
                                                                    )
                                                                    .AsNoTracking()
                                                                    .FirstOrDefaultAsync();
            if (adminRefreshToken != null) return await this.RenewRefresh(adminRefreshToken.Id);

            var expireDate = DateTime.Now.AddMinutes(int.Parse(appConfig["Jwt:Refresh:ExpireInMinutes"]));
            // Insert buffer to database
            adminRefreshToken = new DB.AdminRefreshToken
            {
                MarketplaceUserId = int.Parse(this.Identifier),
                Token = "new build refresh",
                ExpireDate = expireDate,
                DeviceName = this.DeviceName,
            };

            await context.AddAsync(adminRefreshToken);
            await context.SaveChangesAsync();
            // Input buffer id to get refresh token
            var claims = new[]
            {
                new Claim("identifier", adminRefreshToken.Id.ToString()),
                new Claim(ClaimTypes.Role, NVJUtilities.Role.Refresh)
            };
            var refreshToken = this.Create(claims, expireDate, appConfig["Jwt:Refresh:Key"]);
            // Update token to database
            adminRefreshToken.Token = refreshToken;
            await context.SaveChangesAsync();
            context.Entry(adminRefreshToken).State = EntityState.Detached;
            return refreshToken;
        }
        public async Task<string> RenewRefresh(int adminRefreshTokenId)
        {
            var expireDate = DateTime.Now.AddMinutes(int.Parse(appConfig["Jwt:Refresh:ExpireInMinutes"]));
            var claims = new[]
            {
                new Claim("identifier", adminRefreshTokenId.ToString()),
                new Claim(ClaimTypes.Role, NVJUtilities.Role.Refresh)
            };
            var refreshToken = this.Create(claims, expireDate, appConfig["Jwt:Refresh:Key"]);
            // Update token to database
            var adminRefreshToken = await AdminRefreshToken.Get(context, adminRefreshTokenId);
            context.Attach(adminRefreshToken);
            adminRefreshToken.Token = refreshToken;
            adminRefreshToken.ExpireDate = expireDate;
            await context.SaveChangesAsync();
            context.Entry(adminRefreshToken).State = EntityState.Detached;
            CachePer.Remove(CacheKey.AdminRefreshToken(adminRefreshTokenId));
            return refreshToken;
        }

        private string Create(Claim[] claims, DateTime expireDate, string key)
        {
            var encryptedKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var creds = new SigningCredentials(encryptedKey, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                this.JWTIssuer,
                this.JWTAudience,
                claims,
                expires: expireDate,
                signingCredentials: creds
              ); 

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
