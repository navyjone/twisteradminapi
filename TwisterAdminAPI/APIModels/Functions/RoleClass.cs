﻿using Microsoft.EntityFrameworkCore;
using NVJUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DB = Database.Entities;

namespace APIModels.Functions
{
    public class RoleClass
    {
        public static async Task<List<DB.RoleAbility>> GetAbilities (DB.TwisterContext context, int id)
        {
            var roleAbilities = CachePer.Get<List<DB.RoleAbility>>(CacheKey.RoleAbilitiesFromRoleClass(id));
            if (roleAbilities != null) return roleAbilities;

            var roleClass = await context.RoleClass.Where(c => c.Id == id)
                                                            .Include(c => c.RoleClassAbility)
                                                            .AsNoTracking()
                                                            .FirstOrDefaultAsync();
            var roleClassAbilityIds = roleClass.RoleClassAbility.Select(a => a.RoleAbilityId).ToList();
            roleAbilities = await context.RoleAbility.Where(a => roleClassAbilityIds.Contains(a.Id)).ToListAsync();
            CachePer.Set(CacheKey.RoleAbilitiesFromRoleClass(id), roleAbilities);
            return roleAbilities;
        }
    }
}
