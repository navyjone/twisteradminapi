﻿using DB = Database.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using NVJUtilities;

namespace APIModels.Functions
{
    public class AdminRefreshToken
    {
        public static async Task<DB.AdminRefreshToken> Get(DB.TwisterContext context, int id)
        {
            var adminRefreshToken = CachePer.Get<DB.AdminRefreshToken>(CacheKey.AdminRefreshToken(id));
            if (adminRefreshToken != null) return adminRefreshToken;

            adminRefreshToken = await context.AdminRefreshToken.Where(a => a.Id == id)
                                            .Include(a => a.MarketplaceUser)
                                            .AsNoTracking()
                                            .FirstOrDefaultAsync();
            CachePer.Set(CacheKey.AdminRefreshToken(id), adminRefreshToken);
            return adminRefreshToken;
        }

        //public static async Task<List<DB.AdminRefreshToken>> GetByMarketplaceUserId (DB.TwisterContext context, int marketplaceUserId)
        //{
        //    var adminRefreshTokens = CachePer.Get<List<DB.AdminRefreshToken>>(CacheKey.AdminRefreshTokenFromMarketplaceUserId(marketplaceUserId));
        //    if (adminRefreshTokens != null) return adminRefreshTokens;

        //    adminRefreshTokens = await context.AdminRefreshToken.Where(a => a.MarketplaceUserId == marketplaceUserId)
        //                                                        .Include(a => a.MarketplaceUser)
        //                                                        .ToListAsync();
        //    CachePer.Set(CacheKey.AdminRefreshTokenFromMarketplaceUserId(marketplaceUserId), adminRefreshTokens);
        //    return adminRefreshTokens;
        //}
    }
}
