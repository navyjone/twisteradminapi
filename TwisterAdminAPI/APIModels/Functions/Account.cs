﻿using APIModels.Responses;
using DB = Database.Entities;
using FN = APIModels.Functions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NVJUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Primitives;
using System.Diagnostics;
using APIModels.Requests;

namespace APIModels.Functions
{
    public class Account
    {
        private readonly DB.TwisterContext context;
        private readonly IConfiguration appConfig;

        public Account(DB.TwisterContext context, IConfiguration appConfig)
        {
            this.context = context;
            this.appConfig = appConfig;
        }

        public DB.MarketplaceUser MarketplaceUser {get; set;}

        public async Task<HttpStatusCode> SignUp()
        {
            var customerUser = await context.CustomerUser.Where(c =>
                                                c.Id == this.MarketplaceUser.CustomerUserId &&
                                                c.StatusId == Status.Active.Id)
                                                .AsNoTracking()
                                                .FirstOrDefaultAsync();

            if (customerUser is null) return HttpStatusCode.NotFound;
            var marketplaceUser = await context.MarketplaceUser.Where(m => 
                                                 (m.Username == this.MarketplaceUser.Username ||
                                                  m.CustomerUserId == this.MarketplaceUser.CustomerUserId ) &&
                                                  m.StatusId == Status.Active.Id)
                                                .Include(m => m.CustomerUser)
                                                .AsNoTracking()
                                                .ToListAsync();

            if (marketplaceUser.Count > 0) return HttpStatusCode.Conflict;

            this.MarketplaceUser.Password = Crypto.EncryptPassword(this.appConfig, 
                                                this.MarketplaceUser.Username, this.MarketplaceUser.Password);
            this.MarketplaceUser.StatusId = 1;
            this.MarketplaceUser.CreateDate = DateTime.Now;
            this.MarketplaceUser.UpdateDate = DateTime.Now;
            await context.MarketplaceUser.AddAsync(this.MarketplaceUser);
            await context.SaveChangesAsync();
            context.Entry(this.MarketplaceUser).State = EntityState.Detached;
            return HttpStatusCode.Created;
        }

        public async Task<HttpCode<LoginResponse>> Login(string deviceName = null)
        {
            var user = await context.MarketplaceUser.Where(m => (m.Username == this.MarketplaceUser.Username &&
                                                    m.StatusId == Status.Active.Id)
                                                )
                                                .AsNoTracking()
                                                .FirstOrDefaultAsync();
            if (user is null) return new HttpCode<LoginResponse> { StatusCode = HttpStatusCode.NotFound};
            if (user.Password != Crypto.EncryptPassword(this.appConfig, this.MarketplaceUser.Username, this.MarketplaceUser.Password))
                return new HttpCode<LoginResponse> { StatusCode = HttpStatusCode.Unauthorized };
            var token = new Token(context, appConfig)
            {
                Identifier = user.Id.ToString(),
                JWTIssuer = appConfig["Jwt:Issuer"],
                JWTAudience = appConfig[$"Jwt:{Role.Name[user.RoleClassId]}:Audience"],
                JWTKey = appConfig[$"Jwt:{Role.Name[user.RoleClassId]}:Key"],
                ExpireInMinutes = int.Parse(appConfig[$"Jwt:{Role.Name[user.RoleClassId]}:ExpireInMinutes"]),
                DeviceName = deviceName,
                Role = Role.Name[user.RoleClassId]
            };
            var roleAblities = await RoleClass.GetAbilities(context, user.RoleClassId);

            return new HttpCode<LoginResponse>
            {
                StatusCode = HttpStatusCode.OK,
                Object = new LoginResponse
                {
                    AccessToken = token.BuildAccess(),
                    RefreshToken = await token.BuildRefresh(),
                    Abilities = RoleAbilityResponse.Map(roleAblities)
                }
            };
        }

        public async Task ChangePassword(string newPassword)
        {
            this.MarketplaceUser = await context.MarketplaceUser.Where(m =>
                                                 m.Id == this.MarketplaceUser.Id)
                                                .FirstOrDefaultAsync();

            this.MarketplaceUser.Password = Crypto.EncryptPassword(this.appConfig,
                                    this.MarketplaceUser.Username, newPassword);
            await context.SaveChangesAsync();
            context.Entry(this.MarketplaceUser).State = EntityState.Detached;
        }

        public async Task<HttpCode<LoginResponse>> Refresh(int adminRefreshTokenId, string refreshToken)
        {
            var adminRefreshToken = await AdminRefreshToken.Get(context, adminRefreshTokenId);
            var user = await FN.MarketplaceUser.Get(context, adminRefreshToken.MarketplaceUserId);
            if (user is null || user.StatusId != Status.Active.Id)
            {
                return new HttpCode<LoginResponse> { StatusCode = HttpStatusCode.Forbidden };
            }

            if (adminRefreshToken.Token != refreshToken) return new HttpCode<LoginResponse> { StatusCode = HttpStatusCode.Forbidden };

            var token = new Token(context, appConfig)
            {
                Identifier = user.Id.ToString(),
                JWTIssuer = appConfig["Jwt:Issuer"],
                JWTAudience = appConfig[$"Jwt:{Role.Name[user.RoleClassId]}:Audience"],
                JWTKey = appConfig[$"Jwt:{Role.Name[user.RoleClassId]}:Key"],
                ExpireInMinutes = int.Parse(appConfig[$"Jwt:{Role.Name[user.RoleClassId]}:ExpireInMinutes"]),
                DeviceName = adminRefreshToken.DeviceName,
                Role = Role.Name[user.RoleClassId]
            };

            var roleAblities = await RoleClass.GetAbilities(context, user.RoleClassId);
            return new HttpCode<LoginResponse>
            {
                StatusCode = HttpStatusCode.OK,
                Object = new LoginResponse
                {
                    AccessToken = token.BuildAccess(),
                    RefreshToken = this.ShouldRegenerateRefreshToken(adminRefreshToken.ExpireDate) ?
                                        await token.RenewRefresh(adminRefreshToken.Id) : null,
                    Abilities = RoleAbilityResponse.Map(roleAblities)
                }
            };
        }

        private bool ShouldRegenerateRefreshToken(DateTime expireDate)
        {
            var regenarateBeforeExpire = new TimeSpan(0, int.Parse(appConfig["Jwt:Refresh:RegenerateBeforeExpireInMinutes"]), 0);
            var result = DateTime.Now >= expireDate.Subtract(regenarateBeforeExpire);
            return result;
        }
        
    }
}
