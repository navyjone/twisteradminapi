﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APIModels.Requests
{
    public class GroupBuyingConditionGetRequest
    {
        public int? FromQuantity { get; set; }
        public int? ToQuantity { get; set; }
        public int Page { get; set; }
    }
}
