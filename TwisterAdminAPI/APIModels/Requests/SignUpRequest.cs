﻿using AutoMapper;
using Database.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using APIModels;

namespace APIModels.Requests
{
    public class SignUpRequest
    {
        [Required(AllowEmptyStrings = false)]
        public string Username { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Password { get; set; }
        public string Name { get; set; }
        [Required]
        [Range(1, int.MaxValue)]
        public int CustomerUserId { get; set; }
        [Required]
        [Range(1, int.MaxValue)]
        public int RoleClassId { get; set; }

        public T Map <T>()
        {
            var configuration = new MapperConfiguration(cfg => cfg.CreateMap<SignUpRequest, T>());
            var mapper = configuration.CreateMapper();
            return mapper.Map<SignUpRequest, T>(this);
        }
    }
}
