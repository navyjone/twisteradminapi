﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APIModels.Responses
{
    public class ProductGetRequest
    {
        public string Name { get; set; }
        public int? StatusId { get; set; }
        public int? CategorySellerId { get; set; }
        public int? CategoryMarketplaceId { get; set; }
        public int? SellerId { get; set; }
        public int? Page { get; set; }
        public int? ProductTypeId { get; set; }
        public string Sku { get; set; }
    }
}
