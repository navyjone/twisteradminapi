﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace APIModels.Requests
{
    public class ProductImageCreateRequest
    {
        [Required]
        [Range(1, int.MaxValue)]
        public int ProductId { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Url { get; set; }
        public string Description { get; set; }
        [Required]
        public bool IsCover { get; set; }
        [Required]
        [Range(1, int.MaxValue)]
        public int Sequence { get; set; }
        [Required]
        [Range(1, int.MaxValue)]
        public int MediaTypeId { get; set; }
        [Required]
        public bool IsIcon { get; set; }
    }
}
