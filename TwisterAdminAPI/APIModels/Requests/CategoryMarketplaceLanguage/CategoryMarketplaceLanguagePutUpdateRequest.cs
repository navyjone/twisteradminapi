﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace APIModels.Requests
{
    public class CategoryMarketplaceLanguagePutUpdateRequest
    {
        [Required]
        [Range(1, int.MaxValue)]
        public int CategoryMarketplaceId { get; set; }
        [Required]
        [Range(1, int.MaxValue)]
        public int LanguageId { get; set; }
        [Required]
        [MinLength(1)]
        public string Name { get; set; }
    }
}
