﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace APIModels.Requests
{
    public class SellerCreateRequest
    {
        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Image { get; set; }
        public int? Sequence { get; set; }        
        public string Description { get; set; }
        public string TaxNumber { get; set; }
        //public int? SellerGroupId { get; set; }
        public bool? IsFood { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        //public int? SellerWorkdayGroupId { get; set; }

        [Required]
        public List<SellerStatsRequest> Stats { get; set; }
    }
}
