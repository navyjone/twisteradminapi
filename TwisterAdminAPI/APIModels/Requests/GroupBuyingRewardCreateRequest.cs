﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace APIModels.Requests
{
    public class GroupBuyingRewardCreateRequest
    {
        [Required]
        [Range(1, int.MaxValue)]
        public int GroupBuyingAchievementTypeId { get; set; }
        [Required]
        [Range(0, float.MaxValue)]
        public decimal SalePrice { get; set; }
        //public int? Point { get; set; }
        //public int? MarketplaceCouponGroupId { get; set; }
        //public int? ProductVariantId { get; set; }
    }
}
