﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace APIModels.Requests
{
    public class ProductCreateRequest
    {
        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }
        public string Description { get; set; }
        public int? CategorySellerId { get; set; }
        public int? CategoryMarketPlaceId { get; set; }
        [Required]
        [Range(1, int.MaxValue)]
        public int SellerId { get; set; }
        public int? PurchaseLimit { get; set; }
        public bool? IsFood { get; set; }
        public string Url { get; set; }
        public int? ProductTypeId { get; set; }
        public DateTime? ExpireDate { get; set; }
        public int? ExpireDurationInDay { get; set; }

        public _ProductVariantCreateRequest[] ProductVariants { get; set; }
        public _ProductImageCreateRequest[] ProductImages { get; set; }

        public class _ProductVariantCreateRequest
        {
            [Required]
            [Range(1, int.MaxValue)]
            public int Quantity { get; set; }
            [Required]
            [Range(1, int.MaxValue)]
            public decimal Price { get; set; }
            [Required]
            [Range(1, int.MaxValue)]
            public decimal SalePrice { get; set; }
            public decimal? DiscountPercentage { get; set; }
            [Required(AllowEmptyStrings = false)]
            public string Sku { get; set; }
        }

        public class _ProductImageCreateRequest
        {
            [Required]
            public string Url { get; set; }
            public string Description { get; set; }
            [Required]
            public bool IsCover { get; set; }
            [Required]
            [Range(1, int.MaxValue)]
            public int Sequence { get; set; }
            [Required]
            [Range(1, int.MaxValue)]
            public int MediaTypeId { get; set; }
            [Required]
            public bool IsIcon { get; set; }
        }
    }
}
