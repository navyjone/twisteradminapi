﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace APIModels.Requests
{
    public class CategorySellerPatchUpdateRequest
    {
        [Required]
        [Range(1, int.MaxValue)]
        public int Id { get; set; }
        public string Name { get; set; }
        public int? StatusId { get; set; }
        public string Image { get; set; }
        public int? Sequence { get; set; }
        //public string Banner { get; set; }
    }
}
