﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APIModels.Requests
{
    public class CategorySellerGetRequest
    {
        public string Name { get; set; }
        public int? StatusId { get; set; }
        //public string Image { get; set; }
        //public int? Sequence { get; set; }
        //public string Banner { get; set; }
    }
}
