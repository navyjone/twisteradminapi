﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace APIModels.Requests
{
    public class ProductImagePatchUpdateRequest
    {
        [Required]
        [Range(1, int.MaxValue)]
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public bool? IsCover { get; set; }
        public int? Sequence { get; set; }
        public int? StatusId { get; set; }
        public int? MediaTypeId { get; set; }
        public bool? IsIcon { get; set; }
    }
}
