﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace APIModels.Requests
{
    public class GroupBuyingConditionCreateRequest
    {
        [Required]
        [Range(1, int.MaxValue)]
        public int TargetQuantity { get; set; }
        //public decimal? TargetPrice { get; set; }
        //public int? TargetBuyer { get; set; }
    }
}
