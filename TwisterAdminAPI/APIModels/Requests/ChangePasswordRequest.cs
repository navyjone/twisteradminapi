﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace APIModels.Requests
{
    public class ChangePasswordRequest
    {
        [Required(AllowEmptyStrings = false)]
        public string CurrentPassword { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string NewPassword { get; set; }
    }
}
