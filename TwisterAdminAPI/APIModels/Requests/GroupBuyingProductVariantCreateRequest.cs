﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace APIModels.Requests
{
    public class GroupBuyingProductVariantCreateRequest
    {
        [Required]
        [Range(1, int.MaxValue)]
        public int ProductVariantId { get; set; }
        [Required]
        [Range(1, int.MaxValue)]
        public int GroupBuyingConditionId { get; set; }
        [Required]
        [Range(1, int.MaxValue)]
        public int DurationInMinutes { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }
        public List<GroupBuyingRewardCreateRequest> Rewards { get; set; }
    }
}
