﻿using APIModels.Responses;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace APIModels.Requests
{
    public class CategoryMarketplaceCreateRequest
    {
        [Required]
        [MinLength(1)]
        public string Name { get; set; }
        public string Image { get; set; }
        public int Sequence { get; set; }
        //public string Banner { get; set; }
        public _CategoryMarketplaceLanguageRequest[] Languages { get; set; }

        public class _CategoryMarketplaceLanguageRequest
        {
            [Required]
            [Range(1, int.MaxValue)]
            public int LanguageId { get; set; }
            [Required]
            [MinLength(1)]
            public string Name { get; set; }
        }
    }
}
