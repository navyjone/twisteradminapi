﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace APIModels.Requests
{
    public class CategorySellerHierarchyCreateRequest
    {
        [Required]
        [Range(1, int.MaxValue)]
        public int ParentId { get; set; }
        [Required]
        [Range(1, int.MaxValue)]
        public int ChildrenId { get; set; }
    }
}
