﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APIModels.Requests
{
    public class GroupBuyingGetRequest
    {
        public int? GroupbuyingProductVariantId { get; set; }
        public int? CreaterId { get; set; }
        public DateTime? FromStartDate { get; set; }
        public DateTime? ToStartDate { get; set; }
        public DateTime? FromEndDate { get; set; }
        public DateTime? ToEndDate { get; set; }
        public DateTime? FromCreateDate { get; set; }
        public DateTime? ToCreateDate { get; set; }
        public int? StatusId { get; set; }
        public int? ProductVariantId { get; set; }
        public int Page { get; set; }
    }
}
