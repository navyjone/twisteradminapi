using System;
using System.Collections.Generic;
using System.Text;

namespace APIModels.Responses
{
    public class ProductVariantGetRequest
    {
        public int? StatusId { get; set; }
        public string Sku { get; set; }
        public int? Page { get; set; }
    }
}
