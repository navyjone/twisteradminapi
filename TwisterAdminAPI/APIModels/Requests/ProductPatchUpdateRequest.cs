﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace APIModels.Requests
{
    public class ProductPatchUpdateRequest
    {
        [Required]
        [Range(1, int.MaxValue)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? CategorySellerId { get; set; }
        public int? CategoryMarketPlaceId { get; set; }
        public int SellerId { get; set; }
        public int? PurchaseLimit { get; set; }
        public bool? IsFood { get; set; }
        public string Url { get; set; }
        public int? ProductTypeId { get; set; }
        public DateTime? ExpireDate { get; set; }
        public int? ExpireDurationInDay { get; set; }
        public int? StatusId { get; set; }
    }
}
