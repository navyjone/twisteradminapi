﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace APIModels.Requests
{
    public class CustomerUserPatchUpdateRequest
    {
        [Required]
        [Range(1, int.MaxValue)]        
        public int Id { get; set; }
        public string Telephone { get; set; }
        public int? StatusId { get; set; }
    }
}
