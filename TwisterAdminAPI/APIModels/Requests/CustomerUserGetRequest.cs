﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APIModels.Requests
{
    public class CustomerUserGetRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }      
        //public DateTime? Birthday { get; set; }
        public string Sex { get; set; }
        //public int? Age { get; set; }
        public DateTime? FromCreateDate { get; set; }
        public DateTime? ToCreateDate { get; set; }
        public int? StatusId { get; set; }
        public string RemoteNotificationId { get; set; }
        public string UniqueId { get; set; }
        public int? LanguageId { get; set; }
        public DateTime? FromLastLoginDate { get; set; }
        public DateTime? ToLastLoginDate { get; set; }
        public string MobileAppVersion { get; set; }
        public int? Page { get; set; }
    }
}
