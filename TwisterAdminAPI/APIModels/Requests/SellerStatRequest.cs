﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace APIModels.Requests
{
    public class SellerStatsRequest
    {
        [Required]
        [Range(1, int.MaxValue)]
        public int SellerStatsTypeId { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Value { get; set; }
    }
}
