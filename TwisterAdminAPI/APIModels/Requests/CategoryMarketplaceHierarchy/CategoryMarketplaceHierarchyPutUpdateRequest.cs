﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace APIModels.Requests
{
    public class CategoryMarketplaceHierarchyPutUpdateRequest
    {
        [Required]
        [Range(1, int.MaxValue)]
        public int CurrentParentId { get; set; }
        [Required]
        [Range(1, int.MaxValue)]
        public int CurrentChildrenId { get; set; }
        [Required]
        [Range(1, int.MaxValue)]
        public int NewParentId { get; set; }
        [Required]
        [Range(1, int.MaxValue)]
        public int NewChildrenId { get; set; }
    }
}
