﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APIModels.Requests
{
    public class GroupBuyingProductVariantGetRequest
    {
        public int? ProductVariantId { get; set; }
        public int? GroupBuyingConditionId { get; set; }
        //public int? DurationInMinutes { get; set; }
        public DateTime? FromStartDate { get; set; }
        public DateTime? ToStartDate { get; set; }
        public DateTime? FromEndDate { get; set; }
        public DateTime? ToEndDate { get; set; }
        public DateTime? FromCreateDate { get; set; }
        public DateTime? ToCreateDate { get; set; }
        public int? StatusId { get; set; }
        public int? TargetQuantity { get; set; }
        public decimal? FromSalePrice { get; set; }
        public decimal? ToSalePrice { get; set; }
        public int Page { get; set; }
    }
}
