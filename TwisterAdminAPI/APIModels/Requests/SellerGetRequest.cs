﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APIModels.Requests
{
    public class SellerGetRequest
    {
        public string Name { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public int StatusId { get; set; }
        public string TaxNumber { get; set; }
        public bool? IsFood { get; set; }
        public int Page { get; set; }
    }
}
