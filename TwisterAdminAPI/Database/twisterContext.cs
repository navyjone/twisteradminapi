﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Database.Entities
{
    public partial class TwisterContext : DbContext
    {
        public TwisterContext()
        {
        }

        public TwisterContext(DbContextOptions<TwisterContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AdminChangeLog> AdminChangeLog { get; set; }
        public virtual DbSet<AdminRefreshToken> AdminRefreshToken { get; set; }
        public virtual DbSet<AllowOpenLocker> AllowOpenLocker { get; set; }
        public virtual DbSet<Allowstatusinfo> Allowstatusinfo { get; set; }
        public virtual DbSet<AppDeepLink> AppDeepLink { get; set; }
        public virtual DbSet<BannerFoodSeller> BannerFoodSeller { get; set; }
        public virtual DbSet<BannerMarketplace> BannerMarketplace { get; set; }
        public virtual DbSet<BannerMarketplaceProduct> BannerMarketplaceProduct { get; set; }
        public virtual DbSet<BannerSeller> BannerSeller { get; set; }
        public virtual DbSet<Brand> Brand { get; set; }
        public virtual DbSet<BrandLanguage> BrandLanguage { get; set; }
        public virtual DbSet<Cart> Cart { get; set; }
        public virtual DbSet<CartProduct> CartProduct { get; set; }
        public virtual DbSet<CategoryMarketplace> CategoryMarketplace { get; set; }
        public virtual DbSet<CategoryMarketplaceHierarchy> CategoryMarketplaceHierarchy { get; set; }
        public virtual DbSet<CategoryMarketplaceLanguage> CategoryMarketplaceLanguage { get; set; }
        public virtual DbSet<CategoryMarketplaceProduct> CategoryMarketplaceProduct { get; set; }
        public virtual DbSet<CategorySeller> CategorySeller { get; set; }
        public virtual DbSet<CategorySellerHierarchy> CategorySellerHierarchy { get; set; }
        public virtual DbSet<CategorySellerProduct> CategorySellerProduct { get; set; }
        public virtual DbSet<ChatCustomerService> ChatCustomerService { get; set; }
        public virtual DbSet<ChatSeller> ChatSeller { get; set; }
        public virtual DbSet<City> City { get; set; }
        public virtual DbSet<CityLanguage> CityLanguage { get; set; }
        public virtual DbSet<CoinTransaction> CoinTransaction { get; set; }
        public virtual DbSet<CondoRate> CondoRate { get; set; }
        public virtual DbSet<CondoRegister> CondoRegister { get; set; }
        public virtual DbSet<CondoSharebox> CondoSharebox { get; set; }
        public virtual DbSet<CondoadminRegister> CondoadminRegister { get; set; }
        public virtual DbSet<Condoinfo> Condoinfo { get; set; }
        public virtual DbSet<Config> Config { get; set; }
        public virtual DbSet<Country> Country { get; set; }
        public virtual DbSet<CountryLanguage> CountryLanguage { get; set; }
        public virtual DbSet<CouponPrice> CouponPrice { get; set; }
        public virtual DbSet<CouponType> CouponType { get; set; }
        public virtual DbSet<CreditCard> CreditCard { get; set; }
        public virtual DbSet<CreditNote> CreditNote { get; set; }
        public virtual DbSet<CreditNoteProduct> CreditNoteProduct { get; set; }
        public virtual DbSet<CustomerAddress> CustomerAddress { get; set; }
        public virtual DbSet<CustomerCompany> CustomerCompany { get; set; }
        public virtual DbSet<CustomerReviewProduct> CustomerReviewProduct { get; set; }
        public virtual DbSet<CustomerReviewSeller> CustomerReviewSeller { get; set; }
        public virtual DbSet<CustomerSearchHistoryLog> CustomerSearchHistoryLog { get; set; }
        public virtual DbSet<CustomerUser> CustomerUser { get; set; }
        public virtual DbSet<CustomerUserViewHistoryLog> CustomerUserViewHistoryLog { get; set; }
        public virtual DbSet<DiscountType> DiscountType { get; set; }
        public virtual DbSet<Event> Event { get; set; }
        public virtual DbSet<EventLocker> EventLocker { get; set; }
        public virtual DbSet<FavoriteProduct> FavoriteProduct { get; set; }
        public virtual DbSet<Follow> Follow { get; set; }
        public virtual DbSet<GatewayProvider> GatewayProvider { get; set; }
        public virtual DbSet<GatewayProviderPaymentType> GatewayProviderPaymentType { get; set; }
        public virtual DbSet<GroupBuying> GroupBuying { get; set; }
        public virtual DbSet<GroupBuyingAchievementType> GroupBuyingAchievementType { get; set; }
        public virtual DbSet<GroupBuyingCondition> GroupBuyingCondition { get; set; }
        public virtual DbSet<GroupBuyingInvitation> GroupBuyingInvitation { get; set; }
        public virtual DbSet<GroupBuyingProductVariant> GroupBuyingProductVariant { get; set; }
        public virtual DbSet<GroupBuyingReward> GroupBuyingReward { get; set; }
        public virtual DbSet<Groupon> Groupon { get; set; }
        public virtual DbSet<InvoiceReceipt> InvoiceReceipt { get; set; }
        public virtual DbSet<InvoiceReceiptProduct> InvoiceReceiptProduct { get; set; }
        public virtual DbSet<Language> Language { get; set; }
        public virtual DbSet<Lockeraccess> Lockeraccess { get; set; }
        public virtual DbSet<Lockerinfo> Lockerinfo { get; set; }
        public virtual DbSet<Lockertypeinfo> Lockertypeinfo { get; set; }
        public virtual DbSet<Lockerusermapping> Lockerusermapping { get; set; }
        public virtual DbSet<Logdelivery> Logdelivery { get; set; }
        public virtual DbSet<LogisticCompany> LogisticCompany { get; set; }
        public virtual DbSet<LogisticUser> LogisticUser { get; set; }
        public virtual DbSet<Logopenlocker> Logopenlocker { get; set; }
        public virtual DbSet<MarketplaceCoupon> MarketplaceCoupon { get; set; }
        public virtual DbSet<MarketplaceCouponGroup> MarketplaceCouponGroup { get; set; }
        public virtual DbSet<MarketplacePromotion> MarketplacePromotion { get; set; }
        public virtual DbSet<MarketplacePromotionRedeem> MarketplacePromotionRedeem { get; set; }
        public virtual DbSet<MarketplaceReviewSeller> MarketplaceReviewSeller { get; set; }
        public virtual DbSet<MarketplaceUser> MarketplaceUser { get; set; }
        public virtual DbSet<MediaType> MediaType { get; set; }
        public virtual DbSet<NotificationCustomer> NotificationCustomer { get; set; }
        public virtual DbSet<NotificationManual> NotificationManual { get; set; }
        public virtual DbSet<NotificationManualLanguage> NotificationManualLanguage { get; set; }
        public virtual DbSet<NotificationSeller> NotificationSeller { get; set; }
        public virtual DbSet<NotificationTemplate> NotificationTemplate { get; set; }
        public virtual DbSet<NotificationTemplateLanguage> NotificationTemplateLanguage { get; set; }
        public virtual DbSet<NotificationType> NotificationType { get; set; }
        public virtual DbSet<Nvjmigrationdatahistory> Nvjmigrationdatahistory { get; set; }
        public virtual DbSet<Nvjmigrationhistory> Nvjmigrationhistory { get; set; }
        public virtual DbSet<Payment> Payment { get; set; }
        public virtual DbSet<PaymentType> PaymentType { get; set; }
        public virtual DbSet<Point> Point { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<ProductImage> ProductImage { get; set; }
        public virtual DbSet<ProductRating> ProductRating { get; set; }
        public virtual DbSet<ProductSold> ProductSold { get; set; }
        public virtual DbSet<ProductType> ProductType { get; set; }
        public virtual DbSet<ProductVariant> ProductVariant { get; set; }
        public virtual DbSet<ProductWishlist> ProductWishlist { get; set; }
        public virtual DbSet<PromotionPrice> PromotionPrice { get; set; }
        public virtual DbSet<Province> Province { get; set; }
        public virtual DbSet<ProvinceLanguage> ProvinceLanguage { get; set; }
        public virtual DbSet<PurchaseOrder> PurchaseOrder { get; set; }
        public virtual DbSet<PurchaseOrderDelivered> PurchaseOrderDelivered { get; set; }
        public virtual DbSet<PurchaseOrderLog> PurchaseOrderLog { get; set; }
        public virtual DbSet<PurchaseOrderLogisticUser> PurchaseOrderLogisticUser { get; set; }
        public virtual DbSet<PurchaseOrderParcel> PurchaseOrderParcel { get; set; }
        public virtual DbSet<PurchaseOrderProduct> PurchaseOrderProduct { get; set; }
        public virtual DbSet<PurchaseOrderProductActualQuantity> PurchaseOrderProductActualQuantity { get; set; }
        public virtual DbSet<PurchaseOrderProductPiece> PurchaseOrderProductPiece { get; set; }
        public virtual DbSet<PurchaseOrderTracking> PurchaseOrderTracking { get; set; }
        public virtual DbSet<Regiscode> Regiscode { get; set; }
        public virtual DbSet<RelateCategory> RelateCategory { get; set; }
        public virtual DbSet<RelateProduct> RelateProduct { get; set; }
        public virtual DbSet<RoleAbility> RoleAbility { get; set; }
        public virtual DbSet<RoleClass> RoleClass { get; set; }
        public virtual DbSet<RoleClassAbility> RoleClassAbility { get; set; }
        public virtual DbSet<SaleOrder> SaleOrder { get; set; }
        public virtual DbSet<SaleOrderDiscount> SaleOrderDiscount { get; set; }
        public virtual DbSet<SaleOrderProduct> SaleOrderProduct { get; set; }
        public virtual DbSet<Seller> Seller { get; set; }
        public virtual DbSet<SellerAddress> SellerAddress { get; set; }
        public virtual DbSet<SellerArea> SellerArea { get; set; }
        public virtual DbSet<SellerCoupon> SellerCoupon { get; set; }
        public virtual DbSet<SellerCouponRedeem> SellerCouponRedeem { get; set; }
        public virtual DbSet<SellerGroup> SellerGroup { get; set; }
        public virtual DbSet<SellerImage> SellerImage { get; set; }
        public virtual DbSet<SellerProductDelivery> SellerProductDelivery { get; set; }
        public virtual DbSet<SellerPromotion> SellerPromotion { get; set; }
        public virtual DbSet<SellerPromotionRedeem> SellerPromotionRedeem { get; set; }
        public virtual DbSet<SellerStats> SellerStats { get; set; }
        public virtual DbSet<SellerStatsType> SellerStatsType { get; set; }
        public virtual DbSet<SellerStatsTypeLanguage> SellerStatsTypeLanguage { get; set; }
        public virtual DbSet<SellerUser> SellerUser { get; set; }
        public virtual DbSet<SellerWorkdayGroup> SellerWorkdayGroup { get; set; }
        public virtual DbSet<SellerWorkdayGroupHoliday> SellerWorkdayGroupHoliday { get; set; }
        public virtual DbSet<SellerWorkdayProfile> SellerWorkdayProfile { get; set; }
        public virtual DbSet<SellerWorkdayProfileGroup> SellerWorkdayProfileGroup { get; set; }
        public virtual DbSet<Sharelocker> Sharelocker { get; set; }
        public virtual DbSet<Status> Status { get; set; }
        public virtual DbSet<StatusLanguage> StatusLanguage { get; set; }
        public virtual DbSet<Stock> Stock { get; set; }
        public virtual DbSet<StockType> StockType { get; set; }
        public virtual DbSet<Town> Town { get; set; }
        public virtual DbSet<TownLanguage> TownLanguage { get; set; }
        public virtual DbSet<Variant> Variant { get; set; }
        public virtual DbSet<VariantGroup> VariantGroup { get; set; }
        public virtual DbSet<VariantType> VariantType { get; set; }
        public virtual DbSet<Webuser> Webuser { get; set; }
        public virtual DbSet<WorkingDay> WorkingDay { get; set; }
        public virtual DbSet<Zone> Zone { get; set; }

        //        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //        {
        //            if (!optionsBuilder.IsConfigured)
        //            {
        //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
        //                optionsBuilder.UseMySQL("Server=localhost; Database=twister; Uid=root; pwd=root;");
        //            }
        //        }
        //Scaffold-DbContext "Server=localhost; Database=twister; Uid=root; pwd=root;" MySql.Data.EntityFrameworkCore -OutputDir Entities

        //TODO: error note 
        //Could not scaffold the foreign key 'twister.product_variant(variantGroupId)'. A key for 'id' was not found in the principal entity type 'VariantGroup'.
        //Could not scaffold the foreign key 'twister.variant_group(variantId)'. A key for 'id' was not found in the principal entity type 'Variant'.
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.Entity<AdminChangeLog>(entity =>
            {
                entity.ToTable("admin_change_log");

                entity.HasIndex(e => e.MarketplaceUserId)
                    .HasName("FK__acl__marketplaceUserId__markeplace_user__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.APIName)
                    .HasColumnName("tableName")
                    .HasMaxLength(255)
                    .IsRequired();

                entity.Property(e => e.MarketplaceUserId)
                    .HasColumnName("marketplaceUserId")
                    .HasColumnType("int(10)")
                    .IsRequired();

                entity.Property(e => e.Method)
                    .HasColumnName("method")
                    .HasMaxLength(255)
                    .IsRequired();

                entity.Property(e => e.Data)
                    .HasColumnName("data")
                    .HasColumnType("mediumText")
                    .IsRequired();

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.HasOne(d => d.MarketplaceUser)
                    .WithMany(p => p.AdminChangeLog)
                    .HasForeignKey(d => d.MarketplaceUserId)
                    .HasConstraintName("FK__acl__marketplaceUserId__marketplace_user__id");
            });

            modelBuilder.Entity<AdminRefreshToken>(entity =>
            {
                entity.ToTable("admin_refresh_token");

                entity.HasIndex(e => e.MarketplaceUserId)
                    .HasName("FK__art__marketplaceUserId__marketplace_user__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Token)
                    .IsRequired()
                    .HasColumnName("token")
                    .HasColumnType("text");

                entity.Property(e => e.MarketplaceUserId)
                    .IsRequired()
                    .HasColumnName("marketplaceUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.ExpireDate)
                    .IsRequired()
                    .HasColumnName("expireDate");

                entity.Property(e => e.DeviceName)
                    .HasColumnName("deviceName")
                    .HasMaxLength(255);

                entity.HasOne(d => d.MarketplaceUser)
                    .WithMany(p => p.AdminRefreshToken)
                    .HasForeignKey(d => d.MarketplaceUserId)
                    .HasConstraintName("FK__art__marketplaceUserId__marketplace_user__id");
            });

            modelBuilder.Entity<AllowOpenLocker>(entity =>
            {
                entity.ToTable("allow_open_locker");

                entity.HasIndex(e => e.LogDeliveryId)
                    .HasName("FK__allow_open_locker__logDeliveryId__logdelivery__transnum");

                entity.HasIndex(e => e.SaleOrderId)
                    .HasName("FK__allow_open_locker__saleOrderId__sale_order__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.LogDeliveryId)
                    .HasColumnName("logDeliveryId")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.SaleOrderId)
                    .HasColumnName("saleOrderId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Telephone)
                    .IsRequired()
                    .HasColumnName("telephone")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("updateDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.HasOne(d => d.LogDelivery)
                    .WithMany(p => p.AllowOpenLocker)
                    .HasForeignKey(d => d.LogDeliveryId)
                    .HasConstraintName("FK__allow_open_locker__logDeliveryId__logdelivery__transnum");

                entity.HasOne(d => d.SaleOrder)
                    .WithMany(p => p.AllowOpenLocker)
                    .HasForeignKey(d => d.SaleOrderId)
                    .HasConstraintName("FK__allow_open_locker__saleOrderId__sale_order__id");
            });

            modelBuilder.Entity<Allowstatusinfo>(entity =>
            {
                entity.HasKey(e => e.Transnum);

                entity.ToTable("allowstatusinfo");

                entity.Property(e => e.Transnum)
                    .HasColumnName("transnum")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("createdBy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnName("createdDate");

                entity.Property(e => e.StatusCode)
                    .HasColumnName("statusCode")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.StatusComment)
                    .HasColumnName("statusComment")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.StatusDescription)
                    .HasColumnName("statusDescription")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedBy)
                    .HasColumnName("updatedBy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate)
                    .HasColumnName("updatedDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            modelBuilder.Entity<AppDeepLink>(entity =>
            {
                entity.ToTable("app_deep_link");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.Link)
                     .IsRequired()
                     .HasColumnName("link")
                     .HasColumnType("text");

                entity.Property(e => e.Example)
                    .HasColumnName("example")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<BannerFoodSeller>(entity =>
            {
                entity.ToTable("banner_food_seller");

                entity.HasIndex(e => e.BannerMarketplaceId)
                    .HasName("FK_banner_food_seller__bMarketplaceId__banner_marketplace__id");

                entity.HasIndex(e => e.BannerSellerId)
                    .HasName("FK_banner_food_seller__bannerSellerId__banner_seller__id");

                entity.HasIndex(e => e.SellerGroupId)
                    .HasName("FK_banner_food_seller__sellerGroupId__seller_group__id");

                entity.HasIndex(e => e.SellerId)
                    .HasName("FK_banner_food_seller__sellerId__seller__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.BannerMarketplaceId)
                    .HasColumnName("bannerMarketplaceId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.BannerSellerId)
                    .HasColumnName("bannerSellerId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.SellerGroupId)
                    .HasColumnName("sellerGroupId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.SellerId)
                    .HasColumnName("sellerId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.BannerMarketplace)
                    .WithMany(p => p.BannerFoodSeller)
                    .HasForeignKey(d => d.BannerMarketplaceId)
                    .HasConstraintName("FK_banner_food_seller__bMarketplaceId__banner_marketplace__id");

                entity.HasOne(d => d.BannerSeller)
                    .WithMany(p => p.BannerFoodSeller)
                    .HasForeignKey(d => d.BannerSellerId)
                    .HasConstraintName("FK_banner_food_seller__bannerSellerId__banner_seller__id");

                entity.HasOne(d => d.SellerGroup)
                    .WithMany(p => p.BannerFoodSeller)
                    .HasForeignKey(d => d.SellerGroupId)
                    .HasConstraintName("FK_banner_food_seller__sellerGroupId__seller_group__id");

                entity.HasOne(d => d.Seller)
                    .WithMany(p => p.BannerFoodSeller)
                    .HasForeignKey(d => d.SellerId)
                    .HasConstraintName("FK_banner_food_seller__sellerId__seller__id");
            });

            modelBuilder.Entity<BannerMarketplace>(entity =>
            {
                entity.ToTable("banner_marketplace");

                entity.HasIndex(e => e.MarketplaceUserId)
                    .HasName("fk-banner_marketplace-marketplace_user.id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("fk-banner_marketplace-status.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.EndDate).HasColumnName("endDate");

                entity.Property(e => e.Image)
                    .IsRequired()
                    .HasColumnName("image")
                    .IsUnicode(false);

                entity.Property(e => e.Link)
                    .HasColumnName("link")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MarketplaceUserId)
                    .HasColumnName("marketplaceUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Reference)
                    .HasColumnName("reference")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Sequence)
                    .HasColumnName("sequence")
                    .HasColumnType("int(4)")
                    .HasDefaultValueSql("999");

                entity.Property(e => e.StartDate).HasColumnName("startDate");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.MarketplaceUser)
                    .WithMany(p => p.BannerMarketplace)
                    .HasForeignKey(d => d.MarketplaceUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("banner_marketplace_ibfk_1");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.BannerMarketplace)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("banner_marketplace_ibfk_2");
            });

            modelBuilder.Entity<BannerMarketplaceProduct>(entity =>
            {
                entity.HasKey(e => new { e.BannerMarketplaceId, e.ProductId });

                entity.ToTable("banner_marketplace_product");

                entity.HasIndex(e => e.BannerMarketplaceId)
                    .HasName("FK__bmp__bmId__bm__id");

                entity.HasIndex(e => e.ProductId)
                    .HasName("FK_bmp__productId__product__id");

                entity.Property(e => e.BannerMarketplaceId)
                    .HasColumnName("bannerMarketplaceId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("productId")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.BannerMarketplace)
                    .WithMany(p => p.BannerMarketplaceProduct)
                    .HasForeignKey(d => d.BannerMarketplaceId)
                    .HasConstraintName("FK__bmp__bmId__bm__id");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.BannerMarketplaceProduct)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("FK_bmp__productId__product__id");
            });

            modelBuilder.Entity<BannerSeller>(entity =>
            {
                entity.ToTable("banner_seller");

                entity.HasIndex(e => e.SellerId)
                    .HasName("fk-banner_seller-seller.id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("fk-banner_seller-status.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.Image)
                    .IsRequired()
                    .HasColumnName("image")
                    .IsUnicode(false);

                entity.Property(e => e.Link)
                    .HasColumnName("link")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Reference)
                    .HasColumnName("reference")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SellerId)
                    .HasColumnName("sellerId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Sequence)
                    .HasColumnName("sequence")
                    .HasColumnType("int(4)")
                    .HasDefaultValueSql("999");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.Seller)
                    .WithMany(p => p.BannerSeller)
                    .HasForeignKey(d => d.SellerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("banner_seller_ibfk_1");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.BannerSeller)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("banner_seller_ibfk_2");
            });

            modelBuilder.Entity<Brand>(entity =>
            {
                entity.ToTable("brand");

                entity.HasIndex(e => e.StatusId)
                    .HasName("FK__brand__statusId__status__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.Image)
                    .IsRequired()
                    .HasColumnName("image")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Keyword)
                    .IsRequired()
                    .HasColumnName("keyword")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Sequence)
                    .HasColumnName("sequence")
                    .HasColumnType("int(4)");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Brand)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__brand__statusId__status__id");
            });

            modelBuilder.Entity<BrandLanguage>(entity =>
            {
                entity.HasKey(e => new { e.BrandId, e.LanguageId });

                entity.ToTable("brand_language");

                entity.HasIndex(e => e.LanguageId)
                    .HasName("FK__brand_lang__languageId__language__id");

                entity.Property(e => e.BrandId)
                    .HasColumnName("brandId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.LanguageId)
                    .HasColumnName("languageId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Brand)
                    .WithMany(p => p.BrandLanguage)
                    .HasForeignKey(d => d.BrandId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__brand_lang__brandId__brand__id");

                entity.HasOne(d => d.Language)
                    .WithMany(p => p.BrandLanguage)
                    .HasForeignKey(d => d.LanguageId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__brand_lang__languageId__language__id");

            });

            modelBuilder.Entity<Cart>(entity =>
            {
                entity.ToTable("cart");

                entity.HasIndex(e => e.CustomerUserId)
                    .HasName("fk-cart.customerUserId");

                entity.HasIndex(e => e.EventId)
                    .HasName("eventId");

                entity.HasIndex(e => e.StatusId)
                    .HasName("fk-cart-status.id");

                entity.HasIndex(e => e.GroupBuyingId)
                    .HasName("FK-cart_groupBuyingId_group_buying_id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.CustomerUserId)
                    .HasColumnName("customerUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.EventId)
                    .HasColumnName("eventId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.LockerId)
                    .HasColumnName("lockerId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Remark)
                    .HasColumnName("remark")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.StaticCode)
                    .HasColumnName("staticCode")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.Property(e => e.GroupBuyingId)
                    .HasColumnName("groupBuyingId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.CustomerUser)
                    .WithMany(p => p.Cart)
                    .HasForeignKey(d => d.CustomerUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("cart_ibfk_1");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.Cart)
                    .HasForeignKey(d => d.EventId)
                    .HasConstraintName("cart_ibfk_3");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Cart)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("cart_ibfk_2");

                entity.HasOne(d => d.GroupBuying)
                    .WithMany(p => p.Cart)
                    .HasForeignKey(d => d.GroupBuyingId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK-cart_groupBuyingId_group_buying_id");
            });

            modelBuilder.Entity<CartProduct>(entity =>
            {
                entity.HasKey(e => new { e.CartId, e.ProductVariantId });

                entity.ToTable("cart_product");

                entity.HasIndex(e => e.ProductVariantId)
                    .HasName("fk-cart_product-product_variant.id");

                entity.Property(e => e.CartId)
                    .HasColumnName("cartId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.ProductVariantId)
                    .HasColumnName("productVariantId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasColumnType("int(3)");

                entity.Property(e => e.Remark)
                    .HasColumnName("remark")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.Cart)
                    .WithMany(p => p.CartProduct)
                    .HasForeignKey(d => d.CartId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("cart_product_ibfk_1");

                entity.HasOne(d => d.ProductVariant)
                    .WithMany(p => p.CartProduct)
                    .HasForeignKey(d => d.ProductVariantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("cart_product_ibfk_2");
            });

            modelBuilder.Entity<CategoryMarketplace>(entity =>
            {
                entity.ToTable("category_marketplace");

                entity.HasIndex(e => e.StatusId)
                    .HasName("FK__cat_marketplace__statusId__status__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Banner)
                    .HasColumnName("banner")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.Image)
                    .HasColumnName("image")
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Sequence)
                    .HasColumnName("sequence")
                    .HasColumnType("int(4)")
                    .HasDefaultValueSql("999");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.CategoryMarketplace)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__cat_marketplace__statusId__status__id");
            });

            modelBuilder.Entity<CategoryMarketplaceHierarchy>(entity =>
            {
                entity.HasKey(e => new { e.ParentId, e.ChildrenId });

                entity.ToTable("category_marketplace_hierarchy");

                entity.HasIndex(e => e.ChildrenId)
                    .HasName("fk-category_marketplace.id_children");

                entity.Property(e => e.ParentId)
                    .HasColumnName("parentId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.ChildrenId)
                    .HasColumnName("childrenId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.Children)
                    .WithMany(p => p.CategoryMarketplaceHierarchyChildren)
                    .HasForeignKey(d => d.ChildrenId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk-category_marketplace.id_children");

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.CategoryMarketplaceHierarchyParent)
                    .HasForeignKey(d => d.ParentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk-category_marketplace.id_paraent");
            });

            modelBuilder.Entity<CategoryMarketplaceLanguage>(entity =>
            {
                entity.HasKey(e => new { e.CategoryMarketplaceId, e.LanguageId });

                entity.ToTable("category_marketplace_language");

                entity.HasIndex(e => e.LanguageId)
                    .HasName("FK__cat_marketplace__lang__languageId__lang__id");

                entity.Property(e => e.CategoryMarketplaceId)
                    .HasColumnName("categoryMarketplaceId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.LanguageId)
                    .HasColumnName("languageId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.CategoryMarketplace)
                    .WithMany(p => p.CategoryMarketplaceLanguage)
                    .HasForeignKey(d => d.CategoryMarketplaceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__cat_marketplace_lang__catMarId__cat_mar__id");

                entity.HasOne(d => d.Language)
                    .WithMany(p => p.CategoryMarketplaceLanguage)
                    .HasForeignKey(d => d.LanguageId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__cat_marketplace__lang__languageId__lang__id");
            });

            modelBuilder.Entity<CategoryMarketplaceProduct>(entity =>
            {
                entity.HasKey(e => new { e.CategoryMarketPlaceId, e.ProductId });

                entity.ToTable("category_marketplace_product");

                entity.HasIndex(e => e.ProductId)
                    .HasName("fk-category_marketplace_product-product_id");

                entity.Property(e => e.CategoryMarketPlaceId)
                    .HasColumnName("categoryMarketPlaceId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("productId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.CategoryMarketPlace)
                    .WithMany(p => p.CategoryMarketplaceProduct)
                    .HasForeignKey(d => d.CategoryMarketPlaceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("category_marketplace_product_ibfk_1");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.CategoryMarketplaceProduct)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("category_marketplace_product_ibfk_2");
            });

            modelBuilder.Entity<CategorySeller>(entity =>
            {
                entity.ToTable("category_seller");

                entity.HasIndex(e => e.SellerId)
                    .HasName("FK__cat_seller__sellerId__seller__id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("FK__cat_seller__statusId__status__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.Image)
                    .HasColumnName("image")
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SellerId)
                    .HasColumnName("sellerId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Sequence)
                    .HasColumnName("sequence")
                    .HasColumnType("int(4)")
                    .HasDefaultValueSql("999");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.Seller)
                    .WithMany(p => p.CategorySeller)
                    .HasForeignKey(d => d.SellerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__cat_seller__sellerId__seller__id");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.CategorySeller)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__cat_seller__statusId__status__id");
            });

            modelBuilder.Entity<CategorySellerHierarchy>(entity =>
            {
                entity.HasKey(e => new { e.ParentId, e.ChildrenId });

                entity.ToTable("category_seller_hierarchy");

                entity.HasIndex(e => e.ChildrenId)
                    .HasName("fk-category_seller.id_children");

                entity.Property(e => e.ParentId)
                    .HasColumnName("parentId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.ChildrenId)
                    .HasColumnName("childrenId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.Children)
                    .WithMany(p => p.CategorySellerHierarchyChildren)
                    .HasForeignKey(d => d.ChildrenId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk-category_seller.id_children");

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.CategorySellerHierarchyParent)
                    .HasForeignKey(d => d.ParentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk-category_seller.id_paraent");
            });

            modelBuilder.Entity<CategorySellerProduct>(entity =>
            {
                entity.HasKey(e => new { e.CategorySellerId, e.ProductId });

                entity.ToTable("category_seller_product");

                entity.HasIndex(e => e.ProductId)
                    .HasName("fk-category_seller_product-product.id");

                entity.Property(e => e.CategorySellerId)
                    .HasColumnName("categorySellerId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("productId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.CategorySeller)
                    .WithMany(p => p.CategorySellerProduct)
                    .HasForeignKey(d => d.CategorySellerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("category_seller_product_ibfk_1");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.CategorySellerProduct)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("category_seller_product_ibfk_2");
            });

            modelBuilder.Entity<ChatCustomerService>(entity =>
            {
                entity.ToTable("chat_customer_service");

                entity.HasIndex(e => e.CustomerUserId)
                    .HasName("fk-chat_customer_service-customer_user.id");

                entity.HasIndex(e => e.MarketplaceUserId)
                    .HasName("fk-chat_customer_service-marketplace_user.id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("fk-chat_customer_service-status.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.CustomerUserId)
                    .HasColumnName("customerUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.MarketplaceUserId)
                    .HasColumnName("marketplaceUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Message)
                    .IsRequired()
                    .HasColumnName("message")
                    .IsUnicode(false);

                entity.Property(e => e.Owner)
                    .IsRequired()
                    .HasColumnName("owner")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.CustomerUser)
                    .WithMany(p => p.ChatCustomerService)
                    .HasForeignKey(d => d.CustomerUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("chat_customer_service_ibfk_1");

                entity.HasOne(d => d.MarketplaceUser)
                    .WithMany(p => p.ChatCustomerService)
                    .HasForeignKey(d => d.MarketplaceUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("chat_customer_service_ibfk_2");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ChatCustomerService)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("chat_customer_service_ibfk_3");
            });

            modelBuilder.Entity<ChatSeller>(entity =>
            {
                entity.ToTable("chat_seller");

                entity.HasIndex(e => e.ProductVariantId)
                    .HasName("fk-chat_seller-product_variant.id");

                entity.HasIndex(e => e.SellerId)
                    .HasName("fk-chat_seller-seller.id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("fk-chat_seller-status.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.CustomerServiceId)
                    .HasColumnName("customerServiceId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Message)
                    .IsRequired()
                    .HasColumnName("message")
                    .IsUnicode(false);

                entity.Property(e => e.Owner)
                    .IsRequired()
                    .HasColumnName("owner")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ProductVariantId)
                    .HasColumnName("productVariantId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.SellerId)
                    .HasColumnName("sellerId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.ProductVariant)
                    .WithMany(p => p.ChatSeller)
                    .HasForeignKey(d => d.ProductVariantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("chat_seller_ibfk_1");

                entity.HasOne(d => d.Seller)
                    .WithMany(p => p.ChatSeller)
                    .HasForeignKey(d => d.SellerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("chat_seller_ibfk_2");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ChatSeller)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("chat_seller_ibfk_3");
            });

            modelBuilder.Entity<City>(entity =>
            {
                entity.ToTable("city");

                entity.HasIndex(e => e.ProvinceId)
                    .HasName("FK__city__provinceId__province__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ProvinceId)
                    .HasColumnName("provinceId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Zipcode)
                    .HasColumnName("zipcode")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Province)
                    .WithMany(p => p.City)
                    .HasForeignKey(d => d.ProvinceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__city__provinceId__province__id");
            });

            modelBuilder.Entity<CityLanguage>(entity =>
            {
                entity.HasKey(e => new { e.CityId, e.LanguageId });

                entity.ToTable("city_language");

                entity.HasIndex(e => e.LanguageId)
                    .HasName("FK__city_language__languageId__language__id");

                entity.Property(e => e.CityId)
                    .HasColumnName("cityId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.LanguageId)
                    .HasColumnName("languageId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.City)
                    .WithMany(p => p.CityLanguage)
                    .HasForeignKey(d => d.CityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__city_language__cityId__city__id");

                entity.HasOne(d => d.Language)
                    .WithMany(p => p.CityLanguage)
                    .HasForeignKey(d => d.LanguageId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__city_language__languageId__language__id");
            });

            modelBuilder.Entity<CoinTransaction>(entity =>
            {
                entity.ToTable("coin_transaction");

                entity.HasIndex(e => e.CustomerUserId)
                    .HasName("customerUserId");

                entity.HasIndex(e => e.SaleOrderId)
                    .HasName("FK__coin_tran__saleOrderId__sale_order__id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("statusId");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Amount)
                    .HasColumnName("amount")
                    .HasColumnType("decimal(6,2)");

                entity.Property(e => e.Balance)
                    .HasColumnName("balance")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.CustomerUserId)
                    .HasColumnName("customerUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.LogOpenId)
                    .HasColumnName("logOpenId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.SaleOrderId)
                    .HasColumnName("saleOrderId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.CustomerUser)
                    .WithMany(p => p.CoinTransaction)
                    .HasForeignKey(d => d.CustomerUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("coin_transaction_ibfk_2");

                entity.HasOne(d => d.SaleOrder)
                    .WithMany(p => p.CoinTransaction)
                    .HasForeignKey(d => d.SaleOrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__coin_tran__saleOrderId__sale_order__id");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.CoinTransaction)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("coin_transaction_ibfk_1");
            });

            modelBuilder.Entity<CondoRate>(entity =>
            {
                entity.ToTable("condo_rate");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.BoxId)
                    .HasColumnName("boxID")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("createdBy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnName("createdDate");

                entity.Property(e => e.LockerCode)
                    .HasColumnName("lockerCode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Rate)
                    .HasColumnName("rate")
                    .HasColumnType("double(18,8)")
                    .HasDefaultValueSql("0.00000000");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedBy)
                    .HasColumnName("updatedBy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate)
                    .HasColumnName("updatedDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            modelBuilder.Entity<CondoRegister>(entity =>
            {
                entity.ToTable("condo_register");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CondoId)
                    .HasColumnName("condoID")
                    .HasColumnType("bigint(50)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("createdBy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnName("createdDate");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Telephone)
                    .HasColumnName("telephone")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedBy)
                    .HasColumnName("updatedBy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate)
                    .HasColumnName("updatedDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            modelBuilder.Entity<CondoSharebox>(entity =>
            {
                entity.ToTable("condo_sharebox");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CondoId)
                    .HasColumnName("condoID")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("createdBy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnName("createdDate");

                entity.Property(e => e.LockerCode)
                    .HasColumnName("lockerCode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedBy)
                    .HasColumnName("updatedBy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate)
                    .HasColumnName("updatedDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            modelBuilder.Entity<CondoadminRegister>(entity =>
            {
                entity.ToTable("condoadmin_register");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CondoId)
                    .HasColumnName("condoID")
                    .HasColumnType("bigint(20)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("createdBy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnName("createdDate");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Telephone)
                    .HasColumnName("telephone")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedBy)
                    .HasColumnName("updatedBy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate)
                    .HasColumnName("updatedDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            modelBuilder.Entity<Condoinfo>(entity =>
            {
                entity.ToTable("condoinfo");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Address)
                    .HasColumnName("address")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.CondoName)
                    .HasColumnName("condoName")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("createdBy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnName("createdDate");

                entity.Property(e => e.Lat)
                    .HasColumnName("lat")
                    .HasColumnType("double(20,10)")
                    .HasDefaultValueSql("0.0000000000");

                entity.Property(e => e.LinkDelivery)
                    .HasColumnName("linkDelivery")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.LinkOpen)
                    .HasColumnName("linkOpen")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Lon)
                    .HasColumnName("lon")
                    .HasColumnType("double(20,10)")
                    .HasDefaultValueSql("0.0000000000");

                entity.Property(e => e.UpdatedBy)
                    .HasColumnName("updatedBy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate)
                    .HasColumnName("updatedDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            modelBuilder.Entity<Config>(entity =>
            {
                entity.ToTable("config");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnName("value")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Country>(entity =>
            {
                entity.ToTable("country");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(4)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CountryLanguage>(entity =>
            {
                entity.HasKey(e => new { e.CountryId, e.LanguageId });

                entity.ToTable("country_language");

                entity.HasIndex(e => e.LanguageId)
                    .HasName("FK__country_language__languageId__language__id");

                entity.Property(e => e.CountryId)
                    .HasColumnName("countryId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.LanguageId)
                    .HasColumnName("languageId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.CountryLanguage)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__country_language__countryId__country__id");

                entity.HasOne(d => d.Language)
                    .WithMany(p => p.CountryLanguage)
                    .HasForeignKey(d => d.LanguageId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__country_language__languageId__language__id");
            });

            modelBuilder.Entity<CouponPrice>(entity =>
            {
                entity.ToTable("coupon_price");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.DiscountLimit)
                    .HasColumnName("discountLimit")
                    .HasColumnType("decimal(10,0)");

                entity.Property(e => e.DiscountPercent)
                    .HasColumnName("discountPercent")
                    .HasColumnType("decimal(10,0)");

                entity.Property(e => e.DiscountTotal)
                    .HasColumnName("discountTotal")
                    .HasColumnType("decimal(10,0)");

                entity.Property(e => e.MinimumPurchase)
                    .HasColumnName("minimumPurchase")
                    .HasColumnType("decimal(10,0)");
            });

            modelBuilder.Entity<CouponType>(entity =>
            {
                entity.ToTable("coupon_type");

                entity.HasIndex(e => e.CategoryMarketplaceId)
                    .HasName("FK__coupon_type__catMarketplaceId__cat_market__id");

                entity.HasIndex(e => e.CouponPriceId)
                    .HasName("FK__coupon_type__couponPriceId__coupon_price__id");

                entity.HasIndex(e => e.ProductId)
                    .HasName("FK__coupon_type__productId__product__id");

                entity.HasIndex(e => e.SellerId)
                    .HasName("FK__coupon_type__sellerId__seller__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CategoryMarketplaceId)
                    .HasColumnName("categoryMarketplaceId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CouponPriceId)
                    .HasColumnName("couponPriceId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("productId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SellerId)
                    .HasColumnName("sellerId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.WholeCart)
                    .HasColumnName("wholeCart")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("b'0'");

                entity.HasOne(d => d.CategoryMarketplace)
                    .WithMany(p => p.CouponType)
                    .HasForeignKey(d => d.CategoryMarketplaceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__coupon_type__catMarketplaceId__cat_market__id");

                entity.HasOne(d => d.CouponPrice)
                    .WithMany(p => p.CouponType)
                    .HasForeignKey(d => d.CouponPriceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__coupon_type__couponPriceId__coupon_price__id");

                entity.HasOne(d => d.Seller)
                    .WithMany(p => p.CouponType)
                    .HasForeignKey(d => d.SellerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__coupon_type__productId__product__id");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.CouponType)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__coupon_type__sellerId__seller__id");
            });

            modelBuilder.Entity<CreditCard>(entity =>
            {
                entity.ToTable("credit_card");

                entity.HasIndex(e => e.CustomerUserId)
                    .HasName("FK__credit_card__customerUserId__customer_user__id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("statusId");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.CustomerUserId)
                    .HasColumnName("customerUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.ExpireDate)
                    .HasColumnName("expireDate")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Number)
                    .IsRequired()
                    .HasColumnName("number")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.Token)
                    .HasColumnName("token")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.CustomerUser)
                    .WithMany(p => p.CreditCard)
                    .HasForeignKey(d => d.CustomerUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__credit_card__customerUserId__customer_user__id");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.CreditCard)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("credit_card_ibfk_1");
            });

            modelBuilder.Entity<CreditNote>(entity =>
            {
                entity.ToTable("credit_note");

                entity.HasIndex(e => e.CustomerUserId)
                    .HasName("FK__credit_note__customerUserId__customer_user__id");

                entity.HasIndex(e => e.InvoiceReceiptId)
                    .HasName("fk-credit_note-invoiceReceipt.id");

                entity.HasIndex(e => e.SaleOrderId)
                    .HasName("fk-credit_note-saleOrder.id");

                entity.HasIndex(e => e.SellerId)
                    .HasName("fk-credit_note-seller.id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("fk-credit_note-status.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasColumnName("address")
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasColumnName("city")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.CustomerUserId)
                    .HasColumnName("customerUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InvoiceReceiptId)
                    .HasColumnName("invoiceReceiptId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.MarketplaceAddress)
                    .IsRequired()
                    .HasColumnName("marketplaceAddress")
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NetPrice)
                    .HasColumnName("netPrice")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Payment)
                    .HasColumnName("payment")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Province)
                    .IsRequired()
                    .HasColumnName("province")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Remark)
                    .HasColumnName("remark")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SaleOrderCode)
                    .IsRequired()
                    .HasColumnName("saleOrderCode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SaleOrderId)
                    .HasColumnName("saleOrderId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.SellerId)
                    .HasColumnName("sellerId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.Telephone)
                    .IsRequired()
                    .HasColumnName("telephone")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TotalPrice)
                    .HasColumnName("totalPrice")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.Property(e => e.Vat)
                    .HasColumnName("vat")
                    .HasColumnType("decimal(10,2)");

                entity.HasOne(d => d.CustomerUser)
                    .WithMany(p => p.CreditNote)
                    .HasForeignKey(d => d.CustomerUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__credit_note__customerUserId__customer_user__id");

                entity.HasOne(d => d.InvoiceReceipt)
                    .WithMany(p => p.CreditNote)
                    .HasForeignKey(d => d.InvoiceReceiptId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("credit_note_ibfk_1");

                entity.HasOne(d => d.SaleOrder)
                    .WithMany(p => p.CreditNote)
                    .HasForeignKey(d => d.SaleOrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("credit_note_ibfk_2");

                entity.HasOne(d => d.Seller)
                    .WithMany(p => p.CreditNote)
                    .HasForeignKey(d => d.SellerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("credit_note_ibfk_3");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.CreditNote)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("credit_note_ibfk_4");
            });

            modelBuilder.Entity<CreditNoteProduct>(entity =>
            {
                entity.ToTable("credit_note_product");

                entity.HasIndex(e => e.CreditNoteId)
                    .HasName("fk-credit_note_product-credit_note.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreditNoteId)
                    .HasColumnName("creditNoteId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Discount)
                    .HasColumnName("discount")
                    .HasColumnType("decimal(10,0)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NetPrice)
                    .HasColumnName("netPrice")
                    .HasColumnType("decimal(10,0)");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("decimal(10,0)");

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Vat)
                    .HasColumnName("vat")
                    .HasColumnType("decimal(10,0)");

                entity.HasOne(d => d.CreditNote)
                    .WithMany(p => p.CreditNoteProduct)
                    .HasForeignKey(d => d.CreditNoteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("credit_note_product_ibfk_1");
            });

            modelBuilder.Entity<CustomerAddress>(entity =>
            {
                entity.ToTable("customer_address");

                entity.HasIndex(e => e.CityId)
                    .HasName("fk-customer_address-city.id");

                entity.HasIndex(e => e.CountryId)
                    .HasName("fk-customer_address-country.id");

                entity.HasIndex(e => e.CustomerUserId)
                    .HasName("fk-customer_address-customer_user.id");

                entity.HasIndex(e => e.ProvinceId)
                    .HasName("fk-customer_address-province.id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("fk-customer_address-status.id");

                entity.HasIndex(e => e.TownId)
                    .HasName("FK__customer_address__townId__town__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Address1)
                    .IsRequired()
                    .HasColumnName("address1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Address2)
                    .HasColumnName("address2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Address3)
                    .HasColumnName("address3")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CityId)
                    .HasColumnName("cityId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CountryId)
                    .HasColumnName("countryId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.CustomerUserId)
                    .HasColumnName("customerUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ProvinceId)
                    .HasColumnName("provinceId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.TaxNumber)
                    .HasColumnName("taxNumber")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TownId)
                    .HasColumnName("townId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("updateDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Zipcode)
                    .HasColumnName("zipcode")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.City)
                    .WithMany(p => p.CustomerAddress)
                    .HasForeignKey(d => d.CityId)
                    .HasConstraintName("customer_address_ibfk_1");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.CustomerAddress)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("customer_address_ibfk_2");

                entity.HasOne(d => d.CustomerUser)
                    .WithMany(p => p.CustomerAddress)
                    .HasForeignKey(d => d.CustomerUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("customer_address_ibfk_3");

                entity.HasOne(d => d.Province)
                    .WithMany(p => p.CustomerAddress)
                    .HasForeignKey(d => d.ProvinceId)
                    .HasConstraintName("customer_address_ibfk_4");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.CustomerAddress)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("customer_address_ibfk_5");

                entity.HasOne(d => d.Town)
                    .WithMany(p => p.CustomerAddress)
                    .HasForeignKey(d => d.TownId)
                    .HasConstraintName("FK__customer_address__townId__town__id");
            });

            modelBuilder.Entity<CustomerCompany>(entity =>
            {
                entity.ToTable("customer_company");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)")
                    .ValueGeneratedNever();

                entity.Property(e => e.Address)
                    .HasColumnName("address")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Brand)
                    .HasColumnName("brand")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CardId)
                    .HasColumnName("cardID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Company)
                    .HasColumnName("company")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("createdBy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnName("createdDate");

                entity.Property(e => e.FirstName)
                    .HasColumnName("firstName")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Group)
                    .HasColumnName("group")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasColumnName("lastName")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Prefix)
                    .HasColumnName("prefix")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UpdatedBy)
                    .HasColumnName("updatedBy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate)
                    .HasColumnName("updatedDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            modelBuilder.Entity<CustomerReviewProduct>(entity =>
            {
                entity.ToTable("customer_review_product");

                entity.HasIndex(e => e.CustomerUserId)
                    .HasName("fk-customer_review_product-customer_user.id");

                entity.HasIndex(e => e.ProductId)
                    .HasName("fk-customer_review_product-product.id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("fk-customer_review_product-status.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Comment)
                    .HasColumnName("comment")
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.CustomerUserId)
                    .HasColumnName("customerUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("productId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Rating)
                    .HasColumnName("rating")
                    .HasColumnType("int(3)");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.CustomerUser)
                    .WithMany(p => p.CustomerReviewProduct)
                    .HasForeignKey(d => d.CustomerUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("customer_review_product_ibfk_1");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.CustomerReviewProduct)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("customer_review_product_ibfk_2");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.CustomerReviewProduct)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("customer_review_product_ibfk_3");
            });

            modelBuilder.Entity<CustomerReviewSeller>(entity =>
            {
                entity.ToTable("customer_review_seller");

                entity.HasIndex(e => e.CustomerUserId)
                    .HasName("fk-customer_review_seller-customer_user.id");

                entity.HasIndex(e => e.SellerId)
                    .HasName("customer_review_seller-seller.id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("customer_review_seller-status.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Comment)
                    .HasColumnName("comment")
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.CustomerUserId)
                    .HasColumnName("customerUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Rating)
                    .HasColumnName("rating")
                    .HasColumnType("int(3)");

                entity.Property(e => e.SellerId)
                    .HasColumnName("sellerId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.CustomerUser)
                    .WithMany(p => p.CustomerReviewSeller)
                    .HasForeignKey(d => d.CustomerUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("customer_review_seller_ibfk_3");

                entity.HasOne(d => d.Seller)
                    .WithMany(p => p.CustomerReviewSeller)
                    .HasForeignKey(d => d.SellerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("customer_review_seller_ibfk_1");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.CustomerReviewSeller)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("customer_review_seller_ibfk_2");
            });

            modelBuilder.Entity<CustomerSearchHistoryLog>(entity =>
            {
                entity.ToTable("customer_search_history_log");

                entity.HasIndex(e => e.CustomerUserId)
                    .HasName("customer_search_history_log-customer_user.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CustomerUserId)
                    .HasColumnName("customerUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.SearchWord)
                    .IsRequired()
                    .HasColumnName("searchWord")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.CustomerUser)
                    .WithMany(p => p.CustomerSearchHistoryLog)
                    .HasForeignKey(d => d.CustomerUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("customer_search_history_log_ibfk_1");
            });

            modelBuilder.Entity<CustomerUser>(entity =>
            {
                entity.ToTable("customer_user");

                entity.HasIndex(e => e.CountryId)
                    .HasName("fk-customer_user-country.id");

                entity.HasIndex(e => e.LanguageId)
                    .HasName("FK__customer_user__languageId__language__id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("fk-customer_user-status.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Age)
                    .HasColumnName("age")
                    .HasColumnType("int(3)");

                entity.Property(e => e.Birthday).HasColumnName("birthday");

                entity.Property(e => e.CitizenId)
                    .HasColumnName("citizenId")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CoinAmount)
                    .HasColumnName("coinAmount")
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.CountryId)
                    .HasColumnName("countryId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FacebookToken)
                    .HasColumnName("facebookToken")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .HasColumnName("firstName")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GoogleToken)
                    .HasColumnName("googleToken")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Image)
                    .HasColumnName("image")
                    .IsUnicode(false);

                entity.Property(e => e.LanguageId)
                    .HasColumnName("languageId")
                    .HasColumnType("int(4)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.LastLogin).HasColumnName("lastLogin");

                entity.Property(e => e.LastName)
                    .HasColumnName("lastName")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LineToken)
                    .HasColumnName("lineToken")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MobileAppVersion)
                    .HasColumnName("mobileAppVersion")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PassportId)
                    .HasColumnName("passportId")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PassportIssuePlace)
                    .HasColumnName("passportIssuePlace")
                    .HasColumnType("int(4)");

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Point)
                    .HasColumnName("point")
                    .HasColumnType("int(6)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.RemoteNotificationId)
                    .HasColumnName("remoteNotificationId")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Sex)
                    .HasColumnName("sex")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.Telephone)
                    .IsRequired()
                    .HasColumnName("telephone")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UniqueId)
                    .IsRequired()
                    .HasColumnName("uniqueId")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.CustomerUser)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("customer_user_ibfk_1");

                entity.HasOne(d => d.Language)
                    .WithMany(p => p.CustomerUser)
                    .HasForeignKey(d => d.LanguageId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__customer_user__languageId__language__id");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.CustomerUser)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("customer_user_ibfk_2");
            });

            modelBuilder.Entity<CustomerUserViewHistoryLog>(entity =>
            {
                entity.ToTable("customer_user_view_history_log");

                entity.HasIndex(e => e.CustomerUserId)
                    .HasName("fk-customer_view_history_log-customer_user.id");

                entity.HasIndex(e => e.ProductId)
                    .HasName("customer_view_history_log-product.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.CustomerUserId)
                    .HasColumnName("customerUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("productId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.CustomerUser)
                    .WithMany(p => p.CustomerUserViewHistoryLog)
                    .HasForeignKey(d => d.CustomerUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("customer_user_view_history_log_ibfk_2");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.CustomerUserViewHistoryLog)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("customer_user_view_history_log_ibfk_1");
            });

            modelBuilder.Entity<DiscountType>(entity =>
            {
                entity.ToTable("discount_type");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(4)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Event>(entity =>
            {
                entity.ToTable("event");

                entity.HasIndex(e => e.StatusId)
                    .HasName("FK__event__statusId__status__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Address1)
                    .HasColumnName("address1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Address2)
                    .HasColumnName("address2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EndDate).HasColumnName("endDate");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.StartDate).HasColumnName("startDate");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Event)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__event__statusId__status__id");
            });

            modelBuilder.Entity<EventLocker>(entity =>
            {
                entity.HasKey(e => new { e.EventId, e.LockerId });

                entity.ToTable("event_locker");

                entity.HasIndex(e => e.LockerId)
                    .HasName("lockerId");

                entity.Property(e => e.EventId)
                    .HasColumnName("eventId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.LockerId)
                    .HasColumnName("lockerId")
                    .HasColumnType("bigint(20)");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.EventLocker)
                    .HasForeignKey(d => d.EventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("event_locker_ibfk_1");

                entity.HasOne(d => d.Locker)
                    .WithMany(p => p.EventLocker)
                    .HasForeignKey(d => d.LockerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("event_locker_ibfk_2");
            });

            modelBuilder.Entity<FavoriteProduct>(entity =>
            {
                entity.HasKey(e => new { e.CustomerUserId, e.ProductId });

                entity.ToTable("favorite_product");

                entity.HasIndex(e => e.ProductId)
                    .HasName("fk-favorite_product-product.id");

                entity.Property(e => e.CustomerUserId)
                    .HasColumnName("customerUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("productId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.HasOne(d => d.CustomerUser)
                    .WithMany(p => p.FavoriteProduct)
                    .HasForeignKey(d => d.CustomerUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk-favorite_product-customerUserId.id");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.FavoriteProduct)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk-favorite_product-product.id");
            });

            modelBuilder.Entity<Follow>(entity =>
            {
                entity.HasKey(e => new { e.CutomerUserId, e.SellerId });

                entity.ToTable("follow");

                entity.HasIndex(e => e.SellerId)
                    .HasName("fk-follow-seller.id");

                entity.Property(e => e.CutomerUserId)
                    .HasColumnName("cutomerUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.SellerId)
                    .HasColumnName("sellerId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.CutomerUser)
                    .WithMany(p => p.Follow)
                    .HasForeignKey(d => d.CutomerUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("follow_ibfk_1");

                entity.HasOne(d => d.Seller)
                    .WithMany(p => p.Follow)
                    .HasForeignKey(d => d.SellerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("follow_ibfk_2");
            });

            modelBuilder.Entity<GatewayProvider>(entity =>
            {
                entity.ToTable("gateway_provider");

                entity.HasIndex(e => e.StatusId)
                    .HasName("FK__gp__statusId__status__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.GatewayProvider)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__gp__statusId__status__id");

            });

            modelBuilder.Entity<GatewayProviderPaymentType>(entity =>
            {
                entity.ToTable("gateway_provider_payment_type");

                entity.HasIndex(e => e.GatewayProviderId)
                    .HasName("gatewayProviderId");

                entity.HasIndex(e => e.PaymentTypeId)
                    .HasName("paymentTypeId");

                entity.HasIndex(e => e.StatusId)
                    .HasName("statusId");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.GatewayProviderId)
                    .HasColumnName("gatewayProviderId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.PaymentTypeId)
                    .HasColumnName("paymentTypeId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnName("value")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.GatewayProvider)
                    .WithMany(p => p.GatewayProviderPaymentType)
                    .HasForeignKey(d => d.GatewayProviderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("gateway_provider_payment_type_ibfk_2");

                entity.HasOne(d => d.PaymentType)
                    .WithMany(p => p.GatewayProviderPaymentType)
                    .HasForeignKey(d => d.PaymentTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("gateway_provider_payment_type_ibfk_1");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.GatewayProviderPaymentType)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("gateway_provider_payment_type_ibfk_3");
            });

            modelBuilder.Entity<GroupBuying>(entity =>
            {
                entity.ToTable("group_buying");

                entity.HasIndex(e => e.GroupBuyingProductVariantId)
                    .HasName("FK_gb_gbpvId_gbpv_id");

                entity.HasIndex(e => e.CustomerUserId)
                    .HasName("FK_gb_customerUserId_customer_user_id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("FK_gb_statusId_status_id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.GroupBuyingProductVariantId)
                    .HasColumnName("groupBuyingProductVariantId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CustomerUserId)
                    .HasColumnName("customerUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.StartDate)
                    .HasColumnName("startDate");

                entity.Property(e => e.EndDate)
                    .HasColumnName("endDate");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("updateDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.GroupBuyingProductVariant)
                    .WithMany(p => p.GroupBuying)
                    .HasForeignKey(d => d.GroupBuyingProductVariantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_gb_gbpvId_gbpv_id");

                entity.HasOne(d => d.CustomerUser)
                    .WithMany(p => p.GroupBuying)
                    .HasForeignKey(d => d.CustomerUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_gb_customerUserId_customer_user_id");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.GroupBuying)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_gb_statusId_status_id");
            });

            modelBuilder.Entity<GroupBuyingAchievementType>(entity =>
            {
                entity.ToTable("group_buying_achievement_type");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(255)");

            });

            modelBuilder.Entity<GroupBuyingCondition>(entity =>
            {
                entity.ToTable("group_buying_condition");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.TargetBuyer)
                    .HasColumnName("targetBuyer")
                    .HasColumnType("int(10)");

                entity.Property(e => e.TargetQuantity)
                    .HasColumnName("targetQuantity")
                    .HasColumnType("int(10)");

                entity.Property(e => e.TargetPrice)
                    .HasColumnName("targetPrice")
                    .HasColumnType("decimal(10, 2)");

            });

            modelBuilder.Entity<GroupBuyingInvitation>(entity =>
            {
                entity.ToTable("group_buying_invitation");

                entity.HasIndex(e => e.GroupBuyingId)
                    .HasName("FK_gbiv_groupBuyingId_group_buying_id");

                entity.HasIndex(e => e.InviteeCustomerUserId)
                    .HasName("FK_gbiv_inviteeCustomerUserId_customer_user_id");

                entity.HasIndex(e => e.InviterCustomerUserId)
                    .HasName("FK_gbiv_inviterCustomerUserId_customer_user_id");

                entity.HasIndex(e => e.ParentCustomerUserId)
                    .HasName("FK_gbiv_parentCustomerUserId_customer_user_id");

                entity.HasIndex(e => e.InviteeSaleOrderId)
                    .HasName("FK_gbiv_inviteeSaleOrderId_saleOrder_id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.GroupBuyingId)
                    .HasColumnName("groupBuyingId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.InviterCustomerUserId)
                    .HasColumnName("inviterCustomerUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.InviteeSaleOrderId)
                    .HasColumnName("inviteeSaleOrderId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.ParentCustomerUserId)
                    .HasColumnName("parentCustomerUserId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.GroupBuying)
                    .WithMany(p => p.GroupBuyingInvitation)
                    .HasForeignKey(d => d.GroupBuyingId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_gbiv_groupBuyingId_group_buying_id");

                entity.HasOne(d => d.InviteeCustomerUser)
                    .WithMany(p => p.GroupBuyingInvitationInvitee)
                    .HasForeignKey(d => d.InviteeCustomerUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_gbiv_inviteeCustomerUserId_customer_user_id");

                entity.HasOne(d => d.InviterCustomerUser)
                    .WithMany(p => p.GroupBuyingInvitationInviter)
                    .HasForeignKey(d => d.InviterCustomerUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_gbiv_inviterCustomerUserId_customer_user_id");

                entity.HasOne(d => d.InviteeSaleOrder)
                    .WithMany(p => p.GroupBuyingInvitation)
                    .HasForeignKey(d => d.InviteeSaleOrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_gbiv_inviteeSaleOrderId_saleOrder_id");

                entity.HasOne(d => d.ParentCustomerUser)
                    .WithMany(p => p.GroupBuyingInvitationParent)
                    .HasForeignKey(d => d.ParentCustomerUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_gbiv_parentCustomerUserId_customer_user_id");
            });

            modelBuilder.Entity<GroupBuyingProductVariant>(entity =>
            {
                entity.ToTable("group_buying_product_variant");

                entity.HasIndex(e => e.ProductVariantId)
                    .HasName("FK_gbpv_productVariantId_product_variant_id");

                entity.HasIndex(e => e.GroupBuyingConditionId)
                    .HasName("FK_gbpv_gbcId_group_buying_condition_id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("FK_gbpv_statusId_status_id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.ProductVariantId)
                    .HasColumnName("productVariantId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.GroupBuyingConditionId)
                    .HasColumnName("groupBuyingConditionId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.DurationInMinutes)
                    .HasColumnName("durationInMinutes")
                    .HasColumnType("int(10)");

                entity.Property(e => e.StartDate)
                    .HasColumnName("startDate");

                entity.Property(e => e.EndDate)
                    .HasColumnName("endDate");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("updateDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.ProductVariant)
                    .WithMany(p => p.GroupBuyingProductVariant)
                    .HasForeignKey(d => d.ProductVariantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_gbpv_productVariantId_product_variant_id");

                entity.HasOne(d => d.GroupBuyingCondition)
                    .WithMany(p => p.GroupBuyingProductVariant)
                    .HasForeignKey(d => d.GroupBuyingConditionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_gbpv_gbcId_group_buying_condition_id");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.GroupBuyingProductVariant)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_gbpv_statusId_status_id");
            });

            modelBuilder.Entity<GroupBuyingReward>(entity =>
            {
                entity.ToTable("group_buying_reward");

                entity.HasIndex(e => e.GroupBuyingProductVariantId)
                    .HasName("FK_gbr_groupBuyingProductVariantId_gbpv_id");

                entity.HasIndex(e => e.GroupBuyingAchievementTypeId)
                    .HasName("FK_gbr_groupBuyingAchievementTypeId_gba_id");

                entity.HasIndex(e => e.MarketplaceCouponGroupId)
                    .HasName("FK_gbr_marketplaceCouponGroupId_mktcg_id");

                entity.HasIndex(e => e.ProductVariantId)
                    .HasName("FK_gbr_productVariantId_product_variant_id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.GroupBuyingProductVariantId)
                    .HasColumnName("groupBuyingProductVariantId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.GroupBuyingAchievementTypeId)
                    .HasColumnName("groupBuyingAchievementTypeId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.SalePrice)
                    .HasColumnName("salePrice")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Point)
                    .HasColumnName("point")
                    .HasColumnType("int(10)");

                entity.Property(e => e.MarketplaceCouponGroupId)
                    .HasColumnName("marketplaceCouponGroupId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.ProductVariantId)
                    .HasColumnName("productVariantId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.GroupBuyingProductVariant)
                    .WithMany(p => p.GroupBuyingReward)
                    .HasForeignKey(d => d.GroupBuyingProductVariantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_gbr_groupBuyingProductVariantId_gbpv_id");

                entity.HasOne(d => d.GroupBuyingAchievementType)
                    .WithMany(p => p.GroupBuyingReward)
                    .HasForeignKey(d => d.GroupBuyingAchievementTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_gbr_groupBuyingAchievementTypeId_gba_id");

                entity.HasOne(d => d.MarketplaceCouponGroup)
                    .WithMany(p => p.GroupBuyingReward)
                    .HasForeignKey(d => d.MarketplaceCouponGroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_gbr_marketplaceCouponGroupId_mktcg_id");

                entity.HasOne(d => d.ProductVariant)
                    .WithMany(p => p.GroupBuyingReward)
                    .HasForeignKey(d => d.ProductVariantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_gbr_productVariantId_product_variant_id");
            });

            modelBuilder.Entity<Groupon>(entity =>
            {
                entity.ToTable("groupon");

                entity.HasIndex(e => e.SellerId)
                    .HasName("FK__groupon__sellerId__seller__id");

                entity.HasIndex(e => e.CustomerUserId)
                    .HasName("FK__groupon__customerUserId__customer_user__id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("FK__groupon__statusId__status__id");

                entity.HasIndex(e => e.SaleOrderProductId)
                    .HasName("FK__groupon__saleOrderProductId__sale_order_product__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.SellerId)
                    .HasColumnName("sellerId")
                    .HasColumnType("int(10)")
                    .IsRequired();

                entity.Property(e => e.CustomerUserId)
                    .HasColumnName("customerUserId")
                    .HasColumnType("int(10)")
                    .IsRequired();

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("updateDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");

                entity.Property(e => e.ExpireDate)
                    .HasColumnName("expireDate");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasMaxLength(255);

                entity.Property(e => e.ConfirmationCode)
                    .IsRequired(false)
                    .HasColumnName("confirmationCode")
                    .HasMaxLength(255);

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.SaleOrderProductId)
                    .HasColumnName("saleOrderProductId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.Seller)
                    .WithMany(p => p.Groupon)
                    .HasForeignKey(d => d.SellerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__groupon__sellerId__seller__id");

                entity.HasOne(d => d.CustomerUser)
                    .WithMany(p => p.Groupon)
                    .HasForeignKey(d => d.CustomerUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__groupon__customerUserId__customer_user__id");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Groupon)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__groupon__statusId__status__id");

                entity.HasOne(d => d.SaleOrderProduct)
                    .WithMany(p => p.Groupon)
                    .HasForeignKey(d => d.SaleOrderProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__groupon__saleOrderProductId__sale_order_product__id");
            });

            modelBuilder.Entity<InvoiceReceipt>(entity =>
            {
                entity.ToTable("invoice_receipt");

                entity.HasIndex(e => e.CustomerUserId)
                    .HasName("fk-invoice-receipt-customer_user.id");

                entity.HasIndex(e => e.SaleOrderId)
                    .HasName("FK__invrep__saleOrderId__sale_order__id");

                entity.HasIndex(e => e.SellerId)
                    .HasName("fk-invoice-receipt-seller.id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("fk-invoice-receipt-status.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.AccountNumber)
                    .HasColumnName("accountNumber")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AccountTwister)
                    .HasColumnName("accountTwister")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasColumnName("address")
                    .IsUnicode(false);

                entity.Property(e => e.BankName)
                    .HasColumnName("bankName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasColumnName("city")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CounsterServiceName)
                    .HasColumnName("counsterServiceName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CounsterServiceRefNumber)
                    .HasColumnName("counsterServiceRefNumber")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.CreditCardNumber)
                    .HasColumnName("creditCardNumber")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerUserId)
                    .HasColumnName("customerUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Discount)
                    .HasColumnName("discount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GatewayRefNumber)
                    .HasColumnName("gatewayRefNumber")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MarketplaceAddress)
                    .IsRequired()
                    .HasColumnName("marketplaceAddress")
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NetPrice)
                    .HasColumnName("netPrice")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.PaymentMethod)
                    .HasColumnName("paymentMethod")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Province)
                    .HasColumnName("province")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Remark)
                    .HasColumnName("remark")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SaleOrderCode)
                    .IsRequired()
                    .HasColumnName("saleOrderCode")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.SaleOrderId)
                    .HasColumnName("saleOrderId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.SellerAddress)
                    .IsRequired()
                    .HasColumnName("sellerAddress")
                    .IsUnicode(false);

                entity.Property(e => e.SellerId)
                    .HasColumnName("sellerId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.SellerName)
                    .IsRequired()
                    .HasColumnName("sellerName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ShippingFee)
                    .HasColumnName("shippingFee")
                    .HasColumnType("decimal(6,2)");

                entity.Property(e => e.StaticCode)
                    .IsRequired()
                    .HasColumnName("staticCode")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.Telephone)
                    .IsRequired()
                    .HasColumnName("telephone")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TotalPrice)
                    .HasColumnName("totalPrice")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.Property(e => e.Vat)
                    .HasColumnName("vat")
                    .HasColumnType("decimal(8,2)");

                entity.HasOne(d => d.CustomerUser)
                    .WithMany(p => p.InvoiceReceipt)
                    .HasForeignKey(d => d.CustomerUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("invoice_receipt_ibfk_1");

                entity.HasOne(d => d.SaleOrder)
                    .WithMany(p => p.InvoiceReceipt)
                    .HasForeignKey(d => d.SaleOrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__invrep__saleOrderId__sale_order__id");

                entity.HasOne(d => d.Seller)
                    .WithMany(p => p.InvoiceReceipt)
                    .HasForeignKey(d => d.SellerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("invoice_receipt_ibfk_3");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.InvoiceReceipt)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("invoice_receipt_ibfk_4");
            });

            modelBuilder.Entity<InvoiceReceiptProduct>(entity =>
            {
                entity.ToTable("invoice_receipt_product");

                entity.HasIndex(e => e.InvoiceReceiptId)
                    .HasName("fk-invoice_receipt_product-invoice_receipt.id");

                entity.HasIndex(e => e.ProductVariantId)
                    .HasName("FK_invrep_product__productVariantId__product_variant__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.Discount)
                    .HasColumnName("discount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.InvoiceReceiptId)
                    .HasColumnName("invoiceReceiptId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.NetPrice)
                    .HasColumnName("netPrice")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.ProductVariantId)
                    .HasColumnName("productVariantId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasColumnType("int(5)");

                entity.Property(e => e.Remark)
                    .HasColumnName("remark")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Sku)
                    .HasColumnName("sku")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TotalPrice)
                    .HasColumnName("totalPrice")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.Property(e => e.Variant)
                    .HasColumnName("variant")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Vat)
                    .HasColumnName("vat")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.Image)
                    .HasColumnName("image")
                    .HasMaxLength(255);

                entity.HasOne(d => d.InvoiceReceipt)
                    .WithMany(p => p.InvoiceReceiptProduct)
                    .HasForeignKey(d => d.InvoiceReceiptId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("invoice_receipt_product_ibfk_1");

                entity.HasOne(d => d.ProductVariant)
                    .WithMany(p => p.InvoiceReceiptProduct)
                    .HasForeignKey(d => d.ProductVariantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_invrep_product__productVariantId__product_variant__id");
            });

            modelBuilder.Entity<Language>(entity =>
            {
                entity.ToTable("language");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(4)")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Lockeraccess>(entity =>
            {
                entity.HasKey(e => e.AccessId);

                entity.ToTable("lockeraccess");

                entity.Property(e => e.AccessId)
                    .HasColumnName("accessID")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.BoxId)
                    .HasColumnName("boxID")
                    .HasColumnType("int(3)");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("createdBy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnName("createdDate");

                entity.Property(e => e.ExpireDate).HasColumnName("expireDate");

                entity.Property(e => e.GuestTelnum)
                    .HasColumnName("guestTelnum")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LockerCode)
                    .HasColumnName("lockerCode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedBy)
                    .HasColumnName("updatedBy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate)
                    .HasColumnName("updatedDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.ValidDate).HasColumnName("validDate");
            });

            modelBuilder.Entity<Lockerinfo>(entity =>
            {
                entity.HasKey(e => e.LockerId);

                entity.ToTable("lockerinfo");

                entity.Property(e => e.LockerId)
                    .HasColumnName("lockerID")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("createdBy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnName("createdDate");

                entity.Property(e => e.LockerActive)
                    .HasColumnName("lockerActive")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.LockerAddress)
                    .HasColumnName("lockerAddress")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.LockerCode)
                    .HasColumnName("lockerCode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LockerLat)
                    .HasColumnName("lockerLat")
                    .HasColumnType("double(18,10)");

                entity.Property(e => e.LockerLon)
                    .HasColumnName("lockerLon")
                    .HasColumnType("double(18,10)");

                entity.Property(e => e.LockerPassword)
                    .HasColumnName("lockerPassword")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LockerTypeCode)
                    .HasColumnName("lockerTypeCode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PublicName)
                    .HasColumnName("publicName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RegistryCode)
                    .HasColumnName("registryCode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedBy)
                    .HasColumnName("updatedBy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate)
                    .HasColumnName("updatedDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.ZoneId)
                    .HasColumnName("zoneID")
                    .HasColumnType("int(4)");
            });

            modelBuilder.Entity<Lockertypeinfo>(entity =>
            {
                entity.HasKey(e => e.LockerTypeId);

                entity.ToTable("lockertypeinfo");

                entity.Property(e => e.LockerTypeId)
                    .HasColumnName("lockerTypeID")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("createdBy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnName("createdDate");

                entity.Property(e => e.LockerQtyBox)
                    .HasColumnName("lockerQtyBox")
                    .HasColumnType("int(11)");

                entity.Property(e => e.LockerTypeCode)
                    .HasColumnName("lockerTypeCode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LockerTypeName)
                    .HasColumnName("lockerTypeName")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedBy)
                    .HasColumnName("updatedBy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate)
                    .HasColumnName("updatedDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            modelBuilder.Entity<Lockerusermapping>(entity =>
            {
                entity.HasKey(e => e.MappingId);

                entity.ToTable("lockerusermapping");

                entity.Property(e => e.MappingId)
                    .HasColumnName("mappingID")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("createdBy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnName("createdDate");

                entity.Property(e => e.LockerCode)
                    .HasColumnName("lockerCode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LockerName)
                    .HasColumnName("lockerName")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedBy)
                    .HasColumnName("updatedBy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate)
                    .HasColumnName("updatedDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UserActive)
                    .HasColumnName("userActive")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.UserId)
                    .HasColumnName("userID")
                    .HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<Logdelivery>(entity =>
            {
                entity.HasKey(e => e.Transnum);

                entity.ToTable("logdelivery");

                entity.Property(e => e.Transnum)
                    .HasColumnName("transnum")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.AccessId)
                    .HasColumnName("accessID")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.BoxId)
                    .HasColumnName("boxID")
                    .HasColumnType("int(3)");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("createdBy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnName("createdDate");

                entity.Property(e => e.CustomerUserId)
                    .HasColumnName("customerUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.DeliveryId)
                    .HasColumnName("deliveryID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LockerCode)
                    .HasColumnName("lockerCode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OpenDate).HasColumnName("openDate");

                entity.Property(e => e.RefDelivery)
                    .HasColumnName("refDelivery")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedBy)
                    .HasColumnName("updatedBy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate)
                    .HasColumnName("updatedDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UserId)
                    .HasColumnName("userID")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.UserType)
                    .HasColumnName("userType")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LogisticCompany>(entity =>
            {
                entity.ToTable("logistic_company");

                entity.HasIndex(e => e.CityId)
                    .HasName("fk-company-city.id");

                entity.HasIndex(e => e.CountryId)
                    .HasName("FK__logistic_company__countryId__country__id");

                entity.HasIndex(e => e.ProvinceId)
                    .HasName("fk-company-province.id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("fk-company-status.id");

                entity.HasIndex(e => e.TownId)
                    .HasName("FK__company__townId__town__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Address1)
                    .IsRequired()
                    .HasColumnName("address1")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Address2)
                    .HasColumnName("address2")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Address3)
                    .HasColumnName("address3")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CityId)
                    .HasColumnName("cityId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CountryId)
                    .HasColumnName("countryId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Fax)
                    .HasColumnName("fax")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.IsLinkable)
                    .HasColumnName("isLinkable")
                    .HasColumnType("tinyint(1)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProvinceId)
                    .HasColumnName("provinceId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.TaxNumber)
                    .HasColumnName("taxNumber")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Telephone)
                    .HasColumnName("telephone")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TownId)
                    .HasColumnName("townId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.TrackingUrl)
                    .HasColumnName("trackingUrl")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("updateDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.ZipCode)
                    .HasColumnName("zipCode")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.City)
                    .WithMany(p => p.LogisticCompany)
                    .HasForeignKey(d => d.CityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("logistic_company_ibfk_1");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.LogisticCompany)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__logistic_company__countryId__country__id");

                entity.HasOne(d => d.Province)
                    .WithMany(p => p.LogisticCompany)
                    .HasForeignKey(d => d.ProvinceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("logistic_company_ibfk_2");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.LogisticCompany)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("logistic_company_ibfk_3");

                entity.HasOne(d => d.Town)
                    .WithMany(p => p.LogisticCompany)
                    .HasForeignKey(d => d.TownId)
                    .HasConstraintName("FK__company__townId__town__id");
            });

            modelBuilder.Entity<LogisticUser>(entity =>
            {
                entity.ToTable("logistic_user");

                entity.HasIndex(e => e.LogisticCompanyId)
                    .HasName("fk-logistic_user-company.id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("fk-logistic_user-status.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(5)");

                entity.Property(e => e.CitizenId)
                    .HasColumnName("citizenId")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasColumnName("firstName")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasColumnName("lastName")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LogisticCompanyId)
                    .HasColumnName("logisticCompanyId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.Telephone)
                    .IsRequired()
                    .HasColumnName("telephone")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnName("username")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.LogisticCompany)
                    .WithMany(p => p.LogisticUser)
                    .HasForeignKey(d => d.LogisticCompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("logistic_user_ibfk_1");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.LogisticUser)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("logistic_user_ibfk_2");
            });

            modelBuilder.Entity<Logopenlocker>(entity =>
            {
                entity.HasKey(e => e.OpenId);

                entity.ToTable("logopenlocker");

                entity.Property(e => e.OpenId)
                    .HasColumnName("openID")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Batterylevel)
                    .HasColumnName("batterylevel")
                    .HasColumnType("int(10)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.BoxId)
                    .HasColumnName("boxID")
                    .HasColumnType("int(3)");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("createdBy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnName("createdDate");

                entity.Property(e => e.LockerCode)
                    .HasColumnName("lockerCode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OpenDate).HasColumnName("openDate");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TransnumDelivery)
                    .HasColumnName("transnumDelivery")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.UpdatedBy)
                    .HasColumnName("updatedBy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate)
                    .HasColumnName("updatedDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UserId)
                    .HasColumnName("userID")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.UserType)
                    .HasColumnName("userType")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MarketplaceCoupon>(entity =>
            {
                entity.ToTable("marketplace_coupon");

                entity.HasIndex(e => e.CustomerUserId)
                    .HasName("fk-marketplace_coupon_redeem-customer_user.id");

                entity.HasIndex(e => e.MarketplaceCouponGroupId)
                    .HasName("fk-marketplace_coupon_redeem-marketplace_coupon.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.CustomerUserId)
                    .HasColumnName("customerUserId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IsRedeemed)
                    .HasColumnName("isRedeemed")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("b'0'");

                entity.Property(e => e.MarketplaceCouponGroupId)
                    .HasColumnName("marketplaceCouponGroupId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.RedeemCode)
                    .IsRequired()
                    .HasColumnName("redeemCode")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.CustomerUser)
                    .WithMany(p => p.MarketplaceCoupon)
                    .HasForeignKey(d => d.CustomerUserId)
                    .HasConstraintName("marketplace_coupon_ibfk_1");

                entity.HasOne(d => d.MarketplaceCouponGroup)
                    .WithMany(p => p.MarketplaceCoupon)
                    .HasForeignKey(d => d.MarketplaceCouponGroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("marketplace_coupon_ibfk_2");
            });

            modelBuilder.Entity<MarketplaceCouponGroup>(entity =>
            {
                entity.ToTable("marketplace_coupon_group");

                entity.HasIndex(e => e.CouponPriceId)
                    .HasName("FK__mk_coupon_group__couponPriceId__coupon_price__id");

                entity.HasIndex(e => e.MarketplaceUserId)
                    .HasName("fk-marketplace_coupon-marketplace_user.id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("fk-marketplace_coupon-status.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CouponPriceId)
                    .HasColumnName("couponPriceId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.EndDate).HasColumnName("endDate");

                entity.Property(e => e.Image)
                    .IsRequired()
                    .HasColumnName("image")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.IsUniversal)
                    .HasColumnName("isUniversal")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("b'0'");

                entity.Property(e => e.MarketplaceUserId)
                    .HasColumnName("marketplaceUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasColumnType("int(5)");

                entity.Property(e => e.Remark)
                    .IsRequired()
                    .HasColumnName("remark")
                    .IsUnicode(false);

                entity.Property(e => e.StartDate).HasColumnName("startDate");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.CouponPrice)
                    .WithMany(p => p.MarketplaceCouponGroup)
                    .HasForeignKey(d => d.CouponPriceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__mk_coupon_group__couponPriceId__coupon_price__id");

                entity.HasOne(d => d.MarketplaceUser)
                    .WithMany(p => p.MarketplaceCouponGroup)
                    .HasForeignKey(d => d.MarketplaceUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("marketplace_coupon_group_ibfk_1");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.MarketplaceCouponGroup)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("marketplace_coupon_group_ibfk_3");
            });

            modelBuilder.Entity<MarketplacePromotion>(entity =>
            {
                entity.ToTable("marketplace_promotion");

                entity.HasIndex(e => e.CategoryMarketplaceId)
                    .HasName("fk-marketplace_promotion-category_marketplace.id");

                entity.HasIndex(e => e.MarketplaceUserId)
                    .HasName("fk-marketplace_promotion-marketplace_user.id");

                entity.HasIndex(e => e.PricePromotionId)
                    .HasName("fk-marketplace_promotion-price_promotion.id");

                entity.HasIndex(e => e.ProductId)
                    .HasName("fk-marketplace_promotion-product.id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("fk-marketplace_promotion-status.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CategoryMarketplaceId)
                    .HasColumnName("categoryMarketplaceId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.EndDate).HasColumnName("endDate");

                entity.Property(e => e.MarketplaceUserId)
                    .HasColumnName("marketplaceUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.MinQuantity)
                    .HasColumnName("minQuantity")
                    .HasColumnType("int(3)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PricePromotionId)
                    .HasColumnName("pricePromotionId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("productId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasColumnType("int(5)");

                entity.Property(e => e.StartDate).HasColumnName("startDate");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.CategoryMarketplace)
                    .WithMany(p => p.MarketplacePromotion)
                    .HasForeignKey(d => d.CategoryMarketplaceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("marketplace_promotion_ibfk_1");

                entity.HasOne(d => d.MarketplaceUser)
                    .WithMany(p => p.MarketplacePromotion)
                    .HasForeignKey(d => d.MarketplaceUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("marketplace_promotion_ibfk_2");

                entity.HasOne(d => d.PricePromotion)
                    .WithMany(p => p.MarketplacePromotion)
                    .HasForeignKey(d => d.PricePromotionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("marketplace_promotion_ibfk_3");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.MarketplacePromotion)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("marketplace_promotion_ibfk_4");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.MarketplacePromotion)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("marketplace_promotion_ibfk_5");
            });

            modelBuilder.Entity<MarketplacePromotionRedeem>(entity =>
            {
                entity.ToTable("marketplace_promotion_redeem");

                entity.HasIndex(e => e.MarketplacePromotionId)
                    .HasName("fk-marketplace_promotion_redeem-marketplace_promotion.id");

                entity.HasIndex(e => e.SaleOrderId)
                    .HasName("fk-marketplace_promotion_redeem-sale_order.id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("fk-marketplace_promotion_redeem-status.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.MarketplacePromotionId)
                    .HasColumnName("marketplacePromotionId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.SaleOrderId)
                    .HasColumnName("saleOrderId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.MarketplacePromotion)
                    .WithMany(p => p.MarketplacePromotionRedeem)
                    .HasForeignKey(d => d.MarketplacePromotionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("marketplace_promotion_redeem_ibfk_1");

                entity.HasOne(d => d.SaleOrder)
                    .WithMany(p => p.MarketplacePromotionRedeem)
                    .HasForeignKey(d => d.SaleOrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("marketplace_promotion_redeem_ibfk_2");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.MarketplacePromotionRedeem)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("marketplace_promotion_redeem_ibfk_3");
            });

            modelBuilder.Entity<MarketplaceReviewSeller>(entity =>
            {
                entity.ToTable("marketplace_review_seller");

                entity.HasIndex(e => e.MarketplaceUserId)
                    .HasName("fk-marketplace_review_seller-marketplace_user.id");

                entity.HasIndex(e => e.SellerId)
                    .HasName("fk-marketplace_review_seller-seller.id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("fk-marketplace_review_seller-status.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Comment)
                    .HasColumnName("comment")
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.MarketplaceUserId)
                    .HasColumnName("marketplaceUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Rating)
                    .HasColumnName("rating")
                    .HasColumnType("int(3)");

                entity.Property(e => e.SellerId)
                    .HasColumnName("sellerId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.MarketplaceUser)
                    .WithMany(p => p.MarketplaceReviewSeller)
                    .HasForeignKey(d => d.MarketplaceUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("marketplace_review_seller_ibfk_1");

                entity.HasOne(d => d.Seller)
                    .WithMany(p => p.MarketplaceReviewSeller)
                    .HasForeignKey(d => d.SellerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("marketplace_review_seller_ibfk_2");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.MarketplaceReviewSeller)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("marketplace_review_seller_ibfk_3");
            });

            modelBuilder.Entity<MarketplaceUser>(entity =>
            {
                entity.ToTable("marketplace_user");

                entity.HasIndex(e => e.CustomerUserId)
                    .HasName("FK_marketplace_user__custId__customer_user__id");

                entity.HasIndex(e => e.RoleClassId)
                    .HasName("FK_marketplace_user__roleClassId__role_class__id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("fk-marketplace_user-status.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.CustomerUserId)
                    .HasColumnName("customerUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .IsRequired();

                entity.Property(e => e.RoleClassId)
                    .HasColumnName("roleClassId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("updateDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .IsRequired();

                entity.HasOne(d => d.CustomerUser)
                    .WithMany(p => p.MarketplaceUser)
                    .HasForeignKey(d => d.CustomerUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_marketplace_user__custId__customer_user__id");

                entity.HasOne(d => d.RoleClass)
                    .WithMany(p => p.MarketplaceUser)
                    .HasForeignKey(d => d.RoleClassId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_marketplace_user__roleClassId__role_class__id");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.MarketplaceUser)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("marketplace_user_ibfk_2");
            });

            modelBuilder.Entity<MediaType>(entity =>
            {
                entity.ToTable("media_type");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<NotificationCustomer>(entity =>
            {
                entity.ToTable("notification_customer");

                entity.HasIndex(e => e.CustomerUserId)
                    .HasName("fk-notification_customer-customer_user.id");

                entity.HasIndex(e => e.NotificationManualId)
                    .HasName("FK__noti_customer__notiManId__noti_man__id");

                entity.HasIndex(e => e.NotificationTemplateId)
                    .HasName("FK__noti_customer__notiTempId__noti_temp__id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("fk-notification_customer-status.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.CustomerUserId)
                    .HasColumnName("customerUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.LinkParam)
                    .HasColumnName("linkParam")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.NotificationManualId)
                    .HasColumnName("notificationManualId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.NotificationTemplateId)
                    .HasColumnName("notificationTemplateId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Param)
                    .HasColumnName("param")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.CustomerUser)
                    .WithMany(p => p.NotificationCustomer)
                    .HasForeignKey(d => d.CustomerUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("notification_customer_ibfk_1");

                entity.HasOne(d => d.NotificationManual)
                    .WithMany(p => p.NotificationCustomer)
                    .HasForeignKey(d => d.NotificationManualId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__noti_customer__notiManId__noti_man__id");

                entity.HasOne(d => d.NotificationTemplate)
                    .WithMany(p => p.NotificationCustomer)
                    .HasForeignKey(d => d.NotificationTemplateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__noti_customer__notiTempId__noti_temp__id");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.NotificationCustomer)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("notification_customer_ibfk_2");
            });

            modelBuilder.Entity<NotificationManual>(entity =>
            {
                entity.ToTable("notification_manual");

                entity.HasIndex(e => e.NotificationTypeId)
                    .HasName("FK__noti_man__notiTypeId__noti_type__id");

                entity.HasIndex(e => e.AppDeepLinkId)
                      .HasName("fk-notification_manual-app_deep_link.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AppDeepLinkId)
                    .HasColumnName("appDeepLinkId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Method)
                    .IsRequired()
                    .HasColumnName("method")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.NotificationTypeId)
                    .HasColumnName("notificationTypeId")
                    .HasColumnType("int(4)");

                entity.HasOne(d => d.NotificationType)
                    .WithMany(p => p.NotificationManual)
                    .HasForeignKey(d => d.NotificationTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__noti_man__notiTypeId__noti_type__id");

                entity.HasOne(d => d.AppDeepLink)
                    .WithMany(p => p.NotificationManual)
                    .HasForeignKey(d => d.AppDeepLinkId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk-notification_manual-app_deep_link.id");
            });

            modelBuilder.Entity<NotificationManualLanguage>(entity =>
            {
                entity.ToTable("notification_manual_language");

                entity.HasIndex(e => e.LanguageId)
                    .HasName("FK__noti_man_lan__languageId__language__id");

                entity.HasIndex(e => e.NotificationManualId)
                    .HasName("FK__noti_man_lan__notiManId__noti_man__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.LanguageId)
                    .HasColumnName("languageId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.Message)
                    .IsRequired()
                    .HasColumnName("message")
                    .IsUnicode(false);

                entity.Property(e => e.NotificationManualId)
                    .HasColumnName("notificationManualId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Topic)
                    .IsRequired()
                    .HasColumnName("topic")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Language)
                    .WithMany(p => p.NotificationManualLanguage)
                    .HasForeignKey(d => d.LanguageId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__noti_man_lan__languageId__language__id");

                entity.HasOne(d => d.NotificationManual)
                    .WithMany(p => p.NotificationManualLanguage)
                    .HasForeignKey(d => d.NotificationManualId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__noti_man_lan__notiManId__noti_man__id");
            });

            modelBuilder.Entity<NotificationSeller>(entity =>
            {
                entity.ToTable("notification_seller");

                entity.HasIndex(e => e.NotificationManualId)
                    .HasName("FK__noti_seller__notiManId__noti_man__id");

                entity.HasIndex(e => e.NotificationTemplateId)
                    .HasName("FK__noti_seller__notiTempId__noti_temp__id");

                entity.HasIndex(e => e.SellerId)
                    .HasName("fk-notification_seller-seller.id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("fk-notification_seller-status.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.NotificationManualId)
                    .HasColumnName("notificationManualId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.NotificationTemplateId)
                    .HasColumnName("notificationTemplateId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.SellerId)
                    .HasColumnName("sellerId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.Property(e => e.Param)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LinkParam)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.NotificationManual)
                    .WithMany(p => p.NotificationSeller)
                    .HasForeignKey(d => d.NotificationManualId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__noti_seller__notiManId__noti_man__id");

                entity.HasOne(d => d.NotificationTemplate)
                    .WithMany(p => p.NotificationSeller)
                    .HasForeignKey(d => d.NotificationTemplateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__noti_seller__notiTempId__noti_temp__id");

                entity.HasOne(d => d.Seller)
                    .WithMany(p => p.NotificationSeller)
                    .HasForeignKey(d => d.SellerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("notification_seller_ibfk_2");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.NotificationSeller)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("notification_seller_ibfk_3");
            });

            modelBuilder.Entity<NotificationTemplate>(entity =>
            {
                entity.ToTable("notification_template");

                entity.HasIndex(e => e.NotificationTypeId)
                    .HasName("FK__noti_temp__notiTypeId__noti_type__id");
                entity.HasIndex(e => e.AppDeepLinkId)
                        .HasName("fk-notification_template-app_deep_link.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(5)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AppDeepLinkId)
                    .HasColumnName("appDeepLinkId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Method)
                    .IsRequired()
                    .HasColumnName("method")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.NotificationTypeId)
                    .HasColumnName("notificationTypeId")
                    .HasColumnType("int(4)");

                entity.HasOne(d => d.NotificationType)
                    .WithMany(p => p.NotificationTemplate)
                    .HasForeignKey(d => d.NotificationTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__noti_temp__notiTypeId__noti_type__id");

                entity.HasOne(d => d.AppDeepLink)
                    .WithMany(p => p.NotificationTemplate)
                    .HasForeignKey(d => d.AppDeepLinkId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk-notification_template-app_deep_link.id");
            });

            modelBuilder.Entity<NotificationTemplateLanguage>(entity =>
            {
                entity.ToTable("notification_template_language");

                entity.HasIndex(e => e.LanguageId)
                    .HasName("FK__noti_temp_lang__languageId__language__id");

                entity.HasIndex(e => e.NotificationTemplateId)
                    .HasName("FK__noti_temp_lang__notiTempId__noti_temp__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.LanguageId)
                    .HasColumnName("languageId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.Message)
                    .IsRequired()
                    .HasColumnName("message")
                    .IsUnicode(false);

                entity.Property(e => e.NotificationTemplateId)
                    .HasColumnName("notificationTemplateId")
                    .HasColumnType("int(5)");

                entity.Property(e => e.Topic)
                    .IsRequired()
                    .HasColumnName("topic")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Language)
                    .WithMany(p => p.NotificationTemplateLanguage)
                    .HasForeignKey(d => d.LanguageId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__noti_temp_lang__languageId__language__id");

                entity.HasOne(d => d.NotificationTemplate)
                    .WithMany(p => p.NotificationTemplateLanguage)
                    .HasForeignKey(d => d.NotificationTemplateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__noti_temp_lang__notiTempId__noti_temp__id");
            });

            modelBuilder.Entity<NotificationType>(entity =>
            {
                entity.ToTable("notification_type");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(4)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Nvjmigrationdatahistory>(entity =>
            {
                entity.ToTable("__nvjmigrationdatahistory");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .IsUnicode(false);

                entity.Property(e => e.RunDate)
                    .HasColumnName("runDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            modelBuilder.Entity<Nvjmigrationhistory>(entity =>
            {
                entity.ToTable("__nvjmigrationhistory");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .IsUnicode(false);

                entity.Property(e => e.RunDate)
                    .HasColumnName("runDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            modelBuilder.Entity<Payment>(entity =>
            {
                entity.ToTable("payment");

                entity.HasIndex(e => e.CreditCardId)
                    .HasName("creditCardId");

                entity.HasIndex(e => e.GatewayProviderId)
                    .HasName("gatewayProviderId");

                entity.HasIndex(e => e.PaymentTypeId)
                    .HasName("fk-payment-payment_type.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.BankName)
                    .HasColumnName("bankName")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.CreditCardId)
                    .HasColumnName("creditCardId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.GatewayProviderId)
                    .HasColumnName("gatewayProviderId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.PaymentTypeId)
                    .HasColumnName("paymentTypeId")
                    .HasColumnType("int(3)");

                entity.Property(e => e.ReferanceNumber)
                    .HasColumnName("referanceNumber")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.CreditCard)
                    .WithMany(p => p.Payment)
                    .HasForeignKey(d => d.CreditCardId)
                    .HasConstraintName("payment_ibfk_2");

                entity.HasOne(d => d.GatewayProvider)
                    .WithMany(p => p.Payment)
                    .HasForeignKey(d => d.GatewayProviderId)
                    .HasConstraintName("payment_ibfk_3");

                entity.HasOne(d => d.PaymentType)
                    .WithMany(p => p.Payment)
                    .HasForeignKey(d => d.PaymentTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("payment_ibfk_1");
            });

            modelBuilder.Entity<PaymentType>(entity =>
            {
                entity.ToTable("payment_type");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(4)");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Point>(entity =>
            {
                entity.ToTable("point");

                entity.HasIndex(e => e.CustomerUserId)
                    .HasName("fk-point-customer_user.id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("fk-point-status.id");
                
                entity.HasIndex(e => e.SaleOrderId)
                    .HasName("FK__point_saleOrderId__saleOrder_id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("updateDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.ExpireDate)
                    .HasColumnName("expireDate")
                    .IsRequired(false);

                entity.Property(e => e.CustomerUserId)
                    .HasColumnName("customerUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e._Point)
                    .HasColumnName("point")
                    .HasColumnType("int(5)");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.SaleOrderId)
                    .HasColumnName("saleOrderId")
                    .HasColumnType("int(10)")
                    .IsRequired(false);

                entity.HasOne(d => d.CustomerUser)
                    .WithMany(p => p.PointNavigation)
                    .HasForeignKey(d => d.CustomerUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("point_ibfk_1");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Point)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("point_ibfk_4");

                entity.HasOne(d => d.SaleOrder)
                    .WithMany(p => p.Point)
                    .HasForeignKey(d => d.SaleOrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__point_saleOrderId__saleOrder_id");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.ToTable("product");

                entity.HasIndex(e => e.BrandId)
                    .HasName("FK__product__brandId__brand__id");

                entity.HasIndex(e => e.CategoryMarketPlaceId)
                    .HasName("fk-product-category_marketplace.id");

                entity.HasIndex(e => e.CategorySellerId)
                    .HasName("fk-product-category_seller.id");

                entity.HasIndex(e => e.EventId)
                    .HasName("eventId");

                entity.HasIndex(e => e.ProductTypeId)
                    .HasName("fk__product__productTypeId__product_type__id");

                entity.HasIndex(e => e.Id)
                    .HasName("pk-product.id")
                    .IsUnique();

                entity.HasIndex(e => e.SellerAddressId)
                    .HasName("fk-product-seller_address.id");

                entity.HasIndex(e => e.SellerId)
                    .HasName("fk-product-seller.id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("fk-product-status.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.BrandId)
                    .HasColumnName("brandId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CategoryMarketPlaceId)
                    .HasColumnName("categoryMarketPlaceId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CategorySellerId)
                    .HasColumnName("categorySellerId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .IsUnicode(false);

                entity.Property(e => e.EventId)
                    .HasColumnName("eventId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Image)
                    .HasColumnName("image")
                    .IsUnicode(false);

                entity.Property(e => e.IsFood)
                    .HasColumnName("isFood")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("b'0'");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PurchaseLimit)
                    .HasColumnName("purchaseLimit")
                    .HasColumnType("int(4)");

                entity.Property(e => e.Rating)
                    .HasColumnName("rating")
                    .HasColumnType("int(3)");

                entity.Property(e => e.SellerAddressId)
                    .HasColumnName("sellerAddressId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.SellerId)
                    .HasColumnName("sellerId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Size)
                    .HasColumnName("size")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Sku)
                    .HasColumnName("sku")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Sold)
                    .HasColumnName("sold")
                    .HasColumnType("int(6)");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.Property(e => e.Wishlist)
                    .HasColumnName("wishlist")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Url)
                    .HasColumnName("url")
                    .HasColumnType("text");

                entity.Property(e => e.ProductTypeId)
                    .HasColumnName("productTypeId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.ExpireDate)
                    .HasColumnName("expireDate");

                entity.Property(e => e.ExpireDurationInDay)
                    .HasColumnName("expireDurationInDay")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.Brand)
                    .WithMany(p => p.Product)
                    .HasForeignKey(d => d.BrandId)
                    .HasConstraintName("FK__product__brandId__brand__id");

                entity.HasOne(d => d.CategoryMarketPlace)
                    .WithMany(p => p.Product)
                    .HasForeignKey(d => d.CategoryMarketPlaceId)
                    .HasConstraintName("fk-product-category_marketplace.id");

                entity.HasOne(d => d.CategorySeller)
                    .WithMany(p => p.Product)
                    .HasForeignKey(d => d.CategorySellerId)
                    .HasConstraintName("fk-product-category_seller.id");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.Product)
                    .HasForeignKey(d => d.EventId)
                    .HasConstraintName("product_ibfk_1");

                entity.HasOne(d => d.ProductType)
                    .WithMany(p => p.Product)
                    .HasForeignKey(d => d.ProductTypeId)
                    .HasConstraintName("fk__product__productTypeId__product_type__id");

                entity.HasOne(d => d.SellerAddress)
                    .WithMany(p => p.Product)
                    .HasForeignKey(d => d.SellerAddressId)
                    .HasConstraintName("fk-product-seller_address.id");

                entity.HasOne(d => d.Seller)
                    .WithMany(p => p.Product)
                    .HasForeignKey(d => d.SellerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk-product-seller.id");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Product)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk-product-status.id");
            });

            modelBuilder.Entity<ProductImage>(entity =>
            {
                entity.ToTable("product_image");

                entity.HasIndex(e => e.ProductId)
                    .HasName("FK_product_image__productId__product__id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("FK_product_image__statusId__status__id");

                entity.HasIndex(e => e.MediaTypeId)
                    .HasName("FK__product_image__mediaTypeId__media_type__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .IsUnicode(false);

                entity.Property(e => e.IsCover)
                    .HasColumnName("isCover")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("b'0'");

                entity.Property(e => e.ProductId)
                    .HasColumnName("productId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Sequence)
                    .HasColumnName("sequence")
                    .HasColumnType("int(4)")
                    .HasDefaultValueSql("9999");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.Property(e => e.Url)
                    .IsRequired()
                    .HasColumnName("url")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MediaTypeId)
                    .HasColumnName("mediaTypeId")
                    .HasColumnType("int(4)")
                    .HasDefaultValue(1);

                entity.Property(e => e.IsIcon)
                    .HasColumnName("isIcon")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("b'0'");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductImage)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_product_image__productId__product__id");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ProductImage)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_product_image__statusId__status__id");

                entity.HasOne(d => d.MediaType)
                    .WithMany(p => p.ProductImage)
                    .HasForeignKey(d => d.MediaTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_product_image__mediaTypeId__media_type__id");
            });

            modelBuilder.Entity<ProductRating>(entity =>
            {
                entity.ToTable("product_rating");

                entity.HasIndex(e => e.CustomerUserId)
                    .HasName("fk-product_rating-customer_user.id");

                entity.HasIndex(e => e.ProductId)
                    .HasName("fk-product_rating-product.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.CustomerUserId)
                    .HasColumnName("customerUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("productId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Rating)
                    .HasColumnName("rating")
                    .HasColumnType("int(5)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.CustomerUser)
                    .WithMany(p => p.ProductRating)
                    .HasForeignKey(d => d.CustomerUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("product_rating_ibfk_1");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductRating)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("product_rating_ibfk_2");
            });

            modelBuilder.Entity<ProductSold>(entity =>
            {
                entity.HasKey(e => new { e.CustomerUserId, e.ProductId });

                entity.ToTable("product_sold");

                entity.HasIndex(e => e.ProductId)
                    .HasName("FK__product_sold__productId__product__id");

                entity.Property(e => e.CustomerUserId)
                    .HasColumnName("customerUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("productId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.Order)
                    .HasColumnName("order")
                    .HasColumnType("int(6)");

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasColumnType("int(10)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.CustomerUser)
                    .WithMany(p => p.ProductSold)
                    .HasForeignKey(d => d.CustomerUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__product_sold__customerUserId__customer_user__id");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductSold)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__product_sold__productId__product__id");
            });

            modelBuilder.Entity<ProductType>(entity =>
            {
                entity.ToTable("product_type");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(4)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .IsRequired(false);
            });

            modelBuilder.Entity<ProductVariant>(entity =>
            {
                entity.ToTable("product_variant");

                entity.HasIndex(e => e.ProductId)
                    .HasName("fk-product_variant-product.id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("fk-product_variant-status.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Barcode)
                    .HasColumnName("barcode")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.DiscountPercentage)
                    .HasColumnName("discountPercentage")
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.Image)
                    .HasColumnName("image")
                    .IsUnicode(false);

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("productId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasColumnType("int(5)");

                entity.Property(e => e.SalePrice)
                    .HasColumnName("salePrice")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Sku)
                    .HasColumnName("sku")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                //entity.Property(e => e.VariantIds)
                //    .HasColumnName("variantIds")
                //    .HasMaxLength(255)
                //    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductVariant)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk-product_variant-product.id");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ProductVariant)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk-product_variant-status.id");
            });

            modelBuilder.Entity<ProductWishlist>(entity =>
            {
                entity.HasKey(e => new { e.CustomerUserId, e.ProductId });

                entity.ToTable("product_wishlist");

                entity.HasIndex(e => e.ProductId)
                    .HasName("FK__product_wishlist__productId__product__id");

                entity.Property(e => e.CustomerUserId)
                    .HasColumnName("customerUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("productId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.CustomerUser)
                    .WithMany(p => p.ProductWishlist)
                    .HasForeignKey(d => d.CustomerUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__product_wishlist__customerUserId__cust_user__id");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductWishlist)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__product_wishlist__productId__product__id");
            });

            modelBuilder.Entity<PromotionPrice>(entity =>
            {
                entity.ToTable("promotion_price");

                entity.HasIndex(e => e.ProductId)
                    .HasName("fk-price_promotion-product.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.DiscountLimit)
                    .HasColumnName("discountLimit")
                    .HasColumnType("int(5)");

                entity.Property(e => e.DiscountPercent)
                    .HasColumnName("discountPercent")
                    .HasColumnType("int(3)");

                entity.Property(e => e.DiscountTotal)
                    .HasColumnName("discountTotal")
                    .HasColumnType("int(5)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("productId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.PromotionPrice)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("promotion_price_ibfk_1");
            });

            modelBuilder.Entity<Province>(entity =>
            {
                entity.ToTable("province");

                entity.HasIndex(e => e.CountryId)
                    .HasName("FK__province__countryId__country__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CountryId)
                    .HasColumnName("countryId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.Province)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__province__countryId__country__id");
            });

            modelBuilder.Entity<ProvinceLanguage>(entity =>
            {
                entity.HasKey(e => new { e.ProvinceId, e.LanguageId });

                entity.ToTable("province_language");

                entity.HasIndex(e => e.LanguageId)
                    .HasName("FK__province_language__languageId__language__id");

                entity.Property(e => e.ProvinceId)
                    .HasColumnName("provinceId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.LanguageId)
                    .HasColumnName("languageId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Language)
                    .WithMany(p => p.ProvinceLanguage)
                    .HasForeignKey(d => d.LanguageId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__province_language__languageId__language__id");

                entity.HasOne(d => d.Province)
                    .WithMany(p => p.ProvinceLanguage)
                    .HasForeignKey(d => d.ProvinceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__province_language__provinceId__province__id");
            });

            modelBuilder.Entity<PurchaseOrder>(entity =>
            {
                entity.ToTable("purchase_order");

                entity.HasIndex(e => e.LogisticCompanyId)
                    .HasName("FK_purchaseOrderId__logisticCompanyId__logistic_company__id");

                entity.HasIndex(e => e.SaleOrderId)
                    .HasName("FK__purchase_order__saleOrderId__sale_order__id");

                entity.HasIndex(e => e.SellerId)
                    .HasName("fk-purchase_order-seller.id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("fk-purchase_order-status.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Address)
                    .HasColumnName("address")
                    .IsUnicode(false);

                entity.Property(e => e.BillingAddress)
                    .HasColumnName("billingAddress")
                    .IsUnicode(false);

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.Discount)
                    .HasColumnName("discount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LockerCode)
                    .HasColumnName("lockerCode")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LogisticCompanyId)
                    .HasColumnName("logisticCompanyId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.LogisticCompanyName)
                    .HasColumnName("logisticCompanyName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MarketplaceAddress)
                    .IsRequired()
                    .HasColumnName("marketplaceAddress")
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NetPrice)
                    .HasColumnName("netPrice")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.ParcelQuantity)
                    .HasColumnName("parcelQuantity")
                    .HasColumnType("int(6)");

                entity.Property(e => e.Remark)
                    .HasColumnName("remark")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SaleOrderId)
                    .HasColumnName("saleOrderId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.SellerAddress)
                    .IsRequired()
                    .HasColumnName("sellerAddress")
                    .IsUnicode(false);

                entity.Property(e => e.SellerId)
                    .HasColumnName("sellerId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.SellerName)
                    .IsRequired()
                    .HasColumnName("sellerName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SellerTaxNumber)
                    .HasColumnName("sellerTaxNumber")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ShippingAddress)
                    .HasColumnName("shippingAddress")
                    .IsUnicode(false);

                entity.Property(e => e.StaticCode)
                    .IsRequired()
                    .HasColumnName("staticCode")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.Telephone)
                    .IsRequired()
                    .HasColumnName("telephone")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TotalPrice)
                    .HasColumnName("totalPrice")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.TrackingNumber)
                    .HasColumnName("trackingNumber")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.Property(e => e.Vat)
                    .HasColumnName("vat")
                    .HasColumnType("decimal(8,2)");

                entity.HasOne(d => d.LogisticCompany)
                    .WithMany(p => p.PurchaseOrder)
                    .HasForeignKey(d => d.LogisticCompanyId)
                    .HasConstraintName("FK_purchaseOrderId__logisticCompanyId__logistic_company__id");

                entity.HasOne(d => d.SaleOrder)
                    .WithMany(p => p.PurchaseOrder)
                    .HasForeignKey(d => d.SaleOrderId)
                    .HasConstraintName("FK__purchase_order__saleOrderId__sale_order__id");

                entity.HasOne(d => d.Seller)
                    .WithMany(p => p.PurchaseOrder)
                    .HasForeignKey(d => d.SellerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("purchase_order_ibfk_1");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.PurchaseOrder)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("purchase_order_ibfk_2");
            });

            modelBuilder.Entity<PurchaseOrderDelivered>(entity =>
            {
                entity.HasKey(e => new { e.PurchaseOrderId, e.LockerCode, e.BoxId });

                entity.ToTable("purchase_order_delivered");

                entity.HasIndex(e => e.PurchaseOrderId)
                    .HasName("FK__po_delivered__poId__purchase_order__id");

                entity.Property(e => e.PurchaseOrderId)
                    .HasColumnName("purchaseOrderId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.LockerCode)
                    .HasColumnName("lockerCode")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.BoxId)
                    .HasColumnName("boxId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.HasOne(d => d.PurchaseOrder)
                    .WithMany(p => p.PurchaseOrderDelivered)
                    .HasForeignKey(d => d.PurchaseOrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__po_delivered__poId__purchase_order__id");
            });

            modelBuilder.Entity<PurchaseOrderLog>(entity =>
            {
                entity.ToTable("purchase_order_log");

                entity.HasIndex(e => e.LogisticUserId)
                    .HasName("logisticUserId");

                entity.HasIndex(e => e.PurchaseOrderId)
                    .HasName("FK__po_log__poId__purchase_order__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.LogisticUserId)
                    .HasColumnName("logisticUserId")
                    .HasColumnType("int(5)");

                entity.Property(e => e.PurchaseOrderId)
                    .HasColumnName("purchaseOrderId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.HasOne(d => d.LogisticUser)
                    .WithMany(p => p.PurchaseOrderLog)
                    .HasForeignKey(d => d.LogisticUserId)
                    .HasConstraintName("purchase_order_log_ibfk_1");

                entity.HasOne(d => d.PurchaseOrder)
                    .WithMany(p => p.PurchaseOrderLog)
                    .HasForeignKey(d => d.PurchaseOrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__po_log__poId__purchase_order__id");
            });

            modelBuilder.Entity<PurchaseOrderLogisticUser>(entity =>
            {
                entity.HasKey(e => e.PurchaseOrderId);

                entity.ToTable("purchase_order_logistic_user");

                entity.HasIndex(e => e.LogisticUserId)
                    .HasName("FK_PO_logistic_user__logisticUserId__logistic_user__id");

                entity.Property(e => e.PurchaseOrderId)
                    .HasColumnName("purchaseOrderId")
                    .HasColumnType("int(10)")
                    .ValueGeneratedNever();

                entity.Property(e => e.LogisticUserId)
                    .HasColumnName("logisticUserId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.LogisticUser)
                    .WithMany(p => p.PurchaseOrderLogisticUser)
                    .HasForeignKey(d => d.LogisticUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PO_logistic_user__logisticUserId__logistic_user__id");

                entity.HasOne(d => d.PurchaseOrder)
                    .WithOne(p => p.PurchaseOrderLogisticUser)
                    .HasForeignKey<PurchaseOrderLogisticUser>(d => d.PurchaseOrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PO_logistic_user__purchaseOrderId__purchase_order__id");
            });

            modelBuilder.Entity<PurchaseOrderParcel>(entity =>
            {
                entity.ToTable("purchase_order_parcel");

                entity.HasIndex(e => e.PurchaseOrderId)
                    .HasName("FK__po_parcel__poId__purchase_oreder__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.BoxId)
                    .HasColumnName("boxId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.LockerCode)
                    .IsRequired()
                    .HasColumnName("lockerCode")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PurchaseOrderId)
                    .HasColumnName("purchaseOrderId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasColumnType("int(6)");

                entity.HasOne(d => d.PurchaseOrder)
                    .WithMany(p => p.PurchaseOrderParcel)
                    .HasForeignKey(d => d.PurchaseOrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__po_parcel__poId__purchase_oreder__id");
            });

            modelBuilder.Entity<PurchaseOrderProduct>(entity =>
            {
                entity.ToTable("purchase_order_product");

                entity.HasIndex(e => e.ProductVariantId)
                    .HasName("FK__po_product__productVariantId__product_variant__id");

                entity.HasIndex(e => e.PurchaseOrderId)
                    .HasName("fk-purchase_oreder_product-purchase_order.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.Discount)
                    .HasColumnName("discount")
                    .HasColumnType("decimal(10,2)")
                    .IsRequired(true);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.NetPrice)
                    .HasColumnName("netPrice")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.ProductVariantId)
                    .HasColumnName("productVariantId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.PurchaseOrderId)
                    .HasColumnName("purchaseOrderId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasColumnType("int(5)");

                entity.Property(e => e.Remark)
                    .HasColumnName("remark")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Sku)
                    .HasColumnName("sku")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TotalPrice)
                    .HasColumnName("totalPrice")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.Property(e => e.Variant)
                    .HasColumnName("variant")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Vat)
                    .HasColumnName("vat")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.Image)
                    .HasColumnName("image")
                    .HasMaxLength(255);

                entity.HasOne(d => d.ProductVariant)
                    .WithMany(p => p.PurchaseOrderProduct)
                    .HasForeignKey(d => d.ProductVariantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__po_product__productVariantId__product_variant__id");

                entity.HasOne(d => d.PurchaseOrder)
                    .WithMany(p => p.PurchaseOrderProduct)
                    .HasForeignKey(d => d.PurchaseOrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("purchase_order_product_ibfk_1");
            });

            modelBuilder.Entity<PurchaseOrderProductActualQuantity>(entity =>
            {
                entity.HasKey(e => e.PurchaseOrderProductId);

                entity.ToTable("purchase_order_product_actual_quantity");

                entity.Property(e => e.PurchaseOrderProductId)
                    .HasColumnName("purchaseOrderProductId")
                    .HasColumnType("int(10)")
                    .ValueGeneratedNever();

                entity.Property(e => e.ActualQuantity)
                    .HasColumnName("actualQuantity")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.PurchaseOrderProduct)
                    .WithOne(p => p.PurchaseOrderProductActualQuantity)
                    .HasForeignKey<PurchaseOrderProductActualQuantity>(d => d.PurchaseOrderProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_popaq__purchaseOrderProductId__pop__id");
            });

            modelBuilder.Entity<PurchaseOrderProductPiece>(entity =>
            {
                entity.ToTable("purchase_order_product_piece");

                entity.HasIndex(e => e.PurchaseOrderProductId)
                    .HasName("purchaseOrderProductId");

                entity.HasIndex(e => e.PurchaseOrdertrackingId)
                    .HasName("purchaseOrdertrackingId");

                entity.HasIndex(e => e.StatusId)
                    .HasName("FK__po_product_piece__statusId__status__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.PurchaseOrderProductId)
                    .HasColumnName("purchaseOrderProductId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.PurchaseOrdertrackingId)
                    .HasColumnName("purchaseOrdertrackingId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.PurchaseOrderProduct)
                    .WithMany(p => p.PurchaseOrderProductPiece)
                    .HasForeignKey(d => d.PurchaseOrderProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("purchase_order_product_piece_ibfk_1");

                entity.HasOne(d => d.PurchaseOrdertracking)
                    .WithMany(p => p.PurchaseOrderProductPiece)
                    .HasForeignKey(d => d.PurchaseOrdertrackingId)
                    .HasConstraintName("purchase_order_product_piece_ibfk_2");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.PurchaseOrderProductPiece)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK__po_product_piece__statusId__status__id");
            });

            modelBuilder.Entity<PurchaseOrderTracking>(entity =>
            {
                entity.ToTable("purchase_order_tracking");

                entity.HasIndex(e => e.PurchaseOrderId)
                    .HasName("purchaseOrderId");

                entity.HasIndex(e => e.StatusId)
                    .HasName("FK__po_tracking__statusId__status__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.PurchaseOrderId)
                    .HasColumnName("purchaseOrderId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.StaticCode)
                    .IsRequired()
                    .HasColumnName("staticCode")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.PurchaseOrder)
                    .WithMany(p => p.PurchaseOrderTracking)
                    .HasForeignKey(d => d.PurchaseOrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("purchase_order_tracking_ibfk_1");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.PurchaseOrderTracking)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK__po_tracking__statusId__status__id");
            });

            modelBuilder.Entity<Regiscode>(entity =>
            {
                entity.ToTable("regiscode");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.LockerCode)
                    .HasColumnName("lockerCode")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RegistryCode)
                    .IsRequired()
                    .HasColumnName("registryCode")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RelateCategory>(entity =>
            {
                entity.HasKey(e => new { e.ProductId, e.CatagoryMarketplaceId });

                entity.ToTable("relate_category");

                entity.HasIndex(e => e.CatagoryMarketplaceId)
                    .HasName("fk-relate_category-category_marketplace.id");

                entity.Property(e => e.ProductId)
                    .HasColumnName("productId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CatagoryMarketplaceId)
                    .HasColumnName("catagoryMarketplaceId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.CatagoryMarketplace)
                    .WithMany(p => p.RelateCategory)
                    .HasForeignKey(d => d.CatagoryMarketplaceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("relate_category_ibfk_1");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.RelateCategory)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("relate_category_ibfk_2");
            });

            modelBuilder.Entity<RelateProduct>(entity =>
            {
                entity.HasKey(e => new { e.ProductId, e.RelateProductId });

                entity.ToTable("relate_product");

                entity.HasIndex(e => e.RelateProductId)
                    .HasName("fk-ralate_product-product.id-relateProductId");

                entity.Property(e => e.ProductId)
                    .HasColumnName("productId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.RelateProductId)
                    .HasColumnName("relateProductId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.RelateProductProduct)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("relate_product_ibfk_1");

                entity.HasOne(d => d.RelateProductNavigation)
                    .WithMany(p => p.RelateProductRelateProductNavigation)
                    .HasForeignKey(d => d.RelateProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("relate_product_ibfk_2");
            });

            modelBuilder.Entity<RoleAbility>(entity =>
            {
                entity.ToTable("role_ability");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(5)");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RoleClass>(entity =>
            {
                entity.ToTable("role_class");

                entity.HasIndex(e => e.Id)
                    .HasName("id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(5)");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RoleClassAbility>(entity =>
            {
                entity.HasKey(e => new { e.RoleClassId, e.RoleAbilityId });

                entity.ToTable("role_class_ability");

                entity.HasIndex(e => e.RoleAbilityId)
                    .HasName("FK__role_class_ability__roleAbilityId__role_ability__id");

                entity.Property(e => e.RoleClassId)
                    .HasColumnName("roleClassId")
                    .HasColumnType("int(5)");

                entity.Property(e => e.RoleAbilityId)
                    .HasColumnName("roleAbilityId")
                    .HasColumnType("int(5)");

                entity.HasOne(d => d.RoleAbility)
                    .WithMany(p => p.RoleClassAbility)
                    .HasForeignKey(d => d.RoleAbilityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__role_class_ability__roleAbilityId__role_ability__id");

                entity.HasOne(d => d.RoleClass)
                    .WithMany(p => p.RoleClassAbility)
                    .HasForeignKey(d => d.RoleClassId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__role_class_ability__roleClassId__role_class_id");
            });

            modelBuilder.Entity<SaleOrder>(entity =>
            {
                entity.ToTable("sale_order");

                entity.HasIndex(e => e.CartId)
                    .HasName("fk-sale_order-cart.id");

                entity.HasIndex(e => e.CustomerUserId)
                    .HasName("fk-sale_order-customer_user.id");

                entity.HasIndex(e => e.CustomerUserAddressId)
                    .HasName("FK__so__customerAddressId__customer_address__id");

                entity.HasIndex(e => e.PaymentId)
                    .HasName("FK__so__paymentId__payment__id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("fk-sale_order-status.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.BillingAddress)
                    .HasColumnName("billingAddress")
                    .IsUnicode(false);

                entity.Property(e => e.CartId)
                    .HasColumnName("cartId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.CustomerUserAddressId)
                    .HasColumnName("customerUserAddressId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CustomerUserId)
                    .HasColumnName("customerUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Discount)
                    .HasColumnName("discount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.GatewayReferenceStaticCode)
                    .HasColumnName("gatewayReferenceStaticCode")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.GatewayRequestCode)
                    .HasColumnName("gatewayRequestCode")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LockerCode)
                    .HasColumnName("lockerCode")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LogDeliveryId)
                    .HasColumnName("logDeliveryId")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.NetPrice)
                    .HasColumnName("netPrice")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.PaymentId)
                    .HasColumnName("paymentId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Remark)
                    .HasColumnName("remark")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ShippingAddress)
                    .HasColumnName("shippingAddress")
                    .IsUnicode(false);

                entity.Property(e => e.ShippingFee)
                    .HasColumnName("shippingFee")
                    .HasColumnType("decimal(6,2)");

                entity.Property(e => e.StaticCode)
                    .IsRequired()
                    .HasColumnName("staticCode")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.TotalPrice)
                    .HasColumnName("totalPrice")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.Property(e => e.Vat)
                    .HasColumnName("vat")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.ReceiverTelephone)
                    .HasColumnName("receiverTelephone")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LinePayTransactionId)
                    .HasColumnName("LinePayTransactionId")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Cart)
                    .WithMany(p => p.SaleOrder)
                    .HasForeignKey(d => d.CartId)
                    .HasConstraintName("sale_order_ibfk_1");

                entity.HasOne(d => d.CustomerUser)
                    .WithMany(p => p.SaleOrder)
                    .HasForeignKey(d => d.CustomerUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("sale_order_ibfk_2");

                entity.HasOne(d => d.CustomerAddress)
                    .WithMany(p => p.SaleOrder)
                    .HasForeignKey(d => d.CustomerUserAddressId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__so__customerAddressId__customer_address__id");

                entity.HasOne(d => d.Payment)
                    .WithMany(p => p.SaleOrder)
                    .HasForeignKey(d => d.PaymentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__so__paymentId__payment__id");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SaleOrder)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("sale_order_ibfk_3");
            });

            modelBuilder.Entity<SaleOrderDiscount>(entity =>
            {
                entity.HasKey(e => new { e.SaleOrderId, e.DiscountTypeId, e.DiscountId });

                entity.ToTable("sale_order_discount");

                entity.HasIndex(e => e.DiscountTypeId)
                    .HasName("FK__so_discount__discountTypeId__discount_type__id");

                entity.Property(e => e.SaleOrderId)
                    .HasColumnName("saleOrderId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.DiscountTypeId)
                    .HasColumnName("discountTypeId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.DiscountId)
                    .HasColumnName("discountId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.DiscountAmount)
                    .HasColumnName("discountAmount")
                    .HasColumnType("decimal(10,2)");

                entity.HasOne(d => d.SaleOrder)
                    .WithMany(p => p.SaleOrderDiscount)
                    .HasForeignKey(d => d.SaleOrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__so_discount__soId__sale_order__id");

                entity.HasOne(d => d.DiscountType)
                    .WithMany(p => p.SaleOrderDiscount)
                    .HasForeignKey(d => d.DiscountTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__so_discount__discountTypeId__discount_type__id");
            });

            modelBuilder.Entity<SaleOrderProduct>(entity =>
            {
                entity.ToTable("sale_order_product");

                entity.HasIndex(e => e.ProductVariantId)
                    .HasName("fk-sale_order_product-product_variant.id");

                entity.HasIndex(e => e.SaleOrderId)
                    .HasName("fk-sale_order_product-sale_order.id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("fk-sale_order_product-status.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.Discount)
                    .HasColumnName("discount")
                    .HasColumnType("decimal(10,2)")
                    .IsRequired(true);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.NetPrice)
                    .HasColumnName("netPrice")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.ProductVariantId)
                    .HasColumnName("productVariantId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasColumnType("int(5)");

                entity.Property(e => e.Remark)
                    .HasColumnName("remark")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SaleOrderId)
                    .HasColumnName("saleOrderId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Sku)
                    .HasColumnName("sku")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.TotalPrice)
                    .HasColumnName("totalPrice")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.Property(e => e.Variant)
                    .HasColumnName("variant")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Vat)
                    .HasColumnName("vat")
                    .HasColumnType("decimal(8,2)");

                entity.Property(e => e.Image)
                    .HasColumnName("image")
                    .HasMaxLength(255);

                entity.HasOne(d => d.ProductVariant)
                    .WithMany(p => p.SaleOrderProduct)
                    .HasForeignKey(d => d.ProductVariantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("sale_order_product_ibfk_1");

                entity.HasOne(d => d.SaleOrder)
                    .WithMany(p => p.SaleOrderProduct)
                    .HasForeignKey(d => d.SaleOrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("sale_order_product_ibfk_2");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SaleOrderProduct)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("sale_order_product_ibfk_3");
            });

            modelBuilder.Entity<Seller>(entity =>
            {
                entity.ToTable("seller");

                entity.HasIndex(e => e.SellerGroupId)
                    .HasName("FK_seller__sellerGroupId__seller_group__id");

                entity.HasIndex(e => e.SellerWorkdayGroupId)
                    .HasName("FK_seller__sellerWorkdayGroupId__seller_wdg__id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("fk-seller-status.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Image)
                    .IsRequired()
                    .HasColumnName("image")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.IsFood)
                    .HasColumnName("isFood")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("b'0'");

                entity.Property(e => e.Latitude)
                    .HasColumnName("latitude")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Longitude)
                    .HasColumnName("longitude")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SellerGroupId)
                    .HasColumnName("sellerGroupId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.SellerWorkdayGroupId)
                    .HasColumnName("sellerWorkdayGroupId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Sequence)
                    .HasColumnName("sequence")
                    .HasColumnType("int(4)")
                    .HasDefaultValueSql("999");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.TaxNumber)
                    .HasColumnName("taxNumber")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Telephone)
                    .HasColumnName("telephone")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.SellerGroup)
                    .WithMany(p => p.Seller)
                    .HasForeignKey(d => d.SellerGroupId)
                    .HasConstraintName("FK_seller__sellerGroupId__seller_group__id");

                entity.HasOne(d => d.SellerWorkdayGroup)
                    .WithMany(p => p.Seller)
                    .HasForeignKey(d => d.SellerWorkdayGroupId)
                    .HasConstraintName("FK_seller__sellerWorkdayGroupId__seller_wdg__id");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Seller)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("seller_ibfk_1");
            });

            modelBuilder.Entity<SellerAddress>(entity =>
            {
                entity.ToTable("seller_address");

                entity.HasIndex(e => e.CityId)
                    .HasName("fk-seller_address-city.id");

                entity.HasIndex(e => e.CountryId)
                    .HasName("fk-seller_address-country.id");

                entity.HasIndex(e => e.ProvinceId)
                    .HasName("fk-seller_address-province.id");

                entity.HasIndex(e => e.SellerId)
                    .HasName("fk-seller_address-seller.id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("fk-seller_address-status.id");

                entity.HasIndex(e => e.TownId)
                    .HasName("FK__seller_address__townId__town__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Address1)
                    .IsRequired()
                    .HasColumnName("address1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Address2)
                    .HasColumnName("address2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Address3)
                    .HasColumnName("address3")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CityId)
                    .HasColumnName("cityId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CountryId)
                    .HasColumnName("countryId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("createDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ProvinceId)
                    .HasColumnName("provinceId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.SellerId)
                    .HasColumnName("sellerId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.TaxNumber)
                    .HasColumnName("taxNumber")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TownId)
                    .HasColumnName("townId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("updateDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Zipcode)
                    .HasColumnName("zipcode")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.City)
                    .WithMany(p => p.SellerAddress)
                    .HasForeignKey(d => d.CityId)
                    .HasConstraintName("seller_address_ibfk_1");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.SellerAddress)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("seller_address_ibfk_2");

                entity.HasOne(d => d.Province)
                    .WithMany(p => p.SellerAddress)
                    .HasForeignKey(d => d.ProvinceId)
                    .HasConstraintName("seller_address_ibfk_3");

                entity.HasOne(d => d.Seller)
                    .WithMany(p => p.SellerAddress)
                    .HasForeignKey(d => d.SellerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("seller_address_ibfk_4");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SellerAddress)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("seller_address_ibfk_5");

                entity.HasOne(d => d.Town)
                    .WithMany(p => p.SellerAddress)
                    .HasForeignKey(d => d.TownId)
                    .HasConstraintName("FK__seller_address__townId__town__id");
            });

            modelBuilder.Entity<SellerArea>(entity =>
            {
                entity.HasKey(e => new { e.SellerId, e.Sequence });

                entity.ToTable("seller_area");

                entity.Property(e => e.SellerId)
                    .HasColumnName("sellerId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Sequence)
                    .HasColumnName("sequence")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Latitude)
                    .IsRequired()
                    .HasColumnName("latitude")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Longitude)
                    .IsRequired()
                    .HasColumnName("longitude")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Seller)
                    .WithMany(p => p.SellerArea)
                    .HasForeignKey(d => d.SellerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_seller_area__sellerId__seller__id");
            });

            modelBuilder.Entity<SellerCoupon>(entity =>
            {
                entity.ToTable("seller_coupon");

                entity.HasIndex(e => e.MarketplaceUserId)
                    .HasName("fk-seller_coupon-marketplace_user.id");

                entity.HasIndex(e => e.PriceCouponId)
                    .HasName("fk-seller_coupon-price_coupon.id");

                entity.HasIndex(e => e.SellerId)
                    .HasName("fk-seller_coupon-seller.id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("fk-seller_coupon-status.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.EndDate).HasColumnName("endDate");

                entity.Property(e => e.MarketplaceUserId)
                    .HasColumnName("marketplaceUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PriceCouponId)
                    .HasColumnName("priceCouponId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasColumnType("int(5)");

                entity.Property(e => e.RedeemCode)
                    .IsRequired()
                    .HasColumnName("redeemCode")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SellerId)
                    .HasColumnName("sellerId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.StartDate).HasColumnName("startDate");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.MarketplaceUser)
                    .WithMany(p => p.SellerCoupon)
                    .HasForeignKey(d => d.MarketplaceUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("seller_coupon_ibfk_1");

                entity.HasOne(d => d.PriceCoupon)
                    .WithMany(p => p.SellerCoupon)
                    .HasForeignKey(d => d.PriceCouponId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("seller_coupon_ibfk_2");

                entity.HasOne(d => d.Seller)
                    .WithMany(p => p.SellerCoupon)
                    .HasForeignKey(d => d.SellerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("seller_coupon_ibfk_3");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SellerCoupon)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("seller_coupon_ibfk_4");
            });

            modelBuilder.Entity<SellerCouponRedeem>(entity =>
            {
                entity.ToTable("seller_coupon_redeem");

                entity.HasIndex(e => e.CustomerUserId)
                    .HasName("fk-seller_coupon_redeem-customer_user.id");

                entity.HasIndex(e => e.SellerCouponId)
                    .HasName("fk-seller_coupon_redeem-seller_coupon.id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("fk-seller_coupon_redeem-status.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.CustomerUserId)
                    .HasColumnName("customerUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.SellerCouponId)
                    .HasColumnName("sellerCouponId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.CustomerUser)
                    .WithMany(p => p.SellerCouponRedeem)
                    .HasForeignKey(d => d.CustomerUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("seller_coupon_redeem_ibfk_1");

                entity.HasOne(d => d.SellerCoupon)
                    .WithMany(p => p.SellerCouponRedeem)
                    .HasForeignKey(d => d.SellerCouponId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("seller_coupon_redeem_ibfk_2");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SellerCouponRedeem)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("seller_coupon_redeem_ibfk_3");
            });

            modelBuilder.Entity<SellerGroup>(entity =>
            {
                entity.ToTable("seller_group");

                entity.HasIndex(e => e.StatusId)
                    .HasName("FK__seller_group__statusId__status__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SellerGroup)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__seller_group__statusId__status__id");
            });

            modelBuilder.Entity<SellerImage>(entity =>
            {
                entity.ToTable("seller_image");

                entity.HasIndex(e => e.SellerId)
                    .HasName("FK_seller_image__sellerId__seller__id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("FK_seller_image__statusId__status__id");

                entity.HasIndex(e => e.MediaTypeId)
                    .HasName("FK__seller_image__mediaTypeId__media_type__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .IsUnicode(false);

                entity.Property(e => e.IsCover)
                    .HasColumnName("isCover")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("b'0'");

                entity.Property(e => e.SellerId)
                    .HasColumnName("sellerId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Sequence)
                    .HasColumnName("sequence")
                    .HasColumnType("int(4)")
                    .HasDefaultValueSql("9999");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.Property(e => e.Url)
                    .IsRequired()
                    .HasColumnName("url")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MediaTypeId)
                    .HasColumnName("mediaTypeId")
                    .HasColumnType("int(4)")
                    .HasDefaultValue(1);

                entity.HasOne(d => d.Seller)
                    .WithMany(p => p.SellerImage)
                    .HasForeignKey(d => d.SellerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_seller_image__sellerId__seller__id");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SellerImage)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_seller_image__statusId__status__id");

                entity.HasOne(d => d.MediaType)
                    .WithMany(p => p.SellerImage)
                    .HasForeignKey(d => d.MediaTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_seller_image__mediaTypeId__media_type__id");
            });

            modelBuilder.Entity<SellerProductDelivery>(entity =>
            {
                entity.HasKey(e => new { e.SellerId, e.ProvinceId });

                entity.ToTable("seller_product_delivery");

                entity.HasIndex(e => e.ProvinceId)
                    .HasName("FK_seller_pd__provinceId__province__id");

                entity.Property(e => e.SellerId)
                    .HasColumnName("sellerId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.ProvinceId)
                    .HasColumnName("provinceId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.Province)
                    .WithMany(p => p.SellerProductDelivery)
                    .HasForeignKey(d => d.ProvinceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_seller_pd__provinceId__province__id");

                entity.HasOne(d => d.Seller)
                    .WithMany(p => p.SellerProductDelivery)
                    .HasForeignKey(d => d.SellerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_seller_pd__sellerId__seller__id");
            });

            modelBuilder.Entity<SellerPromotion>(entity =>
            {
                entity.ToTable("seller_promotion");

                entity.HasIndex(e => e.CategorySellerId)
                    .HasName("fk-seller_promotion-category_seller.id");

                entity.HasIndex(e => e.MarketplaceUserId)
                    .HasName("fk-seller_promotion-marketplace_user.id");

                entity.HasIndex(e => e.PricePromotionId)
                    .HasName("fk-seller_promotion-price_promotion.id");

                entity.HasIndex(e => e.ProductId)
                    .HasName("fk-seller_promotion-product.id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("fk-seller_promotion-status.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CategorySellerId)
                    .HasColumnName("categorySellerId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.EndDate).HasColumnName("endDate");

                entity.Property(e => e.MarketplaceUserId)
                    .HasColumnName("marketplaceUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.MinQuantity)
                    .HasColumnName("minQuantity")
                    .HasColumnType("int(3)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PricePromotionId)
                    .HasColumnName("pricePromotionId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("productId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasColumnType("int(5)");

                entity.Property(e => e.StartDate).HasColumnName("startDate");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.CategorySeller)
                    .WithMany(p => p.SellerPromotion)
                    .HasForeignKey(d => d.CategorySellerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("seller_promotion_ibfk_1");

                entity.HasOne(d => d.MarketplaceUser)
                    .WithMany(p => p.SellerPromotion)
                    .HasForeignKey(d => d.MarketplaceUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("seller_promotion_ibfk_2");

                entity.HasOne(d => d.PricePromotion)
                    .WithMany(p => p.SellerPromotion)
                    .HasForeignKey(d => d.PricePromotionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("seller_promotion_ibfk_3");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.SellerPromotion)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("seller_promotion_ibfk_4");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SellerPromotion)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("seller_promotion_ibfk_5");
            });

            modelBuilder.Entity<SellerPromotionRedeem>(entity =>
            {
                entity.ToTable("seller_promotion_redeem");

                entity.HasIndex(e => e.SaleOrderId)
                    .HasName("fk-seller_promotion_redeem-sale_order.id");

                entity.HasIndex(e => e.SellerPromotionId)
                    .HasName("fk-seller_promotion_redeem-seller_promotion.id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("fk-seller_promotion_redeem-status.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.SaleOrderId)
                    .HasColumnName("saleOrderId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.SellerPromotionId)
                    .HasColumnName("sellerPromotionId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.SaleOrder)
                    .WithMany(p => p.SellerPromotionRedeem)
                    .HasForeignKey(d => d.SaleOrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("seller_promotion_redeem_ibfk_1");

                entity.HasOne(d => d.SellerPromotion)
                    .WithMany(p => p.SellerPromotionRedeem)
                    .HasForeignKey(d => d.SellerPromotionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("seller_promotion_redeem_ibfk_2");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SellerPromotionRedeem)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("seller_promotion_redeem_ibfk_3");
            });

            modelBuilder.Entity<SellerStats>(entity =>
            {
                entity.HasKey(e => new { e.SellerId, e.SellerStatsTypeId });

                entity.ToTable("seller_stats");

                entity.HasIndex(e => e.SellerStatsTypeId)
                    .HasName("fk-seller_stats-seller_stats_type_id");

                entity.Property(e => e.SellerId)
                    .HasColumnName("sellerId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.SellerStatsTypeId)
                    .HasColumnName("sellerStatsTypeId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnName("value")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Seller)
                    .WithMany(p => p.SellerStats)
                    .HasForeignKey(d => d.SellerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("seller_stats_ibfk_1");

                entity.HasOne(d => d.SellerStatsType)
                    .WithMany(p => p.SellerStats)
                    .HasForeignKey(d => d.SellerStatsTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("seller_stats_ibfk_2");
            });

            modelBuilder.Entity<SellerStatsType>(entity =>
            {
                entity.ToTable("seller_stats_type");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(4)")
                    .ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SellerStatsTypeLanguage>(entity =>
            {
                entity.ToTable("seller_stats_type_language");

                entity.HasIndex(e => e.LanguageId)
                    .HasName("FK_seller_s_types_language__languageId__language__id");

                entity.HasIndex(e => e.SellerStatsTypeId)
                    .HasName("FK_seller_s_types_language__sellerStatsTypeId__seller_s_type__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.LanguageId)
                    .HasColumnName("languageId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SellerStatsTypeId)
                    .HasColumnName("sellerStatsTypeId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.Language)
                    .WithMany(p => p.SellerStatsTypeLanguage)
                    .HasForeignKey(d => d.LanguageId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_seller_s_types_language__languageId__language__id");

                entity.HasOne(d => d.SellerStatsType)
                    .WithMany(p => p.SellerStatsTypeLanguage)
                    .HasForeignKey(d => d.SellerStatsTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_seller_s_types_language__sellerStatsTypeId__seller_s_type__id");
            });

            modelBuilder.Entity<SellerUser>(entity =>
            {
                entity.ToTable("seller_user");

                entity.HasIndex(e => e.CustomerUserId)
                    .HasName("FK_seller_user__customerUserId__customer_user__id");

                entity.HasIndex(e => e.RoleClassId)
                    .HasName("FK_seller_user__roleClassId__role_class__id");

                entity.HasIndex(e => e.SellerId)
                    .HasName("FK_seller_user__sellerId__seller__id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("FK_seller_user__statusId__status__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.CustomerUserId)
                    .HasColumnName("customerUserId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RoleClassId)
                    .HasColumnName("roleClassId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.SellerId)
                    .HasColumnName("sellerId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.CustomerUser)
                    .WithMany(p => p.SellerUser)
                    .HasForeignKey(d => d.CustomerUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_seller_user__customerUserId__customer_user__id");

                entity.HasOne(d => d.RoleClass)
                    .WithMany(p => p.SellerUser)
                    .HasForeignKey(d => d.RoleClassId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_seller_user__roleClassId__role_class__id");

                entity.HasOne(d => d.Seller)
                    .WithMany(p => p.SellerUser)
                    .HasForeignKey(d => d.SellerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_seller_user__sellerId__seller__id");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SellerUser)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_seller_user__statusId__status__id");
            });

            modelBuilder.Entity<SellerWorkdayGroup>(entity =>
            {
                entity.ToTable("seller_workday_group");

                entity.HasIndex(e => e.WorkingDayId)
                    .HasName("FK__swg__workingDayId__working_day__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.WorkingDayId)
                    .HasColumnName("workingDayId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.WorkingDay)
                    .WithMany(p => p.SellerWorkdayGroup)
                    .HasForeignKey(d => d.WorkingDayId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__swg__workingDayId__working_day__id");
            });

            modelBuilder.Entity<SellerWorkdayGroupHoliday>(entity =>
            {
                entity.ToTable("seller_workday_group_holiday");

                entity.HasIndex(e => e.SellerWorkdayGroupId)
                    .HasName("FK_seller_wdg_holiday__workdayGroupId__seller_wdg__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Date).HasColumnName("date");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SellerWorkdayGroupId)
                    .HasColumnName("sellerWorkdayGroupId")
                    .HasColumnType("int(10)");

                entity.HasOne(d => d.SellerWorkdayGroup)
                    .WithMany(p => p.SellerWorkdayGroupHoliday)
                    .HasForeignKey(d => d.SellerWorkdayGroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_seller_wdg_holiday__workdayGroupId__seller_wdg__id");
            });

            modelBuilder.Entity<SellerWorkdayProfile>(entity =>
            {
                entity.ToTable("seller_workday_profile");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)")
                    .ValueGeneratedNever();

                entity.Property(e => e.DayAmount)
                    .HasColumnName("dayAmount")
                    .HasColumnType("int(3)");

                entity.Property(e => e.MonthEnd)
                    .HasColumnName("monthEnd")
                    .HasColumnType("int(2)");

                entity.Property(e => e.MonthStart)
                    .HasColumnName("monthStart")
                    .HasColumnType("int(2)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TimeEnd).HasColumnName("timeEnd");

                entity.Property(e => e.TimeStart).HasColumnName("timeStart");
            });

            modelBuilder.Entity<SellerWorkdayProfileGroup>(entity =>
            {
                entity.HasKey(e => new { e.SellerWorkdayGroupId, e.SellerWorkdayProfileId });

                entity.ToTable("seller_workday_profile_group");

                entity.HasIndex(e => e.SellerWorkdayProfileId)
                    .HasName("FK_seller_wpg__sellerWorkdayProfileId__seller_wp__id");

                entity.Property(e => e.SellerWorkdayGroupId)
                    .HasColumnName("sellerWorkdayGroupId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.SellerWorkdayProfileId)
                    .HasColumnName("sellerWorkdayProfileId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.DayAmount)
                    .HasColumnName("dayAmount")
                    .HasColumnType("int(3)");

                entity.Property(e => e.MonthEnd)
                    .HasColumnName("monthEnd")
                    .HasColumnType("int(2)");

                entity.Property(e => e.MonthStart)
                    .HasColumnName("monthStart")
                    .HasColumnType("int(2)");

                entity.Property(e => e.Priority)
                    .HasColumnName("priority")
                    .HasColumnType("int(3)");

                entity.Property(e => e.TimeEnd).HasColumnName("timeEnd");

                entity.Property(e => e.TimeStart).HasColumnName("timeStart");

                entity.HasOne(d => d.SellerWorkdayGroup)
                    .WithMany(p => p.SellerWorkdayProfileGroup)
                    .HasForeignKey(d => d.SellerWorkdayGroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_seller_wpg__sellerWorkdayGroupId__seller_wdg__id");

                entity.HasOne(d => d.SellerWorkdayProfile)
                    .WithMany(p => p.SellerWorkdayProfileGroup)
                    .HasForeignKey(d => d.SellerWorkdayProfileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_seller_wpg__sellerWorkdayProfileId__seller_wp__id");
            });

            modelBuilder.Entity<Sharelocker>(entity =>
            {
                entity.HasKey(e => e.ShareId);

                entity.ToTable("sharelocker");

                entity.Property(e => e.ShareId)
                    .HasColumnName("shareID")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("createdBy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnName("createdDate");

                entity.Property(e => e.LockerCode)
                    .HasColumnName("lockerCode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedBy)
                    .HasColumnName("updatedBy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate)
                    .HasColumnName("updatedDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UserId)
                    .HasColumnName("userID")
                    .HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<Status>(entity =>
            {
                entity.ToTable("status");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<StatusLanguage>(entity =>
            {
                entity.HasKey(e => new { e.StatusId, e.LanguageId });

                entity.ToTable("status_language");

                entity.HasIndex(e => e.StatusId)
                    .HasName("FK__status_lang__statusId__status__id");

                entity.HasIndex(e => e.LanguageId)
                    .HasName("FK__status_lang__languageId__language__id");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.LanguageId)
                    .HasColumnName("languageId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.StatusLanguage)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__status_lang__statusId__status__id");

                entity.HasOne(d => d.Language)
                    .WithMany(p => p.StatusLanguage)
                    .HasForeignKey(d => d.LanguageId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__status_lang__languageId__language__id");
            });

            modelBuilder.Entity<Stock>(entity =>
            {
                entity.ToTable("stock");

                entity.HasIndex(e => e.ProductVariantId)
                    .HasName("fk-stock-product_variant.id");

                entity.HasIndex(e => e.SaleOrderId)
                    .HasName("fk-stock-sale_order.id");

                entity.HasIndex(e => e.StatusId)
                    .HasName("FK__stock__statusId__status__id");

                entity.HasIndex(e => e.StockTypeId)
                    .HasName("fk-stock-stock_type.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreateDate).HasColumnName("createDate");

                entity.Property(e => e.ProductVariantId)
                    .HasColumnName("productVariantId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasColumnType("int(5)");

                entity.Property(e => e.SaleOrderId)
                    .HasColumnName("saleOrderId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.StatusId)
                    .HasColumnName("statusId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.StockTypeId)
                    .HasColumnName("stockTypeId")
                    .HasColumnType("int(4)");

                entity.Property(e => e.UpdateDate).HasColumnName("updateDate");

                entity.HasOne(d => d.ProductVariant)
                    .WithMany(p => p.Stock)
                    .HasForeignKey(d => d.ProductVariantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("stock_ibfk_1");

                entity.HasOne(d => d.SaleOrder)
                    .WithMany(p => p.Stock)
                    .HasForeignKey(d => d.SaleOrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("stock_ibfk_2");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Stock)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__stock__statusId__status__id");

                entity.HasOne(d => d.StockType)
                    .WithMany(p => p.Stock)
                    .HasForeignKey(d => d.StockTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("stock_ibfk_3");
            });

            modelBuilder.Entity<StockType>(entity =>
            {
                entity.ToTable("stock_type");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Town>(entity =>
            {
                entity.ToTable("town");

                entity.HasIndex(e => e.CityId)
                    .HasName("FK__town__cityId__city__id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CityId)
                    .HasColumnName("cityId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.City)
                    .WithMany(p => p.Town)
                    .HasForeignKey(d => d.CityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__town__cityId__city__id");
            });

            modelBuilder.Entity<TownLanguage>(entity =>
            {
                entity.HasKey(e => new { e.TownId, e.LanguageId });

                entity.ToTable("town_language");

                entity.HasIndex(e => e.LanguageId)
                    .HasName("FK__town_language__languageId__language__id");

                entity.Property(e => e.TownId)
                    .HasColumnName("townId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.LanguageId)
                    .HasColumnName("languageId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Language)
                    .WithMany(p => p.TownLanguage)
                    .HasForeignKey(d => d.LanguageId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__town_language__languageId__language__id");

                entity.HasOne(d => d.Town)
                    .WithMany(p => p.TownLanguage)
                    .HasForeignKey(d => d.TownId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__town_language__townId__town__id");
            });

            modelBuilder.Entity<Variant>(entity =>
            {
                entity.HasKey(e => new { e.Id });

                entity.ToTable("variant");

                entity.HasIndex(e => e.Id)
                    .HasName("id");

                entity.HasIndex(e => e.VariantTypeId)
                    .HasName("fk-variant-variant_type.id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.VariantTypeId)
                    .HasColumnName("variantTypeId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnName("value")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.HasOne(d => d.VariantType)
                    .WithMany(p => p.Variant)
                    .HasForeignKey(d => d.VariantTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk-variant-variant_type.id");
            });

            modelBuilder.Entity<VariantGroup>(entity =>
            {
                entity.HasKey(e => new { e.ProductVariantId, e.VariantId });

                entity.HasIndex(e => e.ProductVariantId)
                    .HasName("FK__variant_group__productVariantId__product_variant__id");

                entity.HasIndex(e => e.VariantId)
                    .HasName("FK__variant_group__variantId__variant__id");

                entity.ToTable("variant_group");

                entity.Property(e => e.ProductVariantId)
                    .HasColumnName("productVariantId")
                    .HasColumnType("int(10)")
                    .IsRequired(true);

                entity.Property(e => e.VariantId)
                    .HasColumnName("VariantId")
                    .HasColumnType("int(10)")
                    .IsRequired(true);

                entity.HasOne(d => d.ProductVariant)
                    .WithMany(p => p.VariantGroup)
                    .HasForeignKey(d => d.ProductVariantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__variant_group__productVariantId__product_variant__id");

                entity.HasOne(d => d.Variant)
                    .WithMany(p => p.VariantGroup)
                    .HasForeignKey(d => d.VariantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__variant_group__variantId__variant__id");
            });

            modelBuilder.Entity<VariantType>(entity =>
            {
                entity.ToTable("variant_type");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Webuser>(entity =>
            {
                entity.ToTable("webuser");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Authorize)
                    .HasColumnName("authorize")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("createdBy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnName("createdDate");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Firstname)
                    .HasColumnName("firstname")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Lastname)
                    .HasColumnName("lastname")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Telephone)
                    .HasColumnName("telephone")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedBy)
                    .HasColumnName("updatedBy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate)
                    .HasColumnName("updatedDate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            modelBuilder.Entity<WorkingDay>(entity =>
            {
                entity.ToTable("working_day");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Friday)
                    .HasColumnName("friday")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("b'0'");

                entity.Property(e => e.Monday)
                    .HasColumnName("monday")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("b'0'");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Saturday)
                    .HasColumnName("saturday")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("b'0'");

                entity.Property(e => e.Sunday)
                    .HasColumnName("sunday")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("b'0'");

                entity.Property(e => e.Thursday)
                    .HasColumnName("thursday")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("b'0'");

                entity.Property(e => e.Tuesday)
                    .HasColumnName("tuesday")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("b'0'");

                entity.Property(e => e.Wednesday)
                    .HasColumnName("wednesday")
                    .HasColumnType("bit(1)")
                    .HasDefaultValueSql("b'0'");
            });

            modelBuilder.Entity<Zone>(entity =>
            {
                entity.ToTable("zone");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(6)");

                entity.Property(e => e.Route)
                    .IsRequired()
                    .HasColumnName("route")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });
        }
    }
}
