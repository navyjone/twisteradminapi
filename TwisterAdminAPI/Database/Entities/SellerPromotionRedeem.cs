﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class SellerPromotionRedeem
    {
        public int Id { get; set; }
        public int SellerPromotionId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int StatusId { get; set; }
        public int SaleOrderId { get; set; }

        public virtual SaleOrder SaleOrder { get; set; }
        public virtual SellerPromotion SellerPromotion { get; set; }
        public virtual Status Status { get; set; }
    }
}
