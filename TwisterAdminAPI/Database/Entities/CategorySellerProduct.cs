﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class CategorySellerProduct
    {
        public int CategorySellerId { get; set; }
        public int ProductId { get; set; }

        public virtual CategorySeller CategorySeller { get; set; }
        public virtual Product Product { get; set; }
    }
}
