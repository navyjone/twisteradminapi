﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class CreditNote
    {
        public CreditNote()
        {
            CreditNoteProduct = new HashSet<CreditNoteProduct>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public int InvoiceReceiptId { get; set; }
        public int SaleOrderId { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal Vat { get; set; }
        public string SaleOrderCode { get; set; }
        public decimal NetPrice { get; set; }
        public string Name { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public int CustomerUserId { get; set; }
        public string Payment { get; set; }
        public string Remark { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int StatusId { get; set; }
        public int SellerId { get; set; }
        public string MarketplaceAddress { get; set; }

        public virtual CustomerUser CustomerUser { get; set; }
        public virtual InvoiceReceipt InvoiceReceipt { get; set; }
        public virtual SaleOrder SaleOrder { get; set; }
        public virtual Seller Seller { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<CreditNoteProduct> CreditNoteProduct { get; set; }
    }
}
