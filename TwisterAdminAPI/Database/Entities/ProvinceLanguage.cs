﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class ProvinceLanguage
    {
        public int ProvinceId { get; set; }
        public int LanguageId { get; set; }
        public string Name { get; set; }

        public virtual Language Language { get; set; }
        public virtual Province Province { get; set; }
    }
}
