﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class Regiscode
    {
        public int Id { get; set; }
        public string LockerCode { get; set; }
        public string RegistryCode { get; set; }
    }
}
