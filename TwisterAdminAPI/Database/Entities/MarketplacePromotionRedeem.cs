﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class MarketplacePromotionRedeem
    {
        public int Id { get; set; }
        public int MarketplacePromotionId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int StatusId { get; set; }
        public int SaleOrderId { get; set; }

        public virtual MarketplacePromotion MarketplacePromotion { get; set; }
        public virtual SaleOrder SaleOrder { get; set; }
        public virtual Status Status { get; set; }
    }
}
