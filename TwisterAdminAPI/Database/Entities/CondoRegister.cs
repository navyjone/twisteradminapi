﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class CondoRegister
    {
        public long Id { get; set; }
        public long CondoId { get; set; }
        public string Telephone { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTimeOffset? UpdatedDate { get; set; }
    }
}
