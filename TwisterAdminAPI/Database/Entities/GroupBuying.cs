﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public partial class GroupBuying
    {
        public GroupBuying()
        {
            Cart = new HashSet<Cart>();
            GroupBuyingInvitation = new HashSet<GroupBuyingInvitation>();
        }

        public int Id { get; set; }
        public int GroupBuyingProductVariantId { get; set; }
        public int CustomerUserId { get; set; } // initiator
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        public DateTimeOffset UpdateDate { get; set; }
        public int StatusId { get; set; }

        public virtual GroupBuyingProductVariant GroupBuyingProductVariant { get; set; }
        public virtual CustomerUser CustomerUser { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<Cart> Cart { get; set; }
        public virtual ICollection<GroupBuyingInvitation> GroupBuyingInvitation { get; set; }
    }
}
