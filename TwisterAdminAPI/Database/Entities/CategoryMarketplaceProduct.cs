﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class CategoryMarketplaceProduct
    {
        public int CategoryMarketPlaceId { get; set; }
        public int ProductId { get; set; }

        public virtual CategoryMarketplace CategoryMarketPlace { get; set; }
        public virtual Product Product { get; set; }
    }
}
