﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class BannerMarketplaceProduct
    {
        public int BannerMarketplaceId { get; set; }
        public int ProductId { get; set; }

        public virtual BannerMarketplace BannerMarketplace { get; set; }
        public virtual Product Product { get; set; }
    }
}
