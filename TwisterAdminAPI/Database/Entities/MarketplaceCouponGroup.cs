﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class MarketplaceCouponGroup
    {
        public MarketplaceCouponGroup()
        {
            GroupBuyingReward = new HashSet<GroupBuyingReward>();
            MarketplaceCoupon = new HashSet<MarketplaceCoupon>();
        }

        public int Id { get; set; }
        public int Quantity { get; set; }
        public string Code { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int StatusId { get; set; }
        public int MarketplaceUserId { get; set; }
        public int CouponPriceId { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public short IsUniversal { get; set; }
        public string Remark { get; set; }

        public virtual CouponPrice CouponPrice { get; set; }
        public virtual MarketplaceUser MarketplaceUser { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<MarketplaceCoupon> MarketplaceCoupon { get; set; }
        public virtual ICollection<GroupBuyingReward> GroupBuyingReward { get; set; }
    }
}
