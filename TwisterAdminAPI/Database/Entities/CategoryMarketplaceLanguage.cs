﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class CategoryMarketplaceLanguage
    {
        public int CategoryMarketplaceId { get; set; }
        public int LanguageId { get; set; }
        public string Name { get; set; }

        public virtual CategoryMarketplace CategoryMarketplace { get; set; }
        public virtual Language Language { get; set; }

    }
}
