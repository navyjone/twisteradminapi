﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class NotificationManualLanguage
    {
        public int Id { get; set; }
        public int NotificationManualId { get; set; }
        public int LanguageId { get; set; }
        public string Topic { get; set; }
        public string Message { get; set; }

        public virtual Language Language { get; set; }
        public virtual NotificationManual NotificationManual { get; set; }
    }
}
