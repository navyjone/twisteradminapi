﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class SellerArea
    {
        public int SellerId { get; set; }
        public int Sequence { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public virtual Seller Seller { get; set; }
    }
}
