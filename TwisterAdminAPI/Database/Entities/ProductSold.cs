﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class ProductSold
    {
        public int CustomerUserId { get; set; }
        public int ProductId { get; set; }
        public int Order { get; set; }
        public int Quantity { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }

        public virtual CustomerUser CustomerUser { get; set; }
        public virtual Product Product { get; set; }
    }
}
