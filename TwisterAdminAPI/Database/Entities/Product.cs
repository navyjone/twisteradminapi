﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class Product
    {
        public Product()
        {
            BannerMarketplaceProduct = new HashSet<BannerMarketplaceProduct>();
            CategoryMarketplaceProduct = new HashSet<CategoryMarketplaceProduct>();
            CategorySellerProduct = new HashSet<CategorySellerProduct>();
            CouponType = new HashSet<CouponType>();
            CustomerReviewProduct = new HashSet<CustomerReviewProduct>();
            CustomerUserViewHistoryLog = new HashSet<CustomerUserViewHistoryLog>();
            FavoriteProduct = new HashSet<FavoriteProduct>();
            MarketplacePromotion = new HashSet<MarketplacePromotion>();
            ProductImage = new HashSet<ProductImage>();
            ProductRating = new HashSet<ProductRating>();
            ProductSold = new HashSet<ProductSold>();
            ProductVariant = new HashSet<ProductVariant>();
            ProductWishlist = new HashSet<ProductWishlist>();
            PromotionPrice = new HashSet<PromotionPrice>();
            RelateCategory = new HashSet<RelateCategory>();
            RelateProductProduct = new HashSet<RelateProduct>();
            RelateProductRelateProductNavigation = new HashSet<RelateProduct>();
            SellerPromotion = new HashSet<SellerPromotion>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int StatusId { get; set; }
        public int? Rating { get; set; }
        public int? Sold { get; set; }
        public int? Wishlist { get; set; }
        public int? CategorySellerId { get; set; }
        public int? CategoryMarketPlaceId { get; set; }
        public string Sku { get; set; }
        public string Size { get; set; }
        public int? SellerAddressId { get; set; }
        public int SellerId { get; set; }
        public string Image { get; set; }
        public int? PurchaseLimit { get; set; }
        public int? BrandId { get; set; }
        public int? EventId { get; set; }
        public short? IsFood { get; set; }
        public string Url { get; set; }
        public int? ProductTypeId { get; set; }
        public DateTime? ExpireDate { get; set; }
        public int? ExpireDurationInDay { get; set; }

        public virtual Brand Brand { get; set; }
        public virtual CategoryMarketplace CategoryMarketPlace { get; set; }
        public virtual CategorySeller CategorySeller { get; set; }
        public virtual Event Event { get; set; }
        public virtual ProductType ProductType { get; set; }
        public virtual Seller Seller { get; set; }
        public virtual SellerAddress SellerAddress { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<BannerMarketplaceProduct> BannerMarketplaceProduct { get; set; }
        public virtual ICollection<CategoryMarketplaceProduct> CategoryMarketplaceProduct { get; set; }
        public virtual ICollection<CategorySellerProduct> CategorySellerProduct { get; set; }
        public virtual ICollection<CouponType> CouponType { get; set; }
        public virtual ICollection<CustomerReviewProduct> CustomerReviewProduct { get; set; }
        public virtual ICollection<CustomerUserViewHistoryLog> CustomerUserViewHistoryLog { get; set; }
        public virtual ICollection<FavoriteProduct> FavoriteProduct { get; set; }
        public virtual ICollection<MarketplacePromotion> MarketplacePromotion { get; set; }
        public virtual ICollection<ProductImage> ProductImage { get; set; }
        public virtual ICollection<ProductRating> ProductRating { get; set; }
        public virtual ICollection<ProductSold> ProductSold { get; set; }
        public virtual ICollection<ProductVariant> ProductVariant { get; set; }
        public virtual ICollection<ProductWishlist> ProductWishlist { get; set; }
        public virtual ICollection<PromotionPrice> PromotionPrice { get; set; }
        public virtual ICollection<RelateCategory> RelateCategory { get; set; }
        public virtual ICollection<RelateProduct> RelateProductProduct { get; set; }
        public virtual ICollection<RelateProduct> RelateProductRelateProductNavigation { get; set; }
        public virtual ICollection<SellerPromotion> SellerPromotion { get; set; }
    }
}
