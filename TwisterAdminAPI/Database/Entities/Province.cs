﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class Province
    {
        public Province()
        {
            City = new HashSet<City>();
            CustomerAddress = new HashSet<CustomerAddress>();
            LogisticCompany = new HashSet<LogisticCompany>();
            ProvinceLanguage = new HashSet<ProvinceLanguage>();
            SellerAddress = new HashSet<SellerAddress>();
            SellerProductDelivery = new HashSet<SellerProductDelivery>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int CountryId { get; set; }

        public virtual Country Country { get; set; }
        public virtual ICollection<City> City { get; set; }
        public virtual ICollection<CustomerAddress> CustomerAddress { get; set; }
        public virtual ICollection<LogisticCompany> LogisticCompany { get; set; }
        public virtual ICollection<ProvinceLanguage> ProvinceLanguage { get; set; }
        public virtual ICollection<SellerAddress> SellerAddress { get; set; }
        public virtual ICollection<SellerProductDelivery> SellerProductDelivery { get; set; }
    }
}
