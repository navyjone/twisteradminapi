﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class RelateCategory
    {
        public int ProductId { get; set; }
        public int CatagoryMarketplaceId { get; set; }

        public virtual CategoryMarketplace CatagoryMarketplace { get; set; }
        public virtual Product Product { get; set; }
    }
}
