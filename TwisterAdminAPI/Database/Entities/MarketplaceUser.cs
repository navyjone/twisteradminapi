﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class MarketplaceUser
    {
        public MarketplaceUser()
        {
            AdminChangeLog = new HashSet<AdminChangeLog>();
            BannerMarketplace = new HashSet<BannerMarketplace>();
            ChatCustomerService = new HashSet<ChatCustomerService>();
            MarketplaceCouponGroup = new HashSet<MarketplaceCouponGroup>();
            MarketplacePromotion = new HashSet<MarketplacePromotion>();
            MarketplaceReviewSeller = new HashSet<MarketplaceReviewSeller>();
            SellerCoupon = new HashSet<SellerCoupon>();
            SellerPromotion = new HashSet<SellerPromotion>();
            AdminRefreshToken = new HashSet<AdminRefreshToken>();
        }

        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int StatusId { get; set; }
        public string Name { get; set; }
        public int CustomerUserId { get; set; }
        public int RoleClassId { get; set; }

        public virtual CustomerUser CustomerUser { get; set; }
        public virtual RoleClass RoleClass { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<AdminChangeLog> AdminChangeLog { get; set; }
        public virtual ICollection<BannerMarketplace> BannerMarketplace { get; set; }
        public virtual ICollection<ChatCustomerService> ChatCustomerService { get; set; }
        public virtual ICollection<MarketplaceCouponGroup> MarketplaceCouponGroup { get; set; }
        public virtual ICollection<MarketplacePromotion> MarketplacePromotion { get; set; }
        public virtual ICollection<MarketplaceReviewSeller> MarketplaceReviewSeller { get; set; }
        public virtual ICollection<SellerCoupon> SellerCoupon { get; set; }
        public virtual ICollection<SellerPromotion> SellerPromotion { get; set; }
        public virtual ICollection<AdminRefreshToken> AdminRefreshToken { get; set; }
    }
}
