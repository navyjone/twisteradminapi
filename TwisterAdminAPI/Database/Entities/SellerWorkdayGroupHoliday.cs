﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class SellerWorkdayGroupHoliday
    {
        public int Id { get; set; }
        public int SellerWorkdayGroupId { get; set; }
        public DateTime Date { get; set; }
        public string Name { get; set; }

        public virtual SellerWorkdayGroup SellerWorkdayGroup { get; set; }
    }
}
