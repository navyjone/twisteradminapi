﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class SaleOrderDiscount
    {
        public int SaleOrderId { get; set; }
        public int DiscountTypeId { get; set; }
        public int DiscountId { get; set; }
        public decimal? DiscountAmount { get; set; }

        public virtual SaleOrder SaleOrder { get; set; }
        public virtual DiscountType DiscountType { get; set; }
    }
}
