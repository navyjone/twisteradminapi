﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class BannerSeller
    {
        public BannerSeller()
        {
            BannerFoodSeller = new HashSet<BannerFoodSeller>();
        }

        public int Id { get; set; }
        public int SellerId { get; set; }
        public string Image { get; set; }
        public string Link { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int StatusId { get; set; }
        public string Reference { get; set; }
        public int? Sequence { get; set; }

        public virtual Seller Seller { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<BannerFoodSeller> BannerFoodSeller { get; set; }
    }
}
