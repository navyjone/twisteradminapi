﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class MarketplaceCoupon
    {
        public int Id { get; set; }
        public int MarketplaceCouponGroupId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int? CustomerUserId { get; set; }
        public string RedeemCode { get; set; }
        public short IsRedeemed { get; set; }

        public virtual CustomerUser CustomerUser { get; set; }
        public virtual MarketplaceCouponGroup MarketplaceCouponGroup { get; set; }
    }
}
