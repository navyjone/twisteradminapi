﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class Stock
    {
        public int Id { get; set; }
        public int ProductVariantId { get; set; }
        public int Quantity { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int StatusId { get; set; }
        public int StockTypeId { get; set; }
        public int SaleOrderId { get; set; }

        public virtual ProductVariant ProductVariant { get; set; }
        public virtual SaleOrder SaleOrder { get; set; }
        public virtual Status Status { get; set; }
        public virtual StockType StockType { get; set; }
    }
}
