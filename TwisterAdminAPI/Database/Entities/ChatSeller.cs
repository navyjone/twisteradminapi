﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class ChatSeller
    {
        public int Id { get; set; }
        public int SellerId { get; set; }
        public int ProductVariantId { get; set; }
        public string Message { get; set; }
        public int CustomerServiceId { get; set; }
        public DateTime CreateDate { get; set; }
        public string Owner { get; set; }
        public int StatusId { get; set; }
        public DateTime UpdateDate { get; set; }

        public virtual ProductVariant ProductVariant { get; set; }
        public virtual Seller Seller { get; set; }
        public virtual Status Status { get; set; }
    }
}
