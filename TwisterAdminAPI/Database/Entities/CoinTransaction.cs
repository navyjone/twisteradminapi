﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class CoinTransaction
    {
        public int Id { get; set; }
        public int CustomerUserId { get; set; }
        public int? SaleOrderId { get; set; }
        public int? LogOpenId { get; set; }
        public decimal Amount { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int StatusId { get; set; }
        public decimal Balance { get; set; }

        public virtual CustomerUser CustomerUser { get; set; }
        public virtual SaleOrder SaleOrder { get; set; }
        public virtual Status Status { get; set; }
    }
}
