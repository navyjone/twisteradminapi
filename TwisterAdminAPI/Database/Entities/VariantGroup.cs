﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public partial class VariantGroup
    {
        public int ProductVariantId { get; set; }
        public int VariantId { get; set; }

        public virtual ProductVariant ProductVariant { get; set; }
        public virtual Variant Variant { get; set; }
    }
}
