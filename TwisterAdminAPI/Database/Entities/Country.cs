﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class Country
    {
        public Country()
        {
            CountryLanguage = new HashSet<CountryLanguage>();
            CustomerAddress = new HashSet<CustomerAddress>();
            CustomerUser = new HashSet<CustomerUser>();
            LogisticCompany = new HashSet<LogisticCompany>();
            Province = new HashSet<Province>();
            SellerAddress = new HashSet<SellerAddress>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<CountryLanguage> CountryLanguage { get; set; }
        public virtual ICollection<CustomerAddress> CustomerAddress { get; set; }
        public virtual ICollection<CustomerUser> CustomerUser { get; set; }
        public virtual ICollection<LogisticCompany> LogisticCompany { get; set; }
        public virtual ICollection<Province> Province { get; set; }
        public virtual ICollection<SellerAddress> SellerAddress { get; set; }
    }
}
