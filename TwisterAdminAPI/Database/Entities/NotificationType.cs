﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class NotificationType
    {
        public NotificationType()
        {
            NotificationManual = new HashSet<NotificationManual>();
            NotificationTemplate = new HashSet<NotificationTemplate>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<NotificationManual> NotificationManual { get; set; }
        public virtual ICollection<NotificationTemplate> NotificationTemplate { get; set; }

    }
}
