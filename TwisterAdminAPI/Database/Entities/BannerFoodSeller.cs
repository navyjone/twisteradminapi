﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class BannerFoodSeller
    {
        public int Id { get; set; }
        public int? BannerMarketplaceId { get; set; }
        public int? BannerSellerId { get; set; }
        public int? SellerId { get; set; }
        public int? SellerGroupId { get; set; }

        public virtual BannerMarketplace BannerMarketplace { get; set; }
        public virtual BannerSeller BannerSeller { get; set; }
        public virtual Seller Seller { get; set; }
        public virtual SellerGroup SellerGroup { get; set; }
    }
}
