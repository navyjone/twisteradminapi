﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class CityLanguage
    {
        public int CityId { get; set; }
        public int LanguageId { get; set; }
        public string Name { get; set; }

        public virtual City City { get; set; }
        public virtual Language Language { get; set; }
    }
}
