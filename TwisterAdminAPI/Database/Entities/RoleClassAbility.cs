﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class RoleClassAbility
    {
        public int RoleClassId { get; set; }
        public int RoleAbilityId { get; set; }

        public virtual RoleAbility RoleAbility { get; set; }
        public virtual RoleClass RoleClass { get; set; }
    }
}
