﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class TownLanguage
    {
        public int TownId { get; set; }
        public int LanguageId { get; set; }
        public string Name { get; set; }

        public virtual Language Language { get; set; }
        public virtual Town Town { get; set; }
    }
}
