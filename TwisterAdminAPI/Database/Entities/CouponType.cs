﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class CouponType
    {
        public int Id { get; set; }
        public int CouponPriceId { get; set; }
        public int? CategoryMarketplaceId { get; set; }
        public int? ProductId { get; set; }
        public int? SellerId { get; set; }
        public short? WholeCart { get; set; }

        public virtual CouponPrice CouponPrice { get; set; }
        public virtual CategoryMarketplace CategoryMarketplace { get; set; }
        public virtual Product Product { get; set; }
        public virtual Seller Seller { get; set; }

    }
}
