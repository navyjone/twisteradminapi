﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class SellerPromotion
    {
        public SellerPromotion()
        {
            SellerPromotionRedeem = new HashSet<SellerPromotionRedeem>();
        }

        public int Id { get; set; }
        public int ProductId { get; set; }
        public int? MinQuantity { get; set; }
        public int PricePromotionId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int StatusId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public int MarketplaceUserId { get; set; }
        public int CategorySellerId { get; set; }

        public virtual CategoryMarketplace CategorySeller { get; set; }
        public virtual MarketplaceUser MarketplaceUser { get; set; }
        public virtual PromotionPrice PricePromotion { get; set; }
        public virtual Product Product { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<SellerPromotionRedeem> SellerPromotionRedeem { get; set; }
    }
}
