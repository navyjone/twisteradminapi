﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class CustomerAddress
    {
        public CustomerAddress()
        {
            SaleOrder = new HashSet<SaleOrder>();
        }

        public int Id { get; set; }
        public int CustomerUserId { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public int? CityId { get; set; }
        public int? ProvinceId { get; set; }
        public int? CountryId { get; set; }
        public string Zipcode { get; set; }
        public string TaxNumber { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        public DateTimeOffset UpdateDate { get; set; }
        public int StatusId { get; set; }
        public int? TownId { get; set; }

        public virtual City City { get; set; }
        public virtual Country Country { get; set; }
        public virtual CustomerUser CustomerUser { get; set; }
        public virtual Province Province { get; set; }
        public virtual Status Status { get; set; }
        public virtual Town Town { get; set; }

        public virtual ICollection<SaleOrder> SaleOrder { get; set; }
    }
}
