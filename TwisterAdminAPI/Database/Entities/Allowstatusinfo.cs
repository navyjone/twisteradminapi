﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class Allowstatusinfo
    {
        public long Transnum { get; set; }
        public string StatusCode { get; set; }
        public string StatusDescription { get; set; }
        public string StatusComment { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTimeOffset? UpdatedDate { get; set; }
    }
}
