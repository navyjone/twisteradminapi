﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class CreditCard
    {
        public CreditCard()
        {
            Payment = new HashSet<Payment>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string ExpireDate { get; set; }
        public string Number { get; set; }
        public string Token { get; set; }
        public int CustomerUserId { get; set; }
        public int StatusId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }

        public virtual CustomerUser CustomerUser { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<Payment> Payment { get; set; }
    }
}
