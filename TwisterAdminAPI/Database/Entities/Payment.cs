﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class Payment
    {
        public Payment()
        {
            SaleOrder = new HashSet<SaleOrder>();
        }

        public int Id { get; set; }
        public int? CreditCardId { get; set; }
        public string BankName { get; set; }
        public string ReferanceNumber { get; set; }
        public DateTime CreateDate { get; set; }
        public int PaymentTypeId { get; set; }
        public int? GatewayProviderId { get; set; }
        public DateTime? UpdateDate { get; set; }

        public virtual CreditCard CreditCard { get; set; }
        public virtual GatewayProvider GatewayProvider { get; set; }
        public virtual PaymentType PaymentType { get; set; }
        public virtual ICollection<SaleOrder> SaleOrder { get; set; }
    }
}
