﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class PurchaseOrderProductActualQuantity
    {
        public int PurchaseOrderProductId { get; set; }
        public int ActualQuantity { get; set; }

        public virtual PurchaseOrderProduct PurchaseOrderProduct { get; set; }
    }
}
