﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class PurchaseOrderTracking
    {
        public PurchaseOrderTracking()
        {
            PurchaseOrderProductPiece = new HashSet<PurchaseOrderProductPiece>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public int StatusId { get; set; }
        public int PurchaseOrderId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string StaticCode { get; set; }

        public virtual PurchaseOrder PurchaseOrder { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<PurchaseOrderProductPiece> PurchaseOrderProductPiece { get; set; }
    }
}
