﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class SellerUser
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int SellerId { get; set; }
        public int CustomerUserId { get; set; }
        public int RoleClassId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int StatusId { get; set; }

        public virtual CustomerUser CustomerUser { get; set; }
        public virtual RoleClass RoleClass { get; set; }
        public virtual Seller Seller { get; set; }
        public virtual Status Status { get; set; }
    }
}
