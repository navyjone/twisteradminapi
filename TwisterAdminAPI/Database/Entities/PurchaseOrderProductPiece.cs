﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class PurchaseOrderProductPiece
    {
        public int Id { get; set; }
        public int PurchaseOrderProductId { get; set; }
        public int StatusId { get; set; }
        public int? PurchaseOrdertrackingId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }

        public virtual PurchaseOrderProduct PurchaseOrderProduct { get; set; }
        public virtual PurchaseOrderTracking PurchaseOrdertracking { get; set; }
        public virtual Status Status { get; set; }
    }
}
