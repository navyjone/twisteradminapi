﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class CustomerCompany
    {
        public long Id { get; set; }
        public string Prefix { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CardId { get; set; }
        public string Address { get; set; }
        public string Company { get; set; }
        public string Brand { get; set; }
        public string Group { get; set; }
        public int? Status { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTimeOffset? UpdatedDate { get; set; }
    }
}
