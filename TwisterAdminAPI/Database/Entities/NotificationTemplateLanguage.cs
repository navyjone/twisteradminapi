﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class NotificationTemplateLanguage
    {
        public int Id { get; set; }
        public int NotificationTemplateId { get; set; }
        public int LanguageId { get; set; }
        public string Topic { get; set; }
        public string Message { get; set; }

        public virtual Language Language { get; set; }
        public virtual NotificationTemplate NotificationTemplate { get; set; }
    }
}
