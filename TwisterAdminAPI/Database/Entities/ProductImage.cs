﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class ProductImage
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public short IsCover { get; set; }
        public int Sequence { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int StatusId { get; set; }
        public int MediaTypeId { get; set; }
        public short IsIcon { get; set; }

        public virtual Product Product { get; set; }
        public virtual Status Status { get; set; }
        public virtual MediaType MediaType { get; set; }
    }
}
