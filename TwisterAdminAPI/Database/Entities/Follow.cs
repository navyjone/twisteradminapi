﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class Follow
    {
        public int CutomerUserId { get; set; }
        public int SellerId { get; set; }

        public virtual CustomerUser CutomerUser { get; set; }
        public virtual Seller Seller { get; set; }
    }
}
