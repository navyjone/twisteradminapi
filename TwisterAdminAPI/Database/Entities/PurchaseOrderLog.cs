﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class PurchaseOrderLog
    {
        public int Id { get; set; }
        public int PurchaseOrderId { get; set; }
        public int StatusId { get; set; }
        public DateTime CreateDate { get; set; }
        public int? LogisticUserId { get; set; }

        public virtual LogisticUser LogisticUser { get; set; }
        public virtual PurchaseOrder PurchaseOrder { get; set; }
    }
}
