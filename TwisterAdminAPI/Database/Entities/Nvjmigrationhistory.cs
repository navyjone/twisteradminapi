﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class Nvjmigrationhistory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTimeOffset RunDate { get; set; }
    }
}
