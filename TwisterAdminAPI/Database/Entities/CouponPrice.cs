﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class CouponPrice
    {
        public CouponPrice()
        {
            CouponType = new HashSet<CouponType>();
            MarketplaceCouponGroup = new HashSet<MarketplaceCouponGroup>();
            SellerCoupon = new HashSet<SellerCoupon>();
        }

        public int Id { get; set; }
        public decimal? DiscountPercent { get; set; }
        public decimal? DiscountTotal { get; set; }
        public decimal? DiscountLimit { get; set; }
        public decimal? MinimumPurchase { get; set; }

        public virtual ICollection<CouponType> CouponType { get; set; }
        public virtual ICollection<MarketplaceCouponGroup> MarketplaceCouponGroup { get; set; }
        public virtual ICollection<SellerCoupon> SellerCoupon { get; set; }
    }
}
