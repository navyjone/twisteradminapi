﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class BrandLanguage
    {
        public int BrandId { get; set; }
        public int LanguageId { get; set; }
        public string Name { get; set; }

        public virtual Brand Brand { get; set; }
        public virtual Language Language { get; set; }
    }
}
