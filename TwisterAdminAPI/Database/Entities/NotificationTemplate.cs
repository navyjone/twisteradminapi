﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class NotificationTemplate
    {
        public NotificationTemplate()
        {
            NotificationCustomer = new HashSet<NotificationCustomer>();
            NotificationSeller = new HashSet<NotificationSeller>();
            NotificationTemplateLanguage = new HashSet<NotificationTemplateLanguage>();
        }

        public int Id { get; set; }
        public int? NotificationTypeId { get; set; }
        public string Method { get; set; }
        public string Description { get; set; }
        public int? AppDeepLinkId { get; set; }
        public DateTime CreateDate { get; set; }

        public virtual AppDeepLink AppDeepLink { get; set; }
        public virtual NotificationType NotificationType { get; set; }
        public virtual ICollection<NotificationCustomer> NotificationCustomer { get; set; }
        public virtual ICollection<NotificationSeller> NotificationSeller { get; set; }
        public virtual ICollection<NotificationTemplateLanguage> NotificationTemplateLanguage { get; set; }
    }
}
