﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public partial class GroupBuyingProductVariant
    {
        public GroupBuyingProductVariant()
        {
            GroupBuying = new HashSet<GroupBuying>();
            GroupBuyingReward = new HashSet<GroupBuyingReward>();
        }

        public int Id { get; set; }
        public int ProductVariantId { get; set; }
        public int GroupBuyingConditionId { get; set; }
        public int DurationInMinutes { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        public DateTimeOffset UpdateDate { get; set; }
        public int StatusId { get; set; }

        public ProductVariant ProductVariant { get; set; }
        public GroupBuyingCondition GroupBuyingCondition { get; set; }
        public Status Status { get; set; }
        public virtual ICollection<GroupBuying> GroupBuying { get; set; }
        public virtual ICollection<GroupBuyingReward> GroupBuyingReward { get; set; }
    }
}
