﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class Point
    {
        public int Id { get; set; }
        public int CustomerUserId { get; set; }
        public int _Point { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        public DateTimeOffset UpdateDate { get; set; }
        public int StatusId { get; set; }
        public DateTime? ExpireDate { get; set; }
        public int? SaleOrderId { get; set; }

        public virtual CustomerUser CustomerUser { get; set; }
        public virtual SaleOrder SaleOrder { get; set; }
        public virtual Status Status { get; set; }
    }
}
