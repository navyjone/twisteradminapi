﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class Webuser
    {
        public long Id { get; set; }
        public string Telephone { get; set; }
        public string Password { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Status { get; set; }
        public string Authorize { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTimeOffset? UpdatedDate { get; set; }
    }
}
