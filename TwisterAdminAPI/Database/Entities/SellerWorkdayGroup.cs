﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class SellerWorkdayGroup
    {
        public SellerWorkdayGroup()
        {
            Seller = new HashSet<Seller>();
            SellerWorkdayGroupHoliday = new HashSet<SellerWorkdayGroupHoliday>();
            SellerWorkdayProfileGroup = new HashSet<SellerWorkdayProfileGroup>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int? WorkingDayId { get; set; }

        public virtual WorkingDay WorkingDay { get; set; }
        public virtual ICollection<Seller> Seller { get; set; }
        public virtual ICollection<SellerWorkdayGroupHoliday> SellerWorkdayGroupHoliday { get; set; }
        public virtual ICollection<SellerWorkdayProfileGroup> SellerWorkdayProfileGroup { get; set; }
    }
}
