﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class Brand
    {
        public Brand ()
        {
            BrandLanguage = new HashSet<BrandLanguage>();
            Product = new HashSet<Product>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Keyword { get; set; }
        public string Image { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Sequence { get; set; }
        public int StatusId { get; set; }

        public virtual Status Status { get; set; }
        public virtual ICollection<BrandLanguage> BrandLanguage { get; set; }
        public virtual ICollection<Product> Product { get; set; }
    }
}
