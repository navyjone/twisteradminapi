﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class PromotionPrice
    {
        public PromotionPrice()
        {
            MarketplacePromotion = new HashSet<MarketplacePromotion>();
            SellerPromotion = new HashSet<SellerPromotion>();
        }

        public int Id { get; set; }
        public int ProductId { get; set; }
        public int? DiscountPercent { get; set; }
        public int? DiscountTotal { get; set; }
        public int? DiscountLimit { get; set; }

        public virtual Product Product { get; set; }
        public virtual ICollection<MarketplacePromotion> MarketplacePromotion { get; set; }
        public virtual ICollection<SellerPromotion> SellerPromotion { get; set; }
    }
}
