﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class PurchaseOrderProduct
    {
        public PurchaseOrderProduct()
        {
            PurchaseOrderProductPiece = new HashSet<PurchaseOrderProductPiece>();
        }

        public int Id { get; set; }
        public int PurchaseOrderId { get; set; }
        public int ProductVariantId { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal Discount { get; set; }
        public decimal? NetPrice { get; set; }
        public decimal? Vat { get; set; }
        public int Quantity { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public decimal Price { get; set; }
        public string Variant { get; set; }
        public string Name { get; set; }
        public string Sku { get; set; }
        public string Remark { get; set; }
        public string Image { get; set; }

        public virtual ProductVariant ProductVariant { get; set; }
        public virtual PurchaseOrder PurchaseOrder { get; set; }
        public virtual PurchaseOrderProductActualQuantity PurchaseOrderProductActualQuantity { get; set; }
        public virtual ICollection<PurchaseOrderProductPiece> PurchaseOrderProductPiece { get; set; }
    }
}
