﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class BannerMarketplace
    {
        public BannerMarketplace()
        {
            BannerFoodSeller = new HashSet<BannerFoodSeller>();
            BannerMarketplaceProduct = new HashSet<BannerMarketplaceProduct>();
        }

        public int Id { get; set; }
        public int MarketplaceUserId { get; set; }
        public string Image { get; set; }
        public string Link { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int StatusId { get; set; }
        public string Reference { get; set; }
        public int? Sequence { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Name { get; set; }

        public virtual MarketplaceUser MarketplaceUser { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<BannerFoodSeller> BannerFoodSeller { get; set; }
        public virtual ICollection<BannerMarketplaceProduct> BannerMarketplaceProduct { get; set; }
    }
}
