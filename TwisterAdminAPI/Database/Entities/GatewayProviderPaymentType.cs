﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class GatewayProviderPaymentType
    {
        public int Id { get; set; }
        public int PaymentTypeId { get; set; }
        public int GatewayProviderId { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public int StatusId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }

        public virtual GatewayProvider GatewayProvider { get; set; }
        public virtual PaymentType PaymentType { get; set; }
        public virtual Status Status { get; set; }
    }
}
