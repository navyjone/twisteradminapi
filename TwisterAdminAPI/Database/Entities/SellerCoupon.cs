﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class SellerCoupon
    {
        public SellerCoupon()
        {
            SellerCouponRedeem = new HashSet<SellerCouponRedeem>();
        }

        public int Id { get; set; }
        public int SellerId { get; set; }
        public int Quantity { get; set; }
        public string RedeemCode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int StatusId { get; set; }
        public int MarketplaceUserId { get; set; }
        public int PriceCouponId { get; set; }
        public string Name { get; set; }

        public virtual MarketplaceUser MarketplaceUser { get; set; }
        public virtual CouponPrice PriceCoupon { get; set; }
        public virtual Seller Seller { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<SellerCouponRedeem> SellerCouponRedeem { get; set; }
    }
}
