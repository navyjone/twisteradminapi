﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public partial class GroupBuyingInvitation
    {
        public int Id { get; set; }
        public int GroupBuyingId { get; set; }
        public int InviteeCustomerUserId { get; set; }
        public int InviterCustomerUserId { get; set; }
        public int? InviteeSaleOrderId { get; set; }
        public int? ParentCustomerUserId { get; set; }

        public virtual GroupBuying GroupBuying { get; set; }
        public virtual CustomerUser InviteeCustomerUser { get; set; }
        public virtual CustomerUser InviterCustomerUser { get; set; }
        public virtual SaleOrder InviteeSaleOrder { get; set; }
        public virtual CustomerUser ParentCustomerUser { get; set; }
    }
}
