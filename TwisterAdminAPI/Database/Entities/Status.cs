﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class Status
    {
        public Status()
        {
            BannerMarketplace = new HashSet<BannerMarketplace>();
            BannerSeller = new HashSet<BannerSeller>();
            Brand = new HashSet<Brand>();
            Cart = new HashSet<Cart>();
            CategoryMarketplace = new HashSet<CategoryMarketplace>();
            CategorySeller = new HashSet<CategorySeller>();
            ChatCustomerService = new HashSet<ChatCustomerService>();
            ChatSeller = new HashSet<ChatSeller>();
            CoinTransaction = new HashSet<CoinTransaction>();
            CreditCard = new HashSet<CreditCard>();
            CreditNote = new HashSet<CreditNote>();
            CustomerAddress = new HashSet<CustomerAddress>();
            CustomerReviewProduct = new HashSet<CustomerReviewProduct>();
            CustomerReviewSeller = new HashSet<CustomerReviewSeller>();
            CustomerUser = new HashSet<CustomerUser>();
            Event = new HashSet<Event>();
            GatewayProvider = new HashSet<GatewayProvider>();
            GatewayProviderPaymentType = new HashSet<GatewayProviderPaymentType>();
            GroupBuyingProductVariant = new HashSet<GroupBuyingProductVariant>();
            GroupBuying = new HashSet<GroupBuying>();
            Groupon = new HashSet<Groupon>();
            InvoiceReceipt = new HashSet<InvoiceReceipt>();
            LogisticCompany = new HashSet<LogisticCompany>();
            LogisticUser = new HashSet<LogisticUser>();
            MarketplaceCouponGroup = new HashSet<MarketplaceCouponGroup>();
            MarketplacePromotion = new HashSet<MarketplacePromotion>();
            MarketplacePromotionRedeem = new HashSet<MarketplacePromotionRedeem>();
            MarketplaceReviewSeller = new HashSet<MarketplaceReviewSeller>();
            MarketplaceUser = new HashSet<MarketplaceUser>();
            NotificationCustomer = new HashSet<NotificationCustomer>();
            NotificationSeller = new HashSet<NotificationSeller>();
            Point = new HashSet<Point>();
            Product = new HashSet<Product>();
            ProductImage = new HashSet<ProductImage>();
            ProductVariant = new HashSet<ProductVariant>();
            PurchaseOrder = new HashSet<PurchaseOrder>();
            PurchaseOrderProductPiece = new HashSet<PurchaseOrderProductPiece>();
            PurchaseOrderTracking = new HashSet<PurchaseOrderTracking>();
            SaleOrder = new HashSet<SaleOrder>();
            SaleOrderProduct = new HashSet<SaleOrderProduct>();
            Seller = new HashSet<Seller>();
            SellerAddress = new HashSet<SellerAddress>();
            SellerCoupon = new HashSet<SellerCoupon>();
            SellerCouponRedeem = new HashSet<SellerCouponRedeem>();
            SellerGroup = new HashSet<SellerGroup>();
            SellerImage = new HashSet<SellerImage>();
            SellerPromotion = new HashSet<SellerPromotion>();
            SellerPromotionRedeem = new HashSet<SellerPromotionRedeem>();
            SellerUser = new HashSet<SellerUser>();
            StatusLanguage = new HashSet<StatusLanguage>();
            Stock = new HashSet<Stock>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }

        public virtual ICollection<BannerMarketplace> BannerMarketplace { get; set; }
        public virtual ICollection<BannerSeller> BannerSeller { get; set; }
        public virtual ICollection<Brand> Brand { get; set; }
        public virtual ICollection<Cart> Cart { get; set; }
        public virtual ICollection<CategoryMarketplace> CategoryMarketplace { get; set; }
        public virtual ICollection<CategorySeller> CategorySeller { get; set; }
        public virtual ICollection<ChatCustomerService> ChatCustomerService { get; set; }
        public virtual ICollection<ChatSeller> ChatSeller { get; set; }
        public virtual ICollection<CoinTransaction> CoinTransaction { get; set; }
        public virtual ICollection<CreditCard> CreditCard { get; set; }
        public virtual ICollection<CreditNote> CreditNote { get; set; }
        public virtual ICollection<CustomerAddress> CustomerAddress { get; set; }
        public virtual ICollection<CustomerReviewProduct> CustomerReviewProduct { get; set; }
        public virtual ICollection<CustomerReviewSeller> CustomerReviewSeller { get; set; }
        public virtual ICollection<CustomerUser> CustomerUser { get; set; }
        public virtual ICollection<Event> Event { get; set; }
        public virtual ICollection<GatewayProvider> GatewayProvider { get; set; }
        public virtual ICollection<GatewayProviderPaymentType> GatewayProviderPaymentType { get; set; }
        public virtual ICollection<GroupBuyingProductVariant> GroupBuyingProductVariant { get; set; }
        public virtual ICollection<GroupBuying> GroupBuying { get; set; }
        public virtual ICollection<Groupon> Groupon { get; set; }
        public virtual ICollection<InvoiceReceipt> InvoiceReceipt { get; set; }
        public virtual ICollection<LogisticCompany> LogisticCompany { get; set; }
        public virtual ICollection<LogisticUser> LogisticUser { get; set; }
        public virtual ICollection<MarketplaceCouponGroup> MarketplaceCouponGroup { get; set; }
        public virtual ICollection<MarketplacePromotion> MarketplacePromotion { get; set; }
        public virtual ICollection<MarketplacePromotionRedeem> MarketplacePromotionRedeem { get; set; }
        public virtual ICollection<MarketplaceReviewSeller> MarketplaceReviewSeller { get; set; }
        public virtual ICollection<MarketplaceUser> MarketplaceUser { get; set; }
        public virtual ICollection<NotificationCustomer> NotificationCustomer { get; set; }
        public virtual ICollection<NotificationSeller> NotificationSeller { get; set; }
        public virtual ICollection<Point> Point { get; set; }
        public virtual ICollection<Product> Product { get; set; }
        public virtual ICollection<ProductImage> ProductImage { get; set; }
        public virtual ICollection<ProductVariant> ProductVariant { get; set; }
        public virtual ICollection<PurchaseOrder> PurchaseOrder { get; set; }
        public virtual ICollection<PurchaseOrderProductPiece> PurchaseOrderProductPiece { get; set; }
        public virtual ICollection<PurchaseOrderTracking> PurchaseOrderTracking { get; set; }
        public virtual ICollection<SaleOrder> SaleOrder { get; set; }
        public virtual ICollection<SaleOrderProduct> SaleOrderProduct { get; set; }
        public virtual ICollection<Seller> Seller { get; set; }
        public virtual ICollection<SellerAddress> SellerAddress { get; set; }
        public virtual ICollection<SellerCoupon> SellerCoupon { get; set; }
        public virtual ICollection<SellerCouponRedeem> SellerCouponRedeem { get; set; }
        public virtual ICollection<SellerGroup> SellerGroup { get; set; }
        public virtual ICollection<SellerImage> SellerImage { get; set; }
        public virtual ICollection<SellerPromotion> SellerPromotion { get; set; }
        public virtual ICollection<SellerPromotionRedeem> SellerPromotionRedeem { get; set; }
        public virtual ICollection<SellerUser> SellerUser { get; set; }
        public virtual ICollection<StatusLanguage> StatusLanguage { get; set; }
        public virtual ICollection<Stock> Stock { get; set; }
    }
}
