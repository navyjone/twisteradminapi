﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class Condoinfo
    {
        public long Id { get; set; }
        public string CondoName { get; set; }
        public string Address { get; set; }
        public double? Lat { get; set; }
        public double? Lon { get; set; }
        public string LinkDelivery { get; set; }
        public string LinkOpen { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTimeOffset? UpdatedDate { get; set; }
    }
}
