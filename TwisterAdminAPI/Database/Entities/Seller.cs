﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class Seller
    {
        public Seller()
        {
            BannerFoodSeller = new HashSet<BannerFoodSeller>();
            BannerSeller = new HashSet<BannerSeller>();
            CategorySeller = new HashSet<CategorySeller>();
            ChatSeller = new HashSet<ChatSeller>();
            CouponType = new HashSet<CouponType>();
            CreditNote = new HashSet<CreditNote>();
            CustomerReviewSeller = new HashSet<CustomerReviewSeller>();
            Follow = new HashSet<Follow>();
            Groupon = new HashSet<Groupon>();
            InvoiceReceipt = new HashSet<InvoiceReceipt>();
            MarketplaceReviewSeller = new HashSet<MarketplaceReviewSeller>();
            NotificationSeller = new HashSet<NotificationSeller>();
            Product = new HashSet<Product>();
            PurchaseOrder = new HashSet<PurchaseOrder>();
            SellerAddress = new HashSet<SellerAddress>();
            SellerArea = new HashSet<SellerArea>();
            SellerCoupon = new HashSet<SellerCoupon>();
            SellerImage = new HashSet<SellerImage>();
            SellerProductDelivery = new HashSet<SellerProductDelivery>();
            SellerStats = new HashSet<SellerStats>();
            SellerUser = new HashSet<SellerUser>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int StatusId { get; set; }
        public string Image { get; set; }
        public int? Sequence { get; set; }
        public string Description { get; set; }
        public string TaxNumber { get; set; }
        public int? SellerGroupId { get; set; }
        public short? IsFood { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public int? SellerWorkdayGroupId { get; set; }

        public virtual SellerGroup SellerGroup { get; set; }
        public virtual SellerWorkdayGroup SellerWorkdayGroup { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<BannerFoodSeller> BannerFoodSeller { get; set; }
        public virtual ICollection<BannerSeller> BannerSeller { get; set; }
        public virtual ICollection<CategorySeller> CategorySeller { get; set; }
        public virtual ICollection<ChatSeller> ChatSeller { get; set; }
        public virtual ICollection<CouponType> CouponType { get; set; }
        public virtual ICollection<CreditNote> CreditNote { get; set; }
        public virtual ICollection<CustomerReviewSeller> CustomerReviewSeller { get; set; }
        public virtual ICollection<Follow> Follow { get; set; }
        public virtual ICollection<Groupon> Groupon { get; set; }
        public virtual ICollection<InvoiceReceipt> InvoiceReceipt { get; set; }
        public virtual ICollection<MarketplaceReviewSeller> MarketplaceReviewSeller { get; set; }
        public virtual ICollection<NotificationSeller> NotificationSeller { get; set; }
        public virtual ICollection<Product> Product { get; set; }
        public virtual ICollection<PurchaseOrder> PurchaseOrder { get; set; }
        public virtual ICollection<SellerAddress> SellerAddress { get; set; }
        public virtual ICollection<SellerArea> SellerArea { get; set; }
        public virtual ICollection<SellerCoupon> SellerCoupon { get; set; }
        public virtual ICollection<SellerImage> SellerImage { get; set; }
        public virtual ICollection<SellerProductDelivery> SellerProductDelivery { get; set; }
        public virtual ICollection<SellerStats> SellerStats { get; set; }
        public virtual ICollection<SellerUser> SellerUser { get; set; }
    }
}
