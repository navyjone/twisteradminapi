﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class Variant
    {
        public Variant()
        {
            VariantGroup = new HashSet<VariantGroup>();
        }

        public int Id { get; set; }
        public int VariantTypeId { get; set; }
        public string Value { get; set; }

        public virtual VariantType VariantType { get; set; }
        public virtual ICollection<VariantGroup> VariantGroup { get; set; }
    }
}
