﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class SaleOrderProduct
    {

        public SaleOrderProduct()
        {
            Groupon = new HashSet<Groupon>();
        }

        public int Id { get; set; }
        public int SaleOrderId { get; set; }
        public int ProductVariantId { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal Discount { get; set; }
        public decimal? NetPrice { get; set; }
        public decimal? Vat { get; set; }
        public int Quantity { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public decimal Price { get; set; }
        public string Variant { get; set; }
        public string Name { get; set; }
        public int StatusId { get; set; }
        public string Sku { get; set; }
        public string Remark { get; set; }
        public string Image { get; set; }

        public virtual ProductVariant ProductVariant { get; set; }
        public virtual SaleOrder SaleOrder { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<Groupon> Groupon { get; set; }
    }
}
