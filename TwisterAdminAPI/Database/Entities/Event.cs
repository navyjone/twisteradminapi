﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class Event
    {
        public Event()
        {
            Cart = new HashSet<Cart>();
            EventLocker = new HashSet<EventLocker>();
            Product = new HashSet<Product>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int StatusId { get; set; }
        public string Description { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Code { get; set; }

        public virtual Status Status { get; set; }
        public virtual ICollection<Cart> Cart { get; set; }
        public virtual ICollection<EventLocker> EventLocker { get; set; }
        public virtual ICollection<Product> Product { get; set; }
    }
}
