﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class City
    {
        public City()
        {
            CityLanguage = new HashSet<CityLanguage>();
            CustomerAddress = new HashSet<CustomerAddress>();
            LogisticCompany = new HashSet<LogisticCompany>();
            SellerAddress = new HashSet<SellerAddress>();
            Town = new HashSet<Town>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int ProvinceId { get; set; }
        public string Zipcode { get; set; }

        public virtual Province Province { get; set; }
        public virtual ICollection<CityLanguage> CityLanguage { get; set; }
        public virtual ICollection<CustomerAddress> CustomerAddress { get; set; }
        public virtual ICollection<LogisticCompany> LogisticCompany { get; set; }
        public virtual ICollection<SellerAddress> SellerAddress { get; set; }
        public virtual ICollection<Town> Town { get; set; }
    }
}
