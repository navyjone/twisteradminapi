﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class CategorySellerHierarchy
    {
        public int ParentId { get; set; }
        public int ChildrenId { get; set; }

        public virtual CategorySeller Children { get; set; }
        public virtual CategorySeller Parent { get; set; }
    }
}
