﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class VariantType
    {
        public VariantType()
        {
            Variant = new HashSet<Variant>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Variant> Variant { get; set; }
    }
}
