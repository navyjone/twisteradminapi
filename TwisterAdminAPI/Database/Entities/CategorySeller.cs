﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class CategorySeller
    {
        public CategorySeller()
        {
            CategorySellerHierarchyChildren = new HashSet<CategorySellerHierarchy>();
            CategorySellerHierarchyParent = new HashSet<CategorySellerHierarchy>();
            CategorySellerProduct = new HashSet<CategorySellerProduct>();
            Product = new HashSet<Product>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int StatusId { get; set; }
        public string Image { get; set; }
        public int? Sequence { get; set; }
        public int SellerId { get; set; }

        public virtual Seller Seller { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<CategorySellerHierarchy> CategorySellerHierarchyChildren { get; set; }
        public virtual ICollection<CategorySellerHierarchy> CategorySellerHierarchyParent { get; set; }
        public virtual ICollection<CategorySellerProduct> CategorySellerProduct { get; set; }
        public virtual ICollection<Product> Product { get; set; }
    }
}
