﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class CustomerSearchHistoryLog
    {
        public int Id { get; set; }
        public int CustomerUserId { get; set; }
        public string SearchWord { get; set; }

        public virtual CustomerUser CustomerUser { get; set; }
    }
}
