﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class SellerWorkdayProfile
    {
        public SellerWorkdayProfile()
        {
            SellerWorkdayProfileGroup = new HashSet<SellerWorkdayProfileGroup>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int? DayAmount { get; set; }
        public TimeSpan TimeStart { get; set; }
        public TimeSpan TimeEnd { get; set; }
        public int? MonthStart { get; set; }
        public int? MonthEnd { get; set; }

        public virtual ICollection<SellerWorkdayProfileGroup> SellerWorkdayProfileGroup { get; set; }
    }
}
