﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class AllowOpenLocker
    {
        public int Id { get; set; }
        public int? SaleOrderId { get; set; }
        public long? LogDeliveryId { get; set; }
        public string Telephone { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        public DateTimeOffset UpdateDate { get; set; }

        public virtual Logdelivery LogDelivery { get; set; }
        public virtual SaleOrder SaleOrder { get; set; }
    }
}
