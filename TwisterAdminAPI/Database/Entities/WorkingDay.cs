﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class WorkingDay
    {
        public WorkingDay()
        {
            SellerWorkdayGroup = new HashSet<SellerWorkdayGroup>();
        }

        public int Id { get; set; }
        public short Sunday { get; set; }
        public short Monday { get; set; }
        public short Tuesday { get; set; }
        public short Wednesday { get; set; }
        public short Thursday { get; set; }
        public short Friday { get; set; }
        public short Saturday { get; set; }
        public string Name { get; set; }

        public virtual ICollection<SellerWorkdayGroup> SellerWorkdayGroup { get; set; }

    }
}
