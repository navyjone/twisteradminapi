﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class CategoryMarketplaceHierarchy
    {
        public int ParentId { get; set; }
        public int ChildrenId { get; set; }

        public virtual CategoryMarketplace Children { get; set; }
        public virtual CategoryMarketplace Parent { get; set; }
    }
}
