﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class PurchaseOrderDelivered
    {
        public int PurchaseOrderId { get; set; }
        public string LockerCode { get; set; }
        public int BoxId { get; set; }
        public DateTime CreateDate { get; set; }

        public virtual PurchaseOrder PurchaseOrder { get; set; }
    }
}
