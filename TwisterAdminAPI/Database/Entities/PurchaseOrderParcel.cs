﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class PurchaseOrderParcel
    {
        public int Id { get; set; }
        public int PurchaseOrderId { get; set; }
        public string LockerCode { get; set; }
        public int BoxId { get; set; }
        public int Quantity { get; set; }
        public DateTime CreateDate { get; set; }

        public virtual PurchaseOrder PurchaseOrder { get; set; }

    }
}
