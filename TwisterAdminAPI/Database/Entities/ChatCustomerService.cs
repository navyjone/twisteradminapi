﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class ChatCustomerService
    {
        public int Id { get; set; }
        public int MarketplaceUserId { get; set; }
        public string Message { get; set; }
        public DateTime CreateDate { get; set; }
        public int CustomerUserId { get; set; }
        public string Owner { get; set; }
        public int StatusId { get; set; }
        public DateTime UpdateDate { get; set; }

        public virtual CustomerUser CustomerUser { get; set; }
        public virtual MarketplaceUser MarketplaceUser { get; set; }
        public virtual Status Status { get; set; }
    }
}
