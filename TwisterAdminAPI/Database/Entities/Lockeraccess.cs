﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class Lockeraccess
    {
        public long AccessId { get; set; }
        public string LockerCode { get; set; }
        public int? BoxId { get; set; }
        public string GuestTelnum { get; set; }
        public DateTime? ValidDate { get; set; }
        public DateTime? ExpireDate { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTimeOffset? UpdatedDate { get; set; }
    }
}
