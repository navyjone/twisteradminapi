﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public partial class MediaType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<ProductImage> ProductImage { get; set; }
        public virtual ICollection<SellerImage> SellerImage { get; set; }
    }
}
