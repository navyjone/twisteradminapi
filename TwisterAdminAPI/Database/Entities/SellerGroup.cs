﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class SellerGroup
    {
        public SellerGroup()
        {
            BannerFoodSeller = new HashSet<BannerFoodSeller>();
            Seller = new HashSet<Seller>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int StatusId { get; set; }

        public virtual Status Status { get; set; }
        public virtual ICollection<BannerFoodSeller> BannerFoodSeller { get; set; }
        public virtual ICollection<Seller> Seller { get; set; }
    }
}
