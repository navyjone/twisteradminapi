﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class DiscountType
    {
        public DiscountType()
        {
            SaleOrderDiscount = new HashSet<SaleOrderDiscount>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<SaleOrderDiscount> SaleOrderDiscount { get; set; }
    }
}
