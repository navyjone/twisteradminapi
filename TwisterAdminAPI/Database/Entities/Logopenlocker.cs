﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class Logopenlocker
    {
        public long OpenId { get; set; }
        public string LockerCode { get; set; }
        public int? BoxId { get; set; }
        public DateTime? OpenDate { get; set; }
        public long? TransnumDelivery { get; set; }
        public long? UserId { get; set; }
        public string UserType { get; set; }
        public string Status { get; set; }
        public int? Batterylevel { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTimeOffset? UpdatedDate { get; set; }
    }
}
