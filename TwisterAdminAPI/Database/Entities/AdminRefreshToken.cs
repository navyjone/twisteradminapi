﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public partial class AdminRefreshToken
    {
        public int Id { get; set; }
        public string Token { get; set; }
        public int MarketplaceUserId { get; set; }
        public DateTime ExpireDate { get; set; }
        public string DeviceName { get; set; }

        public virtual MarketplaceUser MarketplaceUser { get; set; }
    }
}

