﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class Language
    {
        public Language()
        {
            BrandLanguage = new HashSet<BrandLanguage>();
            CategoryMarketplaceLanguage = new HashSet<CategoryMarketplaceLanguage>();
            CityLanguage = new HashSet<CityLanguage>();
            CountryLanguage = new HashSet<CountryLanguage>();
            CustomerUser = new HashSet<CustomerUser>();
            NotificationManualLanguage = new HashSet<NotificationManualLanguage>();
            NotificationTemplateLanguage = new HashSet<NotificationTemplateLanguage>();
            ProvinceLanguage = new HashSet<ProvinceLanguage>();
            SellerStatsTypeLanguage = new HashSet<SellerStatsTypeLanguage>();
            StatusLanguage = new HashSet<StatusLanguage>();
            TownLanguage = new HashSet<TownLanguage>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<BrandLanguage> BrandLanguage { get; set; }
        public virtual ICollection<CategoryMarketplaceLanguage> CategoryMarketplaceLanguage { get; set; }
        public virtual ICollection<CityLanguage> CityLanguage { get; set; }
        public virtual ICollection<CountryLanguage> CountryLanguage { get; set; }
        public virtual ICollection<CustomerUser> CustomerUser { get; set; }
        public virtual ICollection<NotificationManualLanguage> NotificationManualLanguage { get; set; }
        public virtual ICollection<NotificationTemplateLanguage> NotificationTemplateLanguage { get; set; }
        public virtual ICollection<ProvinceLanguage> ProvinceLanguage { get; set; }
        public virtual ICollection<SellerStatsTypeLanguage> SellerStatsTypeLanguage { get; set; }
        public virtual ICollection<StatusLanguage> StatusLanguage { get; set; }
        public virtual ICollection<TownLanguage> TownLanguage { get; set; }
    }
}
