﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class CondoRate
    {
        public long Id { get; set; }
        public string LockerCode { get; set; }
        public int? BoxId { get; set; }
        public double? Rate { get; set; }
        public string Type { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTimeOffset? UpdatedDate { get; set; }
    }
}
