﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class Zone
    {
        public int Id { get; set; }
        public string Route { get; set; }
    }
}
