﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class SellerStatsType
    {
        public SellerStatsType()
        {
            SellerStats = new HashSet<SellerStats>();
            SellerStatsTypeLanguage = new HashSet<SellerStatsTypeLanguage>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<SellerStats> SellerStats { get; set; }
        public virtual ICollection<SellerStatsTypeLanguage> SellerStatsTypeLanguage { get; set; }
    }
}
