﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class Lockerinfo
    {
        public Lockerinfo()
        {
            EventLocker = new HashSet<EventLocker>();
        }

        public long LockerId { get; set; }
        public string LockerCode { get; set; }
        public string LockerTypeCode { get; set; }
        public string PublicName { get; set; }
        public string RegistryCode { get; set; }
        public string LockerPassword { get; set; }
        public string LockerAddress { get; set; }
        public int? ZoneId { get; set; }
        public double? LockerLat { get; set; }
        public double? LockerLon { get; set; }
        public string LockerActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTimeOffset? UpdatedDate { get; set; }

        public virtual ICollection<EventLocker> EventLocker { get; set; }
    }
}
