﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class SellerWorkdayProfileGroup
    {
        public int SellerWorkdayGroupId { get; set; }
        public int SellerWorkdayProfileId { get; set; }
        public int? DayAmount { get; set; }
        public TimeSpan TimeStart { get; set; }
        public TimeSpan TimeEnd { get; set; }
        public int? MonthStart { get; set; }
        public int? MonthEnd { get; set; }
        public int Priority { get; set; }

        public virtual SellerWorkdayGroup SellerWorkdayGroup { get; set; }
        public virtual SellerWorkdayProfile SellerWorkdayProfile { get; set; }
    }
}
