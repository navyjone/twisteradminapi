﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class SellerStatsTypeLanguage
    {
        public int Id { get; set; }
        public int SellerStatsTypeId { get; set; }
        public int LanguageId { get; set; }
        public string Name { get; set; }

        public virtual Language Language { get; set; }
        public virtual SellerStatsType SellerStatsType { get; set; }
    }
}
