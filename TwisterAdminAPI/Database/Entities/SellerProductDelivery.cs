﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class SellerProductDelivery
    {
        public int SellerId { get; set; }
        public int ProvinceId { get; set; }

        public virtual Province Province { get; set; }
        public virtual Seller Seller { get; set; }
    }
}
