﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class CustomerReviewSeller
    {
        public int Id { get; set; }
        public int CustomerUserId { get; set; }
        public int SellerId { get; set; }
        public int Rating { get; set; }
        public string Comment { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int StatusId { get; set; }

        public virtual CustomerUser CustomerUser { get; set; }
        public virtual Seller Seller { get; set; }
        public virtual Status Status { get; set; }
    }
}
