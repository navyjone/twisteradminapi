﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class CustomerUser
    {
        public CustomerUser()
        {
            Cart = new HashSet<Cart>();
            ChatCustomerService = new HashSet<ChatCustomerService>();
            CoinTransaction = new HashSet<CoinTransaction>();
            CreditCard = new HashSet<CreditCard>();
            CreditNote = new HashSet<CreditNote>();
            CustomerAddress = new HashSet<CustomerAddress>();
            CustomerReviewProduct = new HashSet<CustomerReviewProduct>();
            CustomerReviewSeller = new HashSet<CustomerReviewSeller>();
            CustomerSearchHistoryLog = new HashSet<CustomerSearchHistoryLog>();
            CustomerUserViewHistoryLog = new HashSet<CustomerUserViewHistoryLog>();
            FavoriteProduct = new HashSet<FavoriteProduct>();
            Follow = new HashSet<Follow>();
            GroupBuying = new HashSet<GroupBuying>();
            GroupBuyingInvitationInvitee = new HashSet<GroupBuyingInvitation>();
            GroupBuyingInvitationInviter = new HashSet<GroupBuyingInvitation>();
            GroupBuyingInvitationParent = new HashSet<GroupBuyingInvitation>();
            Groupon = new HashSet<Groupon>();
            InvoiceReceipt = new HashSet<InvoiceReceipt>();
            MarketplaceCoupon = new HashSet<MarketplaceCoupon>();
            MarketplaceUser = new HashSet<MarketplaceUser>();
            NotificationCustomer = new HashSet<NotificationCustomer>();
            PointNavigation = new HashSet<Point>();
            ProductRating = new HashSet<ProductRating>();
            ProductSold = new HashSet<ProductSold>();
            SaleOrder = new HashSet<SaleOrder>();
            SellerCouponRedeem = new HashSet<SellerCouponRedeem>();
            SellerUser = new HashSet<SellerUser>();
            ProductWishlist = new HashSet<ProductWishlist>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public string FacebookToken { get; set; }
        public string LineToken { get; set; }
        public string GoogleToken { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public DateTime? Birthday { get; set; }
        public string Sex { get; set; }
        public string CitizenId { get; set; }
        public string PassportId { get; set; }
        public int? CountryId { get; set; }
        public int? PassportIssuePlace { get; set; }
        public int? Age { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int StatusId { get; set; }
        public int Point { get; set; }
        public string RemoteNotificationId { get; set; }
        public string UniqueId { get; set; }
        public string Image { get; set; }
        public decimal CoinAmount { get; set; }
        public int? LanguageId { get; set; }
        public DateTime? LastLogin { get; set; }
        public string MobileAppVersion { get; set; }

        public virtual Country Country { get; set; }
        public virtual Language Language { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<Cart> Cart { get; set; }
        public virtual ICollection<ChatCustomerService> ChatCustomerService { get; set; }
        public virtual ICollection<CoinTransaction> CoinTransaction { get; set; }
        public virtual ICollection<CreditCard> CreditCard { get; set; }
        public virtual ICollection<CreditNote> CreditNote { get; set; }
        public virtual ICollection<CustomerAddress> CustomerAddress { get; set; }
        public virtual ICollection<CustomerReviewProduct> CustomerReviewProduct { get; set; }
        public virtual ICollection<CustomerReviewSeller> CustomerReviewSeller { get; set; }
        public virtual ICollection<CustomerSearchHistoryLog> CustomerSearchHistoryLog { get; set; }
        public virtual ICollection<CustomerUserViewHistoryLog> CustomerUserViewHistoryLog { get; set; }
        public virtual ICollection<FavoriteProduct> FavoriteProduct { get; set; }
        public virtual ICollection<Follow> Follow { get; set; }
        public virtual ICollection<GroupBuying> GroupBuying { get; set; }
        public virtual ICollection<GroupBuyingInvitation> GroupBuyingInvitationInvitee { get; set; }
        public virtual ICollection<GroupBuyingInvitation> GroupBuyingInvitationInviter { get; set; }
        public virtual ICollection<GroupBuyingInvitation> GroupBuyingInvitationParent { get; set; }
        public virtual ICollection<Groupon> Groupon { get; set; }
        public virtual ICollection<InvoiceReceipt> InvoiceReceipt { get; set; }
        public virtual ICollection<MarketplaceCoupon> MarketplaceCoupon { get; set; }
        public virtual ICollection<MarketplaceUser> MarketplaceUser { get; set; }
        public virtual ICollection<NotificationCustomer> NotificationCustomer { get; set; }
        public virtual ICollection<Point> PointNavigation { get; set; }
        public virtual ICollection<ProductRating> ProductRating { get; set; }
        public virtual ICollection<ProductSold> ProductSold { get; set; }
        public virtual ICollection<SaleOrder> SaleOrder { get; set; }
        public virtual ICollection<SellerCouponRedeem> SellerCouponRedeem { get; set; }
        public virtual ICollection<SellerUser> SellerUser { get; set; }
        public virtual ICollection<ProductWishlist> ProductWishlist { get; set; }
    }
}
