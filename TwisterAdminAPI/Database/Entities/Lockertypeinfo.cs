﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class Lockertypeinfo
    {
        public long LockerTypeId { get; set; }
        public string LockerTypeCode { get; set; }
        public string LockerTypeName { get; set; }
        public int? LockerQtyBox { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTimeOffset? UpdatedDate { get; set; }
    }
}
