﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class RoleAbility
    {
        public RoleAbility()
        {
            RoleClassAbility = new HashSet<RoleClassAbility>();
        }

        public int Id { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }

        public virtual ICollection<RoleClassAbility> RoleClassAbility { get; set; }
    }
}
