﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public partial class GroupBuyingReward
    {
        public int Id { get; set; }
        public int GroupBuyingProductVariantId { get; set; }
        public int GroupBuyingAchievementTypeId { get; set; }
        public decimal? SalePrice { get; set; }
        public int? Point { get; set; }
        public int? MarketplaceCouponGroupId { get; set; }
        public int? ProductVariantId { get; set; }

        public virtual GroupBuyingAchievementType GroupBuyingAchievementType { get; set; }
        public virtual GroupBuyingProductVariant GroupBuyingProductVariant { get; set; }
        public virtual MarketplaceCouponGroup MarketplaceCouponGroup { get; set; }
        public virtual ProductVariant ProductVariant { get; set; }
    }
}
