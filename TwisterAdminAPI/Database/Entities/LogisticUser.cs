﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class LogisticUser
    {
        public LogisticUser()
        {
            PurchaseOrderLog = new HashSet<PurchaseOrderLog>();
            PurchaseOrderLogisticUser = new HashSet<PurchaseOrderLogisticUser>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string CitizenId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int StatusId { get; set; }
        public int LogisticCompanyId { get; set; }

        public virtual LogisticCompany LogisticCompany { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<PurchaseOrderLog> PurchaseOrderLog { get; set; }
        public virtual ICollection<PurchaseOrderLogisticUser> PurchaseOrderLogisticUser { get; set; }
    }
}
