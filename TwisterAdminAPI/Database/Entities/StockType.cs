﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class StockType
    {
        public StockType()
        {
            Stock = new HashSet<Stock>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Stock> Stock { get; set; }
    }
}
