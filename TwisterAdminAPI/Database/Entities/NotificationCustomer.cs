﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class NotificationCustomer
    {
        public int Id { get; set; }
        public int CustomerUserId { get; set; }
        public int? NotificationTemplateId { get; set; }
        public int? NotificationManualId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int StatusId { get; set; }
        public string Param { get; set; }
        public string LinkParam { get; set; }

        public virtual CustomerUser CustomerUser { get; set; }
        public virtual NotificationManual NotificationManual { get; set; }
        public virtual NotificationTemplate NotificationTemplate { get; set; }
        public virtual Status Status { get; set; }
    }
}
