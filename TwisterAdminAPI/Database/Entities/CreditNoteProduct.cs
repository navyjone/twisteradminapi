﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class CreditNoteProduct
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public decimal Discount { get; set; }
        public decimal NetPrice { get; set; }
        public int CreditNoteId { get; set; }
        public decimal Vat { get; set; }

        public virtual CreditNote CreditNote { get; set; }
    }
}
