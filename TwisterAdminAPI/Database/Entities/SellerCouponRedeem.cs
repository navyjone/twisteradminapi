﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class SellerCouponRedeem
    {
        public int Id { get; set; }
        public int SellerCouponId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int StatusId { get; set; }
        public int CustomerUserId { get; set; }

        public virtual CustomerUser CustomerUser { get; set; }
        public virtual SellerCoupon SellerCoupon { get; set; }
        public virtual Status Status { get; set; }
    }
}
