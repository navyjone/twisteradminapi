﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class Town
    {
        public Town()
        {
            CustomerAddress = new HashSet<CustomerAddress>();
            LogisticCompany = new HashSet<LogisticCompany>();
            SellerAddress = new HashSet<SellerAddress>();
            TownLanguage = new HashSet<TownLanguage>();
        }

        public int Id { get; set; }
        public int CityId { get; set; }
        public string Name { get; set; }

        public virtual City City { get; set; }
        public virtual ICollection<CustomerAddress> CustomerAddress { get; set; }
        public virtual ICollection<LogisticCompany> LogisticCompany { get; set; }
        public virtual ICollection<SellerAddress> SellerAddress { get; set; }
        public virtual ICollection<TownLanguage> TownLanguage { get; set; }
    }
}
