﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class RoleClass
    {
        public RoleClass()
        {
            MarketplaceUser = new HashSet<MarketplaceUser>();
            RoleClassAbility = new HashSet<RoleClassAbility>();
            SellerUser = new HashSet<SellerUser>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<MarketplaceUser> MarketplaceUser { get; set; }
        public virtual ICollection<RoleClassAbility> RoleClassAbility { get; set; }
        public virtual ICollection<SellerUser> SellerUser { get; set; }
    }
}
