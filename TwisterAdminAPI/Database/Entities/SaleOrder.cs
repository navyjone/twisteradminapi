﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class SaleOrder
    {
        public SaleOrder()
        {
            AllowOpenLocker = new HashSet<AllowOpenLocker>();
            CoinTransaction = new HashSet<CoinTransaction>();
            CreditNote = new HashSet<CreditNote>();
            MarketplacePromotionRedeem = new HashSet<MarketplacePromotionRedeem>();
            InvoiceReceipt = new HashSet<InvoiceReceipt>();
            Point = new HashSet<Point>();
            PurchaseOrder = new HashSet<PurchaseOrder>();
            SaleOrderDiscount = new HashSet<SaleOrderDiscount>();
            GroupBuyingInvitation = new HashSet<GroupBuyingInvitation>();
            SaleOrderProduct = new HashSet<SaleOrderProduct>();
            SellerPromotionRedeem = new HashSet<SellerPromotionRedeem>();
            Stock = new HashSet<Stock>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string StaticCode { get; set; }
        public int? CartId { get; set; }
        public int CustomerUserId { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal Discount { get; set; }
        public decimal? NetPrice { get; set; }
        public decimal? Vat { get; set; }
        public decimal? ShippingFee { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int StatusId { get; set; }
        public string LockerCode { get; set; }
        public int? CustomerUserAddressId { get; set; }
        public int? PaymentId { get; set; }
        public string GatewayRequestCode { get; set; }
        public string GatewayReferenceStaticCode { get; set; }
        public long? LogDeliveryId { get; set; }
        public string Remark { get; set; }
        public string ShippingAddress { get; set; }
        public string BillingAddress { get; set; }
        public string ReceiverTelephone { get; set; }
        public string LinePayTransactionId { get; set; }

        public virtual Cart Cart { get; set; }
        public virtual CustomerUser CustomerUser { get; set; }
        public virtual CustomerAddress CustomerAddress { get; set; }
        public virtual Payment Payment { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<AllowOpenLocker> AllowOpenLocker { get; set; }
        public virtual ICollection<CoinTransaction> CoinTransaction { get; set; }
        public virtual ICollection<CreditNote> CreditNote { get; set; }
        public virtual ICollection<GroupBuyingInvitation> GroupBuyingInvitation { get; set; }
        public virtual ICollection<MarketplacePromotionRedeem> MarketplacePromotionRedeem { get; set; }
        public virtual ICollection<InvoiceReceipt> InvoiceReceipt { get; set; }
        public virtual ICollection<Point> Point { get; set; }
        public virtual ICollection<PurchaseOrder> PurchaseOrder { get; set; }
        public virtual ICollection<SaleOrderDiscount> SaleOrderDiscount { get; set; }
        public virtual ICollection<SaleOrderProduct> SaleOrderProduct { get; set; }
        public virtual ICollection<SellerPromotionRedeem> SellerPromotionRedeem { get; set; }
        public virtual ICollection<Stock> Stock { get; set; }
    }
}
