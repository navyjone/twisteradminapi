﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class SellerStats
    {
        public int SellerId { get; set; }
        public int SellerStatsTypeId { get; set; }
        public string Value { get; set; }
        public DateTime UpdateDate { get; set; }

        public virtual Seller Seller { get; set; }
        public virtual SellerStatsType SellerStatsType { get; set; }
    }
}
