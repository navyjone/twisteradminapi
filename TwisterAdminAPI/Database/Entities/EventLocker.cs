﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class EventLocker
    {
        public int EventId { get; set; }
        public long LockerId { get; set; }

        public virtual Event Event { get; set; }
        public virtual Lockerinfo Locker { get; set; }
    }
}
