﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public partial class AppDeepLink
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Link { get; set; }
        public string Example { get; set; }

        public virtual ICollection<NotificationManual> NotificationManual { get; set; }
        public virtual ICollection<NotificationTemplate> NotificationTemplate { get; set; }
    }
}
