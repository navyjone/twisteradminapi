﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class Logdelivery
    {
        public Logdelivery()
        {
            AllowOpenLocker = new HashSet<AllowOpenLocker>();
        }

        public long Transnum { get; set; }
        public string DeliveryId { get; set; }
        public string LockerCode { get; set; }
        public int? BoxId { get; set; }
        public DateTime? OpenDate { get; set; }
        public long? AccessId { get; set; }
        public string Status { get; set; }
        public long? UserId { get; set; }
        public string UserType { get; set; }
        public int? CustomerUserId { get; set; }
        public string RefDelivery { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTimeOffset? UpdatedDate { get; set; }

        public virtual ICollection<AllowOpenLocker> AllowOpenLocker { get; set; }
    }
}
