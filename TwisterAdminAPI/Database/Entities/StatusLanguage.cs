﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class StatusLanguage
    {
        public int StatusId { get; set; }
        public int LanguageId { get; set; }
        public string Name { get; set; }

        public virtual Language Language { get; set; }
        public virtual Status Status { get; set; }

    }
}
