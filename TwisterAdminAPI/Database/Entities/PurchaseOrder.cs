﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class PurchaseOrder
    {
        public PurchaseOrder()
        {
            PurchaseOrderDelivered = new HashSet<PurchaseOrderDelivered>();
            PurchaseOrderLog = new HashSet<PurchaseOrderLog>();
            PurchaseOrderParcel = new HashSet<PurchaseOrderParcel>();
            PurchaseOrderProduct = new HashSet<PurchaseOrderProduct>();
            PurchaseOrderTracking = new HashSet<PurchaseOrderTracking>();
        }

        public int Id { get; set; }
        public int SaleOrderId { get; set; }
        public string Code { get; set; }
        public string StaticCode { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal Discount { get; set; }
        public decimal? Vat { get; set; }
        public decimal? NetPrice { get; set; }
        public string Name { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Remark { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int StatusId { get; set; }
        public string MarketplaceAddress { get; set; }
        public string LockerCode { get; set; }
        public int? ParcelQuantity { get; set; }
        public int SellerId { get; set; }
        public string SellerName { get; set; }
        public string SellerAddress { get; set; }
        public string SellerTaxNumber { get; set; }
        public string ShippingAddress { get; set; }
        public string BillingAddress { get; set; }
        public string TrackingNumber { get; set; }
        public string LogisticCompanyName { get; set; }
        public int? LogisticCompanyId { get; set; }

        public virtual LogisticCompany LogisticCompany { get; set; }
        public virtual SaleOrder SaleOrder { get; set; }
        public virtual Seller Seller { get; set; }
        public virtual Status Status { get; set; }
        public virtual PurchaseOrderLogisticUser PurchaseOrderLogisticUser { get; set; }
        public virtual ICollection<PurchaseOrderDelivered> PurchaseOrderDelivered { get; set; }
        public virtual ICollection<PurchaseOrderLog> PurchaseOrderLog { get; set; }
        public virtual ICollection<PurchaseOrderParcel> PurchaseOrderParcel { get; set; }
        public virtual ICollection<PurchaseOrderProduct> PurchaseOrderProduct { get; set; }
        public virtual ICollection<PurchaseOrderTracking> PurchaseOrderTracking { get; set; }
    }
}
