﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class ProductWishlist
    {
        public int CustomerUserId { get; set; }
        public int ProductId { get; set; }

        public virtual CustomerUser CustomerUser { get; set; }
        public virtual Product Product { get; set; }
    }
}
