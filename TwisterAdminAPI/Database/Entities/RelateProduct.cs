﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class RelateProduct
    {
        public int ProductId { get; set; }
        public int RelateProductId { get; set; }

        public virtual Product Product { get; set; }
        public virtual Product RelateProductNavigation { get; set; }
    }
}
