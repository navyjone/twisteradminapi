﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class GatewayProvider
    {
        public GatewayProvider()
        {
            GatewayProviderPaymentType = new HashSet<GatewayProviderPaymentType>();
            Payment = new HashSet<Payment>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreateDate { get; set; }
        public int StatusId { get; set; }
        public DateTime? UpdateDate { get; set; }

        public virtual Status Status { get; set; }
        public virtual ICollection<GatewayProviderPaymentType> GatewayProviderPaymentType { get; set; }
        public virtual ICollection<Payment> Payment { get; set; }
    }
}
