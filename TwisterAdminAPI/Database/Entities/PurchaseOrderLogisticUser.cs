﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class PurchaseOrderLogisticUser
    {
        public int PurchaseOrderId { get; set; }
        public int LogisticUserId { get; set; }

        public virtual LogisticUser LogisticUser { get; set; }
        public virtual PurchaseOrder PurchaseOrder { get; set; }
    }
}
