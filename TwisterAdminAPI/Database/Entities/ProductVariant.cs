﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class ProductVariant
    {
        public ProductVariant()
        {
            CartProduct = new HashSet<CartProduct>();
            ChatSeller = new HashSet<ChatSeller>();
            InvoiceReceiptProduct = new HashSet<InvoiceReceiptProduct>();
            PurchaseOrderProduct = new HashSet<PurchaseOrderProduct>();
            GroupBuyingProductVariant = new HashSet<GroupBuyingProductVariant>();
            GroupBuyingReward = new HashSet<GroupBuyingReward>();
            SaleOrderProduct = new HashSet<SaleOrderProduct>();
            Stock = new HashSet<Stock>();
            VariantGroup = new HashSet<VariantGroup>();
        }

        public int Id { get; set; }
        public int ProductId { get; set; }
        //public string VariantIds { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int StatusId { get; set; }
        public string Image { get; set; }
        public decimal? DiscountPercentage { get; set; }
        public decimal SalePrice { get; set; }
        public string Sku { get; set; }
        public string Barcode { get; set; }

        public virtual Product Product { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<CartProduct> CartProduct { get; set; }
        public virtual ICollection<ChatSeller> ChatSeller { get; set; }
        public virtual ICollection<InvoiceReceiptProduct> InvoiceReceiptProduct { get; set; }
        public virtual ICollection<PurchaseOrderProduct> PurchaseOrderProduct { get; set; }
        public virtual ICollection<GroupBuyingProductVariant> GroupBuyingProductVariant { get; set; }
        public virtual ICollection<GroupBuyingReward> GroupBuyingReward { get; set; }
        public virtual ICollection<SaleOrderProduct> SaleOrderProduct { get; set; }
        public virtual ICollection<Stock> Stock { get; set; }
        public virtual ICollection<VariantGroup> VariantGroup { get; set; }
    }
}
