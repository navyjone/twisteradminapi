﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class Cart
    {
        public Cart()
        {
            CartProduct = new HashSet<CartProduct>();
            SaleOrder = new HashSet<SaleOrder>();
        }

        public int Id { get; set; }
        public int CustomerUserId { get; set; }
        public string Code { get; set; }
        public string StaticCode { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int StatusId { get; set; }
        public int? EventId { get; set; }
        public int? LockerId { get; set; }
        public string Remark { get; set; }
        public int? GroupBuyingId { get; set; }

        public virtual CustomerUser CustomerUser { get; set; }
        public virtual Event Event { get; set; }
        public virtual Status Status { get; set; }
        public virtual GroupBuying GroupBuying { get; set; }
        public virtual ICollection<CartProduct> CartProduct { get; set; }
        public virtual ICollection<SaleOrder> SaleOrder { get; set; }
    }
}
