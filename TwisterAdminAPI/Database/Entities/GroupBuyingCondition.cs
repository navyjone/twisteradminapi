﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public partial class GroupBuyingCondition
    {
        public GroupBuyingCondition()
        {
            GroupBuyingProductVariant = new HashSet<GroupBuyingProductVariant>();
        }

        public int Id { get; set; }
        public int? TargetBuyer { get; set; }
        public int? TargetQuantity { get; set; }
        public decimal? TargetPrice { get; set; }

        public virtual ICollection<GroupBuyingProductVariant> GroupBuyingProductVariant { get; set; }
    }
}
