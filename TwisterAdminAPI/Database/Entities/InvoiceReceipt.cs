﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class InvoiceReceipt
    {
        public InvoiceReceipt()
        {
            CreditNote = new HashSet<CreditNote>();
            InvoiceReceiptProduct = new HashSet<InvoiceReceiptProduct>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string StaticCode { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal Discount { get; set; }
        public decimal? Vat { get; set; }
        public decimal? NetPrice { get; set; }
        public int SaleOrderId { get; set; }
        public string SaleOrderCode { get; set; }
        public string Name { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public int CustomerUserId { get; set; }
        public string Remark { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int StatusId { get; set; }
        public string MarketplaceAddress { get; set; }
        public decimal ShippingFee { get; set; }
        public int SellerId { get; set; }
        public string SellerName { get; set; }
        public string SellerAddress { get; set; }
        public string GatewayRefNumber { get; set; }
        public string PaymentMethod { get; set; }
        public string CreditCardNumber { get; set; }
        public string BankName { get; set; }
        public string AccountNumber { get; set; }
        public string AccountTwister { get; set; }
        public string CounsterServiceName { get; set; }
        public string CounsterServiceRefNumber { get; set; }

        public virtual CustomerUser CustomerUser { get; set; }
        public virtual SaleOrder SaleOrder { get; set; }
        public virtual Seller Seller { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<CreditNote> CreditNote { get; set; }
        public virtual ICollection<InvoiceReceiptProduct> InvoiceReceiptProduct { get; set; }
    }
}
