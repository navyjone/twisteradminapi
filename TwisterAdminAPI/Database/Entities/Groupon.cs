﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public partial class Groupon
    {
        public int Id { get; set; }
        public int SellerId { get; set; }
        public int CustomerUserId { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        public DateTimeOffset UpdateDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public string Code { get; set; }
        public string ConfirmationCode { get; set; }
        public int StatusId { get; set; }
        public int SaleOrderProductId { get; set; }

        public virtual CustomerUser CustomerUser { get; set; }
        public virtual SaleOrderProduct SaleOrderProduct { get; set; }
        public virtual Seller Seller { get; set; }
        public virtual Status Status { get; set; }
    }
}
