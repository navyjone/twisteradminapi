﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class CategoryMarketplace
    {
        public CategoryMarketplace()
        {
            CategoryMarketplaceHierarchyChildren = new HashSet<CategoryMarketplaceHierarchy>();
            CategoryMarketplaceHierarchyParent = new HashSet<CategoryMarketplaceHierarchy>();
            CategoryMarketplaceLanguage = new HashSet<CategoryMarketplaceLanguage>();
            CategoryMarketplaceProduct = new HashSet<CategoryMarketplaceProduct>();
            CouponType = new HashSet<CouponType>();
            MarketplacePromotion = new HashSet<MarketplacePromotion>();
            Product = new HashSet<Product>();
            RelateCategory = new HashSet<RelateCategory>();
            SellerPromotion = new HashSet<SellerPromotion>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public int StatusId { get; set; }
        public string Image { get; set; }
        public int? Sequence { get; set; }
        public string Banner { get; set; }

        public virtual Status Status { get; set; }
        public virtual ICollection<CategoryMarketplaceHierarchy> CategoryMarketplaceHierarchyChildren { get; set; }
        public virtual ICollection<CategoryMarketplaceHierarchy> CategoryMarketplaceHierarchyParent { get; set; }
        public virtual ICollection<CategoryMarketplaceLanguage> CategoryMarketplaceLanguage { get; set; }
        public virtual ICollection<CategoryMarketplaceProduct> CategoryMarketplaceProduct { get; set; }
        public virtual ICollection<CouponType> CouponType { get; set; }
        public virtual ICollection<MarketplacePromotion> MarketplacePromotion { get; set; }
        public virtual ICollection<Product> Product { get; set; }
        public virtual ICollection<RelateCategory> RelateCategory { get; set; }
        public virtual ICollection<SellerPromotion> SellerPromotion { get; set; }
    }
}
