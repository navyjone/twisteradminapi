﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public partial class GroupBuyingAchievementType
    {
        public GroupBuyingAchievementType()
        {
            GroupBuyingReward = new HashSet<GroupBuyingReward>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<GroupBuyingReward> GroupBuyingReward { get; set; }
    }
}
