﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class PaymentType
    {
        public PaymentType()
        {
            GatewayProviderPaymentType = new HashSet<GatewayProviderPaymentType>();
            Payment = new HashSet<Payment>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<GatewayProviderPaymentType> GatewayProviderPaymentType { get; set; }
        public virtual ICollection<Payment> Payment { get; set; }
    }
}
