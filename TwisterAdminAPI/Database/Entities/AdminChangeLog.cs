﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Entities
{
    public partial class AdminChangeLog
    {
        public int Id { get; set; }
        public string APIName { get; set; }
        public int MarketplaceUserId { get; set; }
        public string Method { get; set; }
        public string Data { get; set; }
        public DateTimeOffset CreateDate { get; set; }

        public virtual MarketplaceUser MarketplaceUser { get; set; }
    }
}
