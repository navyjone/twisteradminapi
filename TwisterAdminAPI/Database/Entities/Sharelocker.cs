﻿using System;
using System.Collections.Generic;

namespace Database.Entities
{
    public partial class Sharelocker
    {
        public long ShareId { get; set; }
        public string LockerCode { get; set; }
        public long? UserId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTimeOffset? UpdatedDate { get; set; }
    }
}
