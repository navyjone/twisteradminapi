﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_seller_workday_group : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK__swg__workingDayId__working_day__id",
                schema: "twister",
                table: "seller_workday_group",
                column: "workingDayId");

            migrationBuilder.AddForeignKey(
                name: "FK__swg__workingDayId__working_day__id",
                schema: "twister",
                table: "seller_workday_group",
                column: "workingDayId",
                principalSchema: "twister",
                principalTable: "working_day",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__swg__workingDayId__working_day__id",
                schema: "twister",
                table: "seller_workday_group");

            migrationBuilder.DropIndex(
                name: "FK__swg__workingDayId__working_day__id",
                schema: "twister",
                table: "seller_workday_group");
        }
    }
}
