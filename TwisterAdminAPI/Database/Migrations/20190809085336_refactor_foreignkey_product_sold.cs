﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_product_sold : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK__product_sold__productId__product__id",
                schema: "twister",
                table: "product_sold",
                column: "productId");

            migrationBuilder.AddForeignKey(
                name: "FK__product_sold__customerUserId__customer_user__id",
                schema: "twister",
                table: "product_sold",
                column: "customerUserId",
                principalSchema: "twister",
                principalTable: "customer_user",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__product_sold__productId__product__id",
                schema: "twister",
                table: "product_sold",
                column: "productId",
                principalSchema: "twister",
                principalTable: "product",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__product_sold__customerUserId__customer_user__id",
                schema: "twister",
                table: "product_sold");

            migrationBuilder.DropForeignKey(
                name: "FK__product_sold__productId__product__id",
                schema: "twister",
                table: "product_sold");

            migrationBuilder.DropIndex(
                name: "FK__product_sold__productId__product__id",
                schema: "twister",
                table: "product_sold");
        }
    }
}
