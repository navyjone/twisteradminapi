﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class add_column_notification_seller : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "LinkParam",
                schema: "twister",
                table: "notification_seller",
                unicode: false,
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Param",
                schema: "twister",
                table: "notification_seller",
                unicode: false,
                maxLength: 255,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LinkParam",
                schema: "twister",
                table: "notification_seller");

            migrationBuilder.DropColumn(
                name: "Param",
                schema: "twister",
                table: "notification_seller");
        }
    }
}
