﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class add_column_isIcon_to_product_image_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<short>(
                name: "isIcon",
                schema: "twister",
                table: "product_image",
                type: "bit(1)",
                nullable: false,
                defaultValueSql: "b'0'");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "isIcon",
                schema: "twister",
                table: "product_image");
        }
    }
}
