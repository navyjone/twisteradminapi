﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_credit_card : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK__credit_card__customerUserId__customer_user__id",
                schema: "twister",
                table: "credit_card",
                column: "customerUserId");

            migrationBuilder.AddForeignKey(
                name: "FK__credit_card__customerUserId__customer_user__id",
                schema: "twister",
                table: "credit_card",
                column: "customerUserId",
                principalSchema: "twister",
                principalTable: "customer_user",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__credit_card__customerUserId__customer_user__id",
                schema: "twister",
                table: "credit_card");

            migrationBuilder.DropIndex(
                name: "FK__credit_card__customerUserId__customer_user__id",
                schema: "twister",
                table: "credit_card");
        }
    }
}
