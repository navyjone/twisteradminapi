﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_marketplace_coupon_group : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($@"
                ALTER TABLE marketplace_coupon_group RENAME INDEX `fk-marketplace_coupon-price_coupon.id` 
                TO `FK__mk_coupon_group__couponPriceId__coupon_price__id`;
            ");
            //migrationBuilder.RenameIndex(
            //    name: "fk-marketplace_coupon-price_coupon.id",
            //    schema: "twister",
            //    table: "marketplace_coupon_group",
            //    newName: "FK__mk_coupon_group__couponPriceId__coupon_price__id");

            migrationBuilder.AddForeignKey(
                name: "FK__mk_coupon_group__couponPriceId__coupon_price__id",
                schema: "twister",
                table: "marketplace_coupon_group",
                column: "couponPriceId",
                principalSchema: "twister",
                principalTable: "coupon_price",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__mk_coupon_group__couponPriceId__coupon_price__id",
                schema: "twister",
                table: "marketplace_coupon_group");

            //migrationBuilder.RenameIndex(
            //    name: "FK__mk_coupon_group__couponPriceId__coupon_price__id",
            //    schema: "twister",
            //    table: "marketplace_coupon_group",
            //    newName: "fk-marketplace_coupon-price_coupon.id");
            migrationBuilder.Sql($@"
                ALTER TABLE marketplace_coupon_group RENAME INDEX `FK__mk_coupon_group__couponPriceId__coupon_price__id` 
                TO `fk-marketplace_coupon-price_coupon.id`;
            ");
        }
    }
}
