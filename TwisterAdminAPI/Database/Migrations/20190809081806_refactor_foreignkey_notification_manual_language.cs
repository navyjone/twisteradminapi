﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_notification_manual_language : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK__noti_man_lan__languageId__language__id",
                schema: "twister",
                table: "notification_manual_language",
                column: "languageId");

            migrationBuilder.CreateIndex(
                name: "FK__noti_man_lan__notiManId__noti_man__id",
                schema: "twister",
                table: "notification_manual_language",
                column: "notificationManualId");

            migrationBuilder.AddForeignKey(
                name: "FK__noti_man_lan__languageId__language__id",
                schema: "twister",
                table: "notification_manual_language",
                column: "languageId",
                principalSchema: "twister",
                principalTable: "language",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__noti_man_lan__notiManId__noti_man__id",
                schema: "twister",
                table: "notification_manual_language",
                column: "notificationManualId",
                principalSchema: "twister",
                principalTable: "notification_manual",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__noti_man_lan__languageId__language__id",
                schema: "twister",
                table: "notification_manual_language");

            migrationBuilder.DropForeignKey(
                name: "FK__noti_man_lan__notiManId__noti_man__id",
                schema: "twister",
                table: "notification_manual_language");

            migrationBuilder.DropIndex(
                name: "FK__noti_man_lan__languageId__language__id",
                schema: "twister",
                table: "notification_manual_language");

            migrationBuilder.DropIndex(
                name: "FK__noti_man_lan__notiManId__noti_man__id",
                schema: "twister",
                table: "notification_manual_language");
        }
    }
}
