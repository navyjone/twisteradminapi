﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_brand : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK__brand__statusId__status__id",
                schema: "twister",
                table: "brand",
                column: "statusId");

            migrationBuilder.AddForeignKey(
                name: "FK__brand__statusId__status__id",
                schema: "twister",
                table: "brand",
                column: "statusId",
                principalSchema: "twister",
                principalTable: "status",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__brand__statusId__status__id",
                schema: "twister",
                table: "brand");

            migrationBuilder.DropIndex(
                name: "FK__brand__statusId__status__id",
                schema: "twister",
                table: "brand");
        }
    }
}
