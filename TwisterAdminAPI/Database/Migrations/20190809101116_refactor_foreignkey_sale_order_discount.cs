﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_sale_order_discount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK__so_discount__discountTypeId__discount_type__id",
                schema: "twister",
                table: "sale_order_discount",
                column: "discountTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK__so_discount__discountTypeId__discount_type__id",
                schema: "twister",
                table: "sale_order_discount",
                column: "discountTypeId",
                principalSchema: "twister",
                principalTable: "discount_type",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__so_discount__soId__sale_order__id",
                schema: "twister",
                table: "sale_order_discount",
                column: "saleOrderId",
                principalSchema: "twister",
                principalTable: "sale_order",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__so_discount__discountTypeId__discount_type__id",
                schema: "twister",
                table: "sale_order_discount");

            migrationBuilder.DropForeignKey(
                name: "FK__so_discount__soId__sale_order__id",
                schema: "twister",
                table: "sale_order_discount");

            migrationBuilder.DropIndex(
                name: "FK__so_discount__discountTypeId__discount_type__id",
                schema: "twister",
                table: "sale_order_discount");
        }
    }
}
