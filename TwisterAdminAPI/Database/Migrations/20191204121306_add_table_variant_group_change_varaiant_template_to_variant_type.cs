﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class add_table_variant_group_change_varaiant_template_to_variant_type : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "variant_ibfk_1",
            //    schema: "twister",
            //    table: "variant");

            migrationBuilder.Sql("ALTER TABLE `variant` DROP FOREIGN KEY `variant_ibfk_1`;");

            migrationBuilder.DropTable(
                name: "variant_template",
                schema: "twister");

            //migrationBuilder.DropPrimaryKey(
            //    name: "PK_variant",
            //    schema: "twister",
            //    table: "variant");
            migrationBuilder.Sql("ALTER TABLE variant DROP PRIMARY KEY; ");

            //migrationBuilder.RenameColumn(
            //    name: "variantTemplateId",
            //    schema: "twister",
            //    table: "variant",
            //    newName: "variantTypeId");

            migrationBuilder.Sql($@"
                ALTER TABLE variant
                CHANGE COLUMN `variantTemplateId` `variantTypeId` int(10) NOT NULL
            ;");

            //migrationBuilder.RenameIndex(
            //    name: "fk-variant-variant_template.id",
            //    schema: "twister",
            //    table: "variant",
            //    newName: "fk-variant-variant_type.id");

            migrationBuilder.Sql($@"
                ALTER TABLE variant RENAME INDEX `fk-variant-variant_template.id` 
                TO `fk-variant-variant_type.id`;
            ");

            migrationBuilder.AddPrimaryKey(
                name: "PK_variant",
                schema: "twister",
                table: "variant",
                column: "id");

            migrationBuilder.CreateTable(
                name: "variant_group",
                schema: "twister",
                columns: table => new
                {
                    productVariantId = table.Column<int>(type: "int(10)", nullable: false),
                    VariantId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_variant_group", x => new { x.productVariantId, x.VariantId });
                    table.ForeignKey(
                        name: "FK__variant_group__productVariantId__product_variant__id",
                        column: x => x.productVariantId,
                        principalSchema: "twister",
                        principalTable: "product_variant",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__variant_group__variantId__variant__id",
                        column: x => x.VariantId,
                        principalSchema: "twister",
                        principalTable: "variant",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "variant_type",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_variant_type", x => x.id);
                });

            migrationBuilder.CreateIndex(
                name: "FK__variant_group__productVariantId__product_variant__id",
                schema: "twister",
                table: "variant_group",
                column: "productVariantId");

            migrationBuilder.CreateIndex(
                name: "FK__variant_group__variantId__variant__id",
                schema: "twister",
                table: "variant_group",
                column: "VariantId");

            migrationBuilder.AddForeignKey(
                name: "fk-variant-variant_type.id",
                schema: "twister",
                table: "variant",
                column: "variantTypeId",
                principalSchema: "twister",
                principalTable: "variant_type",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "fk-variant-variant_type.id",
            //    schema: "twister",
            //    table: "variant");

            migrationBuilder.Sql("ALTER TABLE `variant` DROP FOREIGN KEY `fk-variant-variant_type.id`;");

            migrationBuilder.DropTable(
                name: "variant_group",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "variant_type",
                schema: "twister");

            //migrationBuilder.DropPrimaryKey(
            //    name: "PK_variant",
            //    schema: "twister",
            //    table: "variant");
            migrationBuilder.Sql("ALTER TABLE variant DROP PRIMARY KEY; ");


            //migrationBuilder.RenameColumn(
            //    name: "variantTypeId",
            //    schema: "twister",
            //    table: "variant",
            //    newName: "variantTemplateId");

            migrationBuilder.Sql($@"
                ALTER TABLE variant
                CHANGE COLUMN `variantTypeId` `variantTemplateId` int(10) NOT NULL
            ;");

            //migrationBuilder.RenameIndex(
            //    name: "fk-variant-variant_type.id",
            //    schema: "twister",
            //    table: "variant",
            //    newName: "fk-variant-variant_template.id");

            migrationBuilder.Sql($@"
                ALTER TABLE variant RENAME INDEX `fk-variant-variant_type.id` 
                TO `fk-variant-variant_template.id`;
            ");

            migrationBuilder.AddPrimaryKey(
                name: "PK_variant",
                schema: "twister",
                table: "variant",
                columns: new[] { "id", "variantTemplateId" });

            migrationBuilder.CreateTable(
                name: "variant_template",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_variant_template", x => x.id);
                });

            migrationBuilder.AddForeignKey(
                name: "variant_ibfk_1",
                schema: "twister",
                table: "variant",
                column: "variantTemplateId",
                principalSchema: "twister",
                principalTable: "variant_template",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
