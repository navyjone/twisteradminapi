﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class change_column_name_product_expireDurationInMinute_to_InDay : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($@"ALTER TABLE `product`
            CHANGE COLUMN `expireDurationInMinute` `expireDurationInDay`  int(10) NULL DEFAULT NULL AFTER `expireDate`;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($@"ALTER TABLE `product`
            CHANGE COLUMN `expireDurationInDay` `expireDurationInMinute`  int(10) NULL DEFAULT NULL AFTER `expireDate`;");
        }
    }
}
