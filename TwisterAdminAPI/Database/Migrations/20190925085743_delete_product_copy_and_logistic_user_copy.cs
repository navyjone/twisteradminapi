﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class delete_product_copy_and_logistic_user_copy : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "logistic_user_copy",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "product_copy",
                schema: "twister");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "logistic_user_copy",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(5)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    citizenId = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    companyId = table.Column<int>(type: "int(4)", nullable: false),
                    createDate = table.Column<DateTime>(nullable: false),
                    email = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    firstName = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    lastName = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    password = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    telephone = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    username = table.Column<string>(unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_logistic_user_copy", x => x.id);
                    table.ForeignKey(
                        name: "logistic_user_copy_ibfk_1",
                        column: x => x.companyId,
                        principalSchema: "twister",
                        principalTable: "logistic_company",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "logistic_user_copy_ibfk_2",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "product_copy",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    brandId = table.Column<int>(type: "int(10)", nullable: true),
                    categoryMarketPlaceId = table.Column<int>(type: "int(10)", nullable: true),
                    categorySellerId = table.Column<int>(type: "int(10)", nullable: true),
                    createDate = table.Column<DateTime>(nullable: false),
                    description = table.Column<string>(unicode: false, nullable: true),
                    eventId = table.Column<int>(type: "int(10)", nullable: true),
                    image = table.Column<string>(unicode: false, nullable: true),
                    isFood = table.Column<short>(type: "bit(1)", nullable: true, defaultValueSql: "b'0'"),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    purchaseLimit = table.Column<int>(type: "int(4)", nullable: true),
                    rating = table.Column<int>(type: "int(3)", nullable: true),
                    sellerAddressId = table.Column<int>(type: "int(10)", nullable: true),
                    sellerId = table.Column<int>(type: "int(10)", nullable: false),
                    size = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    sku = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    sold = table.Column<int>(type: "int(6)", nullable: true),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    wishlist = table.Column<int>(type: "int(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product_copy", x => x.id);
                    table.ForeignKey(
                        name: "product_copy_ibfk_1",
                        column: x => x.categoryMarketPlaceId,
                        principalSchema: "twister",
                        principalTable: "category_marketplace",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "product_copy_ibfk_2",
                        column: x => x.categorySellerId,
                        principalSchema: "twister",
                        principalTable: "category_seller",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "product_copy_ibfk_6",
                        column: x => x.eventId,
                        principalSchema: "twister",
                        principalTable: "event",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "product_copy_ibfk_4",
                        column: x => x.sellerAddressId,
                        principalSchema: "twister",
                        principalTable: "seller_address",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "product_copy_ibfk_3",
                        column: x => x.sellerId,
                        principalSchema: "twister",
                        principalTable: "seller",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "product_copy_ibfk_5",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "fk-logistic_user-company.id",
                schema: "twister",
                table: "logistic_user_copy",
                column: "companyId");

            migrationBuilder.CreateIndex(
                name: "fk-logistic_user-status.id",
                schema: "twister",
                table: "logistic_user_copy",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "fk-product-category_marketplace.id",
                schema: "twister",
                table: "product_copy",
                column: "categoryMarketPlaceId");

            migrationBuilder.CreateIndex(
                name: "fk-product-category_seller.id",
                schema: "twister",
                table: "product_copy",
                column: "categorySellerId");

            migrationBuilder.CreateIndex(
                name: "eventId",
                schema: "twister",
                table: "product_copy",
                column: "eventId");

            migrationBuilder.CreateIndex(
                name: "pk-product.id",
                schema: "twister",
                table: "product_copy",
                column: "id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "fk-product-seller_address.id",
                schema: "twister",
                table: "product_copy",
                column: "sellerAddressId");

            migrationBuilder.CreateIndex(
                name: "fk-product-seller.id",
                schema: "twister",
                table: "product_copy",
                column: "sellerId");

            migrationBuilder.CreateIndex(
                name: "fk-product-status.id",
                schema: "twister",
                table: "product_copy",
                column: "statusId");
        }
    }
}
