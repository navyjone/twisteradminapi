﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_purchase_order : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK__purchase_order__saleOrderId__sale_order__id",
                schema: "twister",
                table: "purchase_order",
                column: "saleOrderId");

            migrationBuilder.AddForeignKey(
                name: "FK__purchase_order__saleOrderId__sale_order__id",
                schema: "twister",
                table: "purchase_order",
                column: "saleOrderId",
                principalSchema: "twister",
                principalTable: "sale_order",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__purchase_order__saleOrderId__sale_order__id",
                schema: "twister",
                table: "purchase_order");

            migrationBuilder.DropIndex(
                name: "FK__purchase_order__saleOrderId__sale_order__id",
                schema: "twister",
                table: "purchase_order");
        }
    }
}
