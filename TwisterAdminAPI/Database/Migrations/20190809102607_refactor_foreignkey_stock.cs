﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_stock : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK__stock__statusId__status__id",
                schema: "twister",
                table: "stock",
                column: "statusId");

            migrationBuilder.AddForeignKey(
                name: "FK__stock__statusId__status__id",
                schema: "twister",
                table: "stock",
                column: "statusId",
                principalSchema: "twister",
                principalTable: "status",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__stock__statusId__status__id",
                schema: "twister",
                table: "stock");

            migrationBuilder.DropIndex(
                name: "FK__stock__statusId__status__id",
                schema: "twister",
                table: "stock");
        }
    }
}
