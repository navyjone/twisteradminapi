﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_purchase_order_log : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK__po_log__poId__purchase_order__id",
                schema: "twister",
                table: "purchase_order_log",
                column: "purchaseOrderId");

            migrationBuilder.AddForeignKey(
                name: "FK__po_log__poId__purchase_order__id",
                schema: "twister",
                table: "purchase_order_log",
                column: "purchaseOrderId",
                principalSchema: "twister",
                principalTable: "purchase_order",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__po_log__poId__purchase_order__id",
                schema: "twister",
                table: "purchase_order_log");

            migrationBuilder.DropIndex(
                name: "FK__po_log__poId__purchase_order__id",
                schema: "twister",
                table: "purchase_order_log");
        }
    }
}
