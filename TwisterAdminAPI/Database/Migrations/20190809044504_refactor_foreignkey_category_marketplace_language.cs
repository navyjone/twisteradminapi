﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_category_marketplace_language : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK__cat_marketplace__lang__languageId__lang__id",
                schema: "twister",
                table: "category_marketplace_language",
                column: "languageId");

            migrationBuilder.AddForeignKey(
                name: "FK__cat_marketplace_lang__catMarId__cat_mar__id",
                schema: "twister",
                table: "category_marketplace_language",
                column: "categoryMarketplaceId",
                principalSchema: "twister",
                principalTable: "category_marketplace",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__cat_marketplace__lang__languageId__lang__id",
                schema: "twister",
                table: "category_marketplace_language",
                column: "languageId",
                principalSchema: "twister",
                principalTable: "language",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__cat_marketplace_lang__catMarId__cat_mar__id",
                schema: "twister",
                table: "category_marketplace_language");

            migrationBuilder.DropForeignKey(
                name: "FK__cat_marketplace__lang__languageId__lang__id",
                schema: "twister",
                table: "category_marketplace_language");

            migrationBuilder.DropIndex(
                name: "FK__cat_marketplace__lang__languageId__lang__id",
                schema: "twister",
                table: "category_marketplace_language");
        }
    }
}
