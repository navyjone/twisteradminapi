﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_credit_note_product : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($@"
                ALTER TABLE credit_note_product
                CHANGE COLUMN `createNoteId` `creditNoteId`  int(10) NOT NULL;
            ");
            //migrationBuilder.RenameColumn(
            //    name: "createNoteId",
            //    schema: "twister",
            //    table: "credit_note_product",
            //    newName: "creditNoteId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($@"
                ALTER TABLE credit_note_product
                CHANGE COLUMN `creditNoteId` `createNoteId`  int(10) NOT NULL;
            ");

            //migrationBuilder.RenameColumn(
            //    name: "creditNoteId",
            //    schema: "twister",
            //    table: "credit_note_product",
            //    newName: "createNoteId");
        }
    }
}
