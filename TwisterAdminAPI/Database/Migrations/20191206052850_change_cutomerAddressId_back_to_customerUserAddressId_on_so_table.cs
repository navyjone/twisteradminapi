﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class change_cutomerAddressId_back_to_customerUserAddressId_on_so_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.RenameColumn(
            //    name: "customerAddressId",
            //    schema: "twister",
            //    table: "sale_order",
            //    newName: "customerUserAddressId");

            migrationBuilder.Sql($@"ALTER TABLE sale_order
                CHANGE COLUMN `customerAddressId` `customerUserAddressId` int(10) NULL
            ;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.RenameColumn(
            //    name: "customerUserAddressId",
            //    schema: "twister",
            //    table: "sale_order",
            //    newName: "customerAddressId");

            migrationBuilder.Sql($@"ALTER TABLE sale_order
                CHANGE COLUMN `customerUserAddressId` `customerAddressId` int(10) NULL
            ;");
        }
    }
}
