﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_province : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK__province__countryId__country__id",
                schema: "twister",
                table: "province",
                column: "countryId");

            migrationBuilder.AddForeignKey(
                name: "FK__province__countryId__country__id",
                schema: "twister",
                table: "province",
                column: "countryId",
                principalSchema: "twister",
                principalTable: "country",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__province__countryId__country__id",
                schema: "twister",
                table: "province");

            migrationBuilder.DropIndex(
                name: "FK__province__countryId__country__id",
                schema: "twister",
                table: "province");
        }
    }
}
