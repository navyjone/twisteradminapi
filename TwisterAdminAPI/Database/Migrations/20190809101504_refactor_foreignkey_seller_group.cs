﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_seller_group : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK__seller_group__statusId__status__id",
                schema: "twister",
                table: "seller_group",
                column: "statusId");

            migrationBuilder.AddForeignKey(
                name: "FK__seller_group__statusId__status__id",
                schema: "twister",
                table: "seller_group",
                column: "statusId",
                principalSchema: "twister",
                principalTable: "status",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__seller_group__statusId__status__id",
                schema: "twister",
                table: "seller_group");

            migrationBuilder.DropIndex(
                name: "FK__seller_group__statusId__status__id",
                schema: "twister",
                table: "seller_group");
        }
    }
}
