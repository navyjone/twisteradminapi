﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_notification_manual : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK__noti_man__notiTypeId__noti_type__id",
                schema: "twister",
                table: "notification_manual",
                column: "notificationTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK__noti_man__notiTypeId__noti_type__id",
                schema: "twister",
                table: "notification_manual",
                column: "notificationTypeId",
                principalSchema: "twister",
                principalTable: "notification_type",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__noti_man__notiTypeId__noti_type__id",
                schema: "twister",
                table: "notification_manual");

            migrationBuilder.DropIndex(
                name: "FK__noti_man__notiTypeId__noti_type__id",
                schema: "twister",
                table: "notification_manual");
        }
    }
}
