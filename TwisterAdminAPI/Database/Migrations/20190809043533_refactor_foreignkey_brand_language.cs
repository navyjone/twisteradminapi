﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_brand_language : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK__brand_lang__languageId__language__id",
                schema: "twister",
                table: "brand_language",
                column: "languageId");

            migrationBuilder.AddForeignKey(
                name: "FK__brand_lang__brandId__brand__id",
                schema: "twister",
                table: "brand_language",
                column: "brandId",
                principalSchema: "twister",
                principalTable: "brand",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__brand_lang__languageId__language__id",
                schema: "twister",
                table: "brand_language",
                column: "languageId",
                principalSchema: "twister",
                principalTable: "language",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__brand_lang__brandId__brand__id",
                schema: "twister",
                table: "brand_language");

            migrationBuilder.DropForeignKey(
                name: "FK__brand_lang__languageId__language__id",
                schema: "twister",
                table: "brand_language");

            migrationBuilder.DropIndex(
                name: "FK__brand_lang__languageId__language__id",
                schema: "twister",
                table: "brand_language");
        }
    }
}
