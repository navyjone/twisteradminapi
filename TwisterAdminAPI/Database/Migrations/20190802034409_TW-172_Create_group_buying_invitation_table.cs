﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class TW172_Create_group_buying_invitation_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "group_buying_invitation",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    groupBuyingId = table.Column<int>(type: "int(10)", nullable: false),
                    inviterCustomerUserId = table.Column<int>(type: "int(10)", nullable: false),
                    inviteeSaleOrderId = table.Column<int>(type: "int(10)", nullable: true),
                    parentCustomerUserId = table.Column<int>(type: "int(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_group_buying_invitation", x => x.id);
                    table.ForeignKey(
                        name: "FK_gbiv_groupBuyingId_group_buying_id",
                        column: x => x.groupBuyingId,
                        principalSchema: "twister",
                        principalTable: "group_buying",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_gbiv_inviteeSaleOrderId_saleOrder_id",
                        column: x => x.inviteeSaleOrderId,
                        principalSchema: "twister",
                        principalTable: "sale_order",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_gbiv_inviterCustomerUserId_customer_user_id",
                        column: x => x.inviterCustomerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_gbiv_parentCustomerUserId_customer_user_id",
                        column: x => x.parentCustomerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "FK_gbiv_groupBuyingId_group_buying_id",
                schema: "twister",
                table: "group_buying_invitation",
                column: "groupBuyingId");

            migrationBuilder.CreateIndex(
                name: "FK_gbiv_inviteeSaleOrderId_saleOrder_id",
                schema: "twister",
                table: "group_buying_invitation",
                column: "inviteeSaleOrderId");

            migrationBuilder.CreateIndex(
                name: "FK_gbiv_inviterCustomerUserId_customer_user_id",
                schema: "twister",
                table: "group_buying_invitation",
                column: "inviterCustomerUserId");

            migrationBuilder.CreateIndex(
                name: "FK_gbiv_parentCustomerUserId_customer_user_id",
                schema: "twister",
                table: "group_buying_invitation",
                column: "parentCustomerUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "group_buying_invitation",
                schema: "twister");
        }
    }
}
