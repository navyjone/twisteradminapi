﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_purchase_order_parcel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK__po_parcel__poId__purchase_oreder__id",
                schema: "twister",
                table: "purchase_order_parcel",
                column: "purchaseOrderId");

            migrationBuilder.AddForeignKey(
                name: "FK__po_parcel__poId__purchase_oreder__id",
                schema: "twister",
                table: "purchase_order_parcel",
                column: "purchaseOrderId",
                principalSchema: "twister",
                principalTable: "purchase_order",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__po_parcel__poId__purchase_oreder__id",
                schema: "twister",
                table: "purchase_order_parcel");

            migrationBuilder.DropIndex(
                name: "FK__po_parcel__poId__purchase_oreder__id",
                schema: "twister",
                table: "purchase_order_parcel");
        }
    }
}
