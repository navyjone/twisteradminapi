﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class add_media_type_and_add_to_product_image_and_seller_image : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "mediaTypeId",
                schema: "twister",
                table: "seller_image",
                type: "int(4)",
                nullable: false,
                defaultValue: 1);

            migrationBuilder.AddColumn<int>(
                name: "mediaTypeId",
                schema: "twister",
                table: "product_image",
                type: "int(4)",
                nullable: false,
                defaultValue: 1);

            migrationBuilder.CreateTable(
                name: "media_type",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    description = table.Column<string>(unicode: false, maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_media_type", x => x.id);
                });

            migrationBuilder.CreateIndex(
                name: "FK__seller_image__mediaTypeId__media_type__id",
                schema: "twister",
                table: "seller_image",
                column: "mediaTypeId");

            migrationBuilder.CreateIndex(
                name: "FK__product_image__mediaTypeId__media_type__id",
                schema: "twister",
                table: "product_image",
                column: "mediaTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_product_image__mediaTypeId__media_type__id",
                schema: "twister",
                table: "product_image",
                column: "mediaTypeId",
                principalSchema: "twister",
                principalTable: "media_type",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_seller_image__mediaTypeId__media_type__id",
                schema: "twister",
                table: "seller_image",
                column: "mediaTypeId",
                principalSchema: "twister",
                principalTable: "media_type",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_product_image__mediaTypeId__media_type__id",
                schema: "twister",
                table: "product_image");

            migrationBuilder.DropForeignKey(
                name: "FK_seller_image__mediaTypeId__media_type__id",
                schema: "twister",
                table: "seller_image");

            migrationBuilder.DropTable(
                name: "media_type",
                schema: "twister");

            migrationBuilder.DropIndex(
                name: "FK__seller_image__mediaTypeId__media_type__id",
                schema: "twister",
                table: "seller_image");

            migrationBuilder.DropIndex(
                name: "FK__product_image__mediaTypeId__media_type__id",
                schema: "twister",
                table: "product_image");

            migrationBuilder.DropColumn(
                name: "mediaTypeId",
                schema: "twister",
                table: "seller_image");

            migrationBuilder.DropColumn(
                name: "mediaTypeId",
                schema: "twister",
                table: "product_image");
        }
    }
}
