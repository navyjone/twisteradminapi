﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_purchase_order_product_tracking : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "statusId",
                schema: "twister",
                table: "purchase_order_tracking",
                type: "int(4)",
                nullable: false,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 4);

            migrationBuilder.CreateIndex(
                name: "FK__po_tracking__statusId__status__id",
                schema: "twister",
                table: "purchase_order_tracking",
                column: "statusId");

            migrationBuilder.AddForeignKey(
                name: "FK__po_tracking__statusId__status__id",
                schema: "twister",
                table: "purchase_order_tracking",
                column: "statusId",
                principalSchema: "twister",
                principalTable: "status",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__po_tracking__statusId__status__id",
                schema: "twister",
                table: "purchase_order_tracking");

            migrationBuilder.DropIndex(
                name: "FK__po_tracking__statusId__status__id",
                schema: "twister",
                table: "purchase_order_tracking");

            migrationBuilder.AlterColumn<string>(
                name: "statusId",
                schema: "twister",
                table: "purchase_order_tracking",
                unicode: false,
                maxLength: 4,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int(4)");
        }
    }
}
