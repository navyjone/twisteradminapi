﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class add_groupon_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "groupon",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    sellerId = table.Column<int>(type: "int(10)", nullable: false),
                    customerUserId = table.Column<int>(type: "int(10)", nullable: false),
                    createDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    updateDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"),
                    expireDate = table.Column<DateTime>(nullable: false),
                    code = table.Column<string>(nullable: false),
                    confirmationCode = table.Column<string>(nullable: true),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    saleOrderProductId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_groupon", x => x.id);
                    table.ForeignKey(
                        name: "FK__groupon__customerUserId__customer_user__id",
                        column: x => x.customerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__groupon__saleOrderProductId__sale_order_product__id",
                        column: x => x.saleOrderProductId,
                        principalSchema: "twister",
                        principalTable: "sale_order_product",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__groupon__sellerId__seller__id",
                        column: x => x.sellerId,
                        principalSchema: "twister",
                        principalTable: "seller",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__groupon__statusId__status__id",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "FK__groupon__customerUserId__customer_user__id",
                schema: "twister",
                table: "groupon",
                column: "customerUserId");

            migrationBuilder.CreateIndex(
                name: "FK__groupon__saleOrderProductId__sale_order_product__id",
                schema: "twister",
                table: "groupon",
                column: "saleOrderProductId");

            migrationBuilder.CreateIndex(
                name: "FK__groupon__sellerId__seller__id",
                schema: "twister",
                table: "groupon",
                column: "sellerId");

            migrationBuilder.CreateIndex(
                name: "FK__groupon__statusId__status__id",
                schema: "twister",
                table: "groupon",
                column: "statusId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "groupon",
                schema: "twister");
        }
    }
}
