﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class change_structure_admin_change_log_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "newData",
                schema: "twister",
                table: "admin_change_log");

            migrationBuilder.DropColumn(
                name: "oldData",
                schema: "twister",
                table: "admin_change_log");

            migrationBuilder.AddColumn<string>(
                name: "data",
                schema: "twister",
                table: "admin_change_log",
                type: "mediumText",
                nullable: false);

            migrationBuilder.AddColumn<string>(
                name: "method",
                schema: "twister",
                table: "admin_change_log",
                maxLength: 255,
                nullable: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "data",
                schema: "twister",
                table: "admin_change_log");

            migrationBuilder.DropColumn(
                name: "method",
                schema: "twister",
                table: "admin_change_log");

            migrationBuilder.AddColumn<string>(
                name: "newData",
                schema: "twister",
                table: "admin_change_log",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "oldData",
                schema: "twister",
                table: "admin_change_log",
                type: "text",
                nullable: true);
        }
    }
}
