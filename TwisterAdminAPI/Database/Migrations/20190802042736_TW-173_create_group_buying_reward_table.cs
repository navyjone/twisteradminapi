﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class TW173_create_group_buying_reward_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "group_buying_reward",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    groupBuyingProductVariantId = table.Column<int>(type: "int(10)", nullable: false),
                    groupBuyingAchievementTypeId = table.Column<int>(type: "int(10)", nullable: false),
                    salePrice = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    point = table.Column<int>(type: "int(10)", nullable: true),
                    marketplaceCouponGroupId = table.Column<int>(type: "int(10)", nullable: true),
                    productVariantId = table.Column<int>(type: "int(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_group_buying_reward", x => x.id);
                    table.ForeignKey(
                        name: "FK_gbr_groupBuyingAchievementTypeId_gba_id",
                        column: x => x.groupBuyingAchievementTypeId,
                        principalSchema: "twister",
                        principalTable: "group_buying_achievement_type",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_gbr_groupBuyingProductVariantId_gbpv_id",
                        column: x => x.groupBuyingProductVariantId,
                        principalSchema: "twister",
                        principalTable: "group_buying_product_variant",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_gbr_marketplaceCouponGroupId_mktcg_id",
                        column: x => x.marketplaceCouponGroupId,
                        principalSchema: "twister",
                        principalTable: "marketplace_coupon_group",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_gbr_productVariantId_product_variant_id",
                        column: x => x.productVariantId,
                        principalSchema: "twister",
                        principalTable: "product_variant",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "FK_gbr_groupBuyingAchievementTypeId_gba_id",
                schema: "twister",
                table: "group_buying_reward",
                column: "groupBuyingAchievementTypeId");

            migrationBuilder.CreateIndex(
                name: "FK_gbr_groupBuyingProductVariantId_gbpv_id",
                schema: "twister",
                table: "group_buying_reward",
                column: "groupBuyingProductVariantId");

            migrationBuilder.CreateIndex(
                name: "FK_gbr_marketplaceCouponGroupId_mktcg_id",
                schema: "twister",
                table: "group_buying_reward",
                column: "marketplaceCouponGroupId");

            migrationBuilder.CreateIndex(
                name: "FK_gbr_productVariantId_product_variant_id",
                schema: "twister",
                table: "group_buying_reward",
                column: "productVariantId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "group_buying_reward",
                schema: "twister");
        }
    }
}
