﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_invoice_receipt_product : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK_invrep_product__productVariantId__product_variant__id",
                schema: "twister",
                table: "invoice_receipt_product",
                column: "productVariantId");

            migrationBuilder.AddForeignKey(
                name: "FK_invrep_product__productVariantId__product_variant__id",
                schema: "twister",
                table: "invoice_receipt_product",
                column: "productVariantId",
                principalSchema: "twister",
                principalTable: "product_variant",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_invrep_product__productVariantId__product_variant__id",
                schema: "twister",
                table: "invoice_receipt_product");

            migrationBuilder.DropIndex(
                name: "FK_invrep_product__productVariantId__product_variant__id",
                schema: "twister",
                table: "invoice_receipt_product");
        }
    }
}
