﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_category_seller : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK__cat_seller__sellerId__seller__id",
                schema: "twister",
                table: "category_seller",
                column: "sellerId");

            migrationBuilder.CreateIndex(
                name: "FK__cat_seller__statusId__status__id",
                schema: "twister",
                table: "category_seller",
                column: "statusId");

            migrationBuilder.AddForeignKey(
                name: "FK__cat_seller__sellerId__seller__id",
                schema: "twister",
                table: "category_seller",
                column: "sellerId",
                principalSchema: "twister",
                principalTable: "seller",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__cat_seller__statusId__status__id",
                schema: "twister",
                table: "category_seller",
                column: "statusId",
                principalSchema: "twister",
                principalTable: "status",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__cat_seller__sellerId__seller__id",
                schema: "twister",
                table: "category_seller");

            migrationBuilder.DropForeignKey(
                name: "FK__cat_seller__statusId__status__id",
                schema: "twister",
                table: "category_seller");

            migrationBuilder.DropIndex(
                name: "FK__cat_seller__sellerId__seller__id",
                schema: "twister",
                table: "category_seller");

            migrationBuilder.DropIndex(
                name: "FK__cat_seller__statusId__status__id",
                schema: "twister",
                table: "category_seller");
        }
    }
}
