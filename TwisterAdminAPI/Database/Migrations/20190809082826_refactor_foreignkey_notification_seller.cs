﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_notification_seller : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($@"
                ALTER TABLE notification_seller
                DROP FOREIGN KEY notification_seller_ibfk_1
            ;");

            //migrationBuilder.DropForeignKey(
            //    name: "notification_seller_ibfk_1",
            //    schema: "twister",
            //    table: "notification_seller");

            migrationBuilder.Sql($@"
                ALTER TABLE notification_seller RENAME INDEX `fk-notification_seller-notification_type.id` 
                TO `FK__noti_seller__notiTempId__noti_temp__id`;
            ");

            //migrationBuilder.RenameIndex(
            //    name: "fk-notification_seller-notification_type.id",
            //    schema: "twister",
            //    table: "notification_seller",
            //    newName: "FK__noti_seller__notiTempId__noti_temp__id");

            migrationBuilder.AlterColumn<int>(
                name: "notificationManualId",
                schema: "twister",
                table: "notification_seller",
                type: "int(10)",
                nullable: false,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 255);

            migrationBuilder.CreateIndex(
                name: "FK__noti_seller__notiManId__noti_man__id",
                schema: "twister",
                table: "notification_seller",
                column: "notificationManualId");

            migrationBuilder.AddForeignKey(
                name: "FK__noti_seller__notiManId__noti_man__id",
                schema: "twister",
                table: "notification_seller",
                column: "notificationManualId",
                principalSchema: "twister",
                principalTable: "notification_manual",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__noti_seller__notiTempId__noti_temp__id",
                schema: "twister",
                table: "notification_seller",
                column: "notificationTemplateId",
                principalSchema: "twister",
                principalTable: "notification_template",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__noti_seller__notiManId__noti_man__id",
                schema: "twister",
                table: "notification_seller");

            migrationBuilder.DropForeignKey(
                name: "FK__noti_seller__notiTempId__noti_temp__id",
                schema: "twister",
                table: "notification_seller");

            migrationBuilder.DropIndex(
                name: "FK__noti_seller__notiManId__noti_man__id",
                schema: "twister",
                table: "notification_seller");

            migrationBuilder.Sql($@"
                ALTER TABLE notification_seller RENAME INDEX `FK__noti_seller__notiTempId__noti_temp__id` 
                TO `fk-notification_seller-notification_type.id`;
            ");

            //migrationBuilder.RenameIndex(
            //    name: "FK__noti_seller__notiTempId__noti_temp__id",
            //    schema: "twister",
            //    table: "notification_seller",
            //    newName: "fk-notification_seller-notification_type.id");

            migrationBuilder.AlterColumn<string>(
                name: "notificationManualId",
                schema: "twister",
                table: "notification_seller",
                unicode: false,
                maxLength: 255,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int(10)");

            migrationBuilder.AddForeignKey(
                name: "notification_seller_ibfk_1",
                schema: "twister",
                table: "notification_seller",
                column: "notificationTemplateId",
                principalSchema: "twister",
                principalTable: "notification_type",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
