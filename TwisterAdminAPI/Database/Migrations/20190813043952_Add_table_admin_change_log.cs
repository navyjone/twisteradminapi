﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class Add_table_admin_change_log : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "admin_change_log",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    tableName = table.Column<string>(maxLength: 255, nullable: false),
                    marketplaceUserId = table.Column<int>(type: "int(10)", nullable: false),
                    newData = table.Column<string>(type: "text", nullable: false),
                    oldData = table.Column<string>(type: "text", nullable: true),
                    createDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_admin_change_log", x => x.id);
                    table.ForeignKey(
                        name: "FK__acl__marketplaceUserId__marketplace_user__id",
                        column: x => x.marketplaceUserId,
                        principalSchema: "twister",
                        principalTable: "marketplace_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "FK__acl__marketplaceUserId__markeplace_user__id",
                schema: "twister",
                table: "admin_change_log",
                column: "marketplaceUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "admin_change_log",
                schema: "twister");
        }
    }
}
