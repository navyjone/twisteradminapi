﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class TW169_create_group_buying_product_variant_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "group_buying_product_variant",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    productVariantId = table.Column<int>(type: "int(10)", nullable: false),
                    groupBuyingConditionId = table.Column<int>(type: "int(10)", nullable: false),
                    durationInMinutes = table.Column<int>(type: "int(10)", nullable: false),
                    startDate = table.Column<DateTime>(nullable: false),
                    endDate = table.Column<DateTime>(nullable: false),
                    createDate = table.Column<DateTime>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    updateDate = table.Column<DateTime>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    statusId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_group_buying_product_variant", x => x.id);
                    table.ForeignKey(
                        name: "FK_gbpv_gbcId_group_buying_condition_id",
                        column: x => x.groupBuyingConditionId,
                        principalSchema: "twister",
                        principalTable: "group_buying_condition",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_gbpv_productVariantId_product_variant_id",
                        column: x => x.productVariantId,
                        principalSchema: "twister",
                        principalTable: "product_variant",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_gbpv_statusId_status_id",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "FK_gbpv_gbcId_group_buying_condition_id",
                schema: "twister",
                table: "group_buying_product_variant",
                column: "groupBuyingConditionId");

            migrationBuilder.CreateIndex(
                name: "FK_gbpv_productVariantId_product_variant_id",
                schema: "twister",
                table: "group_buying_product_variant",
                column: "productVariantId");

            migrationBuilder.CreateIndex(
                name: "FK_gbpv_statusId_status_id",
                schema: "twister",
                table: "group_buying_product_variant",
                column: "statusId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "group_buying_product_variant",
                schema: "twister");
        }
    }
}
