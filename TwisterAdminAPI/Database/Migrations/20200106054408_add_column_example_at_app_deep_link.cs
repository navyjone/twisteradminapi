﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class add_column_example_at_app_deep_link : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameTable(
                name: "zone",
                schema: "twister",
                newName: "zone");

            migrationBuilder.RenameTable(
                name: "working_day",
                schema: "twister",
                newName: "working_day");

            migrationBuilder.RenameTable(
                name: "webuser",
                schema: "twister",
                newName: "webuser");

            migrationBuilder.RenameTable(
                name: "variant_type",
                schema: "twister",
                newName: "variant_type");

            migrationBuilder.RenameTable(
                name: "variant_group",
                schema: "twister",
                newName: "variant_group");

            migrationBuilder.RenameTable(
                name: "variant",
                schema: "twister",
                newName: "variant");

            migrationBuilder.RenameTable(
                name: "town_language",
                schema: "twister",
                newName: "town_language");

            migrationBuilder.RenameTable(
                name: "town",
                schema: "twister",
                newName: "town");

            migrationBuilder.RenameTable(
                name: "stock_type",
                schema: "twister",
                newName: "stock_type");

            migrationBuilder.RenameTable(
                name: "stock",
                schema: "twister",
                newName: "stock");

            migrationBuilder.RenameTable(
                name: "status_language",
                schema: "twister",
                newName: "status_language");

            migrationBuilder.RenameTable(
                name: "status",
                schema: "twister",
                newName: "status");

            migrationBuilder.RenameTable(
                name: "sharelocker",
                schema: "twister",
                newName: "sharelocker");

            migrationBuilder.RenameTable(
                name: "seller_workday_profile_group",
                schema: "twister",
                newName: "seller_workday_profile_group");

            migrationBuilder.RenameTable(
                name: "seller_workday_profile",
                schema: "twister",
                newName: "seller_workday_profile");

            migrationBuilder.RenameTable(
                name: "seller_workday_group_holiday",
                schema: "twister",
                newName: "seller_workday_group_holiday");

            migrationBuilder.RenameTable(
                name: "seller_workday_group",
                schema: "twister",
                newName: "seller_workday_group");

            migrationBuilder.RenameTable(
                name: "seller_user",
                schema: "twister",
                newName: "seller_user");

            migrationBuilder.RenameTable(
                name: "seller_stats_type_language",
                schema: "twister",
                newName: "seller_stats_type_language");

            migrationBuilder.RenameTable(
                name: "seller_stats_type",
                schema: "twister",
                newName: "seller_stats_type");

            migrationBuilder.RenameTable(
                name: "seller_stats",
                schema: "twister",
                newName: "seller_stats");

            migrationBuilder.RenameTable(
                name: "seller_promotion_redeem",
                schema: "twister",
                newName: "seller_promotion_redeem");

            migrationBuilder.RenameTable(
                name: "seller_promotion",
                schema: "twister",
                newName: "seller_promotion");

            migrationBuilder.RenameTable(
                name: "seller_product_delivery",
                schema: "twister",
                newName: "seller_product_delivery");

            migrationBuilder.RenameTable(
                name: "seller_image",
                schema: "twister",
                newName: "seller_image");

            migrationBuilder.RenameTable(
                name: "seller_group",
                schema: "twister",
                newName: "seller_group");

            migrationBuilder.RenameTable(
                name: "seller_coupon_redeem",
                schema: "twister",
                newName: "seller_coupon_redeem");

            migrationBuilder.RenameTable(
                name: "seller_coupon",
                schema: "twister",
                newName: "seller_coupon");

            migrationBuilder.RenameTable(
                name: "seller_area",
                schema: "twister",
                newName: "seller_area");

            migrationBuilder.RenameTable(
                name: "seller_address",
                schema: "twister",
                newName: "seller_address");

            migrationBuilder.RenameTable(
                name: "seller",
                schema: "twister",
                newName: "seller");

            migrationBuilder.RenameTable(
                name: "sale_order_product",
                schema: "twister",
                newName: "sale_order_product");

            migrationBuilder.RenameTable(
                name: "sale_order_discount",
                schema: "twister",
                newName: "sale_order_discount");

            migrationBuilder.RenameTable(
                name: "sale_order",
                schema: "twister",
                newName: "sale_order");

            migrationBuilder.RenameTable(
                name: "role_class_ability",
                schema: "twister",
                newName: "role_class_ability");

            migrationBuilder.RenameTable(
                name: "role_class",
                schema: "twister",
                newName: "role_class");

            migrationBuilder.RenameTable(
                name: "role_ability",
                schema: "twister",
                newName: "role_ability");

            migrationBuilder.RenameTable(
                name: "relate_product",
                schema: "twister",
                newName: "relate_product");

            migrationBuilder.RenameTable(
                name: "relate_category",
                schema: "twister",
                newName: "relate_category");

            migrationBuilder.RenameTable(
                name: "regiscode",
                schema: "twister",
                newName: "regiscode");

            migrationBuilder.RenameTable(
                name: "purchase_order_tracking",
                schema: "twister",
                newName: "purchase_order_tracking");

            migrationBuilder.RenameTable(
                name: "purchase_order_product_piece",
                schema: "twister",
                newName: "purchase_order_product_piece");

            migrationBuilder.RenameTable(
                name: "purchase_order_product_actual_quantity",
                schema: "twister",
                newName: "purchase_order_product_actual_quantity");

            migrationBuilder.RenameTable(
                name: "purchase_order_product",
                schema: "twister",
                newName: "purchase_order_product");

            migrationBuilder.RenameTable(
                name: "purchase_order_parcel",
                schema: "twister",
                newName: "purchase_order_parcel");

            migrationBuilder.RenameTable(
                name: "purchase_order_logistic_user",
                schema: "twister",
                newName: "purchase_order_logistic_user");

            migrationBuilder.RenameTable(
                name: "purchase_order_log",
                schema: "twister",
                newName: "purchase_order_log");

            migrationBuilder.RenameTable(
                name: "purchase_order_delivered",
                schema: "twister",
                newName: "purchase_order_delivered");

            migrationBuilder.RenameTable(
                name: "purchase_order",
                schema: "twister",
                newName: "purchase_order");

            migrationBuilder.RenameTable(
                name: "province_language",
                schema: "twister",
                newName: "province_language");

            migrationBuilder.RenameTable(
                name: "province",
                schema: "twister",
                newName: "province");

            migrationBuilder.RenameTable(
                name: "promotion_price",
                schema: "twister",
                newName: "promotion_price");

            migrationBuilder.RenameTable(
                name: "product_wishlist",
                schema: "twister",
                newName: "product_wishlist");

            migrationBuilder.RenameTable(
                name: "product_variant",
                schema: "twister",
                newName: "product_variant");

            migrationBuilder.RenameTable(
                name: "product_type",
                schema: "twister",
                newName: "product_type");

            migrationBuilder.RenameTable(
                name: "product_sold",
                schema: "twister",
                newName: "product_sold");

            migrationBuilder.RenameTable(
                name: "product_rating",
                schema: "twister",
                newName: "product_rating");

            migrationBuilder.RenameTable(
                name: "product_image",
                schema: "twister",
                newName: "product_image");

            migrationBuilder.RenameTable(
                name: "product",
                schema: "twister",
                newName: "product");

            migrationBuilder.RenameTable(
                name: "point",
                schema: "twister",
                newName: "point");

            migrationBuilder.RenameTable(
                name: "payment_type",
                schema: "twister",
                newName: "payment_type");

            migrationBuilder.RenameTable(
                name: "payment",
                schema: "twister",
                newName: "payment");

            migrationBuilder.RenameTable(
                name: "notification_type",
                schema: "twister",
                newName: "notification_type");

            migrationBuilder.RenameTable(
                name: "notification_template_language",
                schema: "twister",
                newName: "notification_template_language");

            migrationBuilder.RenameTable(
                name: "notification_template",
                schema: "twister",
                newName: "notification_template");

            migrationBuilder.RenameTable(
                name: "notification_seller",
                schema: "twister",
                newName: "notification_seller");

            migrationBuilder.RenameTable(
                name: "notification_manual_language",
                schema: "twister",
                newName: "notification_manual_language");

            migrationBuilder.RenameTable(
                name: "notification_manual",
                schema: "twister",
                newName: "notification_manual");

            migrationBuilder.RenameTable(
                name: "notification_customer",
                schema: "twister",
                newName: "notification_customer");

            migrationBuilder.RenameTable(
                name: "media_type",
                schema: "twister",
                newName: "media_type");

            migrationBuilder.RenameTable(
                name: "marketplace_user",
                schema: "twister",
                newName: "marketplace_user");

            migrationBuilder.RenameTable(
                name: "marketplace_review_seller",
                schema: "twister",
                newName: "marketplace_review_seller");

            migrationBuilder.RenameTable(
                name: "marketplace_promotion_redeem",
                schema: "twister",
                newName: "marketplace_promotion_redeem");

            migrationBuilder.RenameTable(
                name: "marketplace_promotion",
                schema: "twister",
                newName: "marketplace_promotion");

            migrationBuilder.RenameTable(
                name: "marketplace_coupon_group",
                schema: "twister",
                newName: "marketplace_coupon_group");

            migrationBuilder.RenameTable(
                name: "marketplace_coupon",
                schema: "twister",
                newName: "marketplace_coupon");

            migrationBuilder.RenameTable(
                name: "logopenlocker",
                schema: "twister",
                newName: "logopenlocker");

            migrationBuilder.RenameTable(
                name: "logistic_user",
                schema: "twister",
                newName: "logistic_user");

            migrationBuilder.RenameTable(
                name: "logistic_company",
                schema: "twister",
                newName: "logistic_company");

            migrationBuilder.RenameTable(
                name: "logdelivery",
                schema: "twister",
                newName: "logdelivery");

            migrationBuilder.RenameTable(
                name: "lockerusermapping",
                schema: "twister",
                newName: "lockerusermapping");

            migrationBuilder.RenameTable(
                name: "lockertypeinfo",
                schema: "twister",
                newName: "lockertypeinfo");

            migrationBuilder.RenameTable(
                name: "lockerinfo",
                schema: "twister",
                newName: "lockerinfo");

            migrationBuilder.RenameTable(
                name: "lockeraccess",
                schema: "twister",
                newName: "lockeraccess");

            migrationBuilder.RenameTable(
                name: "language",
                schema: "twister",
                newName: "language");

            migrationBuilder.RenameTable(
                name: "invoice_receipt_product",
                schema: "twister",
                newName: "invoice_receipt_product");

            migrationBuilder.RenameTable(
                name: "invoice_receipt",
                schema: "twister",
                newName: "invoice_receipt");

            migrationBuilder.RenameTable(
                name: "groupon",
                schema: "twister",
                newName: "groupon");

            migrationBuilder.RenameTable(
                name: "group_buying_reward",
                schema: "twister",
                newName: "group_buying_reward");

            migrationBuilder.RenameTable(
                name: "group_buying_product_variant",
                schema: "twister",
                newName: "group_buying_product_variant");

            migrationBuilder.RenameTable(
                name: "group_buying_invitation",
                schema: "twister",
                newName: "group_buying_invitation");

            migrationBuilder.RenameTable(
                name: "group_buying_condition",
                schema: "twister",
                newName: "group_buying_condition");

            migrationBuilder.RenameTable(
                name: "group_buying_achievement_type",
                schema: "twister",
                newName: "group_buying_achievement_type");

            migrationBuilder.RenameTable(
                name: "group_buying",
                schema: "twister",
                newName: "group_buying");

            migrationBuilder.RenameTable(
                name: "gateway_provider_payment_type",
                schema: "twister",
                newName: "gateway_provider_payment_type");

            migrationBuilder.RenameTable(
                name: "gateway_provider",
                schema: "twister",
                newName: "gateway_provider");

            migrationBuilder.RenameTable(
                name: "follow",
                schema: "twister",
                newName: "follow");

            migrationBuilder.RenameTable(
                name: "favorite_product",
                schema: "twister",
                newName: "favorite_product");

            migrationBuilder.RenameTable(
                name: "event_locker",
                schema: "twister",
                newName: "event_locker");

            migrationBuilder.RenameTable(
                name: "event",
                schema: "twister",
                newName: "event");

            migrationBuilder.RenameTable(
                name: "discount_type",
                schema: "twister",
                newName: "discount_type");

            migrationBuilder.RenameTable(
                name: "customer_user_view_history_log",
                schema: "twister",
                newName: "customer_user_view_history_log");

            migrationBuilder.RenameTable(
                name: "customer_user",
                schema: "twister",
                newName: "customer_user");

            migrationBuilder.RenameTable(
                name: "customer_search_history_log",
                schema: "twister",
                newName: "customer_search_history_log");

            migrationBuilder.RenameTable(
                name: "customer_review_seller",
                schema: "twister",
                newName: "customer_review_seller");

            migrationBuilder.RenameTable(
                name: "customer_review_product",
                schema: "twister",
                newName: "customer_review_product");

            migrationBuilder.RenameTable(
                name: "customer_company",
                schema: "twister",
                newName: "customer_company");

            migrationBuilder.RenameTable(
                name: "customer_address",
                schema: "twister",
                newName: "customer_address");

            migrationBuilder.RenameTable(
                name: "credit_note_product",
                schema: "twister",
                newName: "credit_note_product");

            migrationBuilder.RenameTable(
                name: "credit_note",
                schema: "twister",
                newName: "credit_note");

            migrationBuilder.RenameTable(
                name: "credit_card",
                schema: "twister",
                newName: "credit_card");

            migrationBuilder.RenameTable(
                name: "coupon_type",
                schema: "twister",
                newName: "coupon_type");

            migrationBuilder.RenameTable(
                name: "coupon_price",
                schema: "twister",
                newName: "coupon_price");

            migrationBuilder.RenameTable(
                name: "country_language",
                schema: "twister",
                newName: "country_language");

            migrationBuilder.RenameTable(
                name: "country",
                schema: "twister",
                newName: "country");

            migrationBuilder.RenameTable(
                name: "config",
                schema: "twister",
                newName: "config");

            migrationBuilder.RenameTable(
                name: "condoinfo",
                schema: "twister",
                newName: "condoinfo");

            migrationBuilder.RenameTable(
                name: "condoadmin_register",
                schema: "twister",
                newName: "condoadmin_register");

            migrationBuilder.RenameTable(
                name: "condo_sharebox",
                schema: "twister",
                newName: "condo_sharebox");

            migrationBuilder.RenameTable(
                name: "condo_register",
                schema: "twister",
                newName: "condo_register");

            migrationBuilder.RenameTable(
                name: "condo_rate",
                schema: "twister",
                newName: "condo_rate");

            migrationBuilder.RenameTable(
                name: "coin_transaction",
                schema: "twister",
                newName: "coin_transaction");

            migrationBuilder.RenameTable(
                name: "city_language",
                schema: "twister",
                newName: "city_language");

            migrationBuilder.RenameTable(
                name: "city",
                schema: "twister",
                newName: "city");

            migrationBuilder.RenameTable(
                name: "chat_seller",
                schema: "twister",
                newName: "chat_seller");

            migrationBuilder.RenameTable(
                name: "chat_customer_service",
                schema: "twister",
                newName: "chat_customer_service");

            migrationBuilder.RenameTable(
                name: "category_seller_product",
                schema: "twister",
                newName: "category_seller_product");

            migrationBuilder.RenameTable(
                name: "category_seller_hierarchy",
                schema: "twister",
                newName: "category_seller_hierarchy");

            migrationBuilder.RenameTable(
                name: "category_seller",
                schema: "twister",
                newName: "category_seller");

            migrationBuilder.RenameTable(
                name: "category_marketplace_product",
                schema: "twister",
                newName: "category_marketplace_product");

            migrationBuilder.RenameTable(
                name: "category_marketplace_language",
                schema: "twister",
                newName: "category_marketplace_language");

            migrationBuilder.RenameTable(
                name: "category_marketplace_hierarchy",
                schema: "twister",
                newName: "category_marketplace_hierarchy");

            migrationBuilder.RenameTable(
                name: "category_marketplace",
                schema: "twister",
                newName: "category_marketplace");

            migrationBuilder.RenameTable(
                name: "cart_product",
                schema: "twister",
                newName: "cart_product");

            migrationBuilder.RenameTable(
                name: "cart",
                schema: "twister",
                newName: "cart");

            migrationBuilder.RenameTable(
                name: "brand_language",
                schema: "twister",
                newName: "brand_language");

            migrationBuilder.RenameTable(
                name: "brand",
                schema: "twister",
                newName: "brand");

            migrationBuilder.RenameTable(
                name: "banner_seller",
                schema: "twister",
                newName: "banner_seller");

            migrationBuilder.RenameTable(
                name: "banner_marketplace_product",
                schema: "twister",
                newName: "banner_marketplace_product");

            migrationBuilder.RenameTable(
                name: "banner_marketplace",
                schema: "twister",
                newName: "banner_marketplace");

            migrationBuilder.RenameTable(
                name: "banner_food_seller",
                schema: "twister",
                newName: "banner_food_seller");

            migrationBuilder.RenameTable(
                name: "app_deep_link",
                schema: "twister",
                newName: "app_deep_link");

            migrationBuilder.RenameTable(
                name: "allowstatusinfo",
                schema: "twister",
                newName: "allowstatusinfo");

            migrationBuilder.RenameTable(
                name: "allow_open_locker",
                schema: "twister",
                newName: "allow_open_locker");

            migrationBuilder.RenameTable(
                name: "admin_refresh_token",
                schema: "twister",
                newName: "admin_refresh_token");

            migrationBuilder.RenameTable(
                name: "admin_change_log",
                schema: "twister",
                newName: "admin_change_log");

            migrationBuilder.RenameTable(
                name: "__nvjmigrationhistory",
                schema: "twister",
                newName: "__nvjmigrationhistory");

            migrationBuilder.RenameTable(
                name: "__nvjmigrationdatahistory",
                schema: "twister",
                newName: "__nvjmigrationdatahistory");

            migrationBuilder.AddColumn<string>(
                name: "example",
                table: "app_deep_link",
                maxLength: 255,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "example",
                table: "app_deep_link");

            migrationBuilder.EnsureSchema(
                name: "twister");

            migrationBuilder.RenameTable(
                name: "zone",
                newName: "zone",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "working_day",
                newName: "working_day",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "webuser",
                newName: "webuser",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "variant_type",
                newName: "variant_type",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "variant_group",
                newName: "variant_group",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "variant",
                newName: "variant",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "town_language",
                newName: "town_language",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "town",
                newName: "town",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "stock_type",
                newName: "stock_type",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "stock",
                newName: "stock",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "status_language",
                newName: "status_language",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "status",
                newName: "status",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "sharelocker",
                newName: "sharelocker",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "seller_workday_profile_group",
                newName: "seller_workday_profile_group",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "seller_workday_profile",
                newName: "seller_workday_profile",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "seller_workday_group_holiday",
                newName: "seller_workday_group_holiday",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "seller_workday_group",
                newName: "seller_workday_group",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "seller_user",
                newName: "seller_user",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "seller_stats_type_language",
                newName: "seller_stats_type_language",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "seller_stats_type",
                newName: "seller_stats_type",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "seller_stats",
                newName: "seller_stats",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "seller_promotion_redeem",
                newName: "seller_promotion_redeem",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "seller_promotion",
                newName: "seller_promotion",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "seller_product_delivery",
                newName: "seller_product_delivery",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "seller_image",
                newName: "seller_image",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "seller_group",
                newName: "seller_group",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "seller_coupon_redeem",
                newName: "seller_coupon_redeem",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "seller_coupon",
                newName: "seller_coupon",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "seller_area",
                newName: "seller_area",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "seller_address",
                newName: "seller_address",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "seller",
                newName: "seller",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "sale_order_product",
                newName: "sale_order_product",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "sale_order_discount",
                newName: "sale_order_discount",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "sale_order",
                newName: "sale_order",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "role_class_ability",
                newName: "role_class_ability",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "role_class",
                newName: "role_class",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "role_ability",
                newName: "role_ability",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "relate_product",
                newName: "relate_product",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "relate_category",
                newName: "relate_category",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "regiscode",
                newName: "regiscode",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "purchase_order_tracking",
                newName: "purchase_order_tracking",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "purchase_order_product_piece",
                newName: "purchase_order_product_piece",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "purchase_order_product_actual_quantity",
                newName: "purchase_order_product_actual_quantity",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "purchase_order_product",
                newName: "purchase_order_product",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "purchase_order_parcel",
                newName: "purchase_order_parcel",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "purchase_order_logistic_user",
                newName: "purchase_order_logistic_user",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "purchase_order_log",
                newName: "purchase_order_log",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "purchase_order_delivered",
                newName: "purchase_order_delivered",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "purchase_order",
                newName: "purchase_order",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "province_language",
                newName: "province_language",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "province",
                newName: "province",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "promotion_price",
                newName: "promotion_price",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "product_wishlist",
                newName: "product_wishlist",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "product_variant",
                newName: "product_variant",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "product_type",
                newName: "product_type",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "product_sold",
                newName: "product_sold",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "product_rating",
                newName: "product_rating",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "product_image",
                newName: "product_image",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "product",
                newName: "product",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "point",
                newName: "point",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "payment_type",
                newName: "payment_type",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "payment",
                newName: "payment",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "notification_type",
                newName: "notification_type",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "notification_template_language",
                newName: "notification_template_language",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "notification_template",
                newName: "notification_template",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "notification_seller",
                newName: "notification_seller",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "notification_manual_language",
                newName: "notification_manual_language",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "notification_manual",
                newName: "notification_manual",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "notification_customer",
                newName: "notification_customer",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "media_type",
                newName: "media_type",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "marketplace_user",
                newName: "marketplace_user",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "marketplace_review_seller",
                newName: "marketplace_review_seller",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "marketplace_promotion_redeem",
                newName: "marketplace_promotion_redeem",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "marketplace_promotion",
                newName: "marketplace_promotion",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "marketplace_coupon_group",
                newName: "marketplace_coupon_group",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "marketplace_coupon",
                newName: "marketplace_coupon",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "logopenlocker",
                newName: "logopenlocker",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "logistic_user",
                newName: "logistic_user",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "logistic_company",
                newName: "logistic_company",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "logdelivery",
                newName: "logdelivery",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "lockerusermapping",
                newName: "lockerusermapping",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "lockertypeinfo",
                newName: "lockertypeinfo",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "lockerinfo",
                newName: "lockerinfo",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "lockeraccess",
                newName: "lockeraccess",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "language",
                newName: "language",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "invoice_receipt_product",
                newName: "invoice_receipt_product",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "invoice_receipt",
                newName: "invoice_receipt",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "groupon",
                newName: "groupon",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "group_buying_reward",
                newName: "group_buying_reward",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "group_buying_product_variant",
                newName: "group_buying_product_variant",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "group_buying_invitation",
                newName: "group_buying_invitation",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "group_buying_condition",
                newName: "group_buying_condition",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "group_buying_achievement_type",
                newName: "group_buying_achievement_type",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "group_buying",
                newName: "group_buying",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "gateway_provider_payment_type",
                newName: "gateway_provider_payment_type",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "gateway_provider",
                newName: "gateway_provider",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "follow",
                newName: "follow",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "favorite_product",
                newName: "favorite_product",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "event_locker",
                newName: "event_locker",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "event",
                newName: "event",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "discount_type",
                newName: "discount_type",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "customer_user_view_history_log",
                newName: "customer_user_view_history_log",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "customer_user",
                newName: "customer_user",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "customer_search_history_log",
                newName: "customer_search_history_log",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "customer_review_seller",
                newName: "customer_review_seller",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "customer_review_product",
                newName: "customer_review_product",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "customer_company",
                newName: "customer_company",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "customer_address",
                newName: "customer_address",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "credit_note_product",
                newName: "credit_note_product",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "credit_note",
                newName: "credit_note",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "credit_card",
                newName: "credit_card",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "coupon_type",
                newName: "coupon_type",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "coupon_price",
                newName: "coupon_price",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "country_language",
                newName: "country_language",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "country",
                newName: "country",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "config",
                newName: "config",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "condoinfo",
                newName: "condoinfo",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "condoadmin_register",
                newName: "condoadmin_register",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "condo_sharebox",
                newName: "condo_sharebox",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "condo_register",
                newName: "condo_register",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "condo_rate",
                newName: "condo_rate",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "coin_transaction",
                newName: "coin_transaction",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "city_language",
                newName: "city_language",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "city",
                newName: "city",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "chat_seller",
                newName: "chat_seller",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "chat_customer_service",
                newName: "chat_customer_service",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "category_seller_product",
                newName: "category_seller_product",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "category_seller_hierarchy",
                newName: "category_seller_hierarchy",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "category_seller",
                newName: "category_seller",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "category_marketplace_product",
                newName: "category_marketplace_product",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "category_marketplace_language",
                newName: "category_marketplace_language",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "category_marketplace_hierarchy",
                newName: "category_marketplace_hierarchy",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "category_marketplace",
                newName: "category_marketplace",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "cart_product",
                newName: "cart_product",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "cart",
                newName: "cart",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "brand_language",
                newName: "brand_language",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "brand",
                newName: "brand",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "banner_seller",
                newName: "banner_seller",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "banner_marketplace_product",
                newName: "banner_marketplace_product",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "banner_marketplace",
                newName: "banner_marketplace",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "banner_food_seller",
                newName: "banner_food_seller",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "app_deep_link",
                newName: "app_deep_link",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "allowstatusinfo",
                newName: "allowstatusinfo",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "allow_open_locker",
                newName: "allow_open_locker",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "admin_refresh_token",
                newName: "admin_refresh_token",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "admin_change_log",
                newName: "admin_change_log",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "__nvjmigrationhistory",
                newName: "__nvjmigrationhistory",
                newSchema: "twister");

            migrationBuilder.RenameTable(
                name: "__nvjmigrationdatahistory",
                newName: "__nvjmigrationdatahistory",
                newSchema: "twister");
        }
    }
}
