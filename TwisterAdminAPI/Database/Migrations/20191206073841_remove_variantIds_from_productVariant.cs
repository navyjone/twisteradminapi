﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class remove_variantIds_from_productVariant : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "variantIds",
                schema: "twister",
                table: "product_variant");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "variantIds",
                schema: "twister",
                table: "product_variant",
                unicode: false,
                maxLength: 255,
                nullable: true);
        }
    }
}
