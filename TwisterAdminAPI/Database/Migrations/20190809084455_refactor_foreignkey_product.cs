﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_product : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK__product__brandId__brand__id",
                schema: "twister",
                table: "product",
                column: "brandId");

            migrationBuilder.AddForeignKey(
                name: "FK__product__brandId__brand__id",
                schema: "twister",
                table: "product",
                column: "brandId",
                principalSchema: "twister",
                principalTable: "brand",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__product__brandId__brand__id",
                schema: "twister",
                table: "product");

            migrationBuilder.DropIndex(
                name: "FK__product__brandId__brand__id",
                schema: "twister",
                table: "product");
        }
    }
}
