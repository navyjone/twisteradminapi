﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_event : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "statusId",
                schema: "twister",
                table: "event",
                type: "int(4)",
                nullable: false,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 4);

            migrationBuilder.CreateIndex(
                name: "FK__event__statusId__status__id",
                schema: "twister",
                table: "event",
                column: "statusId");

            migrationBuilder.AddForeignKey(
                name: "FK__event__statusId__status__id",
                schema: "twister",
                table: "event",
                column: "statusId",
                principalSchema: "twister",
                principalTable: "status",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__event__statusId__status__id",
                schema: "twister",
                table: "event");

            migrationBuilder.DropIndex(
                name: "FK__event__statusId__status__id",
                schema: "twister",
                table: "event");

            migrationBuilder.AlterColumn<string>(
                name: "statusId",
                schema: "twister",
                table: "event",
                unicode: false,
                maxLength: 4,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int(4)");
        }
    }
}
