﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class add_column_image_to_saleOrderProduct_and_purchase_order_product_and_invoiceReceiptProduct : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "image",
                schema: "twister",
                table: "sale_order_product",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "image",
                schema: "twister",
                table: "purchase_order_product",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "image",
                schema: "twister",
                table: "invoice_receipt_product",
                maxLength: 255,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "image",
                schema: "twister",
                table: "sale_order_product");

            migrationBuilder.DropColumn(
                name: "image",
                schema: "twister",
                table: "purchase_order_product");

            migrationBuilder.DropColumn(
                name: "image",
                schema: "twister",
                table: "invoice_receipt_product");
        }
    }
}
