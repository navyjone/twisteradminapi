﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_sale_order : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($@"
                ALTER TABLE sale_order
                CHANGE COLUMN `customerUserAddressId` `customerAddressId`  int(10)
            ;");
            ////migrationBuilder.RenameColumn(
            ////    name: "customerUserAddressId",
            ////    schema: "twister",
            ////    table: "sale_order",
            ////    newName: "customerAddressId");

            migrationBuilder.CreateIndex(
                name: "FK__so__customerAddressId__customer_address__id",
                schema: "twister",
                table: "sale_order",
                column: "customerAddressId");

            migrationBuilder.CreateIndex(
                name: "FK__so__paymentId__payment__id",
                schema: "twister",
                table: "sale_order",
                column: "paymentId");

            migrationBuilder.AddForeignKey(
                name: "FK__so__customerAddressId__cust_address__id",
                schema: "twister",
                table: "sale_order",
                column: "customerAddressId",
                principalSchema: "twister",
                principalTable: "customer_address",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__so__paymentId__payment__id",
                schema: "twister",
                table: "sale_order",
                column: "paymentId",
                principalSchema: "twister",
                principalTable: "payment",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__so__customerAddressId__cust_address__id",
                schema: "twister",
                table: "sale_order");

            migrationBuilder.DropForeignKey(
                name: "FK__so__paymentId__payment__id",
                schema: "twister",
                table: "sale_order");

            migrationBuilder.DropIndex(
                name: "FK__so__customerAddressId__customer_address__id",
                schema: "twister",
                table: "sale_order");

            migrationBuilder.DropIndex(
                name: "FK__so__paymentId__payment__id",
                schema: "twister",
                table: "sale_order");

            //migrationBuilder.RenameColumn(
            //    name: "customerAddressId",
            //    schema: "twister",
            //    table: "sale_order",
            //    newName: "customerUserAddressId");

            migrationBuilder.Sql($@"
                ALTER TABLE sale_order
                CHANGE COLUMN `customerAddressId` `customerUserAddressId`  int(10)
            ;");
        }
    }
}
