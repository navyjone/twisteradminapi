﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class change_table_point : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "point_ibfk_2",
            //    table: "point");

            //migrationBuilder.DropForeignKey(
            //    name: "point_ibfk_3",
            //    table: "point");

            //migrationBuilder.DropIndex(
            //    name: "fk-point-marketplace_coupon.id",
            //    table: "point");

            //migrationBuilder.DropColumn(
            //    name: "marketplaceCouponId",
            //    table: "point");

            //migrationBuilder.RenameColumn(
            //    name: "sellerCouponId",
            //    table: "point",
            //    newName: "saleOrderId");

            //migrationBuilder.RenameIndex(
            //    name: "fk-point-seller_coupon.id",
            //    table: "point",
            //    newName: "FK__point_saleOrderId__saleOrder_id");
            migrationBuilder.Sql($@"ALTER TABLE point DROP FOREIGN KEY `point_ibfk_2` ;");
            migrationBuilder.Sql($@"ALTER TABLE point DROP FOREIGN KEY `point_ibfk_3` ;");
            migrationBuilder.Sql($@"ALTER TABLE point DROP INDEX `fk-point-marketplace_coupon.id` ;");
            migrationBuilder.Sql($@"ALTER TABLE point DROP COLUMN marketplaceCouponId ;");

            migrationBuilder.Sql($@"ALTER TABLE point
                CHANGE COLUMN `sellerCouponId` `saleOrderId` int(10) NULL
            ;");            
            
            migrationBuilder.Sql($@"ALTER TABLE point
                RENAME INDEX `fk-point-seller_coupon.id` TO `FK__point_saleOrderId__saleOrder_id`
            ;");

            migrationBuilder.AlterColumn<DateTimeOffset>(
                name: "updateDate",
                table: "point",
                nullable: false,
                defaultValueSql: "CURRENT_TIMESTAMP  ON UPDATE CURRENT_TIMESTAMP",
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<DateTimeOffset>(
                name: "createDate",
                table: "point",
                nullable: false,
                defaultValueSql: "CURRENT_TIMESTAMP",
                oldClrType: typeof(DateTime));

            migrationBuilder.AddColumn<DateTime>(
                name: "expireDate",
                table: "point",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK__point_saleOrderId__saleOrder_id",
                table: "point",
                column: "saleOrderId",
                principalTable: "sale_order",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__point_saleOrderId__saleOrder_id",
                table: "point");

            migrationBuilder.DropColumn(
                name: "expireDate",
                table: "point");

            //migrationBuilder.RenameColumn(
            //    name: "saleOrderId",
            //    table: "point",
            //    newName: "sellerCouponId");

            //migrationBuilder.RenameIndex(
            //    name: "FK__point_saleOrderId__saleOrder_id",
            //    table: "point",
            //    newName: "fk-point-seller_coupon.id");

            migrationBuilder.Sql($@"ALTER TABLE point
                CHANGE COLUMN `saleOrderId` `sellerCouponId` int(10) NULL
            ;");

            migrationBuilder.Sql($@"ALTER TABLE point
                RENAME INDEX `FK__point_saleOrderId__saleOrder_id` TO `fk-point-seller_coupon.id`
            ;");

            migrationBuilder.AlterColumn<DateTime>(
                name: "updateDate",
                table: "point",
                nullable: false,
                oldClrType: typeof(DateTimeOffset),
                oldDefaultValueSql: "CURRENT_TIMESTAMP");

            migrationBuilder.AlterColumn<DateTime>(
                name: "createDate",
                table: "point",
                nullable: false,
                oldClrType: typeof(DateTimeOffset),
                oldDefaultValueSql: "CURRENT_TIMESTAMP");

            migrationBuilder.AddColumn<int>(
                name: "marketplaceCouponId",
                table: "point",
                type: "int(10)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "fk-point-marketplace_coupon.id",
                table: "point",
                column: "marketplaceCouponId");

            migrationBuilder.AddForeignKey(
                name: "point_ibfk_2",
                table: "point",
                column: "marketplaceCouponId",
                principalTable: "marketplace_coupon_group",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "point_ibfk_3",
                table: "point",
                column: "sellerCouponId",
                principalTable: "seller_coupon",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
