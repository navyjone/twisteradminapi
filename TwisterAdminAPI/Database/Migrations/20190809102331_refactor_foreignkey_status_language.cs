﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_status_language : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK__status_lang__languageId__language__id",
                schema: "twister",
                table: "status_language",
                column: "languageId");

            migrationBuilder.CreateIndex(
                name: "FK__status_lang__statusId__status__id",
                schema: "twister",
                table: "status_language",
                column: "statusId");

            migrationBuilder.AddForeignKey(
                name: "FK__status_lang__languageId__language__id",
                schema: "twister",
                table: "status_language",
                column: "languageId",
                principalSchema: "twister",
                principalTable: "language",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__status_lang__statusId__status__id",
                schema: "twister",
                table: "status_language",
                column: "statusId",
                principalSchema: "twister",
                principalTable: "status",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__status_lang__languageId__language__id",
                schema: "twister",
                table: "status_language");

            migrationBuilder.DropForeignKey(
                name: "FK__status_lang__statusId__status__id",
                schema: "twister",
                table: "status_language");

            migrationBuilder.DropIndex(
                name: "FK__status_lang__languageId__language__id",
                schema: "twister",
                table: "status_language");

            migrationBuilder.DropIndex(
                name: "FK__status_lang__statusId__status__id",
                schema: "twister",
                table: "status_language");
        }
    }
}
