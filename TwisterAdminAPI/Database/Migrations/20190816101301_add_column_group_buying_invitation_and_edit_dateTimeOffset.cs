﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class add_column_group_buying_invitation_and_edit_dateTimeOffset : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTimeOffset>(
                 name: "createDate",
                 schema: "twister",
                 table: "group_buying_product_variant",
                 nullable: false,
                 defaultValueSql: "CURRENT_TIMESTAMP",
                 oldClrType: typeof(DateTime),
                 oldDefaultValueSql: "CURRENT_TIMESTAMP");

            migrationBuilder.AlterColumn<DateTimeOffset>(
                name: "updateDate",
                schema: "twister",
                table: "group_buying_product_variant",
                nullable: false,
                defaultValueSql: "CURRENT_TIMESTAMP",
                oldClrType: typeof(DateTime),
                oldDefaultValueSql: "CURRENT_TIMESTAMP");

            migrationBuilder.AlterColumn<DateTimeOffset>(
                name: "createDate",
                schema: "twister",
                table: "group_buying",
                nullable: false,
                defaultValueSql: "CURRENT_TIMESTAMP",
                oldClrType: typeof(DateTime),
                oldDefaultValueSql: "CURRENT_TIMESTAMP");

            migrationBuilder.AlterColumn<DateTimeOffset>(
                name: "updateDate",
                schema: "twister",
                table: "group_buying",
                nullable: false,
                defaultValueSql: "CURRENT_TIMESTAMP",
                oldClrType: typeof(DateTime),
                oldDefaultValueSql: "CURRENT_TIMESTAMP");

            migrationBuilder.AddColumn<int>(
                name: "InviteeCustomerUserId",
                schema: "twister",
                table: "group_buying_invitation",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "FK_gbiv_inviteeCustomerUserId_customer_user_id",
                schema: "twister",
                table: "group_buying_invitation",
                column: "InviteeCustomerUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_gbiv_inviteeCustomerUserId_customer_user_id",
                schema: "twister",
                table: "group_buying_invitation",
                column: "InviteeCustomerUserId",
                principalSchema: "twister",
                principalTable: "customer_user",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_gbiv_inviteeCustomerUserId_customer_user_id",
                schema: "twister",
                table: "group_buying_invitation");

            migrationBuilder.DropIndex(
                name: "FK_gbiv_inviteeCustomerUserId_customer_user_id",
                schema: "twister",
                table: "group_buying_invitation");

            migrationBuilder.AlterColumn<DateTime>(
                name: "createDate",
                schema: "twister",
                table: "group_buying_product_variant",
                nullable: false,
                defaultValueSql: "CURRENT_TIMESTAMP",
                oldClrType: typeof(DateTimeOffset),
                oldDefaultValueSql: "CURRENT_TIMESTAMP");

            migrationBuilder.AlterColumn<DateTime>(
                name: "updatedDate",
                schema: "twister",
                table: "group_buying_product_variant",
                nullable: false,
                defaultValueSql: "CURRENT_TIMESTAMP",
                oldClrType: typeof(DateTimeOffset),
                oldDefaultValueSql: "CURRENT_TIMESTAMP");

            migrationBuilder.AlterColumn<DateTime>(
                name: "createDate",
                schema: "twister",
                table: "group_buying",
                nullable: false,
                defaultValueSql: "CURRENT_TIMESTAMP",
                oldClrType: typeof(DateTimeOffset),
                oldDefaultValueSql: "CURRENT_TIMESTAMP");

            migrationBuilder.AlterColumn<DateTime>(
                name: "updatedDate",
                schema: "twister",
                table: "group_buying",
                nullable: false,
                defaultValueSql: "CURRENT_TIMESTAMP",
                oldClrType: typeof(DateTimeOffset),
                oldDefaultValueSql: "CURRENT_TIMESTAMP");

            migrationBuilder.DropColumn(
                name: "inviteeCustomerUserId",
                schema: "twister",
                table: "group_buying_invitation");

            migrationBuilder.AlterColumn<DateTime>(
                name: "createDate",
                schema: "twister",
                table: "group_buying",
                nullable: false,
                defaultValueSql: "CURRENT_TIMESTAMP",
                oldClrType: typeof(DateTimeOffset),
                oldDefaultValueSql: "CURRENT_TIMESTAMP");

            migrationBuilder.AlterColumn<DateTime>(
                name: "updatedDate",
                schema: "twister",
                table: "group_buying",
                nullable: false,
                defaultValueSql: "CURRENT_TIMESTAMP",
                oldClrType: typeof(DateTimeOffset),
                oldDefaultValueSql: "CURRENT_TIMESTAMP");
        }
    }
}
