﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_banner_marketplace_product : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK__bmp__bmId__bm__id",
                schema: "twister",
                table: "banner_marketplace_product",
                column: "bannerMarketplaceId");

            migrationBuilder.CreateIndex(
                name: "FK_bmp__productId__product__id",
                schema: "twister",
                table: "banner_marketplace_product",
                column: "productId");

            migrationBuilder.AddForeignKey(
                name: "FK__bmp__bmId__bm__id",
                schema: "twister",
                table: "banner_marketplace_product",
                column: "bannerMarketplaceId",
                principalSchema: "twister",
                principalTable: "banner_marketplace",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_bmp__productId__product__id",
                schema: "twister",
                table: "banner_marketplace_product",
                column: "productId",
                principalSchema: "twister",
                principalTable: "product",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__bmp__bmId__bm__id",
                schema: "twister",
                table: "banner_marketplace_product");

            migrationBuilder.DropForeignKey(
                name: "FK_bmp__productId__product__id",
                schema: "twister",
                table: "banner_marketplace_product");

            migrationBuilder.DropIndex(
                name: "FK__bmp__bmId__bm__id",
                schema: "twister",
                table: "banner_marketplace_product");

            migrationBuilder.DropIndex(
                name: "FK_bmp__productId__product__id",
                schema: "twister",
                table: "banner_marketplace_product");
        }
    }
}
