﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
           
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "__nvjmigrationdatahistory",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "__nvjmigrationhistory",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "allow_open_locker",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "allowstatusinfo",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "banner_food_seller",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "banner_marketplace_product",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "brand",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "brand_language",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "cart_product",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "category_marketplace_hierarchy",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "category_marketplace_language",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "category_marketplace_product",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "category_seller_hierarchy",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "category_seller_product",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "chat_customer_service",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "chat_seller",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "city_language",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "coin_transaction",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "condo_rate",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "condo_register",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "condo_sharebox",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "condoadmin_register",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "condoinfo",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "config",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "country_language",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "coupon_type",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "credit_note_product",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "customer_address",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "customer_company",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "customer_review_product",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "customer_review_seller",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "customer_search_history_log",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "customer_user_view_history_log",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "discount_type",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "event_locker",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "favorite_product",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "follow",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "gateway_provider_payment_type",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "invoice_receipt_product",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "lockeraccess",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "lockertypeinfo",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "lockerusermapping",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "logistic_user_copy",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "logopenlocker",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "marketplace_coupon",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "marketplace_promotion_redeem",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "marketplace_review_seller",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "notification_customer",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "notification_manual",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "notification_manual_language",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "notification_seller",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "notification_template",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "notification_template_language",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "payment",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "point",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "product_copy",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "product_image",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "product_rating",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "product_sold",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "province_language",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "purchase_order_delivered",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "purchase_order_log",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "purchase_order_logistic_user",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "purchase_order_parcel",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "purchase_order_product_actual_quantity",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "purchase_order_product_piece",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "regiscode",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "relate_category",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "relate_product",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "role_class_ability",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "sale_order_discount",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "sale_order_product",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_area",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_coupon_redeem",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_image",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_product_delivery",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_promotion_redeem",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_stats",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_stats_type_language",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_user",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_workday_group_holiday",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_workday_profile_group",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "sharelocker",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "status_language",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "stock",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "town_language",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "variant",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "variant_group",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "webuser",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "wishlist",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "working_day",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "zone",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "logdelivery",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "banner_marketplace",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "banner_seller",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "credit_note",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "lockerinfo",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "marketplace_promotion",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "notification_type",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "credit_card",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "gateway_provider",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "payment_type",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "marketplace_coupon_group",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "logistic_user",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "purchase_order_product",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "purchase_order_tracking",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "role_ability",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_coupon",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_promotion",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_stats_type",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_workday_profile",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "product_variant",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "stock_type",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "language",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "variant_template",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "invoice_receipt",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "sale_order",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "purchase_order",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "coupon_price",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "marketplace_user",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "promotion_price",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "cart",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "logistic_company",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "role_class",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "product",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "customer_user",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "category_marketplace",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "category_seller",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "event",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_address",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "country",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "province",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "town",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_group",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_workday_group",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "status",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "city",
                schema: "twister");
        }
    }
}
