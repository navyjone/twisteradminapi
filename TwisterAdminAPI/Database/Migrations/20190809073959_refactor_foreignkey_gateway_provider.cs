﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_gateway_provider : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK__gp__statusId__status__id",
                schema: "twister",
                table: "gateway_provider",
                column: "statusId");

            migrationBuilder.AddForeignKey(
                name: "FK__gp__statusId__status__id",
                schema: "twister",
                table: "gateway_provider",
                column: "statusId",
                principalSchema: "twister",
                principalTable: "status",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__gp__statusId__status__id",
                schema: "twister",
                table: "gateway_provider");

            migrationBuilder.DropIndex(
                name: "FK__gp__statusId__status__id",
                schema: "twister",
                table: "gateway_provider");
        }
    }
}
