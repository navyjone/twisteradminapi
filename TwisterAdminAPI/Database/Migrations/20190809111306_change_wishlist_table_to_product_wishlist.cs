﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class change_wishlist_table_to_product_wishlist : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "wishlist",
                schema: "twister");

            migrationBuilder.CreateTable(
                name: "product_wishlist",
                schema: "twister",
                columns: table => new
                {
                    customerUserId = table.Column<int>(type: "int(10)", nullable: false),
                    productId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product_wishlist", x => new { x.customerUserId, x.productId });
                    table.ForeignKey(
                        name: "FK__product_wishlist__customerUserId__cust_user__id",
                        column: x => x.customerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__product_wishlist__productId__product__id",
                        column: x => x.productId,
                        principalSchema: "twister",
                        principalTable: "product",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "FK__product_wishlist__productId__product__id",
                schema: "twister",
                table: "product_wishlist",
                column: "productId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "product_wishlist",
                schema: "twister");

            migrationBuilder.CreateTable(
                name: "wishlist",
                schema: "twister",
                columns: table => new
                {
                    customerUserId = table.Column<int>(type: "int(10)", nullable: false),
                    productId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_wishlist", x => new { x.customerUserId, x.productId });
                    table.ForeignKey(
                        name: "wishlist_ibfk_1",
                        column: x => x.customerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });
        }
    }
}
