﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class Add_product_type_table_and_add_column_in_product : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "expireDate",
                schema: "twister",
                table: "product",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "expireDurationInMinute",
                schema: "twister",
                table: "product",
                type: "int(10)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "productTypeId",
                schema: "twister",
                table: "product",
                type: "int(4)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "product_type",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(4)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(unicode: false, nullable: false),
                    description = table.Column<string>(unicode: false, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product_type", x => x.id);
                });

            migrationBuilder.CreateIndex(
                name: "fk__product__productTypeId__product_type__id",
                schema: "twister",
                table: "product",
                column: "productTypeId");

            migrationBuilder.AddForeignKey(
                name: "fk__product__productTypeId__product_type__id",
                schema: "twister",
                table: "product",
                column: "productTypeId",
                principalSchema: "twister",
                principalTable: "product_type",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk__product__productTypeId__product_type__id",
                schema: "twister",
                table: "product");

            migrationBuilder.DropTable(
                name: "product_type",
                schema: "twister");

            migrationBuilder.DropIndex(
                name: "fk__product__productTypeId__product_type__id",
                schema: "twister",
                table: "product");

            migrationBuilder.DropColumn(
                name: "expireDate",
                schema: "twister",
                table: "product");

            migrationBuilder.DropColumn(
                name: "expireDurationInMinute",
                schema: "twister",
                table: "product");

            migrationBuilder.DropColumn(
                name: "productTypeId",
                schema: "twister",
                table: "product");
        }
    }
}
