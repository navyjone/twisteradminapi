﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_coupon_type : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK__coupon_type__catMarketplaceId__cat_market__id",
                schema: "twister",
                table: "coupon_type",
                column: "categoryMarketplaceId");

            migrationBuilder.CreateIndex(
                name: "FK__coupon_type__couponPriceId__coupon_price__id",
                schema: "twister",
                table: "coupon_type",
                column: "couponPriceId");

            migrationBuilder.CreateIndex(
                name: "FK__coupon_type__productId__product__id",
                schema: "twister",
                table: "coupon_type",
                column: "productId");

            migrationBuilder.CreateIndex(
                name: "FK__coupon_type__sellerId__seller__id",
                schema: "twister",
                table: "coupon_type",
                column: "sellerId");

            migrationBuilder.AddForeignKey(
                name: "FK__coupon_type__catMarketplaceId__cat_market__id",
                schema: "twister",
                table: "coupon_type",
                column: "categoryMarketplaceId",
                principalSchema: "twister",
                principalTable: "category_marketplace",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__coupon_type__couponPriceId__coupon_price__id",
                schema: "twister",
                table: "coupon_type",
                column: "couponPriceId",
                principalSchema: "twister",
                principalTable: "coupon_price",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__coupon_type__sellerId__seller__id",
                schema: "twister",
                table: "coupon_type",
                column: "productId",
                principalSchema: "twister",
                principalTable: "product",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__coupon_type__productId__product__id",
                schema: "twister",
                table: "coupon_type",
                column: "sellerId",
                principalSchema: "twister",
                principalTable: "seller",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__coupon_type__catMarketplaceId__cat_market__id",
                schema: "twister",
                table: "coupon_type");

            migrationBuilder.DropForeignKey(
                name: "FK__coupon_type__couponPriceId__coupon_price__id",
                schema: "twister",
                table: "coupon_type");

            migrationBuilder.DropForeignKey(
                name: "FK__coupon_type__sellerId__seller__id",
                schema: "twister",
                table: "coupon_type");

            migrationBuilder.DropForeignKey(
                name: "FK__coupon_type__productId__product__id",
                schema: "twister",
                table: "coupon_type");

            migrationBuilder.DropIndex(
                name: "FK__coupon_type__catMarketplaceId__cat_market__id",
                schema: "twister",
                table: "coupon_type");

            migrationBuilder.DropIndex(
                name: "FK__coupon_type__couponPriceId__coupon_price__id",
                schema: "twister",
                table: "coupon_type");

            migrationBuilder.DropIndex(
                name: "FK__coupon_type__productId__product__id",
                schema: "twister",
                table: "coupon_type");

            migrationBuilder.DropIndex(
                name: "FK__coupon_type__sellerId__seller__id",
                schema: "twister",
                table: "coupon_type");
        }
    }
}
