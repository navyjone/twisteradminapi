﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_category_marketplace : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK__cat_marketplace__statusId__status__id",
                schema: "twister",
                table: "category_marketplace",
                column: "statusId");

            migrationBuilder.AddForeignKey(
                name: "FK__cat_marketplace__statusId__status__id",
                schema: "twister",
                table: "category_marketplace",
                column: "statusId",
                principalSchema: "twister",
                principalTable: "status",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__cat_marketplace__statusId__status__id",
                schema: "twister",
                table: "category_marketplace");

            migrationBuilder.DropIndex(
                name: "FK__cat_marketplace__statusId__status__id",
                schema: "twister",
                table: "category_marketplace");
        }
    }
}
