﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class add_column_deeplinkId_at_notification_manual_and_notification_template : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "link",
                schema: "twister",
                table: "notification_template");

            migrationBuilder.DropColumn(
                name: "link",
                schema: "twister",
                table: "notification_manual");

            migrationBuilder.AddColumn<int>(
                name: "appDeepLinkId",
                schema: "twister",
                table: "notification_template",
                type: "int(10)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "appDeepLinkId",
                schema: "twister",
                table: "notification_manual",
                type: "int(10)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "fk-notification_template-app_deep_link.id",
                schema: "twister",
                table: "notification_template",
                column: "appDeepLinkId");

            migrationBuilder.CreateIndex(
                name: "fk-notification_manual-app_deep_link.id",
                schema: "twister",
                table: "notification_manual",
                column: "appDeepLinkId");

            migrationBuilder.AddForeignKey(
                name: "fk-notification_manual-app_deep_link.id",
                schema: "twister",
                table: "notification_manual",
                column: "appDeepLinkId",
                principalSchema: "twister",
                principalTable: "app_deep_link",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "fk-notification_template-app_deep_link.id",
                schema: "twister",
                table: "notification_template",
                column: "appDeepLinkId",
                principalSchema: "twister",
                principalTable: "app_deep_link",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk-notification_manual-app_deep_link.id",
                schema: "twister",
                table: "notification_manual");

            migrationBuilder.DropForeignKey(
                name: "fk-notification_template-app_deep_link.id",
                schema: "twister",
                table: "notification_template");

            migrationBuilder.DropIndex(
                name: "fk-notification_template-app_deep_link.id",
                schema: "twister",
                table: "notification_template");

            migrationBuilder.DropIndex(
                name: "fk-notification_manual-app_deep_link.id",
                schema: "twister",
                table: "notification_manual");

            migrationBuilder.DropColumn(
                name: "appDeepLinkId",
                schema: "twister",
                table: "notification_template");

            migrationBuilder.DropColumn(
                name: "appDeepLinkId",
                schema: "twister",
                table: "notification_manual");

            migrationBuilder.AddColumn<string>(
                name: "link",
                schema: "twister",
                table: "notification_template",
                unicode: false,
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "link",
                schema: "twister",
                table: "notification_manual",
                unicode: false,
                maxLength: 255,
                nullable: true);
        }
    }
}
