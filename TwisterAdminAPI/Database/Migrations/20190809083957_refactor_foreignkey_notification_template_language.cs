﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_notification_template_language : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK__noti_temp_lang__languageId__language__id",
                schema: "twister",
                table: "notification_template_language",
                column: "languageId");

            migrationBuilder.CreateIndex(
                name: "FK__noti_temp_lang__notiTempId__noti_temp__id",
                schema: "twister",
                table: "notification_template_language",
                column: "notificationTemplateId");

            migrationBuilder.AddForeignKey(
                name: "FK__noti_temp_lang__languageId__language__id",
                schema: "twister",
                table: "notification_template_language",
                column: "languageId",
                principalSchema: "twister",
                principalTable: "language",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__noti_temp_lang__notiTempId__noti_temp__id",
                schema: "twister",
                table: "notification_template_language",
                column: "notificationTemplateId",
                principalSchema: "twister",
                principalTable: "notification_template",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__noti_temp_lang__languageId__language__id",
                schema: "twister",
                table: "notification_template_language");

            migrationBuilder.DropForeignKey(
                name: "FK__noti_temp_lang__notiTempId__noti_temp__id",
                schema: "twister",
                table: "notification_template_language");

            migrationBuilder.DropIndex(
                name: "FK__noti_temp_lang__languageId__language__id",
                schema: "twister",
                table: "notification_template_language");

            migrationBuilder.DropIndex(
                name: "FK__noti_temp_lang__notiTempId__noti_temp__id",
                schema: "twister",
                table: "notification_template_language");
        }
    }
}
