﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_customer_user : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK__customer_user__languageId__language__id",
                schema: "twister",
                table: "customer_user",
                column: "languageId");

            migrationBuilder.AddForeignKey(
                name: "FK__customer_user__languageId__language__id",
                schema: "twister",
                table: "customer_user",
                column: "languageId",
                principalSchema: "twister",
                principalTable: "language",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__customer_user__languageId__language__id",
                schema: "twister",
                table: "customer_user");

            migrationBuilder.DropIndex(
                name: "FK__customer_user__languageId__language__id",
                schema: "twister",
                table: "customer_user");
        }
    }
}
