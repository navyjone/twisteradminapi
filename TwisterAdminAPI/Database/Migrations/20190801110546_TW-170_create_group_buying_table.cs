﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class TW170_create_group_buying_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "group_buying",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    groupBuyingProductVariantId = table.Column<int>(type: "int(10)", nullable: false),
                    customerUserId = table.Column<int>(type: "int(10)", nullable: false),
                    startDate = table.Column<DateTime>(nullable: false),
                    endDate = table.Column<DateTime>(nullable: false),
                    createDate = table.Column<DateTime>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    updateDate = table.Column<DateTime>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    statusId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_group_buying", x => x.id);
                    table.ForeignKey(
                        name: "FK_gb_customerUserId_customer_user_id",
                        column: x => x.customerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_gb_gbpvId_gbpv_id",
                        column: x => x.groupBuyingProductVariantId,
                        principalSchema: "twister",
                        principalTable: "group_buying_product_variant",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_gb_statusId_status_id",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "FK_gb_customerUserId_customer_user_id",
                schema: "twister",
                table: "group_buying",
                column: "customerUserId");

            migrationBuilder.CreateIndex(
                name: "FK_gb_gbpvId_gbpv_id",
                schema: "twister",
                table: "group_buying",
                column: "groupBuyingProductVariantId");

            migrationBuilder.CreateIndex(
                name: "FK_gb_statusId_status_id",
                schema: "twister",
                table: "group_buying",
                column: "statusId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "group_buying",
                schema: "twister");
        }
    }
}
