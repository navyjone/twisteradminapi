﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_credit_note : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK__credit_note__customerUserId__customer_user__id",
                schema: "twister",
                table: "credit_note",
                column: "customerUserId");

            migrationBuilder.AddForeignKey(
                name: "FK__credit_note__customerUserId__customer_user__id",
                schema: "twister",
                table: "credit_note",
                column: "customerUserId",
                principalSchema: "twister",
                principalTable: "customer_user",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__credit_note__customerUserId__customer_user__id",
                schema: "twister",
                table: "credit_note");

            migrationBuilder.DropIndex(
                name: "FK__credit_note__customerUserId__customer_user__id",
                schema: "twister",
                table: "credit_note");
        }
    }
}
