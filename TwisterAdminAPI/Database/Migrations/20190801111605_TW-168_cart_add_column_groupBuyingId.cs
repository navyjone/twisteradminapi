﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class TW168_cart_add_column_groupBuyingId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "groupBuyingId",
                schema: "twister",
                table: "cart",
                type: "int(10)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "FK-cart_groupBuyingId_group_buying_id",
                schema: "twister",
                table: "cart",
                column: "groupBuyingId");

            migrationBuilder.AddForeignKey(
                name: "FK-cart_groupBuyingId_group_buying_id",
                schema: "twister",
                table: "cart",
                column: "groupBuyingId",
                principalSchema: "twister",
                principalTable: "group_buying",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("ALTER TABLE `cart` DROP FOREIGN KEY `FK-cart_groupBuyingId_group_buying_id`");
            //migrationBuilder.DropForeignKey(
            //    name: "FK-cart_groupBuyingId_group_buying_id",
            //    schema: "twister",
            //    table: "cart");

            migrationBuilder.Sql(@"ALTER TABLE `cart` DROP INDEX `FK-cart_groupBuyingId_group_buying_id`;");
            //migrationBuilder.DropIndex(
            //    name: "FK-cart_groupBuyingId_group_buying_id",
            //    schema: "twister",
            //    table: "cart");

            migrationBuilder.DropColumn(
                name: "groupBuyingId",
                schema: "twister",
                table: "cart");
        }
    }
}
