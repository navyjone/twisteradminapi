﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_logistic_company : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK__logistic_company__countryId__country__id",
                schema: "twister",
                table: "logistic_company",
                column: "countryId");

            migrationBuilder.AddForeignKey(
                name: "FK__logistic_company__countryId__country__id",
                schema: "twister",
                table: "logistic_company",
                column: "countryId",
                principalSchema: "twister",
                principalTable: "country",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__logistic_company__countryId__country__id",
                schema: "twister",
                table: "logistic_company");

            migrationBuilder.DropIndex(
                name: "FK__logistic_company__countryId__country__id",
                schema: "twister",
                table: "logistic_company");
        }
    }
}
