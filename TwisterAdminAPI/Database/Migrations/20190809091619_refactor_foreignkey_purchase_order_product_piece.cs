﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_purchase_order_product_piece : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK__po_product_piece__statusId__status__id",
                schema: "twister",
                table: "purchase_order_product_piece",
                column: "statusId");

            migrationBuilder.AddForeignKey(
                name: "FK__po_product_piece__statusId__status__id",
                schema: "twister",
                table: "purchase_order_product_piece",
                column: "statusId",
                principalSchema: "twister",
                principalTable: "status",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__po_product_piece__statusId__status__id",
                schema: "twister",
                table: "purchase_order_product_piece");

            migrationBuilder.DropIndex(
                name: "FK__po_product_piece__statusId__status__id",
                schema: "twister",
                table: "purchase_order_product_piece");
        }
    }
}
