﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_coin_transaction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK__coin_tran__saleOrderId__sale_order__id",
                schema: "twister",
                table: "coin_transaction",
                column: "saleOrderId");

            migrationBuilder.AddForeignKey(
                name: "FK__coin_tran__saleOrderId__sale_order__id",
                schema: "twister",
                table: "coin_transaction",
                column: "saleOrderId",
                principalSchema: "twister",
                principalTable: "sale_order",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__coin_tran__saleOrderId__sale_order__id",
                schema: "twister",
                table: "coin_transaction");

            migrationBuilder.DropIndex(
                name: "FK__coin_tran__saleOrderId__sale_order__id",
                schema: "twister",
                table: "coin_transaction");
        }
    }
}
