﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class create_admin_refresh_token_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "admin_refresh_token",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    token = table.Column<string>(type: "text", nullable: false),
                    marketplaceUserId = table.Column<int>(type: "int(10)", nullable: false),
                    expireDate = table.Column<DateTime>(nullable: false),
                    deviceName = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_admin_refresh_token", x => x.id);
                    table.ForeignKey(
                        name: "FK__art__marketplaceUserId__marketplace_user__id",
                        column: x => x.marketplaceUserId,
                        principalSchema: "twister",
                        principalTable: "marketplace_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "FK__art__marketplaceUserId__marketplace_user__id",
                schema: "twister",
                table: "admin_refresh_token",
                column: "marketplaceUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "admin_refresh_token",
                schema: "twister");
        }
    }
}
