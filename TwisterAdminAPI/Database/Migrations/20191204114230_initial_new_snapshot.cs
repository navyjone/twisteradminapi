﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class initial_new_snapshot : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "twister");

            migrationBuilder.CreateTable(
                name: "__nvjmigrationdatahistory",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(unicode: false, nullable: true),
                    runDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK___nvjmigrationdatahistory", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "__nvjmigrationhistory",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(unicode: false, nullable: true),
                    runDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK___nvjmigrationhistory", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "allowstatusinfo",
                schema: "twister",
                columns: table => new
                {
                    transnum = table.Column<long>(type: "bigint(20)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    statusCode = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    statusDescription = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    statusComment = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    createdBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    createdDate = table.Column<DateTime>(nullable: true),
                    updatedBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    updatedDate = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_allowstatusinfo", x => x.transnum);
                });

            migrationBuilder.CreateTable(
                name: "app_deep_link",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(maxLength: 255, nullable: true),
                    link = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_app_deep_link", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "condo_rate",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint(20)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    lockerCode = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    boxID = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "0"),
                    rate = table.Column<double>(type: "double(18,8)", nullable: true, defaultValueSql: "0.00000000"),
                    type = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    createdBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    createdDate = table.Column<DateTime>(nullable: true),
                    updatedBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    updatedDate = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_condo_rate", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "condo_register",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint(20)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    condoID = table.Column<long>(type: "bigint(50)", nullable: false, defaultValueSql: "0"),
                    telephone = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    status = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    createdBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    createdDate = table.Column<DateTime>(nullable: true),
                    updatedBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    updatedDate = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_condo_register", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "condo_sharebox",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint(20)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    condoID = table.Column<long>(type: "bigint(20)", nullable: true),
                    lockerCode = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    status = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    createdBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    createdDate = table.Column<DateTime>(nullable: true),
                    updatedBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    updatedDate = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_condo_sharebox", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "condoadmin_register",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint(20)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    condoID = table.Column<long>(type: "bigint(20)", nullable: false, defaultValueSql: "0"),
                    telephone = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    status = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    createdBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    createdDate = table.Column<DateTime>(nullable: true),
                    updatedBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    updatedDate = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_condoadmin_register", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "condoinfo",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint(20)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    condoName = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    address = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    lat = table.Column<double>(type: "double(20,10)", nullable: true, defaultValueSql: "0.0000000000"),
                    lon = table.Column<double>(type: "double(20,10)", nullable: true, defaultValueSql: "0.0000000000"),
                    linkDelivery = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    linkOpen = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    createdBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    createdDate = table.Column<DateTime>(nullable: true),
                    updatedBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    updatedDate = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_condoinfo", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "config",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    value = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    description = table.Column<string>(unicode: false, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_config", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "country",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(4)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_country", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "coupon_price",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    discountPercent = table.Column<decimal>(type: "decimal(10,0)", nullable: true),
                    discountTotal = table.Column<decimal>(type: "decimal(10,0)", nullable: true),
                    discountLimit = table.Column<decimal>(type: "decimal(10,0)", nullable: true),
                    minimumPurchase = table.Column<decimal>(type: "decimal(10,0)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_coupon_price", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "customer_company",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint(20)", nullable: false),
                    prefix = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    firstName = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    lastName = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    cardID = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    address = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    company = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    brand = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    group = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    status = table.Column<int>(type: "int(11)", nullable: true),
                    createdBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    createdDate = table.Column<DateTime>(nullable: true),
                    updatedBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    updatedDate = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_customer_company", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "discount_type",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(4)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_discount_type", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "group_buying_achievement_type",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_group_buying_achievement_type", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "group_buying_condition",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    targetBuyer = table.Column<int>(type: "int(10)", nullable: true),
                    targetQuantity = table.Column<int>(type: "int(10)", nullable: true),
                    targetPrice = table.Column<decimal>(type: "decimal(10, 2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_group_buying_condition", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "language",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(4)", nullable: false),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_language", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "lockeraccess",
                schema: "twister",
                columns: table => new
                {
                    accessID = table.Column<long>(type: "bigint(20)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    lockerCode = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    boxID = table.Column<int>(type: "int(3)", nullable: true),
                    guestTelnum = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    validDate = table.Column<DateTime>(nullable: true),
                    expireDate = table.Column<DateTime>(nullable: true),
                    status = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    createdBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    createdDate = table.Column<DateTime>(nullable: true),
                    updatedBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    updatedDate = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_lockeraccess", x => x.accessID);
                });

            migrationBuilder.CreateTable(
                name: "lockerinfo",
                schema: "twister",
                columns: table => new
                {
                    lockerID = table.Column<long>(type: "bigint(20)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    lockerCode = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    lockerTypeCode = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    publicName = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    registryCode = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    lockerPassword = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    lockerAddress = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    zoneID = table.Column<int>(type: "int(4)", nullable: true),
                    lockerLat = table.Column<double>(type: "double(18,10)", nullable: true),
                    lockerLon = table.Column<double>(type: "double(18,10)", nullable: true),
                    lockerActive = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    createdBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    createdDate = table.Column<DateTime>(nullable: true),
                    updatedBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    updatedDate = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_lockerinfo", x => x.lockerID);
                });

            migrationBuilder.CreateTable(
                name: "lockertypeinfo",
                schema: "twister",
                columns: table => new
                {
                    lockerTypeID = table.Column<long>(type: "bigint(20)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    lockerTypeCode = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    lockerTypeName = table.Column<string>(unicode: false, maxLength: 150, nullable: true),
                    lockerQtyBox = table.Column<int>(type: "int(11)", nullable: true),
                    createdBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    createdDate = table.Column<DateTime>(nullable: true),
                    updatedBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    updatedDate = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_lockertypeinfo", x => x.lockerTypeID);
                });

            migrationBuilder.CreateTable(
                name: "lockerusermapping",
                schema: "twister",
                columns: table => new
                {
                    mappingID = table.Column<long>(type: "bigint(20)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    lockerCode = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    userID = table.Column<long>(type: "bigint(20)", nullable: true),
                    lockerName = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    userActive = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    createdBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    createdDate = table.Column<DateTime>(nullable: true),
                    updatedBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    updatedDate = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_lockerusermapping", x => x.mappingID);
                });

            migrationBuilder.CreateTable(
                name: "logdelivery",
                schema: "twister",
                columns: table => new
                {
                    transnum = table.Column<long>(type: "bigint(20)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    deliveryID = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    lockerCode = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    boxID = table.Column<int>(type: "int(3)", nullable: true),
                    openDate = table.Column<DateTime>(nullable: true),
                    accessID = table.Column<long>(type: "bigint(20)", nullable: true),
                    status = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    userID = table.Column<long>(type: "bigint(20)", nullable: true),
                    userType = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    customerUserId = table.Column<int>(type: "int(10)", nullable: true),
                    refDelivery = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    createdBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    createdDate = table.Column<DateTime>(nullable: true),
                    updatedBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    updatedDate = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_logdelivery", x => x.transnum);
                });

            migrationBuilder.CreateTable(
                name: "logopenlocker",
                schema: "twister",
                columns: table => new
                {
                    openID = table.Column<long>(type: "bigint(20)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    lockerCode = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    boxID = table.Column<int>(type: "int(3)", nullable: true),
                    openDate = table.Column<DateTime>(nullable: true),
                    transnumDelivery = table.Column<long>(type: "bigint(20)", nullable: true),
                    userID = table.Column<long>(type: "bigint(20)", nullable: true),
                    userType = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    status = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    batterylevel = table.Column<int>(type: "int(10)", nullable: true, defaultValueSql: "0"),
                    createdBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    createdDate = table.Column<DateTime>(nullable: true),
                    updatedBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    updatedDate = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_logopenlocker", x => x.openID);
                });

            migrationBuilder.CreateTable(
                name: "media_type",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    description = table.Column<string>(unicode: false, maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_media_type", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "notification_type",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(4)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_notification_type", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "payment_type",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(4)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    description = table.Column<string>(unicode: false, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_payment_type", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "product_type",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(4)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    description = table.Column<string>(unicode: false, maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product_type", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "regiscode",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    lockerCode = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    registryCode = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_regiscode", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "role_ability",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(5)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    description = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_role_ability", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "role_class",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(5)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    description = table.Column<string>(unicode: false, maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_role_class", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "seller_stats_type",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(4)", nullable: false),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    description = table.Column<string>(unicode: false, maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_seller_stats_type", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "seller_workday_profile",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    dayAmount = table.Column<int>(type: "int(3)", nullable: true),
                    timeStart = table.Column<TimeSpan>(nullable: false),
                    timeEnd = table.Column<TimeSpan>(nullable: false),
                    monthStart = table.Column<int>(type: "int(2)", nullable: true),
                    monthEnd = table.Column<int>(type: "int(2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_seller_workday_profile", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "sharelocker",
                schema: "twister",
                columns: table => new
                {
                    shareID = table.Column<long>(type: "bigint(20)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    lockerCode = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    userID = table.Column<long>(type: "bigint(20)", nullable: true),
                    createdBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    createdDate = table.Column<DateTime>(nullable: true),
                    updatedBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    updatedDate = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sharelocker", x => x.shareID);
                });

            migrationBuilder.CreateTable(
                name: "status",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    code = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    name = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    type = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    description = table.Column<string>(unicode: false, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_status", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "stock_type",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(unicode: false, maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_stock_type", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "variant_template",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_variant_template", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "webuser",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint(20)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    telephone = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    password = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    firstname = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    lastname = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    email = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    status = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    authorize = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    createdBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    createdDate = table.Column<DateTime>(nullable: true),
                    updatedBy = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    updatedDate = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_webuser", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "working_day",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    sunday = table.Column<short>(type: "bit(1)", nullable: false, defaultValueSql: "b'0'"),
                    monday = table.Column<short>(type: "bit(1)", nullable: false, defaultValueSql: "b'0'"),
                    tuesday = table.Column<short>(type: "bit(1)", nullable: false, defaultValueSql: "b'0'"),
                    wednesday = table.Column<short>(type: "bit(1)", nullable: false, defaultValueSql: "b'0'"),
                    thursday = table.Column<short>(type: "bit(1)", nullable: false, defaultValueSql: "b'0'"),
                    friday = table.Column<short>(type: "bit(1)", nullable: false, defaultValueSql: "b'0'"),
                    saturday = table.Column<short>(type: "bit(1)", nullable: false, defaultValueSql: "b'0'"),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_working_day", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "zone",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(6)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    route = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_zone", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "province",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    countryId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_province", x => x.id);
                    table.ForeignKey(
                        name: "FK__province__countryId__country__id",
                        column: x => x.countryId,
                        principalSchema: "twister",
                        principalTable: "country",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "country_language",
                schema: "twister",
                columns: table => new
                {
                    countryId = table.Column<int>(type: "int(10)", nullable: false),
                    languageId = table.Column<int>(type: "int(10)", nullable: false),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_country_language", x => new { x.countryId, x.languageId });
                    table.ForeignKey(
                        name: "FK__country_language__countryId__country__id",
                        column: x => x.countryId,
                        principalSchema: "twister",
                        principalTable: "country",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__country_language__languageId__language__id",
                        column: x => x.languageId,
                        principalSchema: "twister",
                        principalTable: "language",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "notification_manual",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    notificationTypeId = table.Column<int>(type: "int(4)", nullable: true),
                    method = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    description = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    appDeepLinkId = table.Column<int>(type: "int(10)", nullable: true),
                    createDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_notification_manual", x => x.id);
                    table.ForeignKey(
                        name: "fk-notification_manual-app_deep_link.id",
                        column: x => x.appDeepLinkId,
                        principalSchema: "twister",
                        principalTable: "app_deep_link",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__noti_man__notiTypeId__noti_type__id",
                        column: x => x.notificationTypeId,
                        principalSchema: "twister",
                        principalTable: "notification_type",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "notification_template",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(5)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    notificationTypeId = table.Column<int>(type: "int(4)", nullable: true),
                    method = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    description = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    appDeepLinkId = table.Column<int>(type: "int(10)", nullable: true),
                    createDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_notification_template", x => x.id);
                    table.ForeignKey(
                        name: "fk-notification_template-app_deep_link.id",
                        column: x => x.appDeepLinkId,
                        principalSchema: "twister",
                        principalTable: "app_deep_link",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__noti_temp__notiTypeId__noti_type__id",
                        column: x => x.notificationTypeId,
                        principalSchema: "twister",
                        principalTable: "notification_type",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "role_class_ability",
                schema: "twister",
                columns: table => new
                {
                    roleClassId = table.Column<int>(type: "int(5)", nullable: false),
                    roleAbilityId = table.Column<int>(type: "int(5)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_role_class_ability", x => new { x.roleClassId, x.roleAbilityId });
                    table.ForeignKey(
                        name: "FK__role_class_ability__roleAbilityId__role_ability__id",
                        column: x => x.roleAbilityId,
                        principalSchema: "twister",
                        principalTable: "role_ability",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__role_class_ability__roleClassId__role_class_id",
                        column: x => x.roleClassId,
                        principalSchema: "twister",
                        principalTable: "role_class",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "seller_stats_type_language",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    sellerStatsTypeId = table.Column<int>(type: "int(10)", nullable: false),
                    languageId = table.Column<int>(type: "int(4)", nullable: false),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_seller_stats_type_language", x => x.id);
                    table.ForeignKey(
                        name: "FK_seller_s_types_language__languageId__language__id",
                        column: x => x.languageId,
                        principalSchema: "twister",
                        principalTable: "language",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_seller_s_types_language__sellerStatsTypeId__seller_s_type__id",
                        column: x => x.sellerStatsTypeId,
                        principalSchema: "twister",
                        principalTable: "seller_stats_type",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "brand",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    keyword = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    image = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    createDate = table.Column<DateTime>(nullable: true),
                    updateDate = table.Column<DateTime>(nullable: true),
                    sequence = table.Column<int>(type: "int(4)", nullable: true),
                    statusId = table.Column<int>(type: "int(4)", nullable: false, defaultValueSql: "1")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_brand", x => x.id);
                    table.ForeignKey(
                        name: "FK__brand__statusId__status__id",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "category_marketplace",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    image = table.Column<string>(unicode: false, nullable: true),
                    sequence = table.Column<int>(type: "int(4)", nullable: true, defaultValueSql: "999"),
                    banner = table.Column<string>(unicode: false, maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_category_marketplace", x => x.id);
                    table.ForeignKey(
                        name: "FK__cat_marketplace__statusId__status__id",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "customer_user",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    firstName = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    lastName = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    telephone = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    email = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    facebookToken = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    lineToken = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    googleToken = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    username = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    password = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    birthday = table.Column<DateTime>(nullable: true),
                    sex = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    citizenId = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    passportId = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    countryId = table.Column<int>(type: "int(4)", nullable: true),
                    passportIssuePlace = table.Column<int>(type: "int(4)", nullable: true),
                    age = table.Column<int>(type: "int(3)", nullable: true),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    point = table.Column<int>(type: "int(6)", nullable: false, defaultValueSql: "0"),
                    remoteNotificationId = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    uniqueId = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    image = table.Column<string>(unicode: false, nullable: true),
                    coinAmount = table.Column<decimal>(type: "decimal(10,2)", nullable: false, defaultValueSql: "0.00"),
                    languageId = table.Column<int>(type: "int(4)", nullable: true, defaultValueSql: "1"),
                    lastLogin = table.Column<DateTime>(nullable: true),
                    mobileAppVersion = table.Column<string>(unicode: false, maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_customer_user", x => x.id);
                    table.ForeignKey(
                        name: "customer_user_ibfk_1",
                        column: x => x.countryId,
                        principalSchema: "twister",
                        principalTable: "country",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__customer_user__languageId__language__id",
                        column: x => x.languageId,
                        principalSchema: "twister",
                        principalTable: "language",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "customer_user_ibfk_2",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "event",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    startDate = table.Column<DateTime>(nullable: true),
                    endDate = table.Column<DateTime>(nullable: true),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    description = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    address1 = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    address2 = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    code = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_event", x => x.id);
                    table.ForeignKey(
                        name: "FK__event__statusId__status__id",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "gateway_provider",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    description = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    createDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    updateDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_gateway_provider", x => x.id);
                    table.ForeignKey(
                        name: "FK__gp__statusId__status__id",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "seller_group",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_seller_group", x => x.id);
                    table.ForeignKey(
                        name: "FK__seller_group__statusId__status__id",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "status_language",
                schema: "twister",
                columns: table => new
                {
                    statusId = table.Column<int>(type: "int(10)", nullable: false),
                    languageId = table.Column<int>(type: "int(4)", nullable: false),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_status_language", x => new { x.statusId, x.languageId });
                    table.ForeignKey(
                        name: "FK__status_lang__languageId__language__id",
                        column: x => x.languageId,
                        principalSchema: "twister",
                        principalTable: "language",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__status_lang__statusId__status__id",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "variant",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    variantTemplateId = table.Column<int>(type: "int(10)", nullable: false),
                    value = table.Column<string>(unicode: false, maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_variant", x => new { x.id, x.variantTemplateId });
                    table.ForeignKey(
                        name: "variant_ibfk_1",
                        column: x => x.variantTemplateId,
                        principalSchema: "twister",
                        principalTable: "variant_template",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "seller_workday_group",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    workingDayId = table.Column<int>(type: "int(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_seller_workday_group", x => x.id);
                    table.ForeignKey(
                        name: "FK__swg__workingDayId__working_day__id",
                        column: x => x.workingDayId,
                        principalSchema: "twister",
                        principalTable: "working_day",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "city",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    provinceId = table.Column<int>(type: "int(10)", nullable: false),
                    zipcode = table.Column<string>(unicode: false, maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_city", x => x.id);
                    table.ForeignKey(
                        name: "FK__city__provinceId__province__id",
                        column: x => x.provinceId,
                        principalSchema: "twister",
                        principalTable: "province",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "province_language",
                schema: "twister",
                columns: table => new
                {
                    provinceId = table.Column<int>(type: "int(10)", nullable: false),
                    languageId = table.Column<int>(type: "int(10)", nullable: false),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_province_language", x => new { x.provinceId, x.languageId });
                    table.ForeignKey(
                        name: "FK__province_language__languageId__language__id",
                        column: x => x.languageId,
                        principalSchema: "twister",
                        principalTable: "language",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__province_language__provinceId__province__id",
                        column: x => x.provinceId,
                        principalSchema: "twister",
                        principalTable: "province",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "notification_manual_language",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    notificationManualId = table.Column<int>(type: "int(10)", nullable: false),
                    languageId = table.Column<int>(type: "int(4)", nullable: false),
                    topic = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    message = table.Column<string>(unicode: false, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_notification_manual_language", x => x.id);
                    table.ForeignKey(
                        name: "FK__noti_man_lan__languageId__language__id",
                        column: x => x.languageId,
                        principalSchema: "twister",
                        principalTable: "language",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__noti_man_lan__notiManId__noti_man__id",
                        column: x => x.notificationManualId,
                        principalSchema: "twister",
                        principalTable: "notification_manual",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "notification_template_language",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    notificationTemplateId = table.Column<int>(type: "int(5)", nullable: false),
                    languageId = table.Column<int>(type: "int(4)", nullable: false),
                    topic = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    message = table.Column<string>(unicode: false, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_notification_template_language", x => x.id);
                    table.ForeignKey(
                        name: "FK__noti_temp_lang__languageId__language__id",
                        column: x => x.languageId,
                        principalSchema: "twister",
                        principalTable: "language",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__noti_temp_lang__notiTempId__noti_temp__id",
                        column: x => x.notificationTemplateId,
                        principalSchema: "twister",
                        principalTable: "notification_template",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "brand_language",
                schema: "twister",
                columns: table => new
                {
                    brandId = table.Column<int>(type: "int(10)", nullable: false),
                    languageId = table.Column<int>(type: "int(4)", nullable: false),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_brand_language", x => new { x.brandId, x.languageId });
                    table.ForeignKey(
                        name: "FK__brand_lang__brandId__brand__id",
                        column: x => x.brandId,
                        principalSchema: "twister",
                        principalTable: "brand",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__brand_lang__languageId__language__id",
                        column: x => x.languageId,
                        principalSchema: "twister",
                        principalTable: "language",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "category_marketplace_hierarchy",
                schema: "twister",
                columns: table => new
                {
                    parentId = table.Column<int>(type: "int(10)", nullable: false),
                    childrenId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_category_marketplace_hierarchy", x => new { x.parentId, x.childrenId });
                    table.ForeignKey(
                        name: "fk-category_marketplace.id_children",
                        column: x => x.childrenId,
                        principalSchema: "twister",
                        principalTable: "category_marketplace",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk-category_marketplace.id_paraent",
                        column: x => x.parentId,
                        principalSchema: "twister",
                        principalTable: "category_marketplace",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "category_marketplace_language",
                schema: "twister",
                columns: table => new
                {
                    categoryMarketplaceId = table.Column<int>(type: "int(10)", nullable: false),
                    languageId = table.Column<int>(type: "int(4)", nullable: false),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_category_marketplace_language", x => new { x.categoryMarketplaceId, x.languageId });
                    table.ForeignKey(
                        name: "FK__cat_marketplace_lang__catMarId__cat_mar__id",
                        column: x => x.categoryMarketplaceId,
                        principalSchema: "twister",
                        principalTable: "category_marketplace",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__cat_marketplace__lang__languageId__lang__id",
                        column: x => x.languageId,
                        principalSchema: "twister",
                        principalTable: "language",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "credit_card",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    expireDate = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    number = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    token = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    customerUserId = table.Column<int>(type: "int(10)", nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_credit_card", x => x.id);
                    table.ForeignKey(
                        name: "FK__credit_card__customerUserId__customer_user__id",
                        column: x => x.customerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "credit_card_ibfk_1",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "customer_search_history_log",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    customerUserId = table.Column<int>(type: "int(10)", nullable: false),
                    searchWord = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_customer_search_history_log", x => x.id);
                    table.ForeignKey(
                        name: "customer_search_history_log_ibfk_1",
                        column: x => x.customerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "marketplace_user",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    username = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    password = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    createDate = table.Column<DateTime>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    updateDate = table.Column<DateTime>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    customerUserId = table.Column<int>(type: "int(10)", nullable: false),
                    roleClassId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_marketplace_user", x => x.id);
                    table.ForeignKey(
                        name: "FK_marketplace_user__custId__customer_user__id",
                        column: x => x.customerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_marketplace_user__roleClassId__role_class__id",
                        column: x => x.roleClassId,
                        principalSchema: "twister",
                        principalTable: "role_class",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "marketplace_user_ibfk_2",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "notification_customer",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    customerUserId = table.Column<int>(type: "int(10)", nullable: false),
                    notificationTemplateId = table.Column<int>(type: "int(10)", nullable: true),
                    notificationManualId = table.Column<int>(type: "int(10)", nullable: true),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    param = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    linkParam = table.Column<string>(unicode: false, maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_notification_customer", x => x.id);
                    table.ForeignKey(
                        name: "notification_customer_ibfk_1",
                        column: x => x.customerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__noti_customer__notiManId__noti_man__id",
                        column: x => x.notificationManualId,
                        principalSchema: "twister",
                        principalTable: "notification_manual",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__noti_customer__notiTempId__noti_temp__id",
                        column: x => x.notificationTemplateId,
                        principalSchema: "twister",
                        principalTable: "notification_template",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "notification_customer_ibfk_2",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "event_locker",
                schema: "twister",
                columns: table => new
                {
                    eventId = table.Column<int>(type: "int(10)", nullable: false),
                    lockerId = table.Column<long>(type: "bigint(20)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_event_locker", x => new { x.eventId, x.lockerId });
                    table.ForeignKey(
                        name: "event_locker_ibfk_1",
                        column: x => x.eventId,
                        principalSchema: "twister",
                        principalTable: "event",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "event_locker_ibfk_2",
                        column: x => x.lockerId,
                        principalSchema: "twister",
                        principalTable: "lockerinfo",
                        principalColumn: "lockerID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "gateway_provider_payment_type",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    paymentTypeId = table.Column<int>(type: "int(10)", nullable: false),
                    gatewayProviderId = table.Column<int>(type: "int(10)", nullable: false),
                    value = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    description = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    statusId = table.Column<int>(type: "int(10)", nullable: false),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_gateway_provider_payment_type", x => x.id);
                    table.ForeignKey(
                        name: "gateway_provider_payment_type_ibfk_2",
                        column: x => x.gatewayProviderId,
                        principalSchema: "twister",
                        principalTable: "gateway_provider",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "gateway_provider_payment_type_ibfk_1",
                        column: x => x.paymentTypeId,
                        principalSchema: "twister",
                        principalTable: "payment_type",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "gateway_provider_payment_type_ibfk_3",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "seller",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    telephone = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    email = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    image = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    sequence = table.Column<int>(type: "int(4)", nullable: true, defaultValueSql: "999"),
                    description = table.Column<string>(unicode: false, nullable: true),
                    taxNumber = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    sellerGroupId = table.Column<int>(type: "int(10)", nullable: true),
                    isFood = table.Column<short>(type: "bit(1)", nullable: true, defaultValueSql: "b'0'"),
                    latitude = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    longitude = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    sellerWorkdayGroupId = table.Column<int>(type: "int(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_seller", x => x.id);
                    table.ForeignKey(
                        name: "FK_seller__sellerGroupId__seller_group__id",
                        column: x => x.sellerGroupId,
                        principalSchema: "twister",
                        principalTable: "seller_group",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_seller__sellerWorkdayGroupId__seller_wdg__id",
                        column: x => x.sellerWorkdayGroupId,
                        principalSchema: "twister",
                        principalTable: "seller_workday_group",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "seller_ibfk_1",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "seller_workday_group_holiday",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    sellerWorkdayGroupId = table.Column<int>(type: "int(10)", nullable: false),
                    date = table.Column<DateTime>(nullable: false),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_seller_workday_group_holiday", x => x.id);
                    table.ForeignKey(
                        name: "FK_seller_wdg_holiday__workdayGroupId__seller_wdg__id",
                        column: x => x.sellerWorkdayGroupId,
                        principalSchema: "twister",
                        principalTable: "seller_workday_group",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "seller_workday_profile_group",
                schema: "twister",
                columns: table => new
                {
                    sellerWorkdayGroupId = table.Column<int>(type: "int(10)", nullable: false),
                    sellerWorkdayProfileId = table.Column<int>(type: "int(10)", nullable: false),
                    dayAmount = table.Column<int>(type: "int(3)", nullable: true),
                    timeStart = table.Column<TimeSpan>(nullable: false),
                    timeEnd = table.Column<TimeSpan>(nullable: false),
                    monthStart = table.Column<int>(type: "int(2)", nullable: true),
                    monthEnd = table.Column<int>(type: "int(2)", nullable: true),
                    priority = table.Column<int>(type: "int(3)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_seller_workday_profile_group", x => new { x.sellerWorkdayGroupId, x.sellerWorkdayProfileId });
                    table.ForeignKey(
                        name: "FK_seller_wpg__sellerWorkdayGroupId__seller_wdg__id",
                        column: x => x.sellerWorkdayGroupId,
                        principalSchema: "twister",
                        principalTable: "seller_workday_group",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_seller_wpg__sellerWorkdayProfileId__seller_wp__id",
                        column: x => x.sellerWorkdayProfileId,
                        principalSchema: "twister",
                        principalTable: "seller_workday_profile",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "city_language",
                schema: "twister",
                columns: table => new
                {
                    cityId = table.Column<int>(type: "int(10)", nullable: false),
                    languageId = table.Column<int>(type: "int(10)", nullable: false),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_city_language", x => new { x.cityId, x.languageId });
                    table.ForeignKey(
                        name: "FK__city_language__cityId__city__id",
                        column: x => x.cityId,
                        principalSchema: "twister",
                        principalTable: "city",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__city_language__languageId__language__id",
                        column: x => x.languageId,
                        principalSchema: "twister",
                        principalTable: "language",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "town",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    cityId = table.Column<int>(type: "int(10)", nullable: false),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_town", x => x.id);
                    table.ForeignKey(
                        name: "FK__town__cityId__city__id",
                        column: x => x.cityId,
                        principalSchema: "twister",
                        principalTable: "city",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "payment",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    creditCardId = table.Column<int>(type: "int(10)", nullable: true),
                    bankName = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    referanceNumber = table.Column<string>(unicode: false, maxLength: 30, nullable: true),
                    createDate = table.Column<DateTime>(nullable: false),
                    paymentTypeId = table.Column<int>(type: "int(3)", nullable: false),
                    gatewayProviderId = table.Column<int>(type: "int(10)", nullable: true),
                    updateDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_payment", x => x.id);
                    table.ForeignKey(
                        name: "payment_ibfk_2",
                        column: x => x.creditCardId,
                        principalSchema: "twister",
                        principalTable: "credit_card",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "payment_ibfk_3",
                        column: x => x.gatewayProviderId,
                        principalSchema: "twister",
                        principalTable: "gateway_provider",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "payment_ibfk_1",
                        column: x => x.paymentTypeId,
                        principalSchema: "twister",
                        principalTable: "payment_type",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "admin_change_log",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    tableName = table.Column<string>(maxLength: 255, nullable: false),
                    marketplaceUserId = table.Column<int>(type: "int(10)", nullable: false),
                    method = table.Column<string>(maxLength: 255, nullable: false),
                    data = table.Column<string>(type: "mediumText", nullable: false),
                    createDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_admin_change_log", x => x.id);
                    table.ForeignKey(
                        name: "FK__acl__marketplaceUserId__marketplace_user__id",
                        column: x => x.marketplaceUserId,
                        principalSchema: "twister",
                        principalTable: "marketplace_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "admin_refresh_token",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    token = table.Column<string>(type: "text", nullable: false),
                    marketplaceUserId = table.Column<int>(type: "int(10)", nullable: false),
                    expireDate = table.Column<DateTime>(nullable: false),
                    deviceName = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_admin_refresh_token", x => x.id);
                    table.ForeignKey(
                        name: "FK__art__marketplaceUserId__marketplace_user__id",
                        column: x => x.marketplaceUserId,
                        principalSchema: "twister",
                        principalTable: "marketplace_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "banner_marketplace",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    marketplaceUserId = table.Column<int>(type: "int(10)", nullable: false),
                    image = table.Column<string>(unicode: false, nullable: false),
                    link = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    reference = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    sequence = table.Column<int>(type: "int(4)", nullable: true, defaultValueSql: "999"),
                    startDate = table.Column<DateTime>(nullable: true),
                    endDate = table.Column<DateTime>(nullable: true),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_banner_marketplace", x => x.id);
                    table.ForeignKey(
                        name: "banner_marketplace_ibfk_1",
                        column: x => x.marketplaceUserId,
                        principalSchema: "twister",
                        principalTable: "marketplace_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "banner_marketplace_ibfk_2",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "chat_customer_service",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    marketplaceUserId = table.Column<int>(type: "int(10)", nullable: false),
                    message = table.Column<string>(unicode: false, nullable: false),
                    createDate = table.Column<DateTime>(nullable: false),
                    customerUserId = table.Column<int>(type: "int(10)", nullable: false),
                    owner = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_chat_customer_service", x => x.id);
                    table.ForeignKey(
                        name: "chat_customer_service_ibfk_1",
                        column: x => x.customerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "chat_customer_service_ibfk_2",
                        column: x => x.marketplaceUserId,
                        principalSchema: "twister",
                        principalTable: "marketplace_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "chat_customer_service_ibfk_3",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "marketplace_coupon_group",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    quantity = table.Column<int>(type: "int(5)", nullable: false),
                    code = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    startDate = table.Column<DateTime>(nullable: false),
                    endDate = table.Column<DateTime>(nullable: false),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    marketplaceUserId = table.Column<int>(type: "int(10)", nullable: false),
                    couponPriceId = table.Column<int>(type: "int(11)", nullable: false),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    image = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    isUniversal = table.Column<short>(type: "bit(1)", nullable: false, defaultValueSql: "b'0'"),
                    remark = table.Column<string>(unicode: false, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_marketplace_coupon_group", x => x.id);
                    table.ForeignKey(
                        name: "FK__mk_coupon_group__couponPriceId__coupon_price__id",
                        column: x => x.couponPriceId,
                        principalSchema: "twister",
                        principalTable: "coupon_price",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "marketplace_coupon_group_ibfk_1",
                        column: x => x.marketplaceUserId,
                        principalSchema: "twister",
                        principalTable: "marketplace_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "marketplace_coupon_group_ibfk_3",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "banner_seller",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    sellerId = table.Column<int>(type: "int(10)", nullable: false),
                    image = table.Column<string>(unicode: false, nullable: false),
                    link = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    reference = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    sequence = table.Column<int>(type: "int(4)", nullable: true, defaultValueSql: "999")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_banner_seller", x => x.id);
                    table.ForeignKey(
                        name: "banner_seller_ibfk_1",
                        column: x => x.sellerId,
                        principalSchema: "twister",
                        principalTable: "seller",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "banner_seller_ibfk_2",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "category_seller",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    image = table.Column<string>(unicode: false, nullable: true),
                    sequence = table.Column<int>(type: "int(4)", nullable: true, defaultValueSql: "999"),
                    sellerId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_category_seller", x => x.id);
                    table.ForeignKey(
                        name: "FK__cat_seller__sellerId__seller__id",
                        column: x => x.sellerId,
                        principalSchema: "twister",
                        principalTable: "seller",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__cat_seller__statusId__status__id",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "customer_review_seller",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    customerUserId = table.Column<int>(type: "int(10)", nullable: false),
                    sellerId = table.Column<int>(type: "int(10)", nullable: false),
                    rating = table.Column<int>(type: "int(3)", nullable: false),
                    comment = table.Column<string>(unicode: false, nullable: true),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_customer_review_seller", x => x.id);
                    table.ForeignKey(
                        name: "customer_review_seller_ibfk_3",
                        column: x => x.customerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "customer_review_seller_ibfk_1",
                        column: x => x.sellerId,
                        principalSchema: "twister",
                        principalTable: "seller",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "customer_review_seller_ibfk_2",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "follow",
                schema: "twister",
                columns: table => new
                {
                    cutomerUserId = table.Column<int>(type: "int(10)", nullable: false),
                    sellerId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_follow", x => new { x.cutomerUserId, x.sellerId });
                    table.ForeignKey(
                        name: "follow_ibfk_1",
                        column: x => x.cutomerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "follow_ibfk_2",
                        column: x => x.sellerId,
                        principalSchema: "twister",
                        principalTable: "seller",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "marketplace_review_seller",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    marketplaceUserId = table.Column<int>(type: "int(10)", nullable: false),
                    sellerId = table.Column<int>(type: "int(10)", nullable: false),
                    rating = table.Column<int>(type: "int(3)", nullable: false),
                    comment = table.Column<string>(unicode: false, nullable: true),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_marketplace_review_seller", x => x.id);
                    table.ForeignKey(
                        name: "marketplace_review_seller_ibfk_1",
                        column: x => x.marketplaceUserId,
                        principalSchema: "twister",
                        principalTable: "marketplace_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "marketplace_review_seller_ibfk_2",
                        column: x => x.sellerId,
                        principalSchema: "twister",
                        principalTable: "seller",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "marketplace_review_seller_ibfk_3",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "notification_seller",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    sellerId = table.Column<int>(type: "int(10)", nullable: false),
                    notificationTemplateId = table.Column<int>(type: "int(4)", nullable: false),
                    notificationManualId = table.Column<int>(type: "int(10)", nullable: false),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    Param = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    LinkParam = table.Column<string>(unicode: false, maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_notification_seller", x => x.id);
                    table.ForeignKey(
                        name: "FK__noti_seller__notiManId__noti_man__id",
                        column: x => x.notificationManualId,
                        principalSchema: "twister",
                        principalTable: "notification_manual",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__noti_seller__notiTempId__noti_temp__id",
                        column: x => x.notificationTemplateId,
                        principalSchema: "twister",
                        principalTable: "notification_template",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "notification_seller_ibfk_2",
                        column: x => x.sellerId,
                        principalSchema: "twister",
                        principalTable: "seller",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "notification_seller_ibfk_3",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "seller_area",
                schema: "twister",
                columns: table => new
                {
                    sellerId = table.Column<int>(type: "int(10)", nullable: false),
                    sequence = table.Column<int>(type: "int(10)", nullable: false),
                    latitude = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    longitude = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_seller_area", x => new { x.sellerId, x.sequence });
                    table.ForeignKey(
                        name: "FK_seller_area__sellerId__seller__id",
                        column: x => x.sellerId,
                        principalSchema: "twister",
                        principalTable: "seller",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "seller_coupon",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    sellerId = table.Column<int>(type: "int(10)", nullable: false),
                    quantity = table.Column<int>(type: "int(5)", nullable: false),
                    redeemCode = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    startDate = table.Column<DateTime>(nullable: false),
                    endDate = table.Column<DateTime>(nullable: false),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    marketplaceUserId = table.Column<int>(type: "int(10)", nullable: false),
                    priceCouponId = table.Column<int>(type: "int(10)", nullable: false),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_seller_coupon", x => x.id);
                    table.ForeignKey(
                        name: "seller_coupon_ibfk_1",
                        column: x => x.marketplaceUserId,
                        principalSchema: "twister",
                        principalTable: "marketplace_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "seller_coupon_ibfk_2",
                        column: x => x.priceCouponId,
                        principalSchema: "twister",
                        principalTable: "coupon_price",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "seller_coupon_ibfk_3",
                        column: x => x.sellerId,
                        principalSchema: "twister",
                        principalTable: "seller",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "seller_coupon_ibfk_4",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "seller_image",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    sellerId = table.Column<int>(type: "int(10)", nullable: false),
                    url = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    description = table.Column<string>(unicode: false, nullable: true),
                    isCover = table.Column<short>(type: "bit(1)", nullable: false, defaultValueSql: "b'0'"),
                    sequence = table.Column<int>(type: "int(4)", nullable: false, defaultValueSql: "9999"),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    mediaTypeId = table.Column<int>(type: "int(4)", nullable: false, defaultValue: 1)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_seller_image", x => x.id);
                    table.ForeignKey(
                        name: "FK_seller_image__mediaTypeId__media_type__id",
                        column: x => x.mediaTypeId,
                        principalSchema: "twister",
                        principalTable: "media_type",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_seller_image__sellerId__seller__id",
                        column: x => x.sellerId,
                        principalSchema: "twister",
                        principalTable: "seller",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_seller_image__statusId__status__id",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "seller_product_delivery",
                schema: "twister",
                columns: table => new
                {
                    sellerId = table.Column<int>(type: "int(10)", nullable: false),
                    provinceId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_seller_product_delivery", x => new { x.sellerId, x.provinceId });
                    table.ForeignKey(
                        name: "FK_seller_pd__provinceId__province__id",
                        column: x => x.provinceId,
                        principalSchema: "twister",
                        principalTable: "province",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_seller_pd__sellerId__seller__id",
                        column: x => x.sellerId,
                        principalSchema: "twister",
                        principalTable: "seller",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "seller_stats",
                schema: "twister",
                columns: table => new
                {
                    sellerId = table.Column<int>(type: "int(10)", nullable: false),
                    sellerStatsTypeId = table.Column<int>(type: "int(10)", nullable: false),
                    value = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_seller_stats", x => new { x.sellerId, x.sellerStatsTypeId });
                    table.ForeignKey(
                        name: "seller_stats_ibfk_1",
                        column: x => x.sellerId,
                        principalSchema: "twister",
                        principalTable: "seller",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "seller_stats_ibfk_2",
                        column: x => x.sellerStatsTypeId,
                        principalSchema: "twister",
                        principalTable: "seller_stats_type",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "seller_user",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    username = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    password = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    sellerId = table.Column<int>(type: "int(10)", nullable: false),
                    customerUserId = table.Column<int>(type: "int(10)", nullable: false),
                    roleClassId = table.Column<int>(type: "int(10)", nullable: false),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_seller_user", x => x.id);
                    table.ForeignKey(
                        name: "FK_seller_user__customerUserId__customer_user__id",
                        column: x => x.customerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_seller_user__roleClassId__role_class__id",
                        column: x => x.roleClassId,
                        principalSchema: "twister",
                        principalTable: "role_class",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_seller_user__sellerId__seller__id",
                        column: x => x.sellerId,
                        principalSchema: "twister",
                        principalTable: "seller",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_seller_user__statusId__status__id",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "customer_address",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    customerUserId = table.Column<int>(type: "int(10)", nullable: false),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    address1 = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    address2 = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    address3 = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    cityId = table.Column<int>(type: "int(10)", nullable: true),
                    provinceId = table.Column<int>(type: "int(10)", nullable: true),
                    countryId = table.Column<int>(type: "int(4)", nullable: true),
                    zipcode = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    taxNumber = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    createDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    updateDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    townId = table.Column<int>(type: "int(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_customer_address", x => x.id);
                    table.ForeignKey(
                        name: "customer_address_ibfk_1",
                        column: x => x.cityId,
                        principalSchema: "twister",
                        principalTable: "city",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "customer_address_ibfk_2",
                        column: x => x.countryId,
                        principalSchema: "twister",
                        principalTable: "country",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "customer_address_ibfk_3",
                        column: x => x.customerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "customer_address_ibfk_4",
                        column: x => x.provinceId,
                        principalSchema: "twister",
                        principalTable: "province",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "customer_address_ibfk_5",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__customer_address__townId__town__id",
                        column: x => x.townId,
                        principalSchema: "twister",
                        principalTable: "town",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "logistic_company",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    address1 = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    address2 = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    address3 = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    cityId = table.Column<int>(type: "int(10)", nullable: true),
                    provinceId = table.Column<int>(type: "int(10)", nullable: true),
                    countryId = table.Column<int>(type: "int(4)", nullable: true),
                    zipCode = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    taxNumber = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    email = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    telephone = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    fax = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    createDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    updateDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    townId = table.Column<int>(type: "int(10)", nullable: true),
                    trackingUrl = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    isLinkable = table.Column<byte>(type: "tinyint(1)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_logistic_company", x => x.id);
                    table.ForeignKey(
                        name: "logistic_company_ibfk_1",
                        column: x => x.cityId,
                        principalSchema: "twister",
                        principalTable: "city",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__logistic_company__countryId__country__id",
                        column: x => x.countryId,
                        principalSchema: "twister",
                        principalTable: "country",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "logistic_company_ibfk_2",
                        column: x => x.provinceId,
                        principalSchema: "twister",
                        principalTable: "province",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "logistic_company_ibfk_3",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__company__townId__town__id",
                        column: x => x.townId,
                        principalSchema: "twister",
                        principalTable: "town",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "seller_address",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    sellerId = table.Column<int>(type: "int(10)", nullable: false),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    address1 = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    address2 = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    address3 = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    cityId = table.Column<int>(type: "int(10)", nullable: true),
                    provinceId = table.Column<int>(type: "int(10)", nullable: true),
                    countryId = table.Column<int>(type: "int(4)", nullable: true),
                    zipcode = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    taxNumber = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    createDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    updateDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    townId = table.Column<int>(type: "int(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_seller_address", x => x.id);
                    table.ForeignKey(
                        name: "seller_address_ibfk_1",
                        column: x => x.cityId,
                        principalSchema: "twister",
                        principalTable: "city",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "seller_address_ibfk_2",
                        column: x => x.countryId,
                        principalSchema: "twister",
                        principalTable: "country",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "seller_address_ibfk_3",
                        column: x => x.provinceId,
                        principalSchema: "twister",
                        principalTable: "province",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "seller_address_ibfk_4",
                        column: x => x.sellerId,
                        principalSchema: "twister",
                        principalTable: "seller",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "seller_address_ibfk_5",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__seller_address__townId__town__id",
                        column: x => x.townId,
                        principalSchema: "twister",
                        principalTable: "town",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "town_language",
                schema: "twister",
                columns: table => new
                {
                    townId = table.Column<int>(type: "int(10)", nullable: false),
                    languageId = table.Column<int>(type: "int(10)", nullable: false),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_town_language", x => new { x.townId, x.languageId });
                    table.ForeignKey(
                        name: "FK__town_language__languageId__language__id",
                        column: x => x.languageId,
                        principalSchema: "twister",
                        principalTable: "language",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__town_language__townId__town__id",
                        column: x => x.townId,
                        principalSchema: "twister",
                        principalTable: "town",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "marketplace_coupon",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    marketplaceCouponGroupId = table.Column<int>(type: "int(11)", nullable: false),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    customerUserId = table.Column<int>(type: "int(11)", nullable: true),
                    redeemCode = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    isRedeemed = table.Column<short>(type: "bit(1)", nullable: false, defaultValueSql: "b'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_marketplace_coupon", x => x.id);
                    table.ForeignKey(
                        name: "marketplace_coupon_ibfk_1",
                        column: x => x.customerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "marketplace_coupon_ibfk_2",
                        column: x => x.marketplaceCouponGroupId,
                        principalSchema: "twister",
                        principalTable: "marketplace_coupon_group",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "banner_food_seller",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    bannerMarketplaceId = table.Column<int>(type: "int(10)", nullable: true),
                    bannerSellerId = table.Column<int>(type: "int(10)", nullable: true),
                    sellerId = table.Column<int>(type: "int(10)", nullable: true),
                    sellerGroupId = table.Column<int>(type: "int(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_banner_food_seller", x => x.id);
                    table.ForeignKey(
                        name: "FK_banner_food_seller__bMarketplaceId__banner_marketplace__id",
                        column: x => x.bannerMarketplaceId,
                        principalSchema: "twister",
                        principalTable: "banner_marketplace",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_banner_food_seller__bannerSellerId__banner_seller__id",
                        column: x => x.bannerSellerId,
                        principalSchema: "twister",
                        principalTable: "banner_seller",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_banner_food_seller__sellerGroupId__seller_group__id",
                        column: x => x.sellerGroupId,
                        principalSchema: "twister",
                        principalTable: "seller_group",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_banner_food_seller__sellerId__seller__id",
                        column: x => x.sellerId,
                        principalSchema: "twister",
                        principalTable: "seller",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "category_seller_hierarchy",
                schema: "twister",
                columns: table => new
                {
                    parentId = table.Column<int>(type: "int(10)", nullable: false),
                    childrenId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_category_seller_hierarchy", x => new { x.parentId, x.childrenId });
                    table.ForeignKey(
                        name: "fk-category_seller.id_children",
                        column: x => x.childrenId,
                        principalSchema: "twister",
                        principalTable: "category_seller",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk-category_seller.id_paraent",
                        column: x => x.parentId,
                        principalSchema: "twister",
                        principalTable: "category_seller",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "point",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    customerUserId = table.Column<int>(type: "int(10)", nullable: false),
                    point = table.Column<int>(type: "int(5)", nullable: false),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    sellerCouponId = table.Column<int>(type: "int(10)", nullable: true),
                    marketplaceCouponId = table.Column<int>(type: "int(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_point", x => x.id);
                    table.ForeignKey(
                        name: "point_ibfk_1",
                        column: x => x.customerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "point_ibfk_2",
                        column: x => x.marketplaceCouponId,
                        principalSchema: "twister",
                        principalTable: "marketplace_coupon_group",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "point_ibfk_3",
                        column: x => x.sellerCouponId,
                        principalSchema: "twister",
                        principalTable: "seller_coupon",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "point_ibfk_4",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "seller_coupon_redeem",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    sellerCouponId = table.Column<int>(type: "int(10)", nullable: false),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    customerUserId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_seller_coupon_redeem", x => x.id);
                    table.ForeignKey(
                        name: "seller_coupon_redeem_ibfk_1",
                        column: x => x.customerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "seller_coupon_redeem_ibfk_2",
                        column: x => x.sellerCouponId,
                        principalSchema: "twister",
                        principalTable: "seller_coupon",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "seller_coupon_redeem_ibfk_3",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "logistic_user",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(5)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    firstName = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    lastName = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    telephone = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    email = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    username = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    password = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    citizenId = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    logisticCompanyId = table.Column<int>(type: "int(4)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_logistic_user", x => x.id);
                    table.ForeignKey(
                        name: "logistic_user_ibfk_1",
                        column: x => x.logisticCompanyId,
                        principalSchema: "twister",
                        principalTable: "logistic_company",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "logistic_user_ibfk_2",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "product",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    description = table.Column<string>(unicode: false, nullable: true),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    rating = table.Column<int>(type: "int(3)", nullable: true),
                    sold = table.Column<int>(type: "int(6)", nullable: true),
                    wishlist = table.Column<int>(type: "int(10)", nullable: true),
                    categorySellerId = table.Column<int>(type: "int(10)", nullable: true),
                    categoryMarketPlaceId = table.Column<int>(type: "int(10)", nullable: true),
                    sku = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    size = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    sellerAddressId = table.Column<int>(type: "int(10)", nullable: true),
                    sellerId = table.Column<int>(type: "int(10)", nullable: false),
                    image = table.Column<string>(unicode: false, nullable: true),
                    purchaseLimit = table.Column<int>(type: "int(4)", nullable: true),
                    brandId = table.Column<int>(type: "int(10)", nullable: true),
                    eventId = table.Column<int>(type: "int(10)", nullable: true),
                    isFood = table.Column<short>(type: "bit(1)", nullable: true, defaultValueSql: "b'0'"),
                    url = table.Column<string>(type: "text", nullable: true),
                    productTypeId = table.Column<int>(type: "int(4)", nullable: true),
                    expireDate = table.Column<DateTime>(nullable: true),
                    expireDurationInDay = table.Column<int>(type: "int(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product", x => x.id);
                    table.ForeignKey(
                        name: "FK__product__brandId__brand__id",
                        column: x => x.brandId,
                        principalSchema: "twister",
                        principalTable: "brand",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk-product-category_marketplace.id",
                        column: x => x.categoryMarketPlaceId,
                        principalSchema: "twister",
                        principalTable: "category_marketplace",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk-product-category_seller.id",
                        column: x => x.categorySellerId,
                        principalSchema: "twister",
                        principalTable: "category_seller",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "product_ibfk_1",
                        column: x => x.eventId,
                        principalSchema: "twister",
                        principalTable: "event",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk__product__productTypeId__product_type__id",
                        column: x => x.productTypeId,
                        principalSchema: "twister",
                        principalTable: "product_type",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk-product-seller_address.id",
                        column: x => x.sellerAddressId,
                        principalSchema: "twister",
                        principalTable: "seller_address",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk-product-seller.id",
                        column: x => x.sellerId,
                        principalSchema: "twister",
                        principalTable: "seller",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk-product-status.id",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "banner_marketplace_product",
                schema: "twister",
                columns: table => new
                {
                    bannerMarketplaceId = table.Column<int>(type: "int(11)", nullable: false),
                    productId = table.Column<int>(type: "int(11)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_banner_marketplace_product", x => new { x.bannerMarketplaceId, x.productId });
                    table.ForeignKey(
                        name: "FK__bmp__bmId__bm__id",
                        column: x => x.bannerMarketplaceId,
                        principalSchema: "twister",
                        principalTable: "banner_marketplace",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_bmp__productId__product__id",
                        column: x => x.productId,
                        principalSchema: "twister",
                        principalTable: "product",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "category_marketplace_product",
                schema: "twister",
                columns: table => new
                {
                    categoryMarketPlaceId = table.Column<int>(type: "int(10)", nullable: false),
                    productId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_category_marketplace_product", x => new { x.categoryMarketPlaceId, x.productId });
                    table.ForeignKey(
                        name: "category_marketplace_product_ibfk_1",
                        column: x => x.categoryMarketPlaceId,
                        principalSchema: "twister",
                        principalTable: "category_marketplace",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "category_marketplace_product_ibfk_2",
                        column: x => x.productId,
                        principalSchema: "twister",
                        principalTable: "product",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "category_seller_product",
                schema: "twister",
                columns: table => new
                {
                    categorySellerId = table.Column<int>(type: "int(10)", nullable: false),
                    productId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_category_seller_product", x => new { x.categorySellerId, x.productId });
                    table.ForeignKey(
                        name: "category_seller_product_ibfk_1",
                        column: x => x.categorySellerId,
                        principalSchema: "twister",
                        principalTable: "category_seller",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "category_seller_product_ibfk_2",
                        column: x => x.productId,
                        principalSchema: "twister",
                        principalTable: "product",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "coupon_type",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    couponPriceId = table.Column<int>(type: "int(11)", nullable: false),
                    categoryMarketplaceId = table.Column<int>(type: "int(11)", nullable: true),
                    productId = table.Column<int>(type: "int(11)", nullable: true),
                    sellerId = table.Column<int>(type: "int(11)", nullable: true),
                    wholeCart = table.Column<short>(type: "bit(1)", nullable: true, defaultValueSql: "b'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_coupon_type", x => x.id);
                    table.ForeignKey(
                        name: "FK__coupon_type__catMarketplaceId__cat_market__id",
                        column: x => x.categoryMarketplaceId,
                        principalSchema: "twister",
                        principalTable: "category_marketplace",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__coupon_type__couponPriceId__coupon_price__id",
                        column: x => x.couponPriceId,
                        principalSchema: "twister",
                        principalTable: "coupon_price",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__coupon_type__sellerId__seller__id",
                        column: x => x.productId,
                        principalSchema: "twister",
                        principalTable: "product",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__coupon_type__productId__product__id",
                        column: x => x.sellerId,
                        principalSchema: "twister",
                        principalTable: "seller",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "customer_review_product",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    customerUserId = table.Column<int>(type: "int(10)", nullable: false),
                    productId = table.Column<int>(type: "int(10)", nullable: false),
                    rating = table.Column<int>(type: "int(3)", nullable: false),
                    comment = table.Column<string>(unicode: false, nullable: true),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_customer_review_product", x => x.id);
                    table.ForeignKey(
                        name: "customer_review_product_ibfk_1",
                        column: x => x.customerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "customer_review_product_ibfk_2",
                        column: x => x.productId,
                        principalSchema: "twister",
                        principalTable: "product",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "customer_review_product_ibfk_3",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "customer_user_view_history_log",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    customerUserId = table.Column<int>(type: "int(10)", nullable: false),
                    productId = table.Column<int>(type: "int(10)", nullable: false),
                    createDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_customer_user_view_history_log", x => x.id);
                    table.ForeignKey(
                        name: "customer_user_view_history_log_ibfk_2",
                        column: x => x.customerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "customer_user_view_history_log_ibfk_1",
                        column: x => x.productId,
                        principalSchema: "twister",
                        principalTable: "product",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "favorite_product",
                schema: "twister",
                columns: table => new
                {
                    customerUserId = table.Column<int>(type: "int(10)", nullable: false),
                    productId = table.Column<int>(type: "int(10)", nullable: false),
                    createDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_favorite_product", x => new { x.customerUserId, x.productId });
                    table.ForeignKey(
                        name: "fk-favorite_product-customerUserId.id",
                        column: x => x.customerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk-favorite_product-product.id",
                        column: x => x.productId,
                        principalSchema: "twister",
                        principalTable: "product",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "product_image",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    productId = table.Column<int>(type: "int(10)", nullable: false),
                    url = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    description = table.Column<string>(unicode: false, nullable: true),
                    isCover = table.Column<short>(type: "bit(1)", nullable: false, defaultValueSql: "b'0'"),
                    sequence = table.Column<int>(type: "int(4)", nullable: false, defaultValueSql: "9999"),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    mediaTypeId = table.Column<int>(type: "int(4)", nullable: false, defaultValue: 1),
                    isIcon = table.Column<short>(type: "bit(1)", nullable: false, defaultValueSql: "b'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product_image", x => x.id);
                    table.ForeignKey(
                        name: "FK_product_image__mediaTypeId__media_type__id",
                        column: x => x.mediaTypeId,
                        principalSchema: "twister",
                        principalTable: "media_type",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_product_image__productId__product__id",
                        column: x => x.productId,
                        principalSchema: "twister",
                        principalTable: "product",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_product_image__statusId__status__id",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "product_rating",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    productId = table.Column<int>(type: "int(10)", nullable: false),
                    customerUserId = table.Column<int>(type: "int(10)", nullable: false),
                    rating = table.Column<int>(type: "int(5)", nullable: false),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product_rating", x => x.id);
                    table.ForeignKey(
                        name: "product_rating_ibfk_1",
                        column: x => x.customerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "product_rating_ibfk_2",
                        column: x => x.productId,
                        principalSchema: "twister",
                        principalTable: "product",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "product_sold",
                schema: "twister",
                columns: table => new
                {
                    customerUserId = table.Column<int>(type: "int(10)", nullable: false),
                    productId = table.Column<int>(type: "int(10)", nullable: false),
                    order = table.Column<int>(type: "int(6)", nullable: false),
                    quantity = table.Column<int>(type: "int(10)", nullable: false),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product_sold", x => new { x.customerUserId, x.productId });
                    table.ForeignKey(
                        name: "FK__product_sold__customerUserId__customer_user__id",
                        column: x => x.customerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__product_sold__productId__product__id",
                        column: x => x.productId,
                        principalSchema: "twister",
                        principalTable: "product",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "product_variant",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    productId = table.Column<int>(type: "int(10)", nullable: false),
                    variantIds = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    quantity = table.Column<int>(type: "int(5)", nullable: false),
                    price = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    image = table.Column<string>(unicode: false, nullable: true),
                    discountPercentage = table.Column<decimal>(type: "decimal(10,2)", nullable: true, defaultValueSql: "0.00"),
                    salePrice = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    sku = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    barcode = table.Column<string>(unicode: false, maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product_variant", x => x.id);
                    table.ForeignKey(
                        name: "fk-product_variant-product.id",
                        column: x => x.productId,
                        principalSchema: "twister",
                        principalTable: "product",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk-product_variant-status.id",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "product_wishlist",
                schema: "twister",
                columns: table => new
                {
                    customerUserId = table.Column<int>(type: "int(10)", nullable: false),
                    productId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product_wishlist", x => new { x.customerUserId, x.productId });
                    table.ForeignKey(
                        name: "FK__product_wishlist__customerUserId__cust_user__id",
                        column: x => x.customerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__product_wishlist__productId__product__id",
                        column: x => x.productId,
                        principalSchema: "twister",
                        principalTable: "product",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "promotion_price",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    productId = table.Column<int>(type: "int(10)", nullable: false),
                    discountPercent = table.Column<int>(type: "int(3)", nullable: true),
                    discountTotal = table.Column<int>(type: "int(5)", nullable: true),
                    discountLimit = table.Column<int>(type: "int(5)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_promotion_price", x => x.id);
                    table.ForeignKey(
                        name: "promotion_price_ibfk_1",
                        column: x => x.productId,
                        principalSchema: "twister",
                        principalTable: "product",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "relate_category",
                schema: "twister",
                columns: table => new
                {
                    productId = table.Column<int>(type: "int(10)", nullable: false),
                    catagoryMarketplaceId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_relate_category", x => new { x.productId, x.catagoryMarketplaceId });
                    table.ForeignKey(
                        name: "relate_category_ibfk_1",
                        column: x => x.catagoryMarketplaceId,
                        principalSchema: "twister",
                        principalTable: "category_marketplace",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "relate_category_ibfk_2",
                        column: x => x.productId,
                        principalSchema: "twister",
                        principalTable: "product",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "relate_product",
                schema: "twister",
                columns: table => new
                {
                    productId = table.Column<int>(type: "int(10)", nullable: false),
                    relateProductId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_relate_product", x => new { x.productId, x.relateProductId });
                    table.ForeignKey(
                        name: "relate_product_ibfk_1",
                        column: x => x.productId,
                        principalSchema: "twister",
                        principalTable: "product",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "relate_product_ibfk_2",
                        column: x => x.relateProductId,
                        principalSchema: "twister",
                        principalTable: "product",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "chat_seller",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    sellerId = table.Column<int>(type: "int(10)", nullable: false),
                    productVariantId = table.Column<int>(type: "int(10)", nullable: false),
                    message = table.Column<string>(unicode: false, nullable: false),
                    customerServiceId = table.Column<int>(type: "int(10)", nullable: false),
                    createDate = table.Column<DateTime>(nullable: false),
                    owner = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_chat_seller", x => x.id);
                    table.ForeignKey(
                        name: "chat_seller_ibfk_1",
                        column: x => x.productVariantId,
                        principalSchema: "twister",
                        principalTable: "product_variant",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "chat_seller_ibfk_2",
                        column: x => x.sellerId,
                        principalSchema: "twister",
                        principalTable: "seller",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "chat_seller_ibfk_3",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "group_buying_product_variant",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    productVariantId = table.Column<int>(type: "int(10)", nullable: false),
                    groupBuyingConditionId = table.Column<int>(type: "int(10)", nullable: false),
                    durationInMinutes = table.Column<int>(type: "int(10)", nullable: false),
                    startDate = table.Column<DateTime>(nullable: false),
                    endDate = table.Column<DateTime>(nullable: false),
                    createDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    updateDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    statusId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_group_buying_product_variant", x => x.id);
                    table.ForeignKey(
                        name: "FK_gbpv_gbcId_group_buying_condition_id",
                        column: x => x.groupBuyingConditionId,
                        principalSchema: "twister",
                        principalTable: "group_buying_condition",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_gbpv_productVariantId_product_variant_id",
                        column: x => x.productVariantId,
                        principalSchema: "twister",
                        principalTable: "product_variant",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_gbpv_statusId_status_id",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "marketplace_promotion",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    productId = table.Column<int>(type: "int(10)", nullable: false),
                    minQuantity = table.Column<int>(type: "int(3)", nullable: true),
                    pricePromotionId = table.Column<int>(type: "int(10)", nullable: false),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    startDate = table.Column<DateTime>(nullable: false),
                    endDate = table.Column<DateTime>(nullable: false),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    quantity = table.Column<int>(type: "int(5)", nullable: false),
                    marketplaceUserId = table.Column<int>(type: "int(10)", nullable: false),
                    categoryMarketplaceId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_marketplace_promotion", x => x.id);
                    table.ForeignKey(
                        name: "marketplace_promotion_ibfk_1",
                        column: x => x.categoryMarketplaceId,
                        principalSchema: "twister",
                        principalTable: "category_marketplace",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "marketplace_promotion_ibfk_2",
                        column: x => x.marketplaceUserId,
                        principalSchema: "twister",
                        principalTable: "marketplace_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "marketplace_promotion_ibfk_3",
                        column: x => x.pricePromotionId,
                        principalSchema: "twister",
                        principalTable: "promotion_price",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "marketplace_promotion_ibfk_4",
                        column: x => x.productId,
                        principalSchema: "twister",
                        principalTable: "product",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "marketplace_promotion_ibfk_5",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "seller_promotion",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    productId = table.Column<int>(type: "int(10)", nullable: false),
                    minQuantity = table.Column<int>(type: "int(3)", nullable: true),
                    pricePromotionId = table.Column<int>(type: "int(10)", nullable: false),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    startDate = table.Column<DateTime>(nullable: false),
                    endDate = table.Column<DateTime>(nullable: false),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    quantity = table.Column<int>(type: "int(5)", nullable: false),
                    marketplaceUserId = table.Column<int>(type: "int(10)", nullable: false),
                    categorySellerId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_seller_promotion", x => x.id);
                    table.ForeignKey(
                        name: "seller_promotion_ibfk_1",
                        column: x => x.categorySellerId,
                        principalSchema: "twister",
                        principalTable: "category_marketplace",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "seller_promotion_ibfk_2",
                        column: x => x.marketplaceUserId,
                        principalSchema: "twister",
                        principalTable: "marketplace_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "seller_promotion_ibfk_3",
                        column: x => x.pricePromotionId,
                        principalSchema: "twister",
                        principalTable: "promotion_price",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "seller_promotion_ibfk_4",
                        column: x => x.productId,
                        principalSchema: "twister",
                        principalTable: "product",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "seller_promotion_ibfk_5",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "group_buying",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    groupBuyingProductVariantId = table.Column<int>(type: "int(10)", nullable: false),
                    customerUserId = table.Column<int>(type: "int(10)", nullable: false),
                    startDate = table.Column<DateTime>(nullable: false),
                    endDate = table.Column<DateTime>(nullable: false),
                    createDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    updateDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    statusId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_group_buying", x => x.id);
                    table.ForeignKey(
                        name: "FK_gb_customerUserId_customer_user_id",
                        column: x => x.customerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_gb_gbpvId_gbpv_id",
                        column: x => x.groupBuyingProductVariantId,
                        principalSchema: "twister",
                        principalTable: "group_buying_product_variant",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_gb_statusId_status_id",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "group_buying_reward",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    groupBuyingProductVariantId = table.Column<int>(type: "int(10)", nullable: false),
                    groupBuyingAchievementTypeId = table.Column<int>(type: "int(10)", nullable: false),
                    salePrice = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    point = table.Column<int>(type: "int(10)", nullable: true),
                    marketplaceCouponGroupId = table.Column<int>(type: "int(10)", nullable: true),
                    productVariantId = table.Column<int>(type: "int(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_group_buying_reward", x => x.id);
                    table.ForeignKey(
                        name: "FK_gbr_groupBuyingAchievementTypeId_gba_id",
                        column: x => x.groupBuyingAchievementTypeId,
                        principalSchema: "twister",
                        principalTable: "group_buying_achievement_type",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_gbr_groupBuyingProductVariantId_gbpv_id",
                        column: x => x.groupBuyingProductVariantId,
                        principalSchema: "twister",
                        principalTable: "group_buying_product_variant",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_gbr_marketplaceCouponGroupId_mktcg_id",
                        column: x => x.marketplaceCouponGroupId,
                        principalSchema: "twister",
                        principalTable: "marketplace_coupon_group",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_gbr_productVariantId_product_variant_id",
                        column: x => x.productVariantId,
                        principalSchema: "twister",
                        principalTable: "product_variant",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "cart",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    customerUserId = table.Column<int>(type: "int(10)", nullable: false),
                    code = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    staticCode = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    eventId = table.Column<int>(type: "int(10)", nullable: true),
                    lockerId = table.Column<int>(type: "int(10)", nullable: true),
                    remark = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    groupBuyingId = table.Column<int>(type: "int(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_cart", x => x.id);
                    table.ForeignKey(
                        name: "cart_ibfk_1",
                        column: x => x.customerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "cart_ibfk_3",
                        column: x => x.eventId,
                        principalSchema: "twister",
                        principalTable: "event",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK-cart_groupBuyingId_group_buying_id",
                        column: x => x.groupBuyingId,
                        principalSchema: "twister",
                        principalTable: "group_buying",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "cart_ibfk_2",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "cart_product",
                schema: "twister",
                columns: table => new
                {
                    cartId = table.Column<int>(type: "int(10)", nullable: false),
                    productVariantId = table.Column<int>(type: "int(10)", nullable: false),
                    quantity = table.Column<int>(type: "int(3)", nullable: false),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    remark = table.Column<string>(unicode: false, maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_cart_product", x => new { x.cartId, x.productVariantId });
                    table.ForeignKey(
                        name: "cart_product_ibfk_1",
                        column: x => x.cartId,
                        principalSchema: "twister",
                        principalTable: "cart",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "cart_product_ibfk_2",
                        column: x => x.productVariantId,
                        principalSchema: "twister",
                        principalTable: "product_variant",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "sale_order",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    code = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    staticCode = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    cartId = table.Column<int>(type: "int(10)", nullable: true),
                    customerUserId = table.Column<int>(type: "int(10)", nullable: false),
                    totalPrice = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    discount = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    netPrice = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    vat = table.Column<decimal>(type: "decimal(8,2)", nullable: true),
                    shippingFee = table.Column<decimal>(type: "decimal(6,2)", nullable: true),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    lockerCode = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    customerAddressId = table.Column<int>(type: "int(10)", nullable: true),
                    paymentId = table.Column<int>(type: "int(10)", nullable: true),
                    gatewayRequestCode = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    gatewayReferenceStaticCode = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    logDeliveryId = table.Column<long>(type: "bigint(20)", nullable: true),
                    remark = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    shippingAddress = table.Column<string>(unicode: false, nullable: true),
                    billingAddress = table.Column<string>(unicode: false, nullable: true),
                    receiverTelephone = table.Column<string>(unicode: false, maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sale_order", x => x.id);
                    table.ForeignKey(
                        name: "sale_order_ibfk_1",
                        column: x => x.cartId,
                        principalSchema: "twister",
                        principalTable: "cart",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__so__customerAddressId__customer_address__id",
                        column: x => x.customerAddressId,
                        principalSchema: "twister",
                        principalTable: "customer_address",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "sale_order_ibfk_2",
                        column: x => x.customerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__so__paymentId__payment__id",
                        column: x => x.paymentId,
                        principalSchema: "twister",
                        principalTable: "payment",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "sale_order_ibfk_3",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "allow_open_locker",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    saleOrderId = table.Column<int>(type: "int(10)", nullable: true),
                    logDeliveryId = table.Column<long>(type: "bigint(20)", nullable: true),
                    telephone = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    createDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    updateDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_allow_open_locker", x => x.id);
                    table.ForeignKey(
                        name: "FK__allow_open_locker__logDeliveryId__logdelivery__transnum",
                        column: x => x.logDeliveryId,
                        principalSchema: "twister",
                        principalTable: "logdelivery",
                        principalColumn: "transnum",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__allow_open_locker__saleOrderId__sale_order__id",
                        column: x => x.saleOrderId,
                        principalSchema: "twister",
                        principalTable: "sale_order",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "coin_transaction",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    customerUserId = table.Column<int>(type: "int(10)", nullable: false),
                    saleOrderId = table.Column<int>(type: "int(10)", nullable: true),
                    logOpenId = table.Column<int>(type: "int(10)", nullable: true),
                    amount = table.Column<decimal>(type: "decimal(6,2)", nullable: false),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    balance = table.Column<decimal>(type: "decimal(10,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_coin_transaction", x => x.id);
                    table.ForeignKey(
                        name: "coin_transaction_ibfk_2",
                        column: x => x.customerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__coin_tran__saleOrderId__sale_order__id",
                        column: x => x.saleOrderId,
                        principalSchema: "twister",
                        principalTable: "sale_order",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "coin_transaction_ibfk_1",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "group_buying_invitation",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    groupBuyingId = table.Column<int>(type: "int(10)", nullable: false),
                    InviteeCustomerUserId = table.Column<int>(nullable: false),
                    inviterCustomerUserId = table.Column<int>(type: "int(10)", nullable: false),
                    inviteeSaleOrderId = table.Column<int>(type: "int(10)", nullable: true),
                    parentCustomerUserId = table.Column<int>(type: "int(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_group_buying_invitation", x => x.id);
                    table.ForeignKey(
                        name: "FK_gbiv_groupBuyingId_group_buying_id",
                        column: x => x.groupBuyingId,
                        principalSchema: "twister",
                        principalTable: "group_buying",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_gbiv_inviteeCustomerUserId_customer_user_id",
                        column: x => x.InviteeCustomerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_gbiv_inviteeSaleOrderId_saleOrder_id",
                        column: x => x.inviteeSaleOrderId,
                        principalSchema: "twister",
                        principalTable: "sale_order",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_gbiv_inviterCustomerUserId_customer_user_id",
                        column: x => x.inviterCustomerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_gbiv_parentCustomerUserId_customer_user_id",
                        column: x => x.parentCustomerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "invoice_receipt",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    code = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    staticCode = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    totalPrice = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    discount = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    vat = table.Column<decimal>(type: "decimal(8,2)", nullable: true),
                    netPrice = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    saleOrderId = table.Column<int>(type: "int(10)", nullable: false),
                    saleOrderCode = table.Column<string>(unicode: false, maxLength: 30, nullable: false),
                    name = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    telephone = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    email = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    address = table.Column<string>(unicode: false, nullable: false),
                    city = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    province = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    customerUserId = table.Column<int>(type: "int(10)", nullable: false),
                    remark = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    marketplaceAddress = table.Column<string>(unicode: false, nullable: false),
                    shippingFee = table.Column<decimal>(type: "decimal(6,2)", nullable: false),
                    sellerId = table.Column<int>(type: "int(10)", nullable: false),
                    sellerName = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    sellerAddress = table.Column<string>(unicode: false, nullable: false),
                    gatewayRefNumber = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    paymentMethod = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    creditCardNumber = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    bankName = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    accountNumber = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    accountTwister = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    counsterServiceName = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    counsterServiceRefNumber = table.Column<string>(unicode: false, maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_invoice_receipt", x => x.id);
                    table.ForeignKey(
                        name: "invoice_receipt_ibfk_1",
                        column: x => x.customerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__invrep__saleOrderId__sale_order__id",
                        column: x => x.saleOrderId,
                        principalSchema: "twister",
                        principalTable: "sale_order",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "invoice_receipt_ibfk_3",
                        column: x => x.sellerId,
                        principalSchema: "twister",
                        principalTable: "seller",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "invoice_receipt_ibfk_4",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "marketplace_promotion_redeem",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    marketplacePromotionId = table.Column<int>(type: "int(10)", nullable: false),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    saleOrderId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_marketplace_promotion_redeem", x => x.id);
                    table.ForeignKey(
                        name: "marketplace_promotion_redeem_ibfk_1",
                        column: x => x.marketplacePromotionId,
                        principalSchema: "twister",
                        principalTable: "marketplace_promotion",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "marketplace_promotion_redeem_ibfk_2",
                        column: x => x.saleOrderId,
                        principalSchema: "twister",
                        principalTable: "sale_order",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "marketplace_promotion_redeem_ibfk_3",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "purchase_order",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    saleOrderId = table.Column<int>(type: "int(10)", nullable: false),
                    code = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    staticCode = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    totalPrice = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    discount = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    vat = table.Column<decimal>(type: "decimal(8,2)", nullable: true),
                    netPrice = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    name = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    telephone = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    email = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    address = table.Column<string>(unicode: false, nullable: true),
                    remark = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    marketplaceAddress = table.Column<string>(unicode: false, nullable: false),
                    lockerCode = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    parcelQuantity = table.Column<int>(type: "int(6)", nullable: true),
                    sellerId = table.Column<int>(type: "int(10)", nullable: false),
                    sellerName = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    sellerAddress = table.Column<string>(unicode: false, nullable: false),
                    sellerTaxNumber = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    shippingAddress = table.Column<string>(unicode: false, nullable: true),
                    billingAddress = table.Column<string>(unicode: false, nullable: true),
                    trackingNumber = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    logisticCompanyName = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    logisticCompanyId = table.Column<int>(type: "int(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_purchase_order", x => x.id);
                    table.ForeignKey(
                        name: "FK_purchaseOrderId__logisticCompanyId__logistic_company__id",
                        column: x => x.logisticCompanyId,
                        principalSchema: "twister",
                        principalTable: "logistic_company",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__purchase_order__saleOrderId__sale_order__id",
                        column: x => x.saleOrderId,
                        principalSchema: "twister",
                        principalTable: "sale_order",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "purchase_order_ibfk_1",
                        column: x => x.sellerId,
                        principalSchema: "twister",
                        principalTable: "seller",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "purchase_order_ibfk_2",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "sale_order_discount",
                schema: "twister",
                columns: table => new
                {
                    saleOrderId = table.Column<int>(type: "int(10)", nullable: false),
                    discountTypeId = table.Column<int>(type: "int(4)", nullable: false),
                    discountId = table.Column<int>(type: "int(10)", nullable: false),
                    discountAmount = table.Column<decimal>(type: "decimal(10,2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sale_order_discount", x => new { x.saleOrderId, x.discountTypeId, x.discountId });
                    table.ForeignKey(
                        name: "FK__so_discount__discountTypeId__discount_type__id",
                        column: x => x.discountTypeId,
                        principalSchema: "twister",
                        principalTable: "discount_type",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__so_discount__soId__sale_order__id",
                        column: x => x.saleOrderId,
                        principalSchema: "twister",
                        principalTable: "sale_order",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "sale_order_product",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    saleOrderId = table.Column<int>(type: "int(10)", nullable: false),
                    productVariantId = table.Column<int>(type: "int(10)", nullable: false),
                    totalPrice = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    discount = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    netPrice = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    vat = table.Column<decimal>(type: "decimal(8,2)", nullable: true),
                    quantity = table.Column<int>(type: "int(5)", nullable: false),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    price = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    variant = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    sku = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    remark = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    image = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sale_order_product", x => x.id);
                    table.ForeignKey(
                        name: "sale_order_product_ibfk_1",
                        column: x => x.productVariantId,
                        principalSchema: "twister",
                        principalTable: "product_variant",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "sale_order_product_ibfk_2",
                        column: x => x.saleOrderId,
                        principalSchema: "twister",
                        principalTable: "sale_order",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "sale_order_product_ibfk_3",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "seller_promotion_redeem",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    sellerPromotionId = table.Column<int>(type: "int(10)", nullable: false),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    saleOrderId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_seller_promotion_redeem", x => x.id);
                    table.ForeignKey(
                        name: "seller_promotion_redeem_ibfk_1",
                        column: x => x.saleOrderId,
                        principalSchema: "twister",
                        principalTable: "sale_order",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "seller_promotion_redeem_ibfk_2",
                        column: x => x.sellerPromotionId,
                        principalSchema: "twister",
                        principalTable: "seller_promotion",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "seller_promotion_redeem_ibfk_3",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "stock",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    productVariantId = table.Column<int>(type: "int(10)", nullable: false),
                    quantity = table.Column<int>(type: "int(5)", nullable: false),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    stockTypeId = table.Column<int>(type: "int(4)", nullable: false),
                    saleOrderId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_stock", x => x.id);
                    table.ForeignKey(
                        name: "stock_ibfk_1",
                        column: x => x.productVariantId,
                        principalSchema: "twister",
                        principalTable: "product_variant",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "stock_ibfk_2",
                        column: x => x.saleOrderId,
                        principalSchema: "twister",
                        principalTable: "sale_order",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__stock__statusId__status__id",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "stock_ibfk_3",
                        column: x => x.stockTypeId,
                        principalSchema: "twister",
                        principalTable: "stock_type",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "credit_note",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    code = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    invoiceReceiptId = table.Column<int>(type: "int(10)", nullable: false),
                    saleOrderId = table.Column<int>(type: "int(10)", nullable: false),
                    totalPrice = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    vat = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    saleOrderCode = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    netPrice = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    name = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    telephone = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    email = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    address = table.Column<string>(unicode: false, nullable: false),
                    city = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    province = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    customerUserId = table.Column<int>(type: "int(10)", nullable: false),
                    payment = table.Column<string>(unicode: false, maxLength: 30, nullable: true),
                    remark = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    sellerId = table.Column<int>(type: "int(10)", nullable: false),
                    marketplaceAddress = table.Column<string>(unicode: false, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_credit_note", x => x.id);
                    table.ForeignKey(
                        name: "FK__credit_note__customerUserId__customer_user__id",
                        column: x => x.customerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "credit_note_ibfk_1",
                        column: x => x.invoiceReceiptId,
                        principalSchema: "twister",
                        principalTable: "invoice_receipt",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "credit_note_ibfk_2",
                        column: x => x.saleOrderId,
                        principalSchema: "twister",
                        principalTable: "sale_order",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "credit_note_ibfk_3",
                        column: x => x.sellerId,
                        principalSchema: "twister",
                        principalTable: "seller",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "credit_note_ibfk_4",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "invoice_receipt_product",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    invoiceReceiptId = table.Column<int>(type: "int(10)", nullable: false),
                    productVariantId = table.Column<int>(type: "int(10)", nullable: false),
                    totalPrice = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    discount = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    netPrice = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    vat = table.Column<decimal>(type: "decimal(8,2)", nullable: true),
                    quantity = table.Column<int>(type: "int(5)", nullable: false),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    price = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    variant = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    sku = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    remark = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    image = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_invoice_receipt_product", x => x.id);
                    table.ForeignKey(
                        name: "invoice_receipt_product_ibfk_1",
                        column: x => x.invoiceReceiptId,
                        principalSchema: "twister",
                        principalTable: "invoice_receipt",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_invrep_product__productVariantId__product_variant__id",
                        column: x => x.productVariantId,
                        principalSchema: "twister",
                        principalTable: "product_variant",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "purchase_order_delivered",
                schema: "twister",
                columns: table => new
                {
                    purchaseOrderId = table.Column<int>(type: "int(10)", nullable: false),
                    lockerCode = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    boxId = table.Column<int>(type: "int(10)", nullable: false),
                    createDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_purchase_order_delivered", x => new { x.purchaseOrderId, x.lockerCode, x.boxId });
                    table.ForeignKey(
                        name: "FK__po_delivered__poId__purchase_order__id",
                        column: x => x.purchaseOrderId,
                        principalSchema: "twister",
                        principalTable: "purchase_order",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "purchase_order_log",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    purchaseOrderId = table.Column<int>(type: "int(10)", nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    createDate = table.Column<DateTime>(nullable: false),
                    logisticUserId = table.Column<int>(type: "int(5)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_purchase_order_log", x => x.id);
                    table.ForeignKey(
                        name: "purchase_order_log_ibfk_1",
                        column: x => x.logisticUserId,
                        principalSchema: "twister",
                        principalTable: "logistic_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__po_log__poId__purchase_order__id",
                        column: x => x.purchaseOrderId,
                        principalSchema: "twister",
                        principalTable: "purchase_order",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "purchase_order_logistic_user",
                schema: "twister",
                columns: table => new
                {
                    purchaseOrderId = table.Column<int>(type: "int(10)", nullable: false),
                    logisticUserId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_purchase_order_logistic_user", x => x.purchaseOrderId);
                    table.ForeignKey(
                        name: "FK_PO_logistic_user__logisticUserId__logistic_user__id",
                        column: x => x.logisticUserId,
                        principalSchema: "twister",
                        principalTable: "logistic_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PO_logistic_user__purchaseOrderId__purchase_order__id",
                        column: x => x.purchaseOrderId,
                        principalSchema: "twister",
                        principalTable: "purchase_order",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "purchase_order_parcel",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    purchaseOrderId = table.Column<int>(type: "int(10)", nullable: false),
                    lockerCode = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    boxId = table.Column<int>(type: "int(10)", nullable: false),
                    quantity = table.Column<int>(type: "int(6)", nullable: false),
                    createDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_purchase_order_parcel", x => x.id);
                    table.ForeignKey(
                        name: "FK__po_parcel__poId__purchase_oreder__id",
                        column: x => x.purchaseOrderId,
                        principalSchema: "twister",
                        principalTable: "purchase_order",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "purchase_order_product",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    purchaseOrderId = table.Column<int>(type: "int(10)", nullable: false),
                    productVariantId = table.Column<int>(type: "int(10)", nullable: false),
                    totalPrice = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    discount = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    netPrice = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    vat = table.Column<decimal>(type: "decimal(8,2)", nullable: true),
                    quantity = table.Column<int>(type: "int(5)", nullable: false),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    price = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    variant = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    name = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    sku = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    remark = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    image = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_purchase_order_product", x => x.id);
                    table.ForeignKey(
                        name: "FK__po_product__productVariantId__product_variant__id",
                        column: x => x.productVariantId,
                        principalSchema: "twister",
                        principalTable: "product_variant",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "purchase_order_product_ibfk_1",
                        column: x => x.purchaseOrderId,
                        principalSchema: "twister",
                        principalTable: "purchase_order",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "purchase_order_tracking",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    code = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    purchaseOrderId = table.Column<int>(type: "int(10)", nullable: false),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false),
                    staticCode = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_purchase_order_tracking", x => x.id);
                    table.ForeignKey(
                        name: "purchase_order_tracking_ibfk_1",
                        column: x => x.purchaseOrderId,
                        principalSchema: "twister",
                        principalTable: "purchase_order",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__po_tracking__statusId__status__id",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "groupon",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    sellerId = table.Column<int>(type: "int(10)", nullable: false),
                    customerUserId = table.Column<int>(type: "int(10)", nullable: false),
                    createDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    updateDate = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"),
                    expireDate = table.Column<DateTime>(nullable: false),
                    code = table.Column<string>(maxLength: 255, nullable: false),
                    confirmationCode = table.Column<string>(maxLength: 255, nullable: true),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    saleOrderProductId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_groupon", x => x.id);
                    table.ForeignKey(
                        name: "FK__groupon__customerUserId__customer_user__id",
                        column: x => x.customerUserId,
                        principalSchema: "twister",
                        principalTable: "customer_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__groupon__saleOrderProductId__sale_order_product__id",
                        column: x => x.saleOrderProductId,
                        principalSchema: "twister",
                        principalTable: "sale_order_product",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__groupon__sellerId__seller__id",
                        column: x => x.sellerId,
                        principalSchema: "twister",
                        principalTable: "seller",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__groupon__statusId__status__id",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "credit_note_product",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    name = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    price = table.Column<decimal>(type: "decimal(10,0)", nullable: false),
                    quantity = table.Column<int>(type: "int(10)", nullable: false),
                    discount = table.Column<decimal>(type: "decimal(10,0)", nullable: false),
                    netPrice = table.Column<decimal>(type: "decimal(10,0)", nullable: false),
                    creditNoteId = table.Column<int>(type: "int(10)", nullable: false),
                    vat = table.Column<decimal>(type: "decimal(10,0)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_credit_note_product", x => x.id);
                    table.ForeignKey(
                        name: "credit_note_product_ibfk_1",
                        column: x => x.creditNoteId,
                        principalSchema: "twister",
                        principalTable: "credit_note",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "purchase_order_product_actual_quantity",
                schema: "twister",
                columns: table => new
                {
                    purchaseOrderProductId = table.Column<int>(type: "int(10)", nullable: false),
                    actualQuantity = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_purchase_order_product_actual_quantity", x => x.purchaseOrderProductId);
                    table.ForeignKey(
                        name: "FK_popaq__purchaseOrderProductId__pop__id",
                        column: x => x.purchaseOrderProductId,
                        principalSchema: "twister",
                        principalTable: "purchase_order_product",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "purchase_order_product_piece",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    purchaseOrderProductId = table.Column<int>(type: "int(10)", nullable: false),
                    statusId = table.Column<int>(type: "int(4)", nullable: false),
                    purchaseOrdertrackingId = table.Column<int>(type: "int(10)", nullable: true),
                    createDate = table.Column<DateTime>(nullable: false),
                    updateDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_purchase_order_product_piece", x => x.id);
                    table.ForeignKey(
                        name: "purchase_order_product_piece_ibfk_1",
                        column: x => x.purchaseOrderProductId,
                        principalSchema: "twister",
                        principalTable: "purchase_order_product",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "purchase_order_product_piece_ibfk_2",
                        column: x => x.purchaseOrdertrackingId,
                        principalSchema: "twister",
                        principalTable: "purchase_order_tracking",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__po_product_piece__statusId__status__id",
                        column: x => x.statusId,
                        principalSchema: "twister",
                        principalTable: "status",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "FK__acl__marketplaceUserId__markeplace_user__id",
                schema: "twister",
                table: "admin_change_log",
                column: "marketplaceUserId");

            migrationBuilder.CreateIndex(
                name: "FK__art__marketplaceUserId__marketplace_user__id",
                schema: "twister",
                table: "admin_refresh_token",
                column: "marketplaceUserId");

            migrationBuilder.CreateIndex(
                name: "FK__allow_open_locker__logDeliveryId__logdelivery__transnum",
                schema: "twister",
                table: "allow_open_locker",
                column: "logDeliveryId");

            migrationBuilder.CreateIndex(
                name: "FK__allow_open_locker__saleOrderId__sale_order__id",
                schema: "twister",
                table: "allow_open_locker",
                column: "saleOrderId");

            migrationBuilder.CreateIndex(
                name: "FK_banner_food_seller__bMarketplaceId__banner_marketplace__id",
                schema: "twister",
                table: "banner_food_seller",
                column: "bannerMarketplaceId");

            migrationBuilder.CreateIndex(
                name: "FK_banner_food_seller__bannerSellerId__banner_seller__id",
                schema: "twister",
                table: "banner_food_seller",
                column: "bannerSellerId");

            migrationBuilder.CreateIndex(
                name: "FK_banner_food_seller__sellerGroupId__seller_group__id",
                schema: "twister",
                table: "banner_food_seller",
                column: "sellerGroupId");

            migrationBuilder.CreateIndex(
                name: "FK_banner_food_seller__sellerId__seller__id",
                schema: "twister",
                table: "banner_food_seller",
                column: "sellerId");

            migrationBuilder.CreateIndex(
                name: "fk-banner_marketplace-marketplace_user.id",
                schema: "twister",
                table: "banner_marketplace",
                column: "marketplaceUserId");

            migrationBuilder.CreateIndex(
                name: "fk-banner_marketplace-status.id",
                schema: "twister",
                table: "banner_marketplace",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "FK__bmp__bmId__bm__id",
                schema: "twister",
                table: "banner_marketplace_product",
                column: "bannerMarketplaceId");

            migrationBuilder.CreateIndex(
                name: "FK_bmp__productId__product__id",
                schema: "twister",
                table: "banner_marketplace_product",
                column: "productId");

            migrationBuilder.CreateIndex(
                name: "fk-banner_seller-seller.id",
                schema: "twister",
                table: "banner_seller",
                column: "sellerId");

            migrationBuilder.CreateIndex(
                name: "fk-banner_seller-status.id",
                schema: "twister",
                table: "banner_seller",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "FK__brand__statusId__status__id",
                schema: "twister",
                table: "brand",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "FK__brand_lang__languageId__language__id",
                schema: "twister",
                table: "brand_language",
                column: "languageId");

            migrationBuilder.CreateIndex(
                name: "fk-cart.customerUserId",
                schema: "twister",
                table: "cart",
                column: "customerUserId");

            migrationBuilder.CreateIndex(
                name: "eventId",
                schema: "twister",
                table: "cart",
                column: "eventId");

            migrationBuilder.CreateIndex(
                name: "FK-cart_groupBuyingId_group_buying_id",
                schema: "twister",
                table: "cart",
                column: "groupBuyingId");

            migrationBuilder.CreateIndex(
                name: "fk-cart-status.id",
                schema: "twister",
                table: "cart",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "fk-cart_product-product_variant.id",
                schema: "twister",
                table: "cart_product",
                column: "productVariantId");

            migrationBuilder.CreateIndex(
                name: "FK__cat_marketplace__statusId__status__id",
                schema: "twister",
                table: "category_marketplace",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "fk-category_marketplace.id_children",
                schema: "twister",
                table: "category_marketplace_hierarchy",
                column: "childrenId");

            migrationBuilder.CreateIndex(
                name: "FK__cat_marketplace__lang__languageId__lang__id",
                schema: "twister",
                table: "category_marketplace_language",
                column: "languageId");

            migrationBuilder.CreateIndex(
                name: "fk-category_marketplace_product-product_id",
                schema: "twister",
                table: "category_marketplace_product",
                column: "productId");

            migrationBuilder.CreateIndex(
                name: "FK__cat_seller__sellerId__seller__id",
                schema: "twister",
                table: "category_seller",
                column: "sellerId");

            migrationBuilder.CreateIndex(
                name: "FK__cat_seller__statusId__status__id",
                schema: "twister",
                table: "category_seller",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "fk-category_seller.id_children",
                schema: "twister",
                table: "category_seller_hierarchy",
                column: "childrenId");

            migrationBuilder.CreateIndex(
                name: "fk-category_seller_product-product.id",
                schema: "twister",
                table: "category_seller_product",
                column: "productId");

            migrationBuilder.CreateIndex(
                name: "fk-chat_customer_service-customer_user.id",
                schema: "twister",
                table: "chat_customer_service",
                column: "customerUserId");

            migrationBuilder.CreateIndex(
                name: "fk-chat_customer_service-marketplace_user.id",
                schema: "twister",
                table: "chat_customer_service",
                column: "marketplaceUserId");

            migrationBuilder.CreateIndex(
                name: "fk-chat_customer_service-status.id",
                schema: "twister",
                table: "chat_customer_service",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "fk-chat_seller-product_variant.id",
                schema: "twister",
                table: "chat_seller",
                column: "productVariantId");

            migrationBuilder.CreateIndex(
                name: "fk-chat_seller-seller.id",
                schema: "twister",
                table: "chat_seller",
                column: "sellerId");

            migrationBuilder.CreateIndex(
                name: "fk-chat_seller-status.id",
                schema: "twister",
                table: "chat_seller",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "FK__city__provinceId__province__id",
                schema: "twister",
                table: "city",
                column: "provinceId");

            migrationBuilder.CreateIndex(
                name: "FK__city_language__languageId__language__id",
                schema: "twister",
                table: "city_language",
                column: "languageId");

            migrationBuilder.CreateIndex(
                name: "customerUserId",
                schema: "twister",
                table: "coin_transaction",
                column: "customerUserId");

            migrationBuilder.CreateIndex(
                name: "FK__coin_tran__saleOrderId__sale_order__id",
                schema: "twister",
                table: "coin_transaction",
                column: "saleOrderId");

            migrationBuilder.CreateIndex(
                name: "statusId",
                schema: "twister",
                table: "coin_transaction",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "FK__country_language__languageId__language__id",
                schema: "twister",
                table: "country_language",
                column: "languageId");

            migrationBuilder.CreateIndex(
                name: "FK__coupon_type__catMarketplaceId__cat_market__id",
                schema: "twister",
                table: "coupon_type",
                column: "categoryMarketplaceId");

            migrationBuilder.CreateIndex(
                name: "FK__coupon_type__couponPriceId__coupon_price__id",
                schema: "twister",
                table: "coupon_type",
                column: "couponPriceId");

            migrationBuilder.CreateIndex(
                name: "FK__coupon_type__productId__product__id",
                schema: "twister",
                table: "coupon_type",
                column: "productId");

            migrationBuilder.CreateIndex(
                name: "FK__coupon_type__sellerId__seller__id",
                schema: "twister",
                table: "coupon_type",
                column: "sellerId");

            migrationBuilder.CreateIndex(
                name: "FK__credit_card__customerUserId__customer_user__id",
                schema: "twister",
                table: "credit_card",
                column: "customerUserId");

            migrationBuilder.CreateIndex(
                name: "statusId",
                schema: "twister",
                table: "credit_card",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "FK__credit_note__customerUserId__customer_user__id",
                schema: "twister",
                table: "credit_note",
                column: "customerUserId");

            migrationBuilder.CreateIndex(
                name: "fk-credit_note-invoiceReceipt.id",
                schema: "twister",
                table: "credit_note",
                column: "invoiceReceiptId");

            migrationBuilder.CreateIndex(
                name: "fk-credit_note-saleOrder.id",
                schema: "twister",
                table: "credit_note",
                column: "saleOrderId");

            migrationBuilder.CreateIndex(
                name: "fk-credit_note-seller.id",
                schema: "twister",
                table: "credit_note",
                column: "sellerId");

            migrationBuilder.CreateIndex(
                name: "fk-credit_note-status.id",
                schema: "twister",
                table: "credit_note",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "fk-credit_note_product-credit_note.id",
                schema: "twister",
                table: "credit_note_product",
                column: "creditNoteId");

            migrationBuilder.CreateIndex(
                name: "fk-customer_address-city.id",
                schema: "twister",
                table: "customer_address",
                column: "cityId");

            migrationBuilder.CreateIndex(
                name: "fk-customer_address-country.id",
                schema: "twister",
                table: "customer_address",
                column: "countryId");

            migrationBuilder.CreateIndex(
                name: "fk-customer_address-customer_user.id",
                schema: "twister",
                table: "customer_address",
                column: "customerUserId");

            migrationBuilder.CreateIndex(
                name: "fk-customer_address-province.id",
                schema: "twister",
                table: "customer_address",
                column: "provinceId");

            migrationBuilder.CreateIndex(
                name: "fk-customer_address-status.id",
                schema: "twister",
                table: "customer_address",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "FK__customer_address__townId__town__id",
                schema: "twister",
                table: "customer_address",
                column: "townId");

            migrationBuilder.CreateIndex(
                name: "fk-customer_review_product-customer_user.id",
                schema: "twister",
                table: "customer_review_product",
                column: "customerUserId");

            migrationBuilder.CreateIndex(
                name: "fk-customer_review_product-product.id",
                schema: "twister",
                table: "customer_review_product",
                column: "productId");

            migrationBuilder.CreateIndex(
                name: "fk-customer_review_product-status.id",
                schema: "twister",
                table: "customer_review_product",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "fk-customer_review_seller-customer_user.id",
                schema: "twister",
                table: "customer_review_seller",
                column: "customerUserId");

            migrationBuilder.CreateIndex(
                name: "customer_review_seller-seller.id",
                schema: "twister",
                table: "customer_review_seller",
                column: "sellerId");

            migrationBuilder.CreateIndex(
                name: "customer_review_seller-status.id",
                schema: "twister",
                table: "customer_review_seller",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "customer_search_history_log-customer_user.id",
                schema: "twister",
                table: "customer_search_history_log",
                column: "customerUserId");

            migrationBuilder.CreateIndex(
                name: "fk-customer_user-country.id",
                schema: "twister",
                table: "customer_user",
                column: "countryId");

            migrationBuilder.CreateIndex(
                name: "FK__customer_user__languageId__language__id",
                schema: "twister",
                table: "customer_user",
                column: "languageId");

            migrationBuilder.CreateIndex(
                name: "fk-customer_user-status.id",
                schema: "twister",
                table: "customer_user",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "fk-customer_view_history_log-customer_user.id",
                schema: "twister",
                table: "customer_user_view_history_log",
                column: "customerUserId");

            migrationBuilder.CreateIndex(
                name: "customer_view_history_log-product.id",
                schema: "twister",
                table: "customer_user_view_history_log",
                column: "productId");

            migrationBuilder.CreateIndex(
                name: "FK__event__statusId__status__id",
                schema: "twister",
                table: "event",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "lockerId",
                schema: "twister",
                table: "event_locker",
                column: "lockerId");

            migrationBuilder.CreateIndex(
                name: "fk-favorite_product-product.id",
                schema: "twister",
                table: "favorite_product",
                column: "productId");

            migrationBuilder.CreateIndex(
                name: "fk-follow-seller.id",
                schema: "twister",
                table: "follow",
                column: "sellerId");

            migrationBuilder.CreateIndex(
                name: "FK__gp__statusId__status__id",
                schema: "twister",
                table: "gateway_provider",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "gatewayProviderId",
                schema: "twister",
                table: "gateway_provider_payment_type",
                column: "gatewayProviderId");

            migrationBuilder.CreateIndex(
                name: "paymentTypeId",
                schema: "twister",
                table: "gateway_provider_payment_type",
                column: "paymentTypeId");

            migrationBuilder.CreateIndex(
                name: "statusId",
                schema: "twister",
                table: "gateway_provider_payment_type",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "FK_gb_customerUserId_customer_user_id",
                schema: "twister",
                table: "group_buying",
                column: "customerUserId");

            migrationBuilder.CreateIndex(
                name: "FK_gb_gbpvId_gbpv_id",
                schema: "twister",
                table: "group_buying",
                column: "groupBuyingProductVariantId");

            migrationBuilder.CreateIndex(
                name: "FK_gb_statusId_status_id",
                schema: "twister",
                table: "group_buying",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "FK_gbiv_groupBuyingId_group_buying_id",
                schema: "twister",
                table: "group_buying_invitation",
                column: "groupBuyingId");

            migrationBuilder.CreateIndex(
                name: "FK_gbiv_inviteeCustomerUserId_customer_user_id",
                schema: "twister",
                table: "group_buying_invitation",
                column: "InviteeCustomerUserId");

            migrationBuilder.CreateIndex(
                name: "FK_gbiv_inviteeSaleOrderId_saleOrder_id",
                schema: "twister",
                table: "group_buying_invitation",
                column: "inviteeSaleOrderId");

            migrationBuilder.CreateIndex(
                name: "FK_gbiv_inviterCustomerUserId_customer_user_id",
                schema: "twister",
                table: "group_buying_invitation",
                column: "inviterCustomerUserId");

            migrationBuilder.CreateIndex(
                name: "FK_gbiv_parentCustomerUserId_customer_user_id",
                schema: "twister",
                table: "group_buying_invitation",
                column: "parentCustomerUserId");

            migrationBuilder.CreateIndex(
                name: "FK_gbpv_gbcId_group_buying_condition_id",
                schema: "twister",
                table: "group_buying_product_variant",
                column: "groupBuyingConditionId");

            migrationBuilder.CreateIndex(
                name: "FK_gbpv_productVariantId_product_variant_id",
                schema: "twister",
                table: "group_buying_product_variant",
                column: "productVariantId");

            migrationBuilder.CreateIndex(
                name: "FK_gbpv_statusId_status_id",
                schema: "twister",
                table: "group_buying_product_variant",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "FK_gbr_groupBuyingAchievementTypeId_gba_id",
                schema: "twister",
                table: "group_buying_reward",
                column: "groupBuyingAchievementTypeId");

            migrationBuilder.CreateIndex(
                name: "FK_gbr_groupBuyingProductVariantId_gbpv_id",
                schema: "twister",
                table: "group_buying_reward",
                column: "groupBuyingProductVariantId");

            migrationBuilder.CreateIndex(
                name: "FK_gbr_marketplaceCouponGroupId_mktcg_id",
                schema: "twister",
                table: "group_buying_reward",
                column: "marketplaceCouponGroupId");

            migrationBuilder.CreateIndex(
                name: "FK_gbr_productVariantId_product_variant_id",
                schema: "twister",
                table: "group_buying_reward",
                column: "productVariantId");

            migrationBuilder.CreateIndex(
                name: "FK__groupon__customerUserId__customer_user__id",
                schema: "twister",
                table: "groupon",
                column: "customerUserId");

            migrationBuilder.CreateIndex(
                name: "FK__groupon__saleOrderProductId__sale_order_product__id",
                schema: "twister",
                table: "groupon",
                column: "saleOrderProductId");

            migrationBuilder.CreateIndex(
                name: "FK__groupon__sellerId__seller__id",
                schema: "twister",
                table: "groupon",
                column: "sellerId");

            migrationBuilder.CreateIndex(
                name: "FK__groupon__statusId__status__id",
                schema: "twister",
                table: "groupon",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "fk-invoice-receipt-customer_user.id",
                schema: "twister",
                table: "invoice_receipt",
                column: "customerUserId");

            migrationBuilder.CreateIndex(
                name: "FK__invrep__saleOrderId__sale_order__id",
                schema: "twister",
                table: "invoice_receipt",
                column: "saleOrderId");

            migrationBuilder.CreateIndex(
                name: "fk-invoice-receipt-seller.id",
                schema: "twister",
                table: "invoice_receipt",
                column: "sellerId");

            migrationBuilder.CreateIndex(
                name: "fk-invoice-receipt-status.id",
                schema: "twister",
                table: "invoice_receipt",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "fk-invoice_receipt_product-invoice_receipt.id",
                schema: "twister",
                table: "invoice_receipt_product",
                column: "invoiceReceiptId");

            migrationBuilder.CreateIndex(
                name: "FK_invrep_product__productVariantId__product_variant__id",
                schema: "twister",
                table: "invoice_receipt_product",
                column: "productVariantId");

            migrationBuilder.CreateIndex(
                name: "fk-company-city.id",
                schema: "twister",
                table: "logistic_company",
                column: "cityId");

            migrationBuilder.CreateIndex(
                name: "FK__logistic_company__countryId__country__id",
                schema: "twister",
                table: "logistic_company",
                column: "countryId");

            migrationBuilder.CreateIndex(
                name: "fk-company-province.id",
                schema: "twister",
                table: "logistic_company",
                column: "provinceId");

            migrationBuilder.CreateIndex(
                name: "fk-company-status.id",
                schema: "twister",
                table: "logistic_company",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "FK__company__townId__town__id",
                schema: "twister",
                table: "logistic_company",
                column: "townId");

            migrationBuilder.CreateIndex(
                name: "fk-logistic_user-company.id",
                schema: "twister",
                table: "logistic_user",
                column: "logisticCompanyId");

            migrationBuilder.CreateIndex(
                name: "fk-logistic_user-status.id",
                schema: "twister",
                table: "logistic_user",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "fk-marketplace_coupon_redeem-customer_user.id",
                schema: "twister",
                table: "marketplace_coupon",
                column: "customerUserId");

            migrationBuilder.CreateIndex(
                name: "fk-marketplace_coupon_redeem-marketplace_coupon.id",
                schema: "twister",
                table: "marketplace_coupon",
                column: "marketplaceCouponGroupId");

            migrationBuilder.CreateIndex(
                name: "FK__mk_coupon_group__couponPriceId__coupon_price__id",
                schema: "twister",
                table: "marketplace_coupon_group",
                column: "couponPriceId");

            migrationBuilder.CreateIndex(
                name: "fk-marketplace_coupon-marketplace_user.id",
                schema: "twister",
                table: "marketplace_coupon_group",
                column: "marketplaceUserId");

            migrationBuilder.CreateIndex(
                name: "fk-marketplace_coupon-status.id",
                schema: "twister",
                table: "marketplace_coupon_group",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "fk-marketplace_promotion-category_marketplace.id",
                schema: "twister",
                table: "marketplace_promotion",
                column: "categoryMarketplaceId");

            migrationBuilder.CreateIndex(
                name: "fk-marketplace_promotion-marketplace_user.id",
                schema: "twister",
                table: "marketplace_promotion",
                column: "marketplaceUserId");

            migrationBuilder.CreateIndex(
                name: "fk-marketplace_promotion-price_promotion.id",
                schema: "twister",
                table: "marketplace_promotion",
                column: "pricePromotionId");

            migrationBuilder.CreateIndex(
                name: "fk-marketplace_promotion-product.id",
                schema: "twister",
                table: "marketplace_promotion",
                column: "productId");

            migrationBuilder.CreateIndex(
                name: "fk-marketplace_promotion-status.id",
                schema: "twister",
                table: "marketplace_promotion",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "fk-marketplace_promotion_redeem-marketplace_promotion.id",
                schema: "twister",
                table: "marketplace_promotion_redeem",
                column: "marketplacePromotionId");

            migrationBuilder.CreateIndex(
                name: "fk-marketplace_promotion_redeem-sale_order.id",
                schema: "twister",
                table: "marketplace_promotion_redeem",
                column: "saleOrderId");

            migrationBuilder.CreateIndex(
                name: "fk-marketplace_promotion_redeem-status.id",
                schema: "twister",
                table: "marketplace_promotion_redeem",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "fk-marketplace_review_seller-marketplace_user.id",
                schema: "twister",
                table: "marketplace_review_seller",
                column: "marketplaceUserId");

            migrationBuilder.CreateIndex(
                name: "fk-marketplace_review_seller-seller.id",
                schema: "twister",
                table: "marketplace_review_seller",
                column: "sellerId");

            migrationBuilder.CreateIndex(
                name: "fk-marketplace_review_seller-status.id",
                schema: "twister",
                table: "marketplace_review_seller",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "FK_marketplace_user__custId__customer_user__id",
                schema: "twister",
                table: "marketplace_user",
                column: "customerUserId");

            migrationBuilder.CreateIndex(
                name: "FK_marketplace_user__roleClassId__role_class__id",
                schema: "twister",
                table: "marketplace_user",
                column: "roleClassId");

            migrationBuilder.CreateIndex(
                name: "fk-marketplace_user-status.id",
                schema: "twister",
                table: "marketplace_user",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "fk-notification_customer-customer_user.id",
                schema: "twister",
                table: "notification_customer",
                column: "customerUserId");

            migrationBuilder.CreateIndex(
                name: "FK__noti_customer__notiManId__noti_man__id",
                schema: "twister",
                table: "notification_customer",
                column: "notificationManualId");

            migrationBuilder.CreateIndex(
                name: "FK__noti_customer__notiTempId__noti_temp__id",
                schema: "twister",
                table: "notification_customer",
                column: "notificationTemplateId");

            migrationBuilder.CreateIndex(
                name: "fk-notification_customer-status.id",
                schema: "twister",
                table: "notification_customer",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "fk-notification_manual-app_deep_link.id",
                schema: "twister",
                table: "notification_manual",
                column: "appDeepLinkId");

            migrationBuilder.CreateIndex(
                name: "FK__noti_man__notiTypeId__noti_type__id",
                schema: "twister",
                table: "notification_manual",
                column: "notificationTypeId");

            migrationBuilder.CreateIndex(
                name: "FK__noti_man_lan__languageId__language__id",
                schema: "twister",
                table: "notification_manual_language",
                column: "languageId");

            migrationBuilder.CreateIndex(
                name: "FK__noti_man_lan__notiManId__noti_man__id",
                schema: "twister",
                table: "notification_manual_language",
                column: "notificationManualId");

            migrationBuilder.CreateIndex(
                name: "FK__noti_seller__notiManId__noti_man__id",
                schema: "twister",
                table: "notification_seller",
                column: "notificationManualId");

            migrationBuilder.CreateIndex(
                name: "FK__noti_seller__notiTempId__noti_temp__id",
                schema: "twister",
                table: "notification_seller",
                column: "notificationTemplateId");

            migrationBuilder.CreateIndex(
                name: "fk-notification_seller-seller.id",
                schema: "twister",
                table: "notification_seller",
                column: "sellerId");

            migrationBuilder.CreateIndex(
                name: "fk-notification_seller-status.id",
                schema: "twister",
                table: "notification_seller",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "fk-notification_template-app_deep_link.id",
                schema: "twister",
                table: "notification_template",
                column: "appDeepLinkId");

            migrationBuilder.CreateIndex(
                name: "FK__noti_temp__notiTypeId__noti_type__id",
                schema: "twister",
                table: "notification_template",
                column: "notificationTypeId");

            migrationBuilder.CreateIndex(
                name: "FK__noti_temp_lang__languageId__language__id",
                schema: "twister",
                table: "notification_template_language",
                column: "languageId");

            migrationBuilder.CreateIndex(
                name: "FK__noti_temp_lang__notiTempId__noti_temp__id",
                schema: "twister",
                table: "notification_template_language",
                column: "notificationTemplateId");

            migrationBuilder.CreateIndex(
                name: "creditCardId",
                schema: "twister",
                table: "payment",
                column: "creditCardId");

            migrationBuilder.CreateIndex(
                name: "gatewayProviderId",
                schema: "twister",
                table: "payment",
                column: "gatewayProviderId");

            migrationBuilder.CreateIndex(
                name: "fk-payment-payment_type.id",
                schema: "twister",
                table: "payment",
                column: "paymentTypeId");

            migrationBuilder.CreateIndex(
                name: "fk-point-customer_user.id",
                schema: "twister",
                table: "point",
                column: "customerUserId");

            migrationBuilder.CreateIndex(
                name: "fk-point-marketplace_coupon.id",
                schema: "twister",
                table: "point",
                column: "marketplaceCouponId");

            migrationBuilder.CreateIndex(
                name: "fk-point-seller_coupon.id",
                schema: "twister",
                table: "point",
                column: "sellerCouponId");

            migrationBuilder.CreateIndex(
                name: "fk-point-status.id",
                schema: "twister",
                table: "point",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "FK__product__brandId__brand__id",
                schema: "twister",
                table: "product",
                column: "brandId");

            migrationBuilder.CreateIndex(
                name: "fk-product-category_marketplace.id",
                schema: "twister",
                table: "product",
                column: "categoryMarketPlaceId");

            migrationBuilder.CreateIndex(
                name: "fk-product-category_seller.id",
                schema: "twister",
                table: "product",
                column: "categorySellerId");

            migrationBuilder.CreateIndex(
                name: "eventId",
                schema: "twister",
                table: "product",
                column: "eventId");

            migrationBuilder.CreateIndex(
                name: "pk-product.id",
                schema: "twister",
                table: "product",
                column: "id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "fk__product__productTypeId__product_type__id",
                schema: "twister",
                table: "product",
                column: "productTypeId");

            migrationBuilder.CreateIndex(
                name: "fk-product-seller_address.id",
                schema: "twister",
                table: "product",
                column: "sellerAddressId");

            migrationBuilder.CreateIndex(
                name: "fk-product-seller.id",
                schema: "twister",
                table: "product",
                column: "sellerId");

            migrationBuilder.CreateIndex(
                name: "fk-product-status.id",
                schema: "twister",
                table: "product",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "FK__product_image__mediaTypeId__media_type__id",
                schema: "twister",
                table: "product_image",
                column: "mediaTypeId");

            migrationBuilder.CreateIndex(
                name: "FK_product_image__productId__product__id",
                schema: "twister",
                table: "product_image",
                column: "productId");

            migrationBuilder.CreateIndex(
                name: "FK_product_image__statusId__status__id",
                schema: "twister",
                table: "product_image",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "fk-product_rating-customer_user.id",
                schema: "twister",
                table: "product_rating",
                column: "customerUserId");

            migrationBuilder.CreateIndex(
                name: "fk-product_rating-product.id",
                schema: "twister",
                table: "product_rating",
                column: "productId");

            migrationBuilder.CreateIndex(
                name: "FK__product_sold__productId__product__id",
                schema: "twister",
                table: "product_sold",
                column: "productId");

            migrationBuilder.CreateIndex(
                name: "fk-product_variant-product.id",
                schema: "twister",
                table: "product_variant",
                column: "productId");

            migrationBuilder.CreateIndex(
                name: "fk-product_variant-status.id",
                schema: "twister",
                table: "product_variant",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "FK__product_wishlist__productId__product__id",
                schema: "twister",
                table: "product_wishlist",
                column: "productId");

            migrationBuilder.CreateIndex(
                name: "fk-price_promotion-product.id",
                schema: "twister",
                table: "promotion_price",
                column: "productId");

            migrationBuilder.CreateIndex(
                name: "FK__province__countryId__country__id",
                schema: "twister",
                table: "province",
                column: "countryId");

            migrationBuilder.CreateIndex(
                name: "FK__province_language__languageId__language__id",
                schema: "twister",
                table: "province_language",
                column: "languageId");

            migrationBuilder.CreateIndex(
                name: "FK_purchaseOrderId__logisticCompanyId__logistic_company__id",
                schema: "twister",
                table: "purchase_order",
                column: "logisticCompanyId");

            migrationBuilder.CreateIndex(
                name: "FK__purchase_order__saleOrderId__sale_order__id",
                schema: "twister",
                table: "purchase_order",
                column: "saleOrderId");

            migrationBuilder.CreateIndex(
                name: "fk-purchase_order-seller.id",
                schema: "twister",
                table: "purchase_order",
                column: "sellerId");

            migrationBuilder.CreateIndex(
                name: "fk-purchase_order-status.id",
                schema: "twister",
                table: "purchase_order",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "FK__po_delivered__poId__purchase_order__id",
                schema: "twister",
                table: "purchase_order_delivered",
                column: "purchaseOrderId");

            migrationBuilder.CreateIndex(
                name: "logisticUserId",
                schema: "twister",
                table: "purchase_order_log",
                column: "logisticUserId");

            migrationBuilder.CreateIndex(
                name: "FK__po_log__poId__purchase_order__id",
                schema: "twister",
                table: "purchase_order_log",
                column: "purchaseOrderId");

            migrationBuilder.CreateIndex(
                name: "FK_PO_logistic_user__logisticUserId__logistic_user__id",
                schema: "twister",
                table: "purchase_order_logistic_user",
                column: "logisticUserId");

            migrationBuilder.CreateIndex(
                name: "FK__po_parcel__poId__purchase_oreder__id",
                schema: "twister",
                table: "purchase_order_parcel",
                column: "purchaseOrderId");

            migrationBuilder.CreateIndex(
                name: "FK__po_product__productVariantId__product_variant__id",
                schema: "twister",
                table: "purchase_order_product",
                column: "productVariantId");

            migrationBuilder.CreateIndex(
                name: "fk-purchase_oreder_product-purchase_order.id",
                schema: "twister",
                table: "purchase_order_product",
                column: "purchaseOrderId");

            migrationBuilder.CreateIndex(
                name: "purchaseOrderProductId",
                schema: "twister",
                table: "purchase_order_product_piece",
                column: "purchaseOrderProductId");

            migrationBuilder.CreateIndex(
                name: "purchaseOrdertrackingId",
                schema: "twister",
                table: "purchase_order_product_piece",
                column: "purchaseOrdertrackingId");

            migrationBuilder.CreateIndex(
                name: "FK__po_product_piece__statusId__status__id",
                schema: "twister",
                table: "purchase_order_product_piece",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "purchaseOrderId",
                schema: "twister",
                table: "purchase_order_tracking",
                column: "purchaseOrderId");

            migrationBuilder.CreateIndex(
                name: "FK__po_tracking__statusId__status__id",
                schema: "twister",
                table: "purchase_order_tracking",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "fk-relate_category-category_marketplace.id",
                schema: "twister",
                table: "relate_category",
                column: "catagoryMarketplaceId");

            migrationBuilder.CreateIndex(
                name: "fk-ralate_product-product.id-relateProductId",
                schema: "twister",
                table: "relate_product",
                column: "relateProductId");

            migrationBuilder.CreateIndex(
                name: "id",
                schema: "twister",
                table: "role_class",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "FK__role_class_ability__roleAbilityId__role_ability__id",
                schema: "twister",
                table: "role_class_ability",
                column: "roleAbilityId");

            migrationBuilder.CreateIndex(
                name: "fk-sale_order-cart.id",
                schema: "twister",
                table: "sale_order",
                column: "cartId");

            migrationBuilder.CreateIndex(
                name: "FK__so__customerAddressId__customer_address__id",
                schema: "twister",
                table: "sale_order",
                column: "customerAddressId");

            migrationBuilder.CreateIndex(
                name: "fk-sale_order-customer_user.id",
                schema: "twister",
                table: "sale_order",
                column: "customerUserId");

            migrationBuilder.CreateIndex(
                name: "FK__so__paymentId__payment__id",
                schema: "twister",
                table: "sale_order",
                column: "paymentId");

            migrationBuilder.CreateIndex(
                name: "fk-sale_order-status.id",
                schema: "twister",
                table: "sale_order",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "FK__so_discount__discountTypeId__discount_type__id",
                schema: "twister",
                table: "sale_order_discount",
                column: "discountTypeId");

            migrationBuilder.CreateIndex(
                name: "fk-sale_order_product-product_variant.id",
                schema: "twister",
                table: "sale_order_product",
                column: "productVariantId");

            migrationBuilder.CreateIndex(
                name: "fk-sale_order_product-sale_order.id",
                schema: "twister",
                table: "sale_order_product",
                column: "saleOrderId");

            migrationBuilder.CreateIndex(
                name: "fk-sale_order_product-status.id",
                schema: "twister",
                table: "sale_order_product",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "FK_seller__sellerGroupId__seller_group__id",
                schema: "twister",
                table: "seller",
                column: "sellerGroupId");

            migrationBuilder.CreateIndex(
                name: "FK_seller__sellerWorkdayGroupId__seller_wdg__id",
                schema: "twister",
                table: "seller",
                column: "sellerWorkdayGroupId");

            migrationBuilder.CreateIndex(
                name: "fk-seller-status.id",
                schema: "twister",
                table: "seller",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "fk-seller_address-city.id",
                schema: "twister",
                table: "seller_address",
                column: "cityId");

            migrationBuilder.CreateIndex(
                name: "fk-seller_address-country.id",
                schema: "twister",
                table: "seller_address",
                column: "countryId");

            migrationBuilder.CreateIndex(
                name: "fk-seller_address-province.id",
                schema: "twister",
                table: "seller_address",
                column: "provinceId");

            migrationBuilder.CreateIndex(
                name: "fk-seller_address-seller.id",
                schema: "twister",
                table: "seller_address",
                column: "sellerId");

            migrationBuilder.CreateIndex(
                name: "fk-seller_address-status.id",
                schema: "twister",
                table: "seller_address",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "FK__seller_address__townId__town__id",
                schema: "twister",
                table: "seller_address",
                column: "townId");

            migrationBuilder.CreateIndex(
                name: "fk-seller_coupon-marketplace_user.id",
                schema: "twister",
                table: "seller_coupon",
                column: "marketplaceUserId");

            migrationBuilder.CreateIndex(
                name: "fk-seller_coupon-price_coupon.id",
                schema: "twister",
                table: "seller_coupon",
                column: "priceCouponId");

            migrationBuilder.CreateIndex(
                name: "fk-seller_coupon-seller.id",
                schema: "twister",
                table: "seller_coupon",
                column: "sellerId");

            migrationBuilder.CreateIndex(
                name: "fk-seller_coupon-status.id",
                schema: "twister",
                table: "seller_coupon",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "fk-seller_coupon_redeem-customer_user.id",
                schema: "twister",
                table: "seller_coupon_redeem",
                column: "customerUserId");

            migrationBuilder.CreateIndex(
                name: "fk-seller_coupon_redeem-seller_coupon.id",
                schema: "twister",
                table: "seller_coupon_redeem",
                column: "sellerCouponId");

            migrationBuilder.CreateIndex(
                name: "fk-seller_coupon_redeem-status.id",
                schema: "twister",
                table: "seller_coupon_redeem",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "FK__seller_group__statusId__status__id",
                schema: "twister",
                table: "seller_group",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "FK__seller_image__mediaTypeId__media_type__id",
                schema: "twister",
                table: "seller_image",
                column: "mediaTypeId");

            migrationBuilder.CreateIndex(
                name: "FK_seller_image__sellerId__seller__id",
                schema: "twister",
                table: "seller_image",
                column: "sellerId");

            migrationBuilder.CreateIndex(
                name: "FK_seller_image__statusId__status__id",
                schema: "twister",
                table: "seller_image",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "FK_seller_pd__provinceId__province__id",
                schema: "twister",
                table: "seller_product_delivery",
                column: "provinceId");

            migrationBuilder.CreateIndex(
                name: "fk-seller_promotion-category_seller.id",
                schema: "twister",
                table: "seller_promotion",
                column: "categorySellerId");

            migrationBuilder.CreateIndex(
                name: "fk-seller_promotion-marketplace_user.id",
                schema: "twister",
                table: "seller_promotion",
                column: "marketplaceUserId");

            migrationBuilder.CreateIndex(
                name: "fk-seller_promotion-price_promotion.id",
                schema: "twister",
                table: "seller_promotion",
                column: "pricePromotionId");

            migrationBuilder.CreateIndex(
                name: "fk-seller_promotion-product.id",
                schema: "twister",
                table: "seller_promotion",
                column: "productId");

            migrationBuilder.CreateIndex(
                name: "fk-seller_promotion-status.id",
                schema: "twister",
                table: "seller_promotion",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "fk-seller_promotion_redeem-sale_order.id",
                schema: "twister",
                table: "seller_promotion_redeem",
                column: "saleOrderId");

            migrationBuilder.CreateIndex(
                name: "fk-seller_promotion_redeem-seller_promotion.id",
                schema: "twister",
                table: "seller_promotion_redeem",
                column: "sellerPromotionId");

            migrationBuilder.CreateIndex(
                name: "fk-seller_promotion_redeem-status.id",
                schema: "twister",
                table: "seller_promotion_redeem",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "fk-seller_stats-seller_stats_type_id",
                schema: "twister",
                table: "seller_stats",
                column: "sellerStatsTypeId");

            migrationBuilder.CreateIndex(
                name: "FK_seller_s_types_language__languageId__language__id",
                schema: "twister",
                table: "seller_stats_type_language",
                column: "languageId");

            migrationBuilder.CreateIndex(
                name: "FK_seller_s_types_language__sellerStatsTypeId__seller_s_type__id",
                schema: "twister",
                table: "seller_stats_type_language",
                column: "sellerStatsTypeId");

            migrationBuilder.CreateIndex(
                name: "FK_seller_user__customerUserId__customer_user__id",
                schema: "twister",
                table: "seller_user",
                column: "customerUserId");

            migrationBuilder.CreateIndex(
                name: "FK_seller_user__roleClassId__role_class__id",
                schema: "twister",
                table: "seller_user",
                column: "roleClassId");

            migrationBuilder.CreateIndex(
                name: "FK_seller_user__sellerId__seller__id",
                schema: "twister",
                table: "seller_user",
                column: "sellerId");

            migrationBuilder.CreateIndex(
                name: "FK_seller_user__statusId__status__id",
                schema: "twister",
                table: "seller_user",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "FK__swg__workingDayId__working_day__id",
                schema: "twister",
                table: "seller_workday_group",
                column: "workingDayId");

            migrationBuilder.CreateIndex(
                name: "FK_seller_wdg_holiday__workdayGroupId__seller_wdg__id",
                schema: "twister",
                table: "seller_workday_group_holiday",
                column: "sellerWorkdayGroupId");

            migrationBuilder.CreateIndex(
                name: "FK_seller_wpg__sellerWorkdayProfileId__seller_wp__id",
                schema: "twister",
                table: "seller_workday_profile_group",
                column: "sellerWorkdayProfileId");

            migrationBuilder.CreateIndex(
                name: "FK__status_lang__languageId__language__id",
                schema: "twister",
                table: "status_language",
                column: "languageId");

            migrationBuilder.CreateIndex(
                name: "FK__status_lang__statusId__status__id",
                schema: "twister",
                table: "status_language",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "fk-stock-product_variant.id",
                schema: "twister",
                table: "stock",
                column: "productVariantId");

            migrationBuilder.CreateIndex(
                name: "fk-stock-sale_order.id",
                schema: "twister",
                table: "stock",
                column: "saleOrderId");

            migrationBuilder.CreateIndex(
                name: "FK__stock__statusId__status__id",
                schema: "twister",
                table: "stock",
                column: "statusId");

            migrationBuilder.CreateIndex(
                name: "fk-stock-stock_type.id",
                schema: "twister",
                table: "stock",
                column: "stockTypeId");

            migrationBuilder.CreateIndex(
                name: "FK__town__cityId__city__id",
                schema: "twister",
                table: "town",
                column: "cityId");

            migrationBuilder.CreateIndex(
                name: "FK__town_language__languageId__language__id",
                schema: "twister",
                table: "town_language",
                column: "languageId");

            migrationBuilder.CreateIndex(
                name: "id",
                schema: "twister",
                table: "variant",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "fk-variant-variant_template.id",
                schema: "twister",
                table: "variant",
                column: "variantTemplateId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "__nvjmigrationdatahistory",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "__nvjmigrationhistory",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "admin_change_log",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "admin_refresh_token",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "allow_open_locker",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "allowstatusinfo",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "banner_food_seller",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "banner_marketplace_product",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "brand_language",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "cart_product",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "category_marketplace_hierarchy",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "category_marketplace_language",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "category_marketplace_product",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "category_seller_hierarchy",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "category_seller_product",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "chat_customer_service",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "chat_seller",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "city_language",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "coin_transaction",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "condo_rate",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "condo_register",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "condo_sharebox",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "condoadmin_register",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "condoinfo",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "config",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "country_language",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "coupon_type",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "credit_note_product",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "customer_company",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "customer_review_product",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "customer_review_seller",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "customer_search_history_log",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "customer_user_view_history_log",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "event_locker",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "favorite_product",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "follow",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "gateway_provider_payment_type",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "group_buying_invitation",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "group_buying_reward",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "groupon",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "invoice_receipt_product",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "lockeraccess",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "lockertypeinfo",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "lockerusermapping",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "logopenlocker",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "marketplace_coupon",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "marketplace_promotion_redeem",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "marketplace_review_seller",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "notification_customer",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "notification_manual_language",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "notification_seller",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "notification_template_language",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "point",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "product_image",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "product_rating",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "product_sold",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "product_wishlist",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "province_language",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "purchase_order_delivered",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "purchase_order_log",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "purchase_order_logistic_user",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "purchase_order_parcel",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "purchase_order_product_actual_quantity",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "purchase_order_product_piece",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "regiscode",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "relate_category",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "relate_product",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "role_class_ability",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "sale_order_discount",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_area",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_coupon_redeem",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_image",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_product_delivery",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_promotion_redeem",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_stats",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_stats_type_language",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_user",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_workday_group_holiday",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_workday_profile_group",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "sharelocker",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "status_language",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "stock",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "town_language",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "variant",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "webuser",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "zone",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "logdelivery",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "banner_seller",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "banner_marketplace",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "credit_note",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "lockerinfo",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "group_buying_achievement_type",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "sale_order_product",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "marketplace_promotion",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "notification_manual",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "notification_template",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "marketplace_coupon_group",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "logistic_user",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "purchase_order_product",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "purchase_order_tracking",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "role_ability",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "discount_type",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_coupon",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "media_type",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_promotion",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_stats_type",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_workday_profile",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "stock_type",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "variant_template",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "invoice_receipt",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "app_deep_link",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "notification_type",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "purchase_order",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "coupon_price",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "marketplace_user",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "promotion_price",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "logistic_company",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "sale_order",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "role_class",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "cart",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "customer_address",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "payment",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "group_buying",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "credit_card",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "gateway_provider",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "payment_type",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "group_buying_product_variant",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "customer_user",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "group_buying_condition",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "product_variant",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "language",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "product",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "brand",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "category_marketplace",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "category_seller",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "event",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "product_type",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_address",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "town",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_group",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "seller_workday_group",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "city",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "status",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "working_day",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "province",
                schema: "twister");

            migrationBuilder.DropTable(
                name: "country",
                schema: "twister");
        }
    }
}
