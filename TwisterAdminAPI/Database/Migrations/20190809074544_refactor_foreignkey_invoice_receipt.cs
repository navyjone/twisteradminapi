﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_invoice_receipt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK__invrep__saleOrderId__sale_order__id",
                schema: "twister",
                table: "invoice_receipt",
                column: "saleOrderId");

            migrationBuilder.AddForeignKey(
                name: "FK__invrep__saleOrderId__sale_order__id",
                schema: "twister",
                table: "invoice_receipt",
                column: "saleOrderId",
                principalSchema: "twister",
                principalTable: "sale_order",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__invrep__saleOrderId__sale_order__id",
                schema: "twister",
                table: "invoice_receipt");

            migrationBuilder.DropIndex(
                name: "FK__invrep__saleOrderId__sale_order__id",
                schema: "twister",
                table: "invoice_receipt");
        }
    }
}
