﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_purchase_order_product : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK__po_product__productVariantId__product_variant__id",
                schema: "twister",
                table: "purchase_order_product",
                column: "productVariantId");

            migrationBuilder.AddForeignKey(
                name: "FK__po_product__productVariantId__product_variant__id",
                schema: "twister",
                table: "purchase_order_product",
                column: "productVariantId",
                principalSchema: "twister",
                principalTable: "product_variant",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__po_product__productVariantId__product_variant__id",
                schema: "twister",
                table: "purchase_order_product");

            migrationBuilder.DropIndex(
                name: "FK__po_product__productVariantId__product_variant__id",
                schema: "twister",
                table: "purchase_order_product");
        }
    }
}
