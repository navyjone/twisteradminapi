﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_variant_group_and_product_variant : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($@"
                ALTER TABLE product_variant
                DROP INDEX `fk-product_variant-varaint_group.id`,
                DROP FOREIGN KEY `fk-product_variant-varaint_group.id`,
                DROP COLUMN variantGroupId
            ;");

            migrationBuilder.DropTable(
                name: "variant_group",
                schema: "twister");

            //migrationBuilder.DropIndex(
            //    name: "fk-product_variant-varaint_group.id",
            //    schema: "twister",
            //    table: "product_variant");

            //migrationBuilder.DropColumn(
            //    name: "variantGroupId",
            //    schema: "twister",
            //    table: "product_variant");

            migrationBuilder.AddColumn<string>(
                name: "variantGroups",
                schema: "twister",
                table: "product_variant",
                unicode: false,
                maxLength: 255,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropColumn(
            //    name: "variantGroups",
            //    schema: "twister",
            //    table: "product_variant");

            migrationBuilder.Sql($@"
                ALTER TABLE product_variant
                DROP COLUMN variantGroups
            ;");

            migrationBuilder.AddColumn<int>(
                name: "variantGroupId",
                schema: "twister",
                table: "product_variant",
                type: "int(10)",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "variant_group",
                schema: "twister",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(10)", nullable: false),
                    variantId = table.Column<int>(type: "int(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_variant_group", x => new { x.id, x.variantId });
                });

            migrationBuilder.CreateIndex(
                name: "fk-product_variant-varaint_group.id",
                schema: "twister",
                table: "product_variant",
                column: "variantGroupId");

            migrationBuilder.CreateIndex(
                name: "id",
                schema: "twister",
                table: "variant_group",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "fk-variant_group-variant.id",
                schema: "twister",
                table: "variant_group",
                column: "variantId");
        }
    }
}
