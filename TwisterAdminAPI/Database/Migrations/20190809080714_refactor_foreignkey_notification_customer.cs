﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_notification_customer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK__noti_customer__notiManId__noti_man__id",
                schema: "twister",
                table: "notification_customer",
                column: "notificationManualId");

            migrationBuilder.CreateIndex(
                name: "FK__noti_customer__notiTempId__noti_temp__id",
                schema: "twister",
                table: "notification_customer",
                column: "notificationTemplateId");

            migrationBuilder.AddForeignKey(
                name: "FK__noti_customer__notiManId__noti_man__id",
                schema: "twister",
                table: "notification_customer",
                column: "notificationManualId",
                principalSchema: "twister",
                principalTable: "notification_manual",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK__noti_customer__notiTempId__noti_temp__id",
                schema: "twister",
                table: "notification_customer",
                column: "notificationTemplateId",
                principalSchema: "twister",
                principalTable: "notification_template",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__noti_customer__notiManId__noti_man__id",
                schema: "twister",
                table: "notification_customer");

            migrationBuilder.DropForeignKey(
                name: "FK__noti_customer__notiTempId__noti_temp__id",
                schema: "twister",
                table: "notification_customer");

            migrationBuilder.DropIndex(
                name: "FK__noti_customer__notiManId__noti_man__id",
                schema: "twister",
                table: "notification_customer");

            migrationBuilder.DropIndex(
                name: "FK__noti_customer__notiTempId__noti_temp__id",
                schema: "twister",
                table: "notification_customer");
        }
    }
}
