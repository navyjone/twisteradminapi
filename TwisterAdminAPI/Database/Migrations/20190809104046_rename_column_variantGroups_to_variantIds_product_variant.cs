﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class rename_column_variantGroups_to_variantIds_product_variant : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.RenameColumn(
            //    name: "variantGroups",
            //    schema: "twister",
            //    table: "product_variant",
            //    newName: "variantIds");

            migrationBuilder.Sql($@"
                ALTER TABLE product_variant
                CHANGE COLUMN `variantGroups` `variantIds`  varchar(255)
            ;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.RenameColumn(
            //    name: "variantIds",
            //    schema: "twister",
            //    table: "product_variant",
            //    newName: "variantGroups");

            migrationBuilder.Sql($@"
                ALTER TABLE product_variant
                CHANGE COLUMN `variantIds` `variantGroups`  varchar(255)
            ;");
        }
    }
}
