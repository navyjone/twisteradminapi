﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class refactor_foreignkey_city : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "FK__city__provinceId__province__id",
                schema: "twister",
                table: "city",
                column: "provinceId");

            migrationBuilder.AddForeignKey(
                name: "FK__city__provinceId__province__id",
                schema: "twister",
                table: "city",
                column: "provinceId",
                principalSchema: "twister",
                principalTable: "province",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__city__provinceId__province__id",
                schema: "twister",
                table: "city");

            migrationBuilder.DropIndex(
                name: "FK__city__provinceId__province__id",
                schema: "twister",
                table: "city");
        }
    }
}
