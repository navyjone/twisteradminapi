﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NVJUtilities
{
    public class CacheKey
    {
        private static readonly string prefix = "NVJ";

        public static string AdminRefreshToken(int id)
        {
            return $"{prefix}.AdminRefreshToken.id.{id}";
        }

        //public static string AdminRefreshTokenFromMarketplaceUserId(int marketplaceUserId)
        //{
        //    return $"{prefix}.AdminRefreshToken.markeplaceUserId.{marketplaceUserId}";
        //}

        //public static string CustomerUser(int id)
        //{
        //    return $"{prefix}.CustomerUser.id.{id}";
        //}

        public static string MarketplaceUser(int id)
        {
            return $"{prefix}.MarketplaceUser.id.{id}";
        }

        public static string RoleAbilitiesFromRoleClass(int roleClassId)
        {
            return $"{prefix}.RoleClass.id.{roleClassId}.roleAbilities";
        }
    }
}
