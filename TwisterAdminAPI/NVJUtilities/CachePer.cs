﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace NVJUtilities
{
    public static class CachePer
    {
        private static readonly MemoryCache cache = new MemoryCache(new MemoryCacheOptions());
        private static ConcurrentDictionary<string, DateTime> keys = new ConcurrentDictionary<string, DateTime>();

        public static void Set(string key, Object saveData, TimeSpan? timespan = null)
        {
            var timeToExpired = timespan ?? TimeSpan.FromSeconds(600);
            if (saveData != null && key != "")
            {
                var expire = DateTime.Now.Add(timeToExpired);
                cache.Set(key, saveData, expire);
                if (!keys.ContainsKey(key))
                {
                    keys.TryAdd(key, expire);
                }

                keys[key] = expire;
            }
        }

        public static T Get<T>(string key)
        {
            cache.TryGetValue(key, out T result);
            if (result == null)
            {
                Remove(key);
            }

            return result;
        }

        public static void Remove(string key)
        {
            if (keys.ContainsKey(key))
            {
                cache.Remove(key);
                keys.TryRemove(key, out DateTime value);
            }
        }

        public static void RemoveMany(string key = "")
        {
            var newKeys = new ConcurrentDictionary<string, DateTime>();
            foreach (var entry in keys)
            {
                if (entry.Key.StartsWith(key))
                {
                    cache.Remove(entry.Key);
                }
                else
                {
                    newKeys.TryAdd(entry.Key, entry.Value);
                }
            }

            keys = newKeys;
        }

        public static string[] GetKeys(string key = "")
        {
            var dicKeys = keys.ToDictionary(i => i.Key, i => i.Value);
            dicKeys = dicKeys.Where(k => k.Value > DateTime.Now)
                       .OrderBy(k => k.Key)
                       .ToDictionary(i => i.Key, i => i.Value);

            var filterKeys = dicKeys.Where(k => k.Key.Contains(key))
                                .Select(k => $"{k.Key}")
                                .ToArray();

            return filterKeys;
        }

        public static string[] GetKeysWithExpire(string key = "")
        {
            var dicKeys = keys.ToDictionary(i => i.Key, i => i.Value);
            dicKeys = dicKeys.Where(k => k.Value > DateTime.Now)
                       .OrderBy(k => k.Key)
                       .ToDictionary(i => i.Key, i => i.Value);
            
            var filterKeys = dicKeys.Where(k => k.Key.Contains(key))
                                .Select(k => $"{k.Key} ({k.Value.ToString()})" )
                                .ToArray();
            
            return filterKeys;
        }

        public static int Hours(int hours)
        {
            return 60 * 60 * hours;
        }

        public static int Mins(int mins)
        {
            return 60 * mins;
        }
    }
}
