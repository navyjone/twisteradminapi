﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NVJUtilities
{
    public class Role
    {
        public const string Refresh = "Refresh";
        public class CustomerUser
        {
            public const int Id = 1;
            public const string Name = "CustomerUser";
        }
        public class TwisterUser
        {
            public const int Id = 2;
            public const string Name = "TwisterUser";
        }
        public class SellerUser
        {
            public const int Id = 3;
            public const string Name = "SellerUser";
        }

        public static Dictionary<int, string> Name = new Dictionary<int, string>
        {
            {CustomerUser.Id, CustomerUser.Name },
            {TwisterUser.Id, TwisterUser.Name },
            {SellerUser.Id, SellerUser.Name }
        };

        public static int ToId(string name)
        {
            try
            {
                return Name.Where(n => n.Value.ToLower() == name.ToLower()).FirstOrDefault().Key;
            }
            catch (Exception ex)
            {
                //Logger.Log(Logger.error, ex.ToString());
                return 0;
            }

        }
    }
}
