﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NVJUtilities
{
    public class Status
    {
        public static class Active
        {
            public const string Name = "active";
            public const int Id = 1;
        }
        public static class InActive
        {
            public const string Name = "inactive";
            public const int Id = 2;
        }
        public static class Occupied
        {
            public const string Name = "occupied";
            public const int Id = 3;
        }
        public static class Empty
        {
            public const string Name = "empty";
            public const int Id = 4;
        }
        public static class OutOfStock
        {
            public const string Name = "outOfStock";
            public const int Id = 5;
        }
        public static class Pending
        {
            public const string Name = "pending";
            public const int Id = 6;
        }
        public static class Ordered
        {
            public const string Name = "ordered";
            public const int Id = 7;
        }
        public static class Expired
        {
            public const string Name = "expired";
            public const int Id = 8;
        }
        public static class Used
        {
            public const string Name = "used";
            public const int Id = 9;
        }
        public static class CsReply
        {
            public const string Name = "CSReply";
            public const int Id = 10;
        }
        public static class UsReply
        {
            public const string Name = "USReply";
            public const int Id = 11;
        }
        public static class Sent
        {
            public const string Name = "sent";
            public const int Id = 12;
        }
        public static class Read
        {
            public const string Name = "read";
            public const int Id = 13;
        }
        public static class SlReply
        {
            public const string Name = "SLReply";
            public const int Id = 14;
        }
        public static class Delivering
        {
            public const string Name = "delivering";
            public const int Id = 15;
        }
        public static class Delivered
        {
            public const string Name = "delivered";
            public const int Id = 16;
        }
        public static class Closed
        {
            public const string Name = "closed";
            public const int Id = 17;
        }
        public static class Fail
        {
            public const string Name = "fail";
            public const int Id = 18;
        }
        public static class Done
        {
            public const string Name = "done";
            public const int Id = 19;
        }
        public static class Paid
        {
            public const string Name = "paid";
            public const int Id = 20;
        }
        public static class Cancelled
        {
            public const string Name = "cancelled";
            public const int Id = 21;
        }
        public static class PickedUp
        {
            public const string Name = "pickedUp";
            public const int Id = 22;
        }

        public static class PartiallyDelivered
        {
            public const string Name = "partiallyDelivered";
            public const int Id = 23;
        }

        public static class UserCancelPayment
        {
            public const string Name = "userCancelPayment";
            public const int Id = 24;
        }

        public static class PaymentFail
        {
            public const string Name = "paymentFail";
            public const int Id = 25;
        }

        public static class Packed
        {
            public const string Name = "packed";
            public const int Id = 26;
        }

        public static class ReadyToShip
        {
            public const string Name = "readyToShip";
            public const int Id = 27;
        }

        public static class Packing
        {
            public const string Name = "packing";
            public const int Id = 28;
        }

        public static class DeliverBySeller
        {
            public const string Name = "deliverBySeller";
            public const int Id = 29;
        }

        public static class DeliverByThirdParty
        {
            public const string Name = "deliverByThirdParty";
            public const int Id = 30;
        }

        public static Dictionary<string, int> statusData = new Dictionary<string, int>()
        {
            {"active", 1 },
            {"inactive", 2 },
            {"occupied", 3 },
            {"empty", 4 },
            {"outOfStock", 5 },
            {"pending", 6 },
            {"ordered", 7 },
            {"expired", 8 },
            {"used", 9 },
            {"csReply", 10 },
            {"usReply", 11 },
            {"sent", 12 },
            {"read", 13 },
            {"slReply", 14 },
            {"delivering", 15 },
            {"delivered", 16 },
            {"closed", 17 },
            {"fail", 18 },
            {"done", 19 },
            {"paid", 20 },
            {"cancelled", 21 },
            {"pickedUp", 22 },
            {"partiallyDelivered", 23 },
            {"userCancelPayment", 24 },
            {"paymentFail", 25 },
            {"packed", 26 },
            {"readyToShip", 27 },
            {"packing", 28 },
            {"deliverBySeller", 29 },
            {"deliverByThirdParty", 30 },
        };

        public static int ToId (string name)
        {
            try
            {
                return statusData[name];
            }
            catch (Exception ex)
            {
                //Logger.Log(Logger.error, ex.ToString());
                return 0;
            }
            
        }

        public static string ToString(int id)
        {
            try
            {
                return statusData.FirstOrDefault(x => x.Value == id).Key;
            }
            catch (Exception ex)
            {
                //Logger.Log(Logger.error, ex.ToString());
                return "";
            }
        }
    }
}