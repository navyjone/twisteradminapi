﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NVJUtilities
{
    public class Parse
    {
        public class String
        {
            public static int ToInt (string key)
            {
                if (!string.IsNullOrEmpty(key))
                {
                    return int.Parse(key);
                }

                return 0;
            }

            public static decimal ToDecimal(string key)
            {
                if (!string.IsNullOrEmpty(key))
                {
                    return decimal.Parse(key);
                }

                return 0;
            }

            public static float ToFloat(string key)
            {
                if (!string.IsNullOrEmpty(key))
                {
                    return float.Parse(key);
                }

                return 0;
            }
        }

        public class Url
        {
            public static int[] ToIntArray (string key)
            {
                var str = key.Substring(1, key.Length - 2);
                var array = str.Split(',');
                var list = new List<int>();
                foreach (var a in array)
                {
                    list.Add(Parse.String.ToInt(a));
                }

                return list.ToArray();
            }
        }
    }
}
