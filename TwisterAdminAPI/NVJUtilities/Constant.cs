﻿namespace NVJUtilities
{
    public class Constant
    {
        public const int DefaultLanguage = 1; //Thai

        public class StockTypeId
        {
            public const int Add = 1;
            public const int Remove = 2;
            public const int Sold = 3;
        }

        // mPay
        public class MPay
        {
            public const string SplitSaleOrder = "SPLIT";
            public class Status
            {
                public const string Success = "S";
                public const string Fail = "F";
            }

            public class PaymentStatus
            {
                public const string Open = "OPEN";
                public const string Attempt = "ATTEMPT";
                public const string Pending = "PENDING";
                public const string Success = "SUCCESS";
                public const string Fail = "FAIL";
            }

            public class ResponseCode
            {
                public const string Success = "0000";
                public const string DatabaseError = "0001";
                public const string DataNotFound = "0002";
                public const string NoMoneyToPay = "1037";
                public const string TransactionNotFound = "1038";
                public const string SessionLost = "1044";
                public const string DuplicateTransaction = "1143";
                public const string PasswordLocked = "1278";
                public const string CreditCardExpired = "1422";
                public const string ServiceUnavailable = "1427";
                public const string TransactionFail = "1567";
                public const string BalanceInsufficiency = "1706";
                public const string OrderAlreadyPaid = "1767";
                public const string OrderExpired = "1771";
                public const string DuplicateOrder = "1772";
                public const string IncorrectAmount = "1774";
                public const string MerchantURLLost = "1790";
            }

            public class PaymentMethod
            {
                public const int CreditCard = 4;
            }
        }

        //Seller
        public class Seller
        {
            public class StatsType
            {
                public const int Rating = 1;
                public const int TotalProducts = 2;
                public const int DeliveryTime = 3;
                public const int IsHoliday = 4;
                public const int WorkingHours = 5;
                public const int IsOpening = 6;
                public const int IsOfficialShop = 7;
            }

            public class WorkdayProfile
            {
                public const int NormalDay = 1;
                public const int Weekend = 2;
                public const int Saturday = 3;
                public const int Sunday = 4;
                public const int MondayMorning = 5;
                public const int MondayAfternoon = 6;
                public const int TuesdayMorning = 7;
                public const int TuesdayAfternoon = 8;
                public const int WednesdayMorning = 9;
                public const int WednesdayAfternoon = 10;
                public const int ThursdayMorning = 11;
                public const int ThursdayAfternoon = 12;
                public const int FridayMorning = 13;
                public const int FridayAfternoon = 14;
                public const int SaturdayMorning = 15;
                public const int SaturdayAfternoon = 16;
                public const int SundayMorning = 17;
                public const int SundayAfternoon = 18;
            }
        }      

        public class Day
        {
            public const string Sunday = "sunday";
            public const string Monday = "monday";
            public const string Tuesday = "tuesday";
            public const string Wednesday = "wednesday";
            public const string Thursday = "thursday";
            public const string Friday = "friday";
            public const string Saturday = "saturday";
        }

        public class Role
        {
            public class Admin
            {
                public const string Value = "marketplaceUser";
            }
        }

        public class LanguageId
        {
            public const int Thai = 1;
            public const int English = 2;
        }

        public class NotificationTo
        {
            public const string Customer = "notification_customer";
            public const string Seller = "notification_seller";
        }

        public class ProvinceId
        {
            public const int Bangkok = 1;
        }
    }
}